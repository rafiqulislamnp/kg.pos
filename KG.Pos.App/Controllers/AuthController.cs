﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using KG.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using KG.Core.Services;
using Microsoft.Extensions.Options;
using KG.Pos.App.Models;
using KG.Core.Services.Home;
using System.Threading.Tasks;

namespace KG.App.Controllers
{

    public class AuthController : Controller
    {
       
        private readonly ILogger _logger;       
        private readonly HttpContext httpContext;
        private readonly HomeService _service;
        private readonly string _environment;

        public AuthController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<HomeController> logger, IOptions<AppSettings> appSettings)
        {
            _environment = appSettings.Value.Environment;

            httpContext = httpContextAccessor.HttpContext;

            _service = new HomeService(db, httpContextAccessor.HttpContext);
           // SessionHandler sessionHandler = new SessionHandler(db,httpContextAccessor.HttpContext);
           // sessionHandler.Adjust();
            _logger = logger;
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private void ClearAllSessionAndCookies()
        {
            httpContext.Session.Clear();
            foreach (var kookiesKeys in httpContext.Request.Cookies.Keys)
            {
                Response.Cookies.Delete(kookiesKeys);

            }

        }

        public IActionResult Login()
        {
            ClearAllSessionAndCookies();
            var commonCompany = _service.CommonCompanyGet();


            return View(commonCompany);
        }

       

        [HttpPost]
        public IActionResult Login(VMLogin model)
        {
            if (model.UserName == null || model.Password == null)
            {
                model.LblError = "Please enter Username and Password";
                return View(model);
            }
            var user = _service.UserLogin(model.UserName, model.Password);
            if (user.LblError != null)
            {
                return View(user);
            }
            if (user.LblError == null)
            {
                //Store 
                httpContext.Session.SetObjectAsJson("LoginUser", user);

                httpContext.Session.SetString("User", user.UserName);
                httpContext.Session.SetString("UserID", user.ID.ToString());
                httpContext.Session.SetString("UserAccessLevelId", user.UserAccessLevelId.ToString());
                httpContext.Session.SetString("UserDepartmentId", user.UserDepartmentId.ToString());
              
                // MvcApplication.userName  = VmUser_User.User_User.Name;
                //Store User in Cookie


                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("UserNameCookies", model.UserName);
                Response.Cookies.Append("PasswordCookies", model.Password);
                Response.Cookies.Append("Environment", _environment);

                Response.Cookies.Append("userId", user.ID.ToString());
                cookieOptions.Expires = DateTime.Now.AddDays(1);
                
                return RedirectToAction("Index", "Home");
               

            }
            else
            {
                model.LblError = "User/Password does not match!";
            }
            return RedirectToAction("Login", "Auth");

            //to retrieve session value.
            //var myComplexObject = HttpContext.Session.GetObjectFromJson<VMLogin>("Users");


            //if (model.UserName == "Accountant" && model.Password == "RomoAccount")
            //{
            //    //Updated by Mintu                 
            //    HttpContext.Session.SetString(ControllerName, (string)RouteData.Values["controller"]);
            //    HttpContext.Session.SetString(ActionName, (string)RouteData.Values["action"]);
            //    HttpContext.Session.SetString(SessionName, model.UserName);
            //    HttpContext.Session.SetInt32(SessionId, 1);
            //    var name = HttpContext.Session.GetString(SessionName);
            //    //Updated by Mintu

            //    return RedirectToAction("Index", "Home");
            //}

            //return RedirectToAction("Index", "Home");
        }

       
    }
}
