﻿using System;
using KG.Core.Services;
using KG.Infrastructure;
using KG.Core.Services.User;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using KG.Core.Services.WareHouse;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.DataProtection;
using System.Linq;
using KG.Core.Services.Configuration;
using KG.Core.Services.Procurement;
using Microsoft.Extensions.Configuration;
using System.Net;
using KG.Core.Services.Marketing;

namespace Erp.App.Controllers
{
    public class WareHouseController : Controller
    {
        private readonly ILogger _logger;
        private readonly WareHouseService _service;
        private readonly SessionHandler _sessionHandler;
        private readonly string connString;
        private readonly MarketingSaleService _marketingSaleService;

        public WareHouseController(InfrastructureDbContext db, ILogger<WareHouseController> logger, IHttpContextAccessor httpContextAccessor, IDataProtectionProvider idataProtectionProvide, IConfiguration configuration)
        {
            _logger = logger;
            _sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            _sessionHandler.Adjust();
            _service = new WareHouseService(db, httpContextAccessor.HttpContext, idataProtectionProvide);
            connString = configuration.GetConnectionString("KgMsSqlConnection");
            _marketingSaleService = new MarketingSaleService(db, httpContextAccessor.HttpContext);

        }



        public async Task<IActionResult> GetCommonProductSubCategory(int id)


        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSubCategoryGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).OrderBy(x => x.Text).ToList();
            return Json(list);
        }
        public async Task<IActionResult> GetCommonProduct(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).OrderBy(x => x.Text).ToList();

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "RawItemGet(id), RawItemGet Get by RawSubCategoryid).");
            return Json(list);
        }

        [HttpGet]
        public async Task<IActionResult> WareHouseStoreDepartment()
        {
            // ViewBag.InventoryTypeEnum = new SelectList(_service.InventoryTypeEnumDropDown(), "Value", "Text");

            var vmUserDepartment = await _service.WareHouseStoreDepartmentGet();
            return View(vmUserDepartment);
        }
        [HttpPost]
        public async Task<IActionResult> WareHouseStoreDepartment(VMUserDepartment vmUserDepartment)
        {

            if (vmUserDepartment.ActionEum == ActionEnum.Add)
            {
                await _service.UserStoreDepartmentAdd(vmUserDepartment);

            }
            else if (vmUserDepartment.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserStoreDepartmentEdit(vmUserDepartment);
            }
            else if (vmUserDepartment.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserStoreDepartmentDelete(vmUserDepartment.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseStoreDepartment));
        }

        [HttpGet]
        public async Task<IActionResult> WareHousePurchaseOrder()
        {
            var vmWareHousePOReceivingSlave = await _service.WareHousePurchaseOrderGet();
            return View(vmWareHousePOReceivingSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHousePOSlaveReceivingDetailsByPO(int id)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();
            if (id > 0)
            {
                vmWareHousePOReceivingSlave = await _service.WareHousePOSlaveReceivingDetailsByPOGet(id);
            }
            return View(vmWareHousePOReceivingSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHousePOReceivingSlaveReport(int id = 0)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();
            vmWareHousePOReceivingSlave = await _service.WareHousePOReceivingSlaveGet(id);

            vmWareHousePOReceivingSlave.TotalCostingPriceInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vmWareHousePOReceivingSlave.DataListSlave.Select(x => x.TotalCostingPrice).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);


            return View(vmWareHousePOReceivingSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHousePOReceivingSlave(int id = 0)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();
            if (id == 0)
            {
                vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave()
                {
                    ChallanDate = DateTime.Today
                    // ProcurementPurchaseOrderList = new SelectList(_service.PODropDownList(), "Value", "Text")
                };
            }
            else if (id > 0)
            {
                vmWareHousePOReceivingSlave = await _service.WareHousePOReceivingSlaveGet(id);
            }
            return View(vmWareHousePOReceivingSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHousePOReceivingSlave(VMWareHousePOReceivingSlave vmModel, VMWareHousePOReceivingSlavePartial vmModelList)
        {
            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouse_POReceivingFk == 0)
                {
                    vmModel.WareHouse_POReceivingFk = await _service.WareHousePOReceivingAdd(vmModel);
                }
                await _service.WareHousePOReceivingSlaveAdd(vmModel, vmModelList);
            }
            //else if (model.ActionEum == ActionEnum.Edit)
            //{
            //    //Edit
            //    await _service.ShipmentDeliveryChallanSlaveEdit(model);
            //}
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.ShipmentDeliveryChallanSlaveDelete(model.ID);
            //}

            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHousePOReceivingSlave), new { id = vmModel.WareHouse_POReceivingFk });
        }
        [HttpGet]
        public async Task<IActionResult> WareHousePOSalesReturnListByPO(int id = 0)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();
            vmWareHousePOReceivingSlave = await _service.WareHousePOSlaveReturnDetailsByPOGet(id);

            return View(vmWareHousePOReceivingSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseProductReturnSlaveReport(int id = 0)
        {
            VMWareHousePOReturnSlave vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave();
            vmWareHousePOReturnSlave = await _service.WareHouseProductReturnSlaveGet(id);

            return View(vmWareHousePOReturnSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseReturnBySupplierList()
        {
            VMWareHousePOReturnSlave vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave();
            vmWareHousePOReturnSlave = await _service.WareHouseProductReturnGet();

            return View(vmWareHousePOReturnSlave);
        }

        [HttpGet]
        public async Task<IActionResult> WareHousePOSalesReturnSlave(int id = 0)
        {
            VMWareHousePOReturnSlave vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave();
            if (id == 0)
            {
                vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave()
                {
                    ChallanDate = DateTime.Today,
                    //ProcurementPurchaseOrderList = new SelectList(_service.POForSalesReturnDropDownList(), "Value", "Text")

                };
            }
            else if (id > 0)
            {
                vmWareHousePOReturnSlave = await _service.WareHousePOSalseReturnSlaveGet(id);
            }
            return View(vmWareHousePOReturnSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHousePOSalesReturnSlave(VMWareHousePOReturnSlave vmModel, VMWareHousePOReturnSlavePartial vmModelList)
        {

            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouse_POReceivingFk > 0)
                {
                    vmModel.WareHouse_POReceivingFk = await _service.WareHousePOReturnAdd(vmModel);
                }
                else
                {
                    // to return causes
                    vmModel.WareHouse_POReceivingFk = await _service.WareHousePOReturnAdd(vmModel);
                }
                await _service.WareHousePOSalseReturnSlaveAdd(vmModel, vmModelList);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHousePOSalesReturnSlave), new { id = vmModel.WareHouse_POReceivingFk });
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseProductReturnToSupplier(int id = 0)
        {
            VMWareHousePOReturnSlave vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave();
            if (id > 0)
            {
                vmWareHousePOReturnSlave = await _service.WareHouseProductReturnSlaveGet(id);
            }
            return View(vmWareHousePOReturnSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseProductReturnToSupplier(VMWareHousePOReturnSlave vmModel)
        {

            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouse_POReceivingFk == 0)
                {
                    vmModel.WareHouse_POReceivingFk = await _service.WareHouseProductReturnAdd(vmModel);
                }
                if (vmModel.WareHouse_POReceivingFk > 0)
                {
                    await _service.WareHouseProductReturnToSupplierAdd(vmModel);
                }
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseProductReturnToSupplier), new { id = vmModel.WareHouse_POReceivingFk });
        }

        [HttpGet]
        public async Task<IActionResult> WareHouseProductReturnBySupplierSlave(int id = 0)
        {
            VMWareHousePOReturnSlave vmWareHousePOReturnSlave = new VMWareHousePOReturnSlave();
            if (id > 0)
            {
                vmWareHousePOReturnSlave = await _service.WareHouseProductReturnSlaveGet(id);
            }
            return View(vmWareHousePOReturnSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseProductReturnBySupplierSlave(VMWareHousePOReturnSlave vmModel, VMWareHousePOReturnSlavePartial vmModelList)
        {

            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouse_POReceivingFk == 0)
                {
                    vmModel.WareHouse_POReceivingFk = await _service.WareHouseProductReturnAdd(vmModel);
                }
                if (vmModel.WareHouse_POReceivingFk > 0)
                {
                    await _service.WareHouseProductReturnSlaveAdd(vmModel, vmModelList);

                }

            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseProductReturnBySupplierSlave), new { id = vmModel.WareHouse_POReceivingFk });
        }

        public async Task<IActionResult> GetExistingChallanListByPO(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.GetExistingChallanListByPOData(id));
            return Json(model);
        }
        public async Task<JsonResult> ProcurementPurchaseOrderDetails(int id)
        {
            VMWareHousePOReceivingSlave model = new VMWareHousePOReceivingSlave();
            model = await _service.GetProcurementPurchaseOrder(id);
            return Json(model);
        }
        public async Task<JsonResult> GetCommonUnitByItem(int id)
        {
            VMCommonUnit model = new VMCommonUnit();
            model = await _service.GetCommonUnitByItem(id);
            return Json(model);
        }
        public async Task<JsonResult> RequisitionSingle(int id)
        {
            VMPurchaseRequisition model = new VMPurchaseRequisition();
            model = await _service.ProcurementPurchaseRequisitionGet(id);
            return Json(model);
        }
        public IActionResult ProcurementPOSalesReturnSlaveData(int poReceivingId)
        {
            var model = new VMWareHousePOReturnSlavePartial();
            model.DataListSlavePartial = _service.GetPOSalesReturnData(poReceivingId);
            return PartialView("_POSalesReturnSlaveData", model);
        }

        public IActionResult ProcurementProductReturnBySupplierSlaveData(int supplierId)
        {
            var model = new VMWareHousePOReturnSlavePartial();
            model.DataListSlavePartial = _service.GetProductReturnBySupplierData(supplierId);
            return PartialView("_ProductReturnSlaveData", model);
        }
        public IActionResult ProcurementPurchaseOrderSlaveData(int poId)
        {
            var model = new VMWareHousePOReceivingSlavePartial();
            if (poId > 0)
            {
                model = _service.GetPODetailsByID(poId);
                model.DataListSlavePartial = _service.GetProcurementPurchaseOrderSlaveData(poId);
            }


            return PartialView("_ProcurementPurchaseOrderSlaveData", model);
        }
        public IActionResult GetWareHouseOrderItemRequisitionSlavePartial(int requisitionId)
        {
            var model = new VMWareHouseRequisitionSlavePartial();
            model.DataListSlavePartial = _service.WareHouseRequisitionSlaveData(requisitionId);
            return PartialView("_WareHouseRequisitionSlaveData", model);
        }


        public JsonResult AutoCompletePRGet(string id)
        {
            var products = _service.GetAutoCompletePR(id);
            return Json(products);
        }

        [HttpGet]
        public ActionResult WareHouseConsumptionReport()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct.ProductCategoryList = new SelectList(_service.CommonRawCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<ActionResult> WareHouseConsumptionReportView(VMCommonProduct vmCommonProduct)
        {
            vmCommonProduct = await _service.WareHouseConsumptionReportGet(vmCommonProduct);

            return View(vmCommonProduct);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseOrderItemConsumptionSlaveReport(int id = 0)
        {
            VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave();
            vmWareHouseConsumptionSlave = await _service.VMWareHouseConsumptionSlaveGet(id);

            return View(vmWareHouseConsumptionSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseOrderItemConsumptionSlave(int id = 0)
        {
            VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave();
            vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave()
            {
                Date = DateTime.Today,
                //RequisitionList = new SelectList(_service.PurchaseRequisitionGetDropDown(), "Value", "Text")
            };
            if (id > 0)
            {
                vmWareHouseConsumptionSlave = await _service.VMWareHouseConsumptionSlaveGet(id);
            }
            return View(vmWareHouseConsumptionSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseOrderItemConsumptionSlave(VMWareHouseConsumptionSlave vmModel, VMWareHouseRequisitionSlavePartial vmModelList)
        {
            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouseConsumptionFk == 0)
                {
                    // vmModel.EnumStoreTypeFk = (int)EnumStoreType.WareHouse;
                    vmModel.SendByStoreRequisition = true;
                    vmModel.WareHouseConsumptionFk = await _service.WareHouseConsumptionAdd(vmModel);
                }
                await _service.WareHouseConsumptionSlaveAdd(vmModel, vmModelList);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseOrderItemConsumptionSlave), new { id = vmModel.WareHouseConsumptionFk });
        }

        [HttpGet]
        public async Task<IActionResult> WareHouseGeneralOrderItemConsumptionSlave(int id = 0)
        {
            VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave();
            vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave()
            {
                Date = DateTime.Today,
                ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text")

            };
            if (id > 0)
            {
                vmWareHouseConsumptionSlave = await _service.VMWareHouseConsumptionSlaveGet(id);
            }
            return View(vmWareHouseConsumptionSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseGeneralOrderItemConsumptionSlave(VMWareHouseConsumptionSlave vmModel)
        {
            if (vmModel.ActionEum == ActionEnum.Add)
            {
                if (vmModel.WareHouseConsumptionFk == 0)
                {
                    vmModel.ToUser_DepartmentFK = (int)UserDepartment.Sales;
                    vmModel.SendByStoreRequisition = false;
                    vmModel.WareHouseConsumptionFk = await _service.WareHouseConsumptionAdd(vmModel);
                }
                await _service.WareHouseGeneralConsumptionSlaveAdd(vmModel);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseGeneralOrderItemConsumptionSlave), new { id = vmModel.WareHouseConsumptionFk });
        }

        [HttpGet]
        public async Task<IActionResult> WareHouseOrderItemConsumption() //Store Out
        {
            VMWareHouseConsumption vmWareHouseConsumption = new VMWareHouseConsumption();

            vmWareHouseConsumption = await _service.WareHouseConsumptionGet();
            return View(vmWareHouseConsumption);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseOrderItemConsumptionReceived() //Store In
        {
            VMWareHouseConsumption vmWareHouseConsumption = new VMWareHouseConsumption();

            vmWareHouseConsumption = await _service.WareHouseConsumptionGetReceived();
            return View(vmWareHouseConsumption);
        }
        [HttpPost]
        public async Task<IActionResult> WareHouseOrderItemConsumption(VMWareHouseConsumption vmWareHouseConsumption)
        {
            if (vmWareHouseConsumption.ActionEum == ActionEnum.Acknowledgement)
            {
                await _service.WareHouseonsumptionAcknowledgement(vmWareHouseConsumption.ID);
            }
            return RedirectToAction(nameof(WareHouseOrderItemConsumptionReceived));
        }


        [HttpGet]
        public async Task<IActionResult> WareHouseOrderItemConsumptionSlaveReturn(int id = 0)
        {
            VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave();
            vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave()
            {
                Date = DateTime.Today,
                ConsumptionList = new SelectList(_service.WareHouseConsumptionGetDropDown(), "Value", "Text")
            };
            if (id > 0)
            {
                vmWareHouseConsumptionSlave = await _service.VMWareHouseConsumptionSlaveGet(id);
            }
            return View(vmWareHouseConsumptionSlave);
        }

        public JsonResult AutoWareHouseConsumptionGet(string id)
        {
            var products = _service.GetAutoWareHouseConsumption(id);
            return Json(products);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseOrderItemConsumptionSlaveReturn(VMWareHouseConsumptionSlave vmModel)
        {
            if (vmModel.ActionEum == ActionEnum.Add)
            {
                await _service.WareHouseConsumptionReturnAdd(vmModel);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseOrderItemConsumptionSlaveReturn), new { id = vmModel.WareHouseConsumptionFk });
        }
        public IActionResult GetWareHouseOrderItemConsumptionSlavePartial(int consumptionId)
        {
            var model = new VMWareHouseConsumptionSlave();
            model.DataToListSlave = _service.WareHouseConsumptionSlaveData(consumptionId);
            return PartialView("_WareHouseConsumptionSlaveData", model);
        }


        [HttpGet]
        public ActionResult CSStock()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct.FromDate = DateTime.Today;
            vmCommonProduct.ToDate = DateTime.Today;
            vmCommonProduct.ProductCategoryList = new SelectList(_marketingSaleService.ProductCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_marketingSaleService.CommonSupplierDropDownList(), "Value", "Text");

            return View(vmCommonProduct);
        }
        [HttpPost]
        public ActionResult CSStockReport(VMCommonProduct vmCommonProduct)
        {
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/CSStock&rs:Command=Render&rs:Format=PDF");

            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "CSStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "CSStock.doc");
            }

            return View();

        }

        [HttpGet]
        public ActionResult KGeComCSStock()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct.FromDate = DateTime.Today;
            vmCommonProduct.ToDate = DateTime.Today;
            vmCommonProduct.ProductCategoryList = new SelectList(_marketingSaleService.ProductCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_marketingSaleService.CommonSupplierDropDownList(), "Value", "Text");

            return View(vmCommonProduct);
        }
        [HttpPost]
        public ActionResult KGeComCSStockReport(VMCommonProduct vmCommonProduct)
        {
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComCSStock&rs:Command=Render&rs:Format=PDF");

            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "CSStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "CSStock.doc");
            }

            return View();

        }

        [HttpGet]
        public ActionResult BazaarBakCSStock()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct.FromDate = DateTime.Today;
            vmCommonProduct.ToDate = DateTime.Today;
            vmCommonProduct.ProductCategoryList = new SelectList(_marketingSaleService.ProductCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_marketingSaleService.CommonSupplierDropDownList(), "Value", "Text");

            return View(vmCommonProduct);
        }
        public ActionResult BazaarBakStockReport(VMCommonProduct vmCommonProduct)
        {
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/BazaarBakCSStock&rs:Command=Render&rs:Format=PDF");

            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "CSStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "CSStock.doc");
            }

            return View();

        }

        [HttpGet]
        public ActionResult WareHouseRawItemInventory()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct.ProductCategoryList = new SelectList(_service.CommonRawCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.BrandList = new SelectList(_service.CommonCommonBrandList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_service.CommonCommonSupplierList(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public ActionResult ShopStockReport(VMCommonProduct vmCommonProduct)
        {
            if (vmCommonProduct.Common_BrandFk == null)
            {
                vmCommonProduct.Common_BrandFk = 0;
            }
            if (vmCommonProduct.Common_SupplierFk == null)
            {
                vmCommonProduct.Common_SupplierFk = 0;
            }
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/ShopStock&rs:Command=Render&rs:Format={0}&Common_ShopFK={1}&Common_SupplierFk={2}&Common_BrandFk={3}&Common_ProductCategoryFk={4}&Common_ProductSubCategoryFk={5}&Common_ProductFK={6}", vmCommonProduct.ReportType, vmCommonProduct.Common_ShopFK, vmCommonProduct.Common_SupplierFk, vmCommonProduct.Common_BrandFk, vmCommonProduct.Common_ProductCategoryFk, vmCommonProduct.Common_ProductSubCategoryFk, vmCommonProduct.ID);


            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ShopStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ShopStock.doc");
            }

            return View();

        }

        [HttpGet]
        public ActionResult KgeComWareHouseRawItemInventory()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct.ProductCategoryList = new SelectList(_service.CommonRawCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.BrandList = new SelectList(_service.CommonCommonBrandList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_service.CommonCommonSupplierList(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public ActionResult KgeComShopStockReport(VMCommonProduct vmCommonProduct)
        {
            if (vmCommonProduct.Common_BrandFk == null)
            {
                vmCommonProduct.Common_BrandFk = 0;
            }
            if (vmCommonProduct.Common_SupplierFk == null)
            {
                vmCommonProduct.Common_SupplierFk = 0;
            }
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KgeComShopStock&rs:Command=Render&rs:Format={0}&Common_ShopFK={1}&Common_SupplierFk={2}&Common_BrandFk={3}&Common_ProductCategoryFk={4}&Common_ProductSubCategoryFk={5}&Common_ProductFK={6}", vmCommonProduct.ReportType, vmCommonProduct.Common_ShopFK, vmCommonProduct.Common_SupplierFk, vmCommonProduct.Common_BrandFk, vmCommonProduct.Common_ProductCategoryFk, vmCommonProduct.Common_ProductSubCategoryFk, vmCommonProduct.ID);

            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ShopStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ShopStock.doc");
            }

            return View();

        }

        [HttpGet]
        public ActionResult BazaarBakWareHouseRawItemInventory()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct.ProductCategoryList = new SelectList(_service.CommonRawCategoryDropDownList(), "Value", "Text");
            vmCommonProduct.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmCommonProduct.BrandList = new SelectList(_service.CommonCommonBrandList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_service.CommonCommonSupplierList(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public ActionResult BazaarBakShopStockReport(VMCommonProduct vmCommonProduct)
        {
            if (vmCommonProduct.Common_BrandFk == null)
            {
                vmCommonProduct.Common_BrandFk = 0;
            }
            if (vmCommonProduct.Common_SupplierFk == null)
            {
                vmCommonProduct.Common_SupplierFk = 0;
            }
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KgeComShopStock&rs:Command=Render&rs:Format={0}&Common_ShopFK={1}&Common_SupplierFk={2}&Common_BrandFk={3}&Common_ProductCategoryFk={4}&Common_ProductSubCategoryFk={5}&Common_ProductFK={6}", vmCommonProduct.ReportType, vmCommonProduct.Common_ShopFK, vmCommonProduct.Common_SupplierFk, vmCommonProduct.Common_BrandFk, vmCommonProduct.Common_ProductCategoryFk, vmCommonProduct.Common_ProductSubCategoryFk, vmCommonProduct.ID);

            if (vmCommonProduct.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ShopStock.xls");
            }
            if (vmCommonProduct.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmCommonProduct.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ShopStock.doc");
            }

            return View();

        }

        [HttpPost]
        public async Task<ActionResult> WareHouseRawItemInventoryView(VMCommonProduct vmCommonProduct)
        {
            vmCommonProduct = await _service.WareHouseRawItemInventoryGet(vmCommonProduct);

            return View(vmCommonProduct);
        }

        [HttpGet]
        public async Task<IActionResult> WareHouseShopToShopTransferSlaveReport(int id = 0)
        {
            VMWareHouse_TransferShopToShopSlave vmWareHouseConsumptionSlave = new VMWareHouse_TransferShopToShopSlave();
            vmWareHouseConsumptionSlave = await _service.VMWareHouseTransferShopToShopSlaveGet(id);

            return View(vmWareHouseConsumptionSlave);
        }
        [HttpGet]
        public async Task<IActionResult> WareHouseShopToShopTransferSlave(int id = 0)
        {
            VMWareHouse_TransferShopToShopSlave vmWareHouseConsumptionSlave = new VMWareHouse_TransferShopToShopSlave();
            vmWareHouseConsumptionSlave = new VMWareHouse_TransferShopToShopSlave()
            {
                Date = DateTime.Today,
                ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text")

            };
            if (id > 0)
            {
                vmWareHouseConsumptionSlave = await _service.VMWareHouseTransferShopToShopSlaveGet(id);
            }
            return View(vmWareHouseConsumptionSlave);
        }

        [HttpPost]
        public async Task<IActionResult> WareHouseShopToShopTransferSlave(VMWareHouse_TransferShopToShopSlave vmModel)
        {
            if (vmModel.ActionEum == ActionEnum.Add)
            {


                if (vmModel.WareHouse_TransferShopToShopFk == 0)
                {

                    vmModel.WareHouse_TransferShopToShopFk = await _service.VMWareHouseTransferShopToShopAdd(vmModel);

                }
                await _service.WareHouseTransferShopToShopSlaveAdd(vmModel);

            }
            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(WareHouseShopToShopTransferSlave), new { id = vmModel.WareHouse_TransferShopToShopFk });
        }


        [HttpGet]
        public async Task<IActionResult> WareHouseShopToShopTransfer()
        {
            VMWareHouse_TransferShopToShopSlave vmWareHouseTransferShopToShopSlave = new VMWareHouse_TransferShopToShopSlave();

            vmWareHouseTransferShopToShopSlave = await _service.WareHouseTransferShopToShopGet();
            return View(vmWareHouseTransferShopToShopSlave);
        }
        [HttpPost]
        public async Task<IActionResult> WareHouseShopToShopTransfer(VMWareHouse_TransferShopToShopSlave transferShopToShopSlave)
        {
            if (transferShopToShopSlave.ActionEum == ActionEnum.Acknowledgement)
            {
                await _service.WareHouseTransferShopToShopAcknowledgement(transferShopToShopSlave.ID);
            }
            return RedirectToAction(nameof(WareHouseShopToShopTransfer));
        }
    }


}