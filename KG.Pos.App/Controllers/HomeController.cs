﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KG.Core.Services;
using Microsoft.AspNetCore.Http;
using KG.Core.Services.User;
using KG.Infrastructure;
using KG.Core.Services.Home;
using System.Reflection;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace KG.App.Controllers
{
   
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private readonly HttpContext httpContext;
       private readonly HomeService _service;
      

        public HomeController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<HomeController> logger)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new HomeService(db, httpContextAccessor.HttpContext);
          
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _logger = logger;
        }
        public IActionResult Index()
        {

            var user = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");

            var commonCompany = _service.CommonCompanyGet();

            //InfrastructureDbContext infrastructureDbContext = new InfrastructureDbContext();
            //AccountingService accountingService = new AccountingService(infrastructureDbContext);

            //var model = accountingService.GetAccountTypes().Result;
            return View(commonCompany);
        }

    }
}

