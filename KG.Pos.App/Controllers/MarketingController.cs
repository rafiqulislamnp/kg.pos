﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KG.Core.Services;
using Microsoft.AspNetCore.Http;
using KG.Core.Services.User;
using KG.Infrastructure;
using KG.Core.Services.Home;
using System.Reflection;
using System.Linq;
using System;
using KG.Core.Services.Configuration;
using KG.Core.Services.Marketing;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using KG.Core.Services.Procurement;
using System.Net;

namespace KG.App.Controllers
{
    //[SoftwareAdmin]
    public class MarketingController : Controller
    {
        private ILogger<MarketingController> _logger;
        private MarketingSaleService _service;
        private ConfigurationService _configurationService;
        private readonly VMLogin _vmLogin;


        public MarketingController(InfrastructureDbContext db, ILogger<MarketingController> logger, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _service = new MarketingSaleService(db, httpContextAccessor.HttpContext);
            _configurationService = new ConfigurationService(db, httpContextAccessor.HttpContext);
            _logger = logger;
            _vmLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");

        }
        [HttpGet]
        public async Task<IActionResult> MarketingSalesReport()
        {
            VMMarketingSalesReport vmMarketingSalesReport = new VMMarketingSalesReport();
            vmMarketingSalesReport.FromDate = DateTime.Today;
            vmMarketingSalesReport.ToDate = DateTime.Today;
            vmMarketingSalesReport.ProductCategoryList = new SelectList(_service.ProductCategoryDropDownList(), "Value", "Text");
            vmMarketingSalesReport.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmMarketingSalesReport.SupplierList = new SelectList(_service.CommonSupplierDropDownList(), "Value", "Text");


            return View(vmMarketingSalesReport);
        }

       
        [HttpPost]
        public ActionResult SalesReportRDLCGet(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var FromDate = vmMarketingSalesSlave.FromDate.ToString("yyyy-MM-dd");
            var ToDate = vmMarketingSalesSlave.ToDate.ToString("yyyy-MM-dd");

            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = "";
            if (vmMarketingSalesSlave.IsSalesSummery == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/SalesReports&rs:Command=Render&rs:Format={0}&IsSalesSummery={1}&FromDate={2}&ToDate={3}&Common_ShopFK={4}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSalesSummery, FromDate, ToDate, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsCategoryWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/CategoryWiseSalesReports&rs:Command=Render&rs:Format={0}&IsCategoryWise={1}&FromDate={2}&ToDate={3}&Common_ProductCategoryFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsCategoryWise, FromDate, ToDate, vmMarketingSalesSlave.Common_ProductCategoryFK, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsSupplierWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/SupplierWiseSalesReports&rs:Command=Render&rs:Format={0}&IsSupplierWise={1}&FromDate={2}&ToDate={3}&Common_SupplierFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSupplierWise, FromDate, ToDate, vmMarketingSalesSlave.Common_SupplierFk, vmMarketingSalesSlave.Common_ShopFK);

            }

            //KGeCom
            //if (vmMarketingSalesSlave.IsSalesSummery == true)
            //{

            //    reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComSalesReports&rs:Command=Render&rs:Format={0}&IsSalesSummery={1}&FromDate={2}&ToDate={3}&Common_ShopFK={4}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSalesSummery, FromDate, ToDate, vmMarketingSalesSlave.Common_ShopFK);

            //}
            //else if (vmMarketingSalesSlave.IsCategoryWise == true)
            //{
            //    reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComCategoryWiseSalesReports&rs:Command=Render&rs:Format={0}&IsCategoryWise={1}&FromDate={2}&ToDate={3}&Common_ProductCategoryFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsCategoryWise, FromDate, ToDate, vmMarketingSalesSlave.Common_ProductCategoryFK, vmMarketingSalesSlave.Common_ShopFK);

            //}
            //else if (vmMarketingSalesSlave.IsSupplierWise == true)
            //{
            //    reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComSupplierWiseSalesReports&rs:Command=Render&rs:Format={0}&IsSupplierWise={1}&FromDate={2}&ToDate={3}&Common_SupplierFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSupplierWise, FromDate, ToDate, vmMarketingSalesSlave.Common_SupplierFk, vmMarketingSalesSlave.Common_ShopFK);

            //}
            //else
            //{
            //}

            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ProdReference.xls");
            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ProdReference.doc");
            }

            return View();

        }
        [HttpGet]
        public async Task<IActionResult> MarketingKGeComReportSalesReport()
        {
            VMMarketingSalesReport vmMarketingSalesReport = new VMMarketingSalesReport();
            vmMarketingSalesReport.FromDate = DateTime.Today;
            vmMarketingSalesReport.ToDate = DateTime.Today;
            vmMarketingSalesReport.ProductCategoryList = new SelectList(_service.ProductCategoryDropDownList(), "Value", "Text");
            vmMarketingSalesReport.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmMarketingSalesReport.SupplierList = new SelectList(_service.CommonSupplierDropDownList(), "Value", "Text");


            return View(vmMarketingSalesReport);
        }


        [HttpPost]
        public ActionResult SalesKGeComReportRDLCGet(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var FromDate = vmMarketingSalesSlave.FromDate.ToString("yyyy-MM-dd");
            var ToDate = vmMarketingSalesSlave.ToDate.ToString("yyyy-MM-dd");

            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = "";


            //KGeCom
            if (vmMarketingSalesSlave.IsSalesSummery == true)
            {

                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComSalesReports&rs:Command=Render&rs:Format={0}&IsSalesSummery={1}&FromDate={2}&ToDate={3}&Common_ShopFK={4}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSalesSummery, FromDate, ToDate, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsCategoryWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComCategoryWiseSalesReports&rs:Command=Render&rs:Format={0}&IsCategoryWise={1}&FromDate={2}&ToDate={3}&Common_ProductCategoryFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsCategoryWise, FromDate, ToDate, vmMarketingSalesSlave.Common_ProductCategoryFK, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsSupplierWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/KGeComSupplierWiseSalesReports&rs:Command=Render&rs:Format={0}&IsSupplierWise={1}&FromDate={2}&ToDate={3}&Common_SupplierFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSupplierWise, FromDate, ToDate, vmMarketingSalesSlave.Common_SupplierFk, vmMarketingSalesSlave.Common_ShopFK);

            }
          

            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ProdReference.xls");
            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ProdReference.doc");
            }

            return View();

        }

        [HttpGet]
        public async Task<IActionResult> MarketingBazaarBakSalesReport()
        {
            VMMarketingSalesReport vmMarketingSalesReport = new VMMarketingSalesReport();
            vmMarketingSalesReport.FromDate = DateTime.Today;
            vmMarketingSalesReport.ToDate = DateTime.Today;
            vmMarketingSalesReport.ProductCategoryList = new SelectList(_service.ProductCategoryDropDownList(), "Value", "Text");
            vmMarketingSalesReport.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");
            vmMarketingSalesReport.SupplierList = new SelectList(_service.CommonSupplierDropDownList(), "Value", "Text");


            return View(vmMarketingSalesReport);
        }


        [HttpPost]
        public ActionResult SalesBazaarBakRDLCGet(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var FromDate = vmMarketingSalesSlave.FromDate.ToString("yyyy-MM-dd");
            var ToDate = vmMarketingSalesSlave.ToDate.ToString("yyyy-MM-dd");

            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = "";


            //KGeCom
            if (vmMarketingSalesSlave.IsSalesSummery == true)
            {

                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/BazaarBakSalesReports&rs:Command=Render&rs:Format={0}&IsSalesSummery={1}&FromDate={2}&ToDate={3}&Common_ShopFK={4}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSalesSummery, FromDate, ToDate, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsCategoryWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/BazaarBakCategoryWiseSalesReports&rs:Command=Render&rs:Format={0}&IsCategoryWise={1}&FromDate={2}&ToDate={3}&Common_ProductCategoryFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsCategoryWise, FromDate, ToDate, vmMarketingSalesSlave.Common_ProductCategoryFK, vmMarketingSalesSlave.Common_ShopFK);

            }
            else if (vmMarketingSalesSlave.IsSupplierWise == true)
            {
                reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/BazaarBakSupplierWiseSalesReports&rs:Command=Render&rs:Format={0}&IsSupplierWise={1}&FromDate={2}&ToDate={3}&Common_SupplierFK={4}&Common_ShopFK={5}", vmMarketingSalesSlave.ReportType, vmMarketingSalesSlave.IsSupplierWise, FromDate, ToDate, vmMarketingSalesSlave.Common_SupplierFk, vmMarketingSalesSlave.Common_ShopFK);

            }


            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ProdReference.xls");
            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vmMarketingSalesSlave.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ProdReference.doc");
            }

            return View();

        }
        [HttpGet]
        public async Task<IActionResult> MarketingSalesSlavePrint(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
            }
            return View(vmMarketingSalesSlave);
        }

        [HttpGet]
        public async Task<IActionResult> MarketingDamageSlavePrint(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
            }
            return View(vmMarketingSalesSlave);
        }
        [HttpGet]
        public async Task<IActionResult> MarketingSalesList()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> MarketingSalesListView(VMMarketingSalesReport marketingSales)
        {
            marketingSales.DataList = await _service.MarketingSalesGet(marketingSales);


            return View(marketingSales);
        }


        [HttpGet]
        public async Task<IActionResult> MarketingDamageList()
        {

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> MarketingDamageListView(VMMarketingSalesReport marketingSales)
        {
            marketingSales.DataList = await _service.MarketingDamageGet(marketingSales);


            return View(marketingSales);
        }

        [HttpPost]
        public async Task<IActionResult> MarketingSalesReportView(VMMarketingSalesReport model)
        {
            var vMMarketingSales = await _service.MarketingSalesReportGet(model);
            if (model.IsSalesSummery)
            {
                vMMarketingSales.ActualInvoiceValueInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vMMarketingSales.DataListSalesReport.Select(x => x.ActualInvoiceValue).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);

            }
            else
            {
                vMMarketingSales.ActualInvoiceValueInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vMMarketingSales.DataListSalesReport.Select(x => x.SubTotal).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);

            }


            return View(vMMarketingSales);
        }
        [HttpGet]
        public async Task<IActionResult> CustomerDueReport(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            vmMarketingSalesSlave.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");


            return View(vmMarketingSalesSlave);
        }
        
        [HttpPost]
        public async Task<ActionResult> CustomerDueReportView(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            vmMarketingSalesSlave = await _service.CustomerDueReportGet(vmMarketingSalesSlave);

            return View(vmMarketingSalesSlave);
        }
        [HttpGet]
        public async Task<IActionResult> CustomerInvoiceReport(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            vmMarketingSalesSlave.ShopList = new SelectList(_service.CommonShopDropDownList(), "Value", "Text");


            return View(vmMarketingSalesSlave);
        }
        [HttpPost]
        public async Task<ActionResult> CustomerInvoiceReportView(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            vmMarketingSalesSlave = await _service.CustomerInvoiceReportGet(vmMarketingSalesSlave);

            return View(vmMarketingSalesSlave);
        }
        [HttpGet]
        public async Task<IActionResult> MarketingSalesSlave(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
                //vmMarketingSalesSlave.TotalVat = Math.Round(vmMarketingSalesSlave.DataListSlave.Select(x => x.TotalVat).DefaultIfEmpty(0).Sum(), 2);
            }
            else
            {
                vmMarketingSalesSlave = await _service.ReCallLastInvoice();
            }
            return View(vmMarketingSalesSlave);
        }

        [HttpPost]
        public async Task<IActionResult> MarketingSalesSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            
            if (vmMarketingSalesSlave.ActionEum == ActionEnum.Add)
            {

                if (vmMarketingSalesSlave.Marketing_SalesFk == 0 && vmMarketingSalesSlave.Common_ProductFK > 0)
                {
                    vmMarketingSalesSlave.Marketing_SalesFk = await _service.MarketingSalesAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Quantity > 0)
                {
                    await _service.MarketingSalesSlaveAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    if (vmMarketingSalesSlave.TotalInvoiceDiscount > 0 && !vmMarketingSalesSlave.IsDiscounted)
                    {
                        await _service.DistributeDiscountOnMarketingSalesSlave(vmMarketingSalesSlave);

                    }
                    if (vmMarketingSalesSlave.RedeemPoint > 0 && !vmMarketingSalesSlave.IsRedeemPoint)
                    {
                        await _service.DistributeRedeemPointOnMarketingSalesSlave(vmMarketingSalesSlave);

                    }
                    var sales = await _service.MarketingSalesEdit(vmMarketingSalesSlave);
                    
                }

            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.MarketingSalesSlaveEdit(vmMarketingSalesSlave);
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    await _service.MarketingSalesEdit(vmMarketingSalesSlave);
                }

            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.MarketingSalesSlaveDelete(vmMarketingSalesSlave.ID);
            }
            return RedirectToAction(nameof(MarketingSalesSlave), new { id = vmMarketingSalesSlave.Marketing_SalesFk });
        }


        [HttpPost]
        public async Task<IActionResult> DeleteMarketingSalesSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            if (vmMarketingSalesSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                vmMarketingSalesSlave.Marketing_SalesFk = await _service.MarketingSalesSlaveDelete(vmMarketingSalesSlave.ID);
            }
            return RedirectToAction(nameof(MarketingSalesSlave), new { id = vmMarketingSalesSlave.Marketing_SalesFk });
        }
        public async Task<IActionResult> GetCommonProductSubCategory(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSubCategoryGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }


        public JsonResult AutoCompleteProductGet(string id)
        {
            var products = _service.GetAutoCompleteProduct(id);
            return Json(products);
        }

        public JsonResult AutoCompleteProductGetBySupplier(string id, int supplierId)
        {
            var products = _service.GetAutoCompleteProductBySupplier(id, supplierId);
            return Json(products);
        }
        public JsonResult AutoCompleteCustomerGet(string id)
        {
            var products = _service.GetAutoCompleteCustomer(id);
            return Json(products);
        }

        public async Task<IActionResult> GetCommonProduct(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();

            return Json(list);
        }
        public async Task<IActionResult> GetCommonProductSingle(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSingle(id));

            return Json(model);
        }
        public async Task<IActionResult> GetCustomerByID(int id)
        {

            var model = await Task.Run(() => _service.CommonCustomerById(id));

            return Json(model);
        }
        public async Task<IActionResult> GetCustomerByMemberShipNo(string memberShipNo)
        {

            var model = await Task.Run(() => _service.CommonCustomerSingle(memberShipNo));

            return Json(model);
        }
        public async Task<IActionResult> GetCustomerByMobileNo(string phone)
        {

            var model = await Task.Run(() => _service.CommonCustomerSingleBuPhone(phone));

            return Json(model);
        }
        public async Task<IActionResult> GetCommonCustomerSingle(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonCustomerSingle(id));

            return Json(model);
        }

        public async Task<JsonResult> SingleMarketingSalesSlaveGet(int id)
        {
            VMMarketingSalesSlave model = new VMMarketingSalesSlave();
            model = await _service.GetSingleMarketingSalesSlave(id);
            return Json(model);
        }

        public async Task<JsonResult> SingleMarketingSalesCustomerGet(int id)
        {
            VMMarketingSalesSlave model = new VMMarketingSalesSlave();
            model = await _service.GetSingleMarketingSalesCustomer(id);
            return Json(model);
        }


        [HttpGet]
        public async Task<IActionResult> MarketingWholesaleSalesSlave(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
                //vmMarketingSalesSlave.TotalVat = Math.Round(vmMarketingSalesSlave.DataListSlave.Select(x => x.TotalVat).DefaultIfEmpty(0).Sum(), 2);
            }
            else
            {
                vmMarketingSalesSlave = await _service.ReCallLastInvoice();
            }
            return View(vmMarketingSalesSlave);
        }

        [HttpPost]
        public async Task<IActionResult> MarketingWholesaleSalesSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {

            if (vmMarketingSalesSlave.ActionEum == ActionEnum.Add)
            {

                if (vmMarketingSalesSlave.Marketing_SalesFk == 0 && vmMarketingSalesSlave.Common_ProductFK > 0)
                {
                    vmMarketingSalesSlave.Marketing_SalesFk = await _service.MarketingSalesAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Quantity > 0)
                {
                    await _service.MarketingSalesSlaveAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    if (vmMarketingSalesSlave.TotalInvoiceDiscount > 0 && !vmMarketingSalesSlave.IsDiscounted)
                    {
                        await _service.DistributeDiscountOnMarketingSalesSlave(vmMarketingSalesSlave);

                    }
                    if (vmMarketingSalesSlave.RedeemPoint > 0 && !vmMarketingSalesSlave.IsRedeemPoint)
                    {
                        await _service.DistributeRedeemPointOnMarketingSalesSlave(vmMarketingSalesSlave);

                    }
                    var sales = await _service.MarketingSalesEdit(vmMarketingSalesSlave);
                }
            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.MarketingSalesSlaveEdit(vmMarketingSalesSlave);
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    await _service.MarketingSalesEdit(vmMarketingSalesSlave);
                }
            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.MarketingSalesSlaveDelete(vmMarketingSalesSlave.ID);
            }
            return RedirectToAction(nameof(MarketingWholesaleSalesSlave), new { id = vmMarketingSalesSlave.Marketing_SalesFk });
        }
        [HttpGet]
        public async Task<IActionResult> MarketingWholesaleSalesSlavePrint(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
            }
            return View(vmMarketingSalesSlave);
        }


        [HttpGet]
        public async Task<IActionResult> MarketingDamageProductSlave(int id = 0)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            if (id > 0)
            {
                vmMarketingSalesSlave = await _service.MarketingSalesGetByID(id);
                vmMarketingSalesSlave.DataListSlave = await _service.MarketingSalesSlaveGet(id);
            }
            else
            {
                vmMarketingSalesSlave = await _service.ReCallLastDamageInvoice();
            }
            return View(vmMarketingSalesSlave);
        }

        [HttpPost]
        public async Task<IActionResult> MarketingDamageProductSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            if (vmMarketingSalesSlave.ActionEum == ActionEnum.Add)
            {
                if (vmMarketingSalesSlave.Marketing_SalesFk == 0 && vmMarketingSalesSlave.Common_ProductFK > 0)
                {
                    vmMarketingSalesSlave.Marketing_SalesFk = await _service.MarketingDamageProductAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Quantity > 0)
                {
                    await _service.MarketingDamageProductSlaveAdd(vmMarketingSalesSlave);
                }
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    var sales = await _service.MarketingDamageProductEdit(vmMarketingSalesSlave);
                }
            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Edit)
            {
                // Edit
                await _service.MarketingDamageProductSlaveEdit(vmMarketingSalesSlave);
                if (vmMarketingSalesSlave.Marketing_SalesFk > 0)
                {
                    await _service.MarketingDamageProductEdit(vmMarketingSalesSlave);
                }
            }
            else if (vmMarketingSalesSlave.ActionEum == ActionEnum.Delete)
            {
                // Delete
                await _service.MarketingSalesSlaveDelete(vmMarketingSalesSlave.ID);
            }
            return RedirectToAction(nameof(MarketingDamageProductSlave), new { id = vmMarketingSalesSlave.Marketing_SalesFk });
        }

    }
}