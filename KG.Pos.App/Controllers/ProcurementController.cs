﻿using KG.Core.Services;
using KG.Core.Services.Configuration;
using KG.Core.Services.Home;
using KG.Core.Services.Procurement;
using KG.Core.Services.User;
using KG.Infrastructure;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace KG.App.Controllers
{
    //[SoftwareAdmin]
    public class ProcurementController : Controller
    {
        private readonly ILogger _logger;
        private readonly ProcurementService _service;
        private readonly HttpContext _httpContext;
        private readonly VMLogin _vMLogin;
        public ProcurementController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<ProcurementController> logger, IDataProtectionProvider provider)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _vMLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _httpContext = httpContextAccessor.HttpContext;
            _service = new ProcurementService(db, httpContextAccessor.HttpContext);
            _logger = logger;
        }



        #region Purchase Requisition
        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseRequisitionSlave(int id = 0)
        {
            VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave = new VMPurchaseRequisitionSlave();
            if (id == 0)
            {
                vmPurchaseRequisitionSlave.Status = (int)EnumPRStatus.Draft;
            }
            else if (id > 0)
            {
                vmPurchaseRequisitionSlave = await _service.ProcurementPurchaseRequisitionGetByID(id);
                vmPurchaseRequisitionSlave.DataListSlave = await _service.ProcurementPurchaseRequisitionSlaveGet(id);
            }
            //vmPurchaseRequisitionSlave.ProductCategorySelectList = new SelectList(_service.ProductCategoryDropDownList(), "Value", "Text");
            //vmPurchaseRequisitionSlave.ProductSubCategorySelectList = new SelectList(_service.ProductSubCategoryDropDownList(), "Value", "Text");
            //vmPurchaseRequisitionSlave.ProductSelectList = new SelectList(_service.ProductDropDownList(), "Value", "Text");
            return View(vmPurchaseRequisitionSlave);
        }

        [HttpPost]
        public async Task<IActionResult> ProcurementPurchaseRequisitionSlave(VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave)
        {

            if (vmPurchaseRequisitionSlave.ActionEum == ActionEnum.Add)
            {
                if (vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK == 0)
                {
                    vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK = await _service.ProcurementPurchaseRequisitionAdd(vmPurchaseRequisitionSlave);

                }
                await _service.ProcurementPurchaseRequisitionSlaveAdd(vmPurchaseRequisitionSlave);
            }
            else if (vmPurchaseRequisitionSlave.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProcurementPurchaseRequisitionSlaveEdit(vmPurchaseRequisitionSlave);
            }
            else if (vmPurchaseRequisitionSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProcurementPurchaseRequisitionSlaveDelete(vmPurchaseRequisitionSlave.ID);
            }
            //else if ((int)vmPurchaseRequisitionSlave.ActionEum > 3 && vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK > 0)
            //{
            //    //Delete
            //    await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { User = sUser, ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            //}
            //else
            //{
            //    _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Regular PR Functionalities failed).");
            //    return RedirectToAction("Error", "Home");
            //}
            //_logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Regular PR Functionalities).");
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionSlave), new { id = vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK });
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProcurementPurchaseRequisitionSlave(VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave)
        {
            if (vmPurchaseRequisitionSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK = await _service.ProcurementPurchaseRequisitionSlaveDelete(vmPurchaseRequisitionSlave.ID);
            }
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionSlave), new { id = vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK });
        }

        [HttpGet]
        public async Task<IActionResult> ProcurementPOStatusInPRPurchaseRequisitionList()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition = await _service.ProcurementPOStatusInPRPurchaseRequisitionListGet();

            return View(vmPurchaseRequisition);
        }



        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseRequisitionList()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition = await _service.ProcurementPurchaseRequisitionListGet();

            return View(vmPurchaseRequisition);
        }

        [HttpPost]
        public async Task<IActionResult> ProcurementPurchaseRequisitionList(VMPurchaseRequisition vmPurchaseRequisition)
        {

            if (vmPurchaseRequisition.ActionEum == ActionEnum.Edit)
            {

                await _service.ProcurementPurchaseRequisitionEdit(vmPurchaseRequisition);
            }

            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }
        [HttpGet]
        public async Task<IActionResult> ProcurementClosedPurchaseRequisitionList()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition = await _service.ProcurementClosedPurchaseRequisitionListGet();

            return View(vmPurchaseRequisition);
        }
        [HttpGet]
        public async Task<IActionResult> ProcurementHoldPurchaseRequisitionList()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition = await _service.ProcurementHoldPurchaseRequisitionListGet();

            return View(vmPurchaseRequisition);
        }
       
        [HttpGet]
        public async Task<IActionResult> ProcurementCancelPurchaseRequisitionList()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition = await _service.ProcurementCancelPurchaseRequisitionListGet();

            return View(vmPurchaseRequisition);
        }

       
        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseRequisitionReport(int id = 0)
        {
            VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave = new VMPurchaseRequisitionSlave();
            if (id > 0)
            {
                vmPurchaseRequisitionSlave = await _service.ProcurementPurchaseRequisitionGetByID(id);
                vmPurchaseRequisitionSlave.DataListSlave = await _service.ProcurementPurchaseRequisitionSlaveGet(id);
            }
            return View(vmPurchaseRequisitionSlave);
        }

        [HttpGet]
        public async Task<IActionResult> PurchaseRequisitionSummery(int id = 0)
        {
            VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave = new VMPurchaseRequisitionSlave();
            if (id > 0)
            {
                vmPurchaseRequisitionSlave = await _service.ProcurementPurchaseRequisitionGetByID(id);
                vmPurchaseRequisitionSlave.DataListSlave = await _service.PurchaseRequisitionSummeryGet(id);
            }
            return View(vmPurchaseRequisitionSlave);
        }

        [HttpGet]
        public async Task<IActionResult> ProcurementStockDetailsByProduct(int id = 0)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = await _service.CommonProductSingle(id);
                vmCommonProduct.DataList = await _service.ProcurementStockDetailsByProductGet(id);
            }
            return View(vmCommonProduct);
        }
        

        public async Task<JsonResult> SingleProcurementPurchaseRequisitionSlave(int id)
        {
            VMPurchaseRequisitionSlave model = new VMPurchaseRequisitionSlave();
            model = await _service.GetSingleProcurementPurchaseRequisitionSlave(id);
            return Json(model);
        }

        public async Task<JsonResult> SingleProcurementPurchaseRequisition(int id)
        {
            VMPurchaseRequisition model = new VMPurchaseRequisition();
            model = await _service.GetSingleProcurementPurchaseRequisition(id);
            return Json(model);
        }

        public JsonResult AutoCompletePRGet(string id)
        {
            var products = _service.GetAutoCompletePR(id);
            return Json(products);
        }
        public JsonResult AutoCompletePOGet(string id)
        {
            var products = _service.GetAutoCompletePO(id);
            return Json(products);
        }
        public JsonResult GetAutoCompleteSupplierGet(string id)
        {
            var products = _service.GetAutoCompleteSupplier(id);
            return Json(products);
        }
        public async Task<IActionResult> GetCommonProductSingle(int id = 0, int shopId = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSingle(id, shopId));

            return Json(model);
        }

        public async Task<IActionResult> GetCommonProductByShopSingle(int id,int shopId)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSingle(id));

            return Json(model);
        }
        public async Task<IActionResult> GetCommonProductSubCategory(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductSubCategoryGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> GetCommonProduct(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.CommonProductGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();

            return Json(list);
        }

        #region PR Submit HoldUnHold CancelRenew  ClosedReopen Delete
        [HttpPost]
        public async Task<IActionResult> SubmitProcurementPurchaseRequisition(VMPurchaseRequisition vmPurchaseRequisition)
        {
            vmPurchaseRequisition.ID = await _service.ProcurementPurchaseRequisitionSubmit(vmPurchaseRequisition.ID);
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }
        [HttpPost]
        public async Task<IActionResult> SubmitProcurementPurchaseRequisitionFromSlave(VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave)
        {
            vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK = await _service.ProcurementPurchaseRequisitionSubmit(vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK);
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionSlave), "Procurement", new { id = vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK });
        }
        [HttpPost]
        public async Task<IActionResult> HoldUnHoldProcurementPurchaseRequisition(VMPurchaseRequisition vmPurchaseRequisition)
        {
            vmPurchaseRequisition.ID = await _service.ProcurementPurchaseRequisitionHoldUnHold(vmPurchaseRequisition.ID);
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }

        [HttpPost]
        public async Task<IActionResult> CancelRenewProcurementPurchaseRequisition(VMPurchaseRequisition vmPurchaseRequisition)
        {
            vmPurchaseRequisition.ID = await _service.ProcurementPurchaseRequisitionCancelRenew(vmPurchaseRequisition.ID);
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }

        [HttpPost]
        public async Task<IActionResult> ClosedReopenProcurementPurchaseRequisition(VMPurchaseRequisition vmPurchaseRequisition)
        {
            vmPurchaseRequisition.ID = await _service.ProcurementPurchaseOrderClosedReopen(vmPurchaseRequisition.ID);
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProcurementPurchaseRequisition(VMPurchaseRequisition vmPurchaseRequisition)
        {
            if (vmPurchaseRequisition.ActionEum == ActionEnum.Delete)
            {
                //Delete
                vmPurchaseRequisition.ID = await _service.ProcurementPurchaseRequisitionDelete(vmPurchaseRequisition.ID);
            }
            return RedirectToAction(nameof(ProcurementPurchaseRequisitionList));
        }

        #endregion



        #endregion

       
        #region Purchase Order


        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseOrderSlave(int id = 0)
        {
            VMPurchaseOrderSlave vmPurchaseOrderSlave = new VMPurchaseOrderSlave();
            if (id == 0)
            {
                vmPurchaseOrderSlave.Status = (int)EnumPOStatus.Draft;
            }
            else if (id > 0)
            {
                vmPurchaseOrderSlave = await Task.Run(() => _service.ProcurementPurchaseOrderGetByID(id));
                vmPurchaseOrderSlave.DataListSlave = await Task.Run(() => _service.ProcurementPurchaseOrderSlaveGet(id));
                vmPurchaseOrderSlave.TotalPriceInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vmPurchaseOrderSlave.DataListSlave.Select(x => x.PurchaseQuantity * x.PurchasingPrice).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);

            }
            vmPurchaseOrderSlave.TermNCondition = new SelectList(_service.CommonTremsAndConditionDropDownList(), "Value", "Text");

            return View(vmPurchaseOrderSlave);
        }
        [HttpPost]
        public async Task<IActionResult> ProcurementPurchaseOrderSlave(VMPurchaseOrderSlave vmPurchaseOrderSlave, VMPurchaseOrderSlavePartial vmPurchaseOrderSlavePartial)
        {

            if (vmPurchaseOrderSlave.ActionEum == ActionEnum.Add)
            {
                if (vmPurchaseOrderSlave.Procurement_PurchaseOrderFK == 0)
                {
                    vmPurchaseOrderSlave.Procurement_PurchaseOrderFK = await _service.ProcurementPurchaseOrderAdd(vmPurchaseOrderSlave);

                }
                await _service.ProcurementPurchaseOrderSlaveAdd(vmPurchaseOrderSlave, vmPurchaseOrderSlavePartial);
            }
            else if (vmPurchaseOrderSlave.ActionEum == ActionEnum.Edit)
            {
                //Delete
                await _service.ProcurementPurchaseOrderSlaveEdit(vmPurchaseOrderSlave);
            }
            return RedirectToAction(nameof(ProcurementPurchaseOrderSlave), new { id = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK });
        }

        [HttpGet]
        public async Task<IActionResult> ProcurementGeneralPurchaseOrderSlave(int id = 0)
        {
            VMPurchaseOrderSlave vmPurchaseOrderSlave = new VMPurchaseOrderSlave();
            if (id == 0)
            {
                vmPurchaseOrderSlave.Status = (int)EnumPOStatus.Draft;
            }
            else if (id > 0)
            {
                vmPurchaseOrderSlave = await Task.Run(() => _service.ProcurementPurchaseOrderGetByID(id));
                vmPurchaseOrderSlave.DataListSlave = await Task.Run(() => _service.ProcurementPurchaseOrderSlaveGet(id));
                vmPurchaseOrderSlave.TotalPriceInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vmPurchaseOrderSlave.DataListSlave.Select(x => x.PurchaseQuantity * x.PurchasingPrice).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);

            }
            vmPurchaseOrderSlave.TermNCondition = new SelectList(_service.CommonTremsAndConditionDropDownList(), "Value", "Text");

            return View(vmPurchaseOrderSlave);
        }
        [HttpPost]
        public async Task<IActionResult> ProcurementGeneralPurchaseOrderSlave(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {

            if (vmPurchaseOrderSlave.ActionEum == ActionEnum.Add)
            {
                if (vmPurchaseOrderSlave.Procurement_PurchaseOrderFK == 0)
                {
                    vmPurchaseOrderSlave.Procurement_PurchaseOrderFK = await _service.ProcurementPurchaseOrderAdd(vmPurchaseOrderSlave);

                }
                await _service.ProcurementGeneralPurchaseOrderSlaveAdd(vmPurchaseOrderSlave);
            }
            else if (vmPurchaseOrderSlave.ActionEum == ActionEnum.Edit)
            {
                //Delete
                await _service.ProcurementPurchaseOrderSlaveEdit(vmPurchaseOrderSlave);
            }
            return RedirectToAction(nameof(ProcurementGeneralPurchaseOrderSlave), new { id = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK });
        }

        public async Task<IActionResult> ProcurementPurchaseRequisitionSlavePartial(int purchaseRequisitionId)
        {
            var vmPurchaseOrderSlave = await _service.ProcurementPurchaseRequisitionSlavePartialGet(purchaseRequisitionId);
            return PartialView("_ProcurementPurchaseRequisitionSlave", vmPurchaseOrderSlave);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProcurementPurchaseOrderSlave(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {
            if (vmPurchaseOrderSlave.ActionEum == ActionEnum.Delete)
            {
                //Delete
                vmPurchaseOrderSlave.Procurement_PurchaseOrderFK = await _service.ProcurementPurchaseOrderSlaveDelete(vmPurchaseOrderSlave.ID);
            }
            return RedirectToAction(nameof(ProcurementPurchaseOrderSlave), new { id = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK });
        }

        
        public JsonResult GetTermNCondition(int id)
        {
            if (id != 0)
            {
                var list = _service.CommonTermsAndConditionsDropDownList(id);
                return Json(list);
            }
            return null;
        }
        public async Task<JsonResult> SingleProcurementPurchaseOrderSlave(int id)
        {
            var model = await _service.GetSingleProcurementPurchaseOrderSlave(id);
            return Json(model);
        }
        public async Task<JsonResult> SingleGetSinglePurchaseRequisitionSlave(int id)
        {
            var model = await _service.GetSinglePurchaseRequisitionSlave(id);
            return Json(model);
        }

        public async Task<IActionResult> GetProductCategory()
        {


            var model = await Task.Run(() => _service.ProductCategoryGet());
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }


        public async Task<JsonResult> SingleProcurementPurchaseOrder(int id)
        {
            VMPurchaseOrder model = new VMPurchaseOrder();
            model = await _service.GetSingleProcurementPurchaseOrder(id);
            return Json(model);
        }
        public async Task<IActionResult> ProcurementPurchaseOrderGet(int id)
        {
            //if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.ProcurementPurchaseOrderGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.CID }).ToList();
            return Json(list);
        }



        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseOrderList()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder = await _service.ProcurementPurchaseOrderListGet();
            vmPurchaseOrder.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            vmPurchaseOrder.TermNCondition = new SelectList(_service.CommonTremsAndConditionDropDownList(), "Value", "Text");
            return View(vmPurchaseOrder);
        }
        [HttpPost]
        public async Task<IActionResult> ProcurementPurchaseOrderList(VMPurchaseOrder vmPurchaseOrder)
        {
            if (vmPurchaseOrder.ActionEum == ActionEnum.Edit)
            {
                await _service.ProcurementPurchaseOrderEdit(vmPurchaseOrder);
            }
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }
        [HttpGet]
        public async Task<IActionResult> ProcurementCancelPurchaseOrderList()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder = await _service.ProcurementCancelPurchaseOrderListGet();
            return View(vmPurchaseOrder);
        }
        [HttpGet]
        public async Task<IActionResult> ProcurementHoldPurchaseOrderList()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder = await _service.ProcurementHoldPurchaseOrderListGet();

            return View(vmPurchaseOrder);
        }
       
        [HttpGet]
        public async Task<IActionResult> ProcurementClosedPurchaseOrderList()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder = await _service.ProcurementClosedPurchaseOrderListGet();
         
            return View(vmPurchaseOrder);
        }
        [HttpGet]
        public async Task<IActionResult> ProcurementPurchaseOrderReport(int id = 0)
        {
            VMPurchaseOrderSlave vmPurchaseOrderSlave = new VMPurchaseOrderSlave();
            if (id > 0)
            {
                vmPurchaseOrderSlave = await Task.Run(() => _service.ProcurementPurchaseOrderGetByID(id));
                vmPurchaseOrderSlave.DataListSlave = await Task.Run(() => _service.ProcurementPurchaseOrderSlaveGet(id));
                vmPurchaseOrderSlave.TotalPriceInWord = VmCommonCurrency.NumberToWords(Convert.ToDecimal(vmPurchaseOrderSlave.DataListSlave.Select(x => x.PurchaseQuantity * x.PurchasingPrice).DefaultIfEmpty(0).Sum()), CurrencyType.BDT);

            }
            return View(vmPurchaseOrderSlave);
        }
       
      
       
      
        #region PO Submit HoldUnHold CancelRenew  ClosedReopen Delete
        [HttpPost]
        public async Task<IActionResult> SubmitProcurementPurchaseOrder(VMPurchaseOrder vmPurchaseOrder)
        {
            vmPurchaseOrder.ID = await _service.ProcurementPurchaseOrderSubmit(vmPurchaseOrder.ID);
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }
        [HttpPost]
        public async Task<IActionResult> SubmitProcurementPurchaseOrderFromSlave(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {
            vmPurchaseOrderSlave.Procurement_PurchaseOrderFK = await _service.ProcurementPurchaseOrderSubmit(vmPurchaseOrderSlave.Procurement_PurchaseOrderFK);
            return RedirectToAction(nameof(ProcurementPurchaseOrderSlave), "Procurement", new { id = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK });
        }
        [HttpPost]
        public async Task<IActionResult> HoldUnHoldProcurementPurchaseOrder(VMPurchaseOrder vmPurchaseOrder)
        {
            vmPurchaseOrder.ID = await _service.ProcurementPurchaseOrderHoldUnHold(vmPurchaseOrder.ID);
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }

        [HttpPost]
        public async Task<IActionResult> CancelRenewProcurementPurchaseOrder(VMPurchaseOrder vmPurchaseOrder)
        {
            vmPurchaseOrder.ID = await _service.ProcurementPurchaseOrderCancelRenew(vmPurchaseOrder.ID);
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }
        [HttpPost]
        public async Task<IActionResult> ClosedReopenProcurementPurchaseOrder(VMPurchaseOrder vmPurchaseOrder)
        {
            vmPurchaseOrder.ID = await _service.ProcurementPurchaseOrderClosedReopen(vmPurchaseOrder.ID);
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteProcurementPurchaseOrder(VMPurchaseOrder vmPurchaseOrder)
        {
            if (vmPurchaseOrder.ActionEum == ActionEnum.Delete)
            {
                //Delete
                vmPurchaseOrder.ID = await _service.ProcurementPurchaseOrderDelete(vmPurchaseOrder.ID);
            }
            return RedirectToAction(nameof(ProcurementPurchaseOrderList));
        }
        //kkk

        #endregion


        
      

       
        #endregion



    }
}