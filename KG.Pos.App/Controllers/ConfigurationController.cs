﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using KG.Core.Services;
using KG.Core.Services.Configuration;
using KG.Core.Services.Home;
using KG.Core.Services.User;
using KG.Infrastructure;
using System.Net;
using BarcodeLib;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using KG.Core.Services.Marketing;

namespace Pos.App.Controllers
{
    //[SoftwareAdmin]
    public class ConfigurationController : Controller
    {
        private readonly ILogger _logger;
        private IHostingEnvironment _webHostEnvironment;
        private HttpContext httpContext;
        private readonly ConfigurationService _service;
        private readonly VMLogin _vmLogin;

        public ConfigurationController(InfrastructureDbContext db, ILogger<ConfigurationController> logger, IHostingEnvironment webHostEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            httpContext = httpContextAccessor.HttpContext;
            _webHostEnvironment = webHostEnvironment;
            _service = new ConfigurationService(db, httpContextAccessor.HttpContext);
            _logger = logger;
            _vmLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }

        [HttpPost]
        public ActionResult CustomerRDLGet(VMCommonCustomer vMCommonCustomer)
        {
            NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
            WebClient client = new WebClient();
            client.Credentials = nwc;
            string reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/{0}&rs:Command=Render&rs:Format={1}", vMCommonCustomer.ReportName, vMCommonCustomer.ReportType);

            if (vMCommonCustomer.ReportType.Equals(ReportType.EXCEL))
            {
                return File(client.DownloadData(reportURL), "application/vnd.ms-excel", "ProdReference.xls");
            }
            if (vMCommonCustomer.ReportType.Equals(ReportType.PDF))
            {
                return File(client.DownloadData(reportURL), "application/pdf");

            }
            if (vMCommonCustomer.ReportType.Equals(ReportType.WORD))
            {
                return File(client.DownloadData(reportURL), "application/msword", "ProdReference.doc");
            }

            return View();

        }

        #region Unit
        public async Task<IActionResult> CommonUnit()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonUnit vmCommonUnit = new VMCommonUnit();
            vmCommonUnit = await Task.Run(() => _service.GetUnit());
            return View(vmCommonUnit);
        }
        public async Task<JsonResult> SingleCommonUnit(int id)
        {

            VMCommonUnit model = new VMCommonUnit();
            model = await _service.GetSingleCommonUnit(id);
            return Json(model);
        }
        [HttpPost]
        public async Task<IActionResult> CommonUnit(VMCommonUnit vmCommonUnit)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonUnit.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UnitAdd(vmCommonUnit);
            }
            else if (vmCommonUnit.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UnitEdit(vmCommonUnit);
            }
            else if (vmCommonUnit.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UnitDelete(vmCommonUnit.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonUnit));
        }
        [HttpPost]
        public async Task<IActionResult> CommonUnitDelete(VMCommonUnit vmCommonUnit)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonUnit.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UnitDelete(vmCommonUnit.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonUnit));
        }
        [HttpPost]
        public async Task<IActionResult> CommonUnitLockUnlock(VMCommonUnit vmCommonUnit)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            await _service.UnitLockUnlock(vmCommonUnit.ID);
            return RedirectToAction(nameof(CommonUnit));
        }
        #endregion

        #region Common Supplier


        public JsonResult CommonSupplierByIDGet(int id)
        {
            var model = _service.GetCommonSupplierByID(id);
            return Json(model);
        }


        public async Task<IActionResult> CommonSupplier()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonSupplier vmCommonSupplier = new VMCommonSupplier();
            vmCommonSupplier = await Task.Run(() => _service.GetSupplier());
            //vmCommonSupplier.CountryList = new SelectList(_service.CommonCountriesDropDownList(), "Value", "Text");
            vmCommonSupplier.DistrictList = new SelectList(_service.CommonDistrictsDropDownList(), "Value", "Text");
            vmCommonSupplier.DivisionList = new SelectList(_service.CommonDivisionsDropDownList(), "Value", "Text");

            vmCommonSupplier.ThanaList = new SelectList(_service.CommonThanaDropDownList(), "Value", "Text");


            return View(vmCommonSupplier);
        }
        [HttpPost]
        public async Task<IActionResult> CommonSupplier(VMCommonSupplier vmCommonSupplier)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonSupplier.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SupplierAdd(vmCommonSupplier);
            }
            else if (vmCommonSupplier.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SupplierEdit(vmCommonSupplier);
            }
            else if (vmCommonSupplier.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SupplierDelete(vmCommonSupplier.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonSupplier));
        }
        [HttpPost]
        public async Task<IActionResult> CommonSupplierDelete(VMCommonSupplier vmCommonSupplier)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonSupplier.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SupplierDelete(vmCommonSupplier.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonSupplier));
        }



        #endregion

        #region Common Product Category
        public async Task<JsonResult> SingleProductCategory(int id)
        {
            var model = await _service.GetSingleProductCategory(id);
            return Json(model);
        }
        public async Task<IActionResult> CommonProductCategory()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProductCategory vmCommonProductCategory = new VMCommonProductCategory();
            vmCommonProductCategory = await Task.Run(() => _service.GetProductCategory());
            return View(vmCommonProductCategory);
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductCategory(VMCommonProductCategory vmCommonProductCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonProductCategory.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ProductCategoryAdd(vmCommonProductCategory);
            }
            else if (vmCommonProductCategory.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductCategoryEdit(vmCommonProductCategory);
            }
            else if (vmCommonProductCategory.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductCategoryDelete(vmCommonProductCategory.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProductCategory));
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductCategoryDelete(VMCommonProductCategory vmCommonProductCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonProductCategory.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductCategoryDelete(vmCommonProductCategory.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProductCategory));
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductCategoryLockUnlock(VMCommonProductCategory vmCommonProductCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            await _service.CommonProductCategoryLockUnlock(vmCommonProductCategory.ID);
            return RedirectToAction(nameof(CommonProductCategory));
        }
        #endregion... 

        #region Common Product SubCategory

        public async Task<IActionResult> CommonProductSubCategoryGet(int id)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var vmCommonProductSubCategory = await Task.Run(() => _service.CommonProductSubCategoryGet(id));
            var list = vmCommonProductSubCategory.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }
        public async Task<JsonResult> SingleCommonProductSubcategory(int id)
        {
            var model = await _service.GetSingleProductSubCategory(id);
            return Json(model);
        }
        public async Task<IActionResult> CommonProductSubCategory(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProductSubCategory vmCommonProductSubCategory = new VMCommonProductSubCategory();
            vmCommonProductSubCategory = await Task.Run(() => _service.GetProductSubCategory(id));
            return View(vmCommonProductSubCategory);
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductSubCategory(VMCommonProductSubCategory vmCommonProductSubCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonProductSubCategory.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ProductSubCategoryAdd(vmCommonProductSubCategory);
            }
            else if (vmCommonProductSubCategory.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductSubCategoryEdit(vmCommonProductSubCategory);
            }
            else if (vmCommonProductSubCategory.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductSubCategoryDelete(vmCommonProductSubCategory.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProductSubCategory), new { id = vmCommonProductSubCategory.Common_ProductCategoryFk });
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductSubCategoryDelete(VMCommonProductSubCategory vmCommonProductSubCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonProductSubCategory.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductSubCategoryDelete(vmCommonProductSubCategory.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProductSubCategory));
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductSubCategoryLockUnlock(VMCommonProductSubCategory vmCommonProductSubCategory)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            await _service.CommonProductSubCategoryLockUnlock(vmCommonProductSubCategory.ID);
            return RedirectToAction(nameof(CommonProductSubCategory));
        }
        #endregion
        public JsonResult AutoCompleteBrandGet(string id)
        {
            var products = _service.GetAutoCompleteBrand(id);
            return Json(products);
        }
        public JsonResult AutoCompleteSupplierGet(string id)
        {
            var products = _service.GetAutoCompleteSupplier(id);
            return Json(products);
        }
        public JsonResult AutoCompleteProductCategoryGet(string id)
        {
            var products = _service.GetAutoCompleteProductCategory(id);
            return Json(products);
        }
        #region Common Product
        [HttpPost]
        public async Task<IActionResult> CommonAllProduct(string allProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct = await Task.Run(() => _service.GetAllProduct());

            vmCommonProduct.UnitList = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");


            return View(vmCommonProduct);
        }

        [HttpGet]
        public async Task<IActionResult> DiscountedProductList()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct = await Task.Run(() => _service.GetDiscountedtProductList());

            return View(vmCommonProduct);
        }
        [HttpGet]
        public async Task<IActionResult> CommonProduct(int id = 0, int id2 = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0 || id2 > 0 || id2 == -1) // -1 means filter by Brand
            {
                vmCommonProduct = await Task.Run(() => _service.GetProduct(id, id2));
            }

            vmCommonProduct.UnitList = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            vmCommonProduct.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");


            return View(vmCommonProduct);
        }

        [HttpPost]
        public async Task<IActionResult> CommonProduct(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            // this block used for show all product. 
            if (vmCommonProduct.ActionEum == ActionEnum.Get)
            {

                vmCommonProduct = await Task.Run(() => _service.GetAllProduct());

                vmCommonProduct.UnitList = new SelectList(_service.UnitDropDownList(), "Value", "Text");
                vmCommonProduct.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
                return View(vmCommonProduct);

            }
            if (vmCommonProduct.ImageFile != null)
            {
                vmCommonProduct.Image = _service.UploadFile(vmCommonProduct.ImageFile, "Product", _webHostEnvironment.WebRootPath);
            }
            if (vmCommonProduct.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ProductAdd(vmCommonProduct);
            }
            else if (vmCommonProduct.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductEdit(vmCommonProduct);
            }
            else if (vmCommonProduct.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductDelete(vmCommonProduct.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProduct), new { id = vmCommonProduct.Common_ProductSubCategoryFk });
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductDelete(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonProduct.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductDelete(vmCommonProduct.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonProduct));
        }

        [HttpPost]
        public async Task<IActionResult> CommonProductLockUnlock(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            await _service.CommonProductLockUnlock(vmCommonProduct.ID);
            return RedirectToAction(nameof(CommonProduct));
        }
        public JsonResult CommonProductByIDGet(int id)
        {
            var model = _service.GetCommonProductByID(id);
            return Json(model);
        }
        public async Task<IActionResult> AssignProductVat()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> AssignProductVat(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            await _service.ProductVatEdit(vmCommonProduct);
            return RedirectToAction(nameof(AssignProductVat));
        }

        public async Task<IActionResult> EnableOrDisableProductVat()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> EnableOrDisableProductVat(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }

            await _service.EnableOrDisableProductVatEdit(vmCommonProduct);
            return RedirectToAction(nameof(EnableOrDisableProductVat));
        }



        public JsonResult MakeSystemBarcode(int productCategoryFk)
        {
            var model = _service.MakeSystemBarcode(productCategoryFk);
            return Json(model);
        }

        public async Task<IActionResult> ChangeCommonProductExpiryDate(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);
            }

            // vmCommonProduct.ProductList = new SelectList(_service.CommonProductDropDown(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeCommonProductExpiryDate(VMCommonProduct vmCommonProduct)
        {

            if (vmCommonProduct.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductExpiryDateEdit(vmCommonProduct);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(ChangeCommonProductExpiryDate), new { id = vmCommonProduct.ID });
        }
        public async Task<IActionResult> ChangeProductMRPPrice(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);
            }

            //vmCommonProduct.ProductList = new SelectList(_service.CommonProductDropDown(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeProductMRPPrice(VMCommonProduct vmCommonProduct)
        {

            if (vmCommonProduct.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductMRPPriceEdit(vmCommonProduct);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(ChangeProductMRPPrice), new { id = vmCommonProduct.ID });
        }

        public async Task<IActionResult> MakeCommonProductDiscount(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);
            }

            //vmCommonProduct.ProductList = new SelectList(_service.CommonProductDropDown(), "Value", "Text");


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> MakeCommonProductDiscount(VMCommonProduct vmCommonProduct)
        {

            if (vmCommonProduct.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductDiscountEdit(vmCommonProduct);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(MakeCommonProductDiscount), new { id = vmCommonProduct.ID });
        }

        public async Task<IActionResult> AssignProductDiscount()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();


            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> AssignProductDiscount(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            await _service.ProductDiscountEdit(vmCommonProduct);
            return RedirectToAction(nameof(AssignProductDiscount));
        }


        public async Task<IActionResult> MakeCommonProductGRN(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);
            }

            return View(vmCommonProduct);
        }


        [HttpPost]
        public async Task<IActionResult> MakeCommonProductGRN(VMCommonProduct vmCommonProduct)
        {

            if (vmCommonProduct.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductGRNEdit(vmCommonProduct);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(MakeCommonProductGRN), new { id = vmCommonProduct.ID });
        }


        //public ActionResult GenerateProducBarcode(int id = 0, int id2 = 0)
        //{


        //    NetworkCredential nwc = new NetworkCredential("Administrator", "Gocorona!9");
        //    WebClient client = new WebClient();
        //    client.Credentials = nwc;
        //    string reportURL = "";
        //    reportURL = string.Format("http://192.168.0.7/ReportServer_SQLEXPRESS/?%2fPos.RDL/Barcode&rs:Command=Render&rs:Format=PDF&PrintQuantity={0}&ProductId={1}", id2, id);



        //    return File(client.DownloadData(reportURL), "application/pdf");
        [HttpGet]
        public async Task<IActionResult> CommonProductBarcodeGenerate()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct.DataToList = new List<VMCommonProductBarcode> { new VMCommonProductBarcode() {  ID = 0, QtyOfBarcodeGen = 0, Name = "" } };
            return View(vmCommonProduct);
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductBarcodeGenerateView(VMCommonProduct vmCommonProduct)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            vmCommonProduct = _service.GetCommonProductInfoToBarcodeGenarate(vmCommonProduct);           
            return View(vmCommonProduct);
        }

        public async Task<IActionResult> GenerateProducBarcodeByChallan(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var vmCommonProduct = _service.GetCommonProductByChallanID(id);
            return View(vmCommonProduct);
        }

        public async Task<IActionResult> GenerateProducBarcode(int id = 0, int id2 = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);

                vmCommonProduct.QtyOfBarcodeGen = id2;

            }

            return View(vmCommonProduct);
        }

        public async Task<IActionResult> GenerateBarcode(int id)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            if (id > 0)
            {
                vmCommonProduct = _service.GetCommonProductByID(id);



            }
            Barcode barcode = new Barcode();


            barcode.Alignment = AlignmentPositions.CENTER;
            barcode.BackColor = System.Drawing.Color.White;
            barcode.ForeColor = System.Drawing.Color.Black;
            barcode.IncludeLabel = true;

            barcode.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;


            Image barcodeImage = barcode.Encode(TYPE.CODE128, vmCommonProduct.SystemBarcode, Color.Black, Color.White, 170, 50);
            var data = ConvertImageToBytes(barcodeImage);


            return File(data, "image/png");
        }

        private byte[] ConvertImageToBytes(Image barcodeImage)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                barcodeImage.Save(memoryStream, ImageFormat.Png);
                return memoryStream.ToArray();
            }
        }

        public JsonResult WareHousePOReceivingGRNNotCompletedDropDownGet(int productId)
        {
            var model = _service.WareHousePOReceivingGRNNotCompletedDropDown(productId);
            return Json(model);
        }
        public JsonResult CommonProductByID(int id)
        {

            var model = _service.GetCommonProductByID(id);
            return Json(model);
        }

        public JsonResult POReceivingSlaveByIDGet(int receivingId, int productId)
        {

            var model = _service.GetPOReceivingSlaveByID(receivingId, productId);
            return Json(model);
        }
        #endregion


        #region Common Customer
        public JsonResult CommonCustomerByIDGet(int id)
        {
            var model = _service.GetCommonCustomerByID(id);
            return Json(model);
        }
        public async Task<IActionResult> CommonCustomer(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();
            vmCommonCustomer.DOB = DateTime.Now;
            return View(vmCommonCustomer);
        }
        [HttpPost]
        public async Task<IActionResult> CommonCustomer(VMCommonCustomer vmCommonCustomer)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonCustomer.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CustomerAdd(vmCommonCustomer);
            }
            else if (vmCommonCustomer.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CustomerEdit(vmCommonCustomer);
            }
            else if (vmCommonCustomer.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CustomerDelete(vmCommonCustomer.ID);
            }
            else if (vmCommonCustomer.ActionEum == ActionEnum.Get)
            {
                vmCommonCustomer = await Task.Run(() => _service.GetCustomer(vmCommonCustomer));
                return View(vmCommonCustomer);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonCustomer));
        }
        [HttpPost]
        public async Task<IActionResult> CommonCustomerDelete(VMCommonCustomer vmCommonCustomer)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonCustomer.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CustomerDelete(vmCommonCustomer.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonCustomer));
        }

        //[HttpGet]
        //public async Task<IActionResult> CustomerContact(int id = 0)
        //{
        //    if (_vmLogin == null)
        //    {
        //        return RedirectToAction("Login", "Auth");
        //    }
        //    VMCommonCustomerContact vmCommonCustomerContact = new VMCommonCustomerContact();

        //    vmCommonCustomerContact = await _service.GetCustomerContact(id);
        //    return View(vmCommonCustomerContact);
        //}

        //[HttpPost]
        //public async Task<IActionResult> CustomerContact(VMCommonCustomerContact vmCommonCustomerContact)
        //{
        //    if (_vmLogin == null)
        //    {
        //        return RedirectToAction("Login", "Auth");
        //    }
        //    if (vmCommonCustomerContact.ActionEum == ActionEnum.Add)
        //    {
        //        await _service.CustomerContactAdd(vmCommonCustomerContact);
        //    }
        //    else if (vmCommonCustomerContact.ActionEum == ActionEnum.Edit)
        //    {
        //        //Edit
        //        await _service.CommonCustomerContactEdit(vmCommonCustomerContact);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Error", "Home");
        //    }
        //    return RedirectToAction(nameof(CustomerContact), new { id = vmCommonCustomerContact.Common_CustomerFk });
        //}
        //[HttpPost]
        //public async Task<IActionResult> CommonCustomerContactDelete(VMCommonCustomerContact vmCommonCustomerContact)
        //{
        //    if (_vmLogin == null)
        //    {
        //        return RedirectToAction("Login", "Auth");
        //    }
        //    if (vmCommonCustomerContact.ActionEum == ActionEnum.Delete)
        //    {
        //        //Delete
        //        await _service.CommonCustomerContactDelete(vmCommonCustomerContact.ID);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Error");
        //    }
        //    return RedirectToAction(nameof(CustomerContact), new { id = vmCommonCustomerContact.Common_CustomerFk });

        //}

        #endregion



        #region Common Shop
        public JsonResult CommonShopByIDGet(int id)
        {
            var model = _service.GetCommonShopByID(id);
            return Json(model);
        }
        public async Task<IActionResult> CommonShop()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonShop vmCommonShop = new VMCommonShop();
            vmCommonShop = await Task.Run(() => _service.GetShop());
            vmCommonShop.DistrictList = new SelectList(_service.CommonDistrictsDropDownList(), "Value", "Text");

            vmCommonShop.DivisionList = new SelectList(_service.CommonDivisionsDropDownList(), "Value", "Text");
            vmCommonShop.ThanaList = new SelectList(_service.CommonThanaDropDownList(), "Value", "Text");

            vmCommonShop.TradeLicenceExpireDate = DateTime.Now;

            return View(vmCommonShop);
        }
        [HttpPost]
        public async Task<IActionResult> CommonShop(VMCommonShop vmCommonShop)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonShop.TradeLicenceFile != null)
            {
                vmCommonShop.TradeLicenceUrl = _service.UploadFile(vmCommonShop.TradeLicenceFile, "Shop", _webHostEnvironment.WebRootPath);

            }

            if (vmCommonShop.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ShopAdd(vmCommonShop);
            }
            else if (vmCommonShop.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShopEdit(vmCommonShop);
            }
            else if (vmCommonShop.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShopDelete(vmCommonShop.ID);
            }

            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonShop));
        }
        [HttpPost]
        public async Task<IActionResult> CommonShopDelete(VMCommonShop vmCommonShop)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonShop.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShopDelete(vmCommonShop.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonShop));
        }
        #endregion

        #region Common Shop Counter
        public async Task<IActionResult> CommonShopCounter(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonShopCounter vmCommonShopCounter = new VMCommonShopCounter();

            vmCommonShopCounter = await Task.Run(() => _service.GetShopFloor(id));
            vmCommonShopCounter.ShopList = new SelectList(_service.CommonShopDropDown(id), "Value", "Text");

            return View(vmCommonShopCounter);
        }
        [HttpPost]
        public async Task<IActionResult> CommonShopCounter(VMCommonShopCounter vmCommonShopCounter)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonShopCounter.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ShopFloorAdd(vmCommonShopCounter);
            }
            else if (vmCommonShopCounter.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShopFloorEdit(vmCommonShopCounter);
            }
            else if (vmCommonShopCounter.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShopFloorDelete(vmCommonShopCounter.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonShopCounter));
        }
        [HttpPost]
        public async Task<IActionResult> CommonShopCounterDelete(VMCommonShopCounter vmCommonShopCounter)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonShopCounter.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShopFloorDelete(vmCommonShopCounter.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonShopCounter));
        }
        #endregion

        #region Geolocation
        public async Task<IActionResult> CommonDistrictsGet(int id)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var vmCDistricts = await Task.Run(() => _service.CommonDistrictsGet(id));
            var list = vmCDistricts.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> CommonThanaGet(int id)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var vmPoliceStations = await Task.Run(() => _service.DDLThanaListByDistrictID(id));
            return Json(vmPoliceStations);
        }

        #endregion

        #region Rack
        public async Task<JsonResult> CommonBinSingle(int id)
        {
            VMCommonBin vmCommonBin = new VMCommonBin();
            vmCommonBin = await _service.CommonBinSingleGet(id);
            return Json(vmCommonBin);
        }
        [HttpPost]
        public IActionResult GetVirtualBin(VMCommonBinSlave vmCommonBinSlave)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            vmCommonBinSlave.DataSlaveToList = _service.GetVirtualBinData(vmCommonBinSlave);
            return PartialView("_VirtualBinPartial", vmCommonBinSlave);
        }
        [HttpGet]
        public async Task<IActionResult> CommonBinList()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonBin vmCommonBin = new VMCommonBin();
            vmCommonBin = await _service.CommonBinGet();
            vmCommonBin.ShopList = new SelectList(_service.CommonShopDropDown(), "Value", "Text");

            return View(vmCommonBin);
        }
        [HttpPost]
        public async Task<IActionResult> CommonBinList(VMCommonBin vmCommonBin)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBin.ActionEum == ActionEnum.Edit)
            {
                await _service.CommonBinEdit(vmCommonBin);
            }
            return RedirectToAction(nameof(CommonBinList));
        }
        [HttpPost]
        public async Task<IActionResult> CommonBinDelete(VMCommonBin vmCommonBin)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBin.ActionEum == ActionEnum.Delete)
            {
                await _service.CommonBinDelete(vmCommonBin.ID);
            }
            return RedirectToAction(nameof(CommonBinList));
        }

        [HttpGet]
        public async Task<IActionResult> CommonBinSlave(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonBinSlave vmCommonBinSlave = new VMCommonBinSlave();
            if (id > 0)
            {
                vmCommonBinSlave = await _service.CommonBinSlaveGet(id);

            }
            if (id == -1)
            {
                vmCommonBinSlave.error = "This Rack no already define, please chose the another no";

            }

            return View(vmCommonBinSlave);
        }
        [HttpPost]
        public async Task<IActionResult> CommonBinSlave(VMCommonBinSlave vmCommonBinSlave)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBinSlave.ActionEum == ActionEnum.Add)
            {
                int exRack = _service.GetExRackNo(vmCommonBinSlave.RackNo);
                if (exRack > 0)
                {
                    return RedirectToAction(nameof(CommonBinSlave), new { id = -1 });

                }
                else
                {
                    if (vmCommonBinSlave.Common_BinFk == 0)
                    {
                        vmCommonBinSlave.Common_BinFk = await _service.CommonBinAdd(vmCommonBinSlave);
                    }
                    await _service.CommonBinSlaveAdd(vmCommonBinSlave);
                }

            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction(nameof(CommonBinSlave), new { id = vmCommonBinSlave.Common_BinFk });
        }
        [HttpGet]
        public async Task<IActionResult> CommonProductBinSlave(int id = 0)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonBinSlave vmCommonBinSlave = new VMCommonBinSlave();
            vmCommonBinSlave = await _service.CommonBinSlaveByIDGet(id);
            vmCommonBinSlave.VMCommonProduct = await _service.GetProductByBinSlaveId(id);

            //var existingProduct = _service.AddedProductInBin(id);
            //if (existingProduct != null)
            //{
            //    vmCommonBinSlave.ProductList = new MultiSelectList(_service.CommonProductDropDown(), "Value", "Text", existingProduct);
            //}
            //else
            //{
            //    vmCommonBinSlave.ProductList = new MultiSelectList(_service.CommonProductDropDown(), "Value", "Text");

            //}

            return View(vmCommonBinSlave);
        }
        [HttpPost]
        public async Task<IActionResult> CommonProductBinSlave(VMCommonBinSlave vmCommonBinSlave)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBinSlave.ActionEum == ActionEnum.Add)
            {
                if (vmCommonBinSlave.Common_ProductFK > 0)
                {
                    await _service.CommonProductInBinSlaveAdd(vmCommonBinSlave);
                }
            }
            else if (vmCommonBinSlave.ActionEum == ActionEnum.Delete)
            {
                await _service.CommonProductInBinSlaveDelete(vmCommonBinSlave);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction(nameof(CommonProductBinSlave), new { id = vmCommonBinSlave.ID });
        }

        #endregion


        #region Common Brand
        public async Task<IActionResult> CommonBrand()
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMCommonBrand vmCommonBrand = new VMCommonBrand();
            vmCommonBrand = await Task.Run(() => _service.GetCommonBrand());
            return View(vmCommonBrand);
        }
        [HttpPost]
        public async Task<IActionResult> CommonBrand(VMCommonBrand vmCommonBrand)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBrand.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CommonBrandAdd(vmCommonBrand);
            }
            else if (vmCommonBrand.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CommonBrandEdit(vmCommonBrand);
            }
            else if (vmCommonBrand.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CommonBrandDelete(vmCommonBrand.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonBrand));
        }
        [HttpPost]
        public async Task<IActionResult> CommonBrandDelete(VMCommonBrand vmCommonBrand)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (vmCommonBrand.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CommonBrandDelete(vmCommonBrand.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(CommonBrand));
        }
        [HttpPost]
        public async Task<IActionResult> CommonBrandLockUnlock(VMCommonBrand vmCommonBrand)
        {
            if (_vmLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            await _service.CommonBrandLockUnlock(vmCommonBrand.ID);
            return RedirectToAction(nameof(CommonBrand));
        }
        #endregion..   . 


        public async Task<ActionResult> MarketingSalesInvoiceByCustomer(int id)
        {
            VMMarketingSalesSlave vmOrderMaster = new VMMarketingSalesSlave();
            vmOrderMaster = await Task.Run(() => _service.ProcurementOrderMastersListGetByCustomer(id));
            return View(vmOrderMaster);
        }

        [HttpGet]
        public async Task<ActionResult> OrderMasterByID(int id)
        {
            VMSalesOrder vmOrderMaster = new VMSalesOrder();
            vmOrderMaster = await Task.Run(() => _service.ProcurementOrderMastersGetByID(id));
            return View(vmOrderMaster);
        }
        [HttpPost]
        public async Task<ActionResult> OrderMasterByID(VMSalesOrder vmSalesOrder)
        {

            if (vmSalesOrder.ActionEum == ActionEnum.Add)
            {

                await _service.PaymentAdd(vmSalesOrder);
            }


            else
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction(nameof(OrderMasterByID), new { id = vmSalesOrder.Common_CustomerFK });
        }

      

        [HttpGet]
        public async Task<ActionResult> InvoiceWiseCustomerLedgerOpening(int id)
        {
            VmTransaction vmTransaction = new VmTransaction();
            vmTransaction.VMCommonCustomer = new VMCommonCustomer();
            vmTransaction.FromDate = DateTime.Now.AddDays(-30);
            vmTransaction.ToDate = DateTime.Now;
            vmTransaction.Common_CustomerFK = id;           
            vmTransaction.VMCommonCustomer = await Task.Run(() => _service.GetCustomerById(id));

            return View(vmTransaction);
        }
        [HttpPost]
        public async Task<ActionResult> InvoiceWiseCustomerLedgerOpeningView(VmTransaction vmTransaction)
        {
            var vmCommonSupplierLedger = await Task.Run(() => _service.GetLedgerInfoByCustomer(vmTransaction));
            return View(vmCommonSupplierLedger);
        }


      

      
    }
}
