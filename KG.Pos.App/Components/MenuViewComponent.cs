﻿using KG.Core.Services.Home;
using KG.Core.Services.User;
using KG.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KG.App.Components
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly HomeService _service;

        public MenuViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _service = new HomeService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
