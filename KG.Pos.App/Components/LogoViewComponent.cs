﻿using KG.Core.Services.Home;
using KG.Core.Services.User;
using KG.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KG.App.Components
{
    public class LogoViewComponent : ViewComponent
    {
        private readonly HomeService _service;

        public LogoViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _service = new HomeService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult  Invoke()
        {
            var commonCompany = _service.CommonCompanyGet();


            return View(commonCompany);
        }
    }
}
