﻿using KG.Core.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace KG.Core.Services.WareHouse
{
    
   

    public class VMWareHousePOReceiving : BaseVM
    {
        public string ChallanCID { get; set; }
        public string Challan { get; set; }
        public DateTime ChallanDate { get; set; }
        public int Procurement_PurchaseOrderFk { get; set; }
        public int User_DepartmentFk { get; set; }
        public string POCID { get; set; }
        public DateTime PODate { get; set; }
        public string SupplierName { get; set; }

        public int Common_SupplierFK { get; set; }
        public int Common_ProductFk { get; set; }

        public IEnumerable<VMWareHousePOReceiving> DataList { get; set; }
        public SelectList ProcurementPurchaseOrderList { get; set; } = new SelectList(new List<object>());
        public SelectList ExistingChallanList { get; set; } = new SelectList(new List<object>());


        public decimal LaborCost { get; set; }
        public decimal ManufacturingOverhead { get; set; }
        public decimal TransportationOverhead { get; set; }
        public decimal OthersCost { get; set; }
        public string OthersCostNote { get; set; }
    }

    public class VMWareHousePOReceivingSlave : VMWareHousePOReceiving
    {
        public int WareHouse_POReceivingFk { get; set; }

        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public decimal ReceivedQuantity { get; set; } = 0;
        public decimal POQuantity { get; set; } = 0;
        public decimal PriviousReceivedQuantity { get; set; } = 0;
        public decimal RemainingQuantity { get; set; } = 0;


        public decimal StockLossQuantity { get; set; } = 0;
        public bool IsReturn { get; set; }

        public string ProductName { get; set; }

        public string ProductCategory { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryAddress { get; set; }
        public decimal ReturnQuantity { get; set; }
        public decimal ExtSendQuantity { get; set; }

        public IEnumerable<VMWareHousePOReceivingSlave> DataListSlave { get; set; }
        public bool IsGRNCompleted { get; set; }
        public string ProductSubCategory { get; set; }
        public string SystemBarcode { get; set; }
        public string Brand { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal TotalCostingPrice { get; set; }
        public decimal TotalMRPPrice { get; set; }
        public decimal PurchasingPrice { get; set; }
        public decimal GPParcentage { get; set; }
        public string TotalCostingPriceInWord { get; set; }
        public string ShopName { get; set; }
    }

    public class VMWareHousePOReturnSlave : VMWareHousePOReceiving
    {
        public int WareHouse_POReceivingFk { get; set; }

        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public decimal ReturnQuantity { get; set; } = 0;
        public decimal POQuantity { get; set; } = 0;
        public decimal PriviousReceivedQuantity { get; set; } = 0;
        public decimal RemainingQuantity { get; set; } = 0;
        public string CausesOfReturn { get; set; }
        public string SystemBarcode { get; set; }

        
        public decimal StockLossQuantity { get; set; } = 0;
        public bool IsReturn { get; set; }

        public string ProductName { get; set; }
        public string SubCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string UnitName { get; set; }
        public string BrandName { get; set; }

        public string ProductDescription { get; set; }

        public IEnumerable<VMWareHousePOReturnSlave> DataListSlave { get; set; }
      
        public DateTime? DeliveryDate { get; set; }
        public string DeliveryAddress { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal TotalCostingPrice { get; set; }

        public decimal MRPPrice { get; set; }
        public decimal TotalMRPPrice { get; set; }


    }

    public class VMWareHousePOReceivingSlavePartial : BaseVM
    {

        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public int Common_ProductFK { get; set; }
        public int? Common_BrandFk { get; set; }
        public int? Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public decimal ReceivedQuantity { get; set; } = 0;
        public decimal TotalOverhead { get; set; } = 0;
        public decimal ReturnQuantity { get; set; } = 0;
        public decimal PurchasingPrice { get; set; } = 0;

        public decimal Damage { get; set; } = 0;

        public decimal POQuantity { get; set; } = 0;
        public decimal PriviousReceivedQuantity { get; set; } = 0;
        public decimal RemainingQuantity { get; set; } = 0;


        public decimal StockLossQuantity { get; set; } = 0;
        public bool IsReturn { get; set; }
     

        public string ProductName { get; set; }
        public string ProductDescription { get; set; }

        public List<VMWareHousePOReceivingSlavePartial> DataListSlavePartial { get; set; }
        public decimal PriviousReturnQuantity { get; set; }
        public string UnitName { get; set; }
        public int WareHouse_BinSlaveFk { get; set; }
        public SelectList WareHouseBinSlaveList { get; set; } = new SelectList(new List<object>());
      
        public int Procurement_PurchaseOrderFk { get; set; }
        public string SupplierName { get; set; }
        public string POCID { get; set; }
        public DateTime PODate { get; set; }
        
    }

    public class VMWareHouseRequisitionPOSlavePartial : BaseVM
    {

        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public int CategoryID { get; set; }

        public decimal RequisitionQuantity { get; set; } = 0;
        public decimal Damage { get; set; } = 0;

        public decimal POQuantity { get; set; } = 0;
        public decimal StoreAvailableQuantity { get; set; } = 0;
        public decimal RequisiteFor { get; set; } = 0;

        public decimal StockLossQuantity { get; set; } = 0;
        public bool IsReturn { get; set; }
        public bool IsAsset { get; set; }

        public string ProductName { get; set; }
        public string ProductDescription { get; set; }

        public List<VMWareHouseRequisitionPOSlavePartial> DataListSlavePartial { get; set; }

        public string UnitName { get; set; }
      

        public int WareHouse_BinSlaveFk { get; set; }
        public SelectList WareHouseBinSlaveList { get; set; } = new SelectList(new List<object>());
        public decimal PreviousConsume { get; internal set; }
    }
    
    public class VMWareHousePOReturnSlavePartial : BaseVM
    {

        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public int Common_ProductFK { get; set; }
        public decimal ReturnQuantity { get; set; } = 0;
        public decimal POQuantity { get; set; } = 0;
     
        public decimal PriviousReceivedQuantity { get; set; } = 0;
        public decimal SalesQuantity { get; set; } = 0;

        public decimal RemainingQuantity { get; set; } = 0;
        public decimal ReceivedChallanQuantity { get; set; } = 0;
        public decimal StockLossQuantity { get; set; } = 0;
        public bool IsReturn { get; set; }

        public string ProductName { get; set; }
        public string UnitName { get; set; }
        public string ProductDescription { get; set; }
        public List<VMWareHousePOReturnSlavePartial> DataListSlavePartial { get; set; }
        public decimal PriviousReturnQuantity { get; set; }
        public string POCID { get; internal set; }
        public string ChallanCID { get; internal set; }
    }
    

    public class VMWareHouse_Requisition : BaseVM
    {

        public string RequisitionCID { get; set; }
        public DateTime Date { get; set; }
        public DateTime CuttingDate { get; set; }

        public string Remarks { get; set; }
        //public int? Mkt_BOMFK { get; set; }       
        public int FromUser_DepartmentFk { get; set; }
        public string FromUserDepartmentName { get; set; }

        public int ToUser_DepartmentFK { get; set; }
        public string ToUserDepartmentName { get; set; }
        public bool IsInternal { get; set; }
        public bool IsComplete { get; set; }
        public bool IsAutherized { get; set; }
        public DateTime? AutherizedDate { get; set; }

        public int EnumStoreTypeFk { get; set; }
        public IEnumerable<VMWareHouse_Requisition> RequisitionDataList { get; set; }

        public SelectList UserStoreDepartmentList { get; set; } = new SelectList(new List<object>());
        public string UserName { get; set; }
    }

    public class VMWareHouse_RequisitionSlave : VMWareHouse_Requisition
    {

        public int WareHouse_RequisitionFk { get; set; }
        public int? Common_ProductFk { get; set; }
        public string CommonProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CommonUnitName { get; set; }

    
        public int? Procurement_PurchaseOrderSlaveFk { get; set; }
        public decimal EstimatedPrice { get; set; } = 0;
        public decimal RequiredQuantity { get; set; } = 0;
        public decimal POQuantity { get; set; } = 0;
        public bool IsFinalize { get; set; }
        public IEnumerable<VMWareHouse_RequisitionSlave> DataListSlave { get; set; }
        public SelectList MerchandisingStyleList { get; set; } = new SelectList(new List<object>());
        public SelectList ProdReferencePlanList { get; set; } = new SelectList(new List<object>());
      
        public string POCID { get; set; }
    }

    public class VMWareHouseRequisitionSlavePartial
    {

        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public  decimal RequestQuantity { get; set; } = 0;
        public decimal ConsumeQuantity { get; set; } = 0;
        public decimal PreviousSendQuantity { get; set; } = 0;
        public decimal StoreAvailableQuantity { get; set; } = 0;
        public decimal MaxValidation { get; set; } = 0;



        public string CommonProductName { get; set; }
        public string ProductDescription { get; set; }
        public string CommonUnitName { get; set; }
        public List<VMWareHouseRequisitionSlavePartial> DataListSlavePartial { get; set; }
   
        public string RequisitionCID { get; set; }
        public string POCID { get; set; }

        //accounting
        public int Common_ShopFK { get; set; }
      
        public int Common_ProductFK { get; set; }
        public decimal ReceivedQty { get; set; }
        public int WareHouse_ConsumptionFk { get; set; }
       
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal CostingPrice { get; set; }
        public int? Procurement_PurchaseOrderSlaveFK { get; set; }
        public string BrandName { get; set; }
        public string SubCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string SystemBarcode { get; set; }
    }
  

    public class VMWareHouseConsumption : BaseVM
    {
        public string ConsumptionCID { get; set; }
        public DateTime Date { get; set; }
        public int ToCommon_ShopFK { get; set; }

        public int FromUser_DepartmentFk { get; set; }
        public string FromUserDepartmentName { get; set; }
        public string UserName { get; set; }

        public int ToUser_DepartmentFK { get; set; }
        public string ToUserDepartmentName { get; set; }
        public int Procurement_PurchaseRequisitionFK { get; set; }

        public bool Acknowledgement { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public int EnumStoreTypeFk { get; set; }
        public IEnumerable<VMWareHouseConsumption> ConsumptionDataList { get; set; }
        public string Remarks { get; set; }
        public List<string> RequisitionCIDList { get; set; }
        public bool SendByStoreRequisition { get; set; }
        public string ShopName { get; set; }
    }

    public class VMWareHouseConsumptionSlave : VMWareHouseConsumption
    {
        public int WareHouse_ConsumptionFk { get; set; }
        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int Common_ProductFK { get; set; }
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal ConsumeQuantity { get; set; } = 0;
        public int WareHouseConsumptionFk { get; set; }
        public int WareHouse_RequisitionSlaveFk { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }

        public string ProductDescription { get; set; }
       
      
        public decimal PreviousSendQuantity { get; set; } = 0;
        public decimal StoreAvailableQuantity { get; set; } = 0;
      
        public decimal ReturnQuantity { get; set; } = 0;
        public decimal UnitPrice { get; set; } = 0;

        
        public string POID { get; set; }
        public SelectList ConsumptionList { get; set; } = new SelectList(new List<object>());
        public SelectList ShopList { get; set; } = new SelectList(new List<object>());

        public SelectList RequisitionList { get; set; } = new SelectList(new List<object>());
        public SelectList ToDepartmentList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMWareHouseConsumptionSlave> DataListSlave { get; set; }
        public List<VMWareHouseConsumptionSlave> DataToListSlave { get; set; }

        public decimal RequiredQuantity { get; set; }
     
        public string RequisitionCID { get; set; }
        public string SystemBarcode { get; set; }
        public string SubCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public string AcknowledgedBy { get; set; }
    }

    public class VMWareHouse_TransferShopToShop : BaseVM
    {
       
        public string TransferCID { get; set; }
        
        public DateTime Date { get; set; }
        public int ToCommon_ShopFK { get; set; }
        public bool Acknowledgement { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public string AcknowledgedBy { get; set; }
    }

    public class VMWareHouse_TransferShopToShopSlave : VMWareHouse_TransferShopToShop
    {
        public int WareHouse_TransferShopToShopFk { get; set; }
        public int Common_ProductFK { get; set; }

      
        public decimal TransferQuantity { get; set; } = 0;
        public decimal CostingPrice { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal StoreAvailableQuantity { get; set; }
        public string UserName { get;  set; }
        public string FromShopName { get;  set; }
        public string ToShopName { get;  set; }
        public string SystemBarcode { get; set; }
        public string BrandName { get; set; }
        public string SubCategoryName { get; set; }
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string UnitName { get; set; }

     
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }       
        public SelectList ShopList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMWareHouse_TransferShopToShopSlave> DataListSlave { get; set; }


    }

}
