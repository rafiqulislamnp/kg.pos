﻿
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KG.Core.Services;
using KG.Core.Services.Home;
using KG.Core.Entity.WareHouse;
using KG.Core.Services.WareHouse;
using KG.Core.Services.User;
using KG.Core.Entity.User;
using KG.Core.Services.Configuration;
using KG.Core.Services.Procurement;
using KG.Core.Entity.Configuration;

namespace KG.Core.Services.WareHouse
{
    public class WareHouseService : BaseService
    {
        private readonly VMLogin _vMLogin;
        private readonly HttpContext _httpContext;
        private readonly IDataProtector _protector;

        public WareHouseService(IPosDbContext db, HttpContext httpContext, IDataProtectionProvider provider)
        {
            _db = db;
            _httpContext = httpContext;
            _vMLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }
        public async Task<int> WareHousePOReceivingAdd(VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave)
        {
            var result = -1;
            #region Genarate Store-In ID
            int poReceivingCount = _db.WareHouse_POReceiving.Count();

            if (poReceivingCount == 0)
            {
                poReceivingCount = 1;
            }
            else
            {
                poReceivingCount++;
            }


            string poReceivingCID = "SI" +
                                DateTime.Now.ToString("dd") +
                                DateTime.Now.ToString("MM") +
                                DateTime.Now.ToString("yy") + poReceivingCount.ToString().PadLeft(5, '0');
            #endregion
            WareHouse_POReceiving wareHousePOReceiving = new WareHouse_POReceiving
            {
                ChallanCID = poReceivingCID,
                Procurement_PurchaseOrderFk = vmWareHousePOReceivingSlave.Procurement_PurchaseOrderFk,
                Challan = vmWareHousePOReceivingSlave.Challan,
                ChallanDate = vmWareHousePOReceivingSlave.ChallanDate,
                User_DepartmentFk = _vMLogin.UserDepartmentId,
                UserID = _vMLogin.ID,
                Common_ShopFK = _vMLogin.Common_ShopFK

            };
            _db.WareHouse_POReceiving.Add(wareHousePOReceiving);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHousePOReceiving.ID;
            }
            return result;
        }




        private decimal GetReceivingQuantity(int? commonRawItemFK)//cutting sewing
        {
            var quantity = (from t1 in _db.WareHouse_POReceivingSlave
                            join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t2.ID
                            //join t6 in _db.WareHouse_RequisitionSlave on t1.Procurement_PurchaseOrderSlaveFk equals t6.Procurement_PurchaseOrderSlaveFk
                            join t7 in _db.WareHouse_ConsumptionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t7.Procurement_PurchaseRequisitionSlaveFK
                            join t8 in _db.WareHouse_Consumption on t7.WareHouse_ConsumptionFk equals t8.ID
                            where t1.Active && t2.Active && t7.Active && t8.Active
                            && t2.Common_ProductFK == commonRawItemFK

                            select t7.ConsumeQuantity).DefaultIfEmpty(0).Sum();


            return (quantity);


        }
        private decimal GetYarnReceivingQuantity(int? commonRawItemFK)//cutting sewing
        {
            var quantity = (from t1 in _db.WareHouse_POReceivingSlave
                            join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t2.ID
                            //join t6 in _db.WareHouse_RequisitionSlave on t1.Procurement_PurchaseOrderSlaveFk equals t6.Procurement_PurchaseOrderSlaveFk
                            join t7 in _db.WareHouse_ConsumptionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t7.Procurement_PurchaseRequisitionSlaveFK
                            join t8 in _db.WareHouse_Consumption on t7.WareHouse_ConsumptionFk equals t8.ID
                            where t1.Active && t2.Active && t7.Active && t8.Active
                            && t2.Common_ProductFK == commonRawItemFK

                            select t7.ConsumeQuantity).DefaultIfEmpty(0).Sum();
            return quantity;
        }


        private decimal GetStoreOutQuantity(int? commonProductFK)
        {

            var consumeQuantity = (from t1 in _db.WareHouse_POReceivingSlave
                                   join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t2.ID
                                   //join t6 in _db.Procurement_PurchaseRequisitionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t6.ID
                                   join t7 in _db.WareHouse_ConsumptionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t7.Procurement_PurchaseRequisitionSlaveFK
                                   where t1.Active && t2.Active && t7.Active
                                   && t2.Common_ProductFK == commonProductFK
                                   select t7.ConsumeQuantity).DefaultIfEmpty(0).Sum();



            return (consumeQuantity);
        }
        private decimal GetYarnStoreOutQuantity(int? commonProductFK)
        {

            var consumeQuantity = (from t1 in _db.WareHouse_POReceivingSlave
                                   join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t2.ID
                                   //join t6 in _db.Procurement_PurchaseRequisitionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t6.ID
                                   join t7 in _db.WareHouse_ConsumptionSlave on t2.Procurement_PurchaseRequisitionSlaveFK equals t7.Procurement_PurchaseRequisitionSlaveFK
                                   where t1.Active && t2.Active && t7.Active
                                   && t2.Common_ProductFK == commonProductFK
                                   select t7.ConsumeQuantity).DefaultIfEmpty(0).Sum();

            return consumeQuantity;
        }


        public async Task<int> WareHouseConsumptionAdd(VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave)
        {

            var result = -1;
            #region Genarate Store-Out ID
            int consumptionCount = _db.WareHouse_Consumption.Count();

            if (consumptionCount == 0)
            {
                consumptionCount = 1;
            }
            else
            {
                consumptionCount++;
            }

            string consumptionCID =
                                DateTime.Now.ToString("dd") +
                                DateTime.Now.ToString("MM") +
                                DateTime.Now.ToString("yy") + consumptionCount.ToString().PadLeft(5, '0');
            #endregion
            WareHouse_Consumption wareHouseConsumption = new WareHouse_Consumption
            {
                ConsumptionCID = consumptionCID,
                Date = DateTime.Now,
                FromUser_DepartmentFk = _vMLogin.UserDepartmentId,
                ToUser_DepartmentFK = vmWareHouseConsumptionSlave.ToUser_DepartmentFK,
                Acknowledgement = vmWareHouseConsumptionSlave.Acknowledgement,
                Common_ShopFK = vmWareHouseConsumptionSlave.Common_ShopFK,
                SendByStoreRequisition = vmWareHouseConsumptionSlave.SendByStoreRequisition,
                User = _vMLogin.Name.ToString(),
                UserID = _vMLogin.ID,


            };
            _db.WareHouse_Consumption.Add(wareHouseConsumption);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseConsumption.ID;
            }
            return result;
        }

        public async Task<int> VMWareHouseTransferShopToShopAdd(VMWareHouse_TransferShopToShopSlave vmTransfer)
        {

            var result = -1;
            #region Genarate Shop To Shop transfer ID
            int transferCount = _db.WareHouse_TransferShopToShop.Count();

            if (transferCount == 0)
            {
                transferCount = 1;
            }
            else
            {
                transferCount++;
            }

            string consumptionCID =
                                DateTime.Now.ToString("dd") +
                                DateTime.Now.ToString("MM") +
                                DateTime.Now.ToString("yy") + transferCount.ToString().PadLeft(5, '0');
            #endregion
            WareHouse_TransferShopToShop wareHouseTransferShopToShop = new WareHouse_TransferShopToShop
            {
                TransferCID = consumptionCID,
                Date = DateTime.Now,
                Common_ShopFK = vmTransfer.Common_ShopFK,
                ToCommon_ShopFK = vmTransfer.ToCommon_ShopFK,

                User = _vMLogin.Name.ToString(),
                UserID = _vMLogin.ID,


            };
            _db.WareHouse_TransferShopToShop.Add(wareHouseTransferShopToShop);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseTransferShopToShop.ID;
            }
            return result;
        }

        public async Task<int> WareHousePOReturnAdd(VMWareHousePOReturnSlave vmWareHousePOReceivingSlave)
        {
            var result = -1;
            #region Genarate Store-In ID
            int poReceivingCount = _db.WareHouse_POReceiving.Count();

            if (poReceivingCount == 0)
            {
                poReceivingCount = 1;
            }
            else
            {
                poReceivingCount++;
            }

            string poReceivingCID = "SR" +
                                DateTime.Now.ToString("dd") +
                                DateTime.Now.ToString("MM") +
                                DateTime.Now.ToString("yy") + poReceivingCount.ToString().PadLeft(5, '0');
            #endregion
            string challanNo = _db.WareHouse_POReceiving.Where(x => x.ID == vmWareHousePOReceivingSlave.WareHouse_POReceivingFk).Select(x => x.Challan).FirstOrDefault();
            WareHouse_POReceiving wareHousePOReceiving = new WareHouse_POReceiving
            {
                ChallanCID = poReceivingCID,
                Procurement_PurchaseOrderFk = vmWareHousePOReceivingSlave.Procurement_PurchaseOrderFk,
                Challan = challanNo,
                ChallanDate = vmWareHousePOReceivingSlave.ChallanDate,
                User_DepartmentFk = _vMLogin.UserDepartmentId,
                Remarks = vmWareHousePOReceivingSlave.CausesOfReturn,
                UserID = _vMLogin.ID,
                Common_ShopFK = _vMLogin.Common_ShopFK

            };
            _db.WareHouse_POReceiving.Add(wareHousePOReceiving);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHousePOReceiving.ID;
            }
            return result;
        }


        public async Task<int> WareHouseProductReturnAdd(VMWareHousePOReturnSlave vmWareHousePOReceivingSlave)
        {
            var result = -1;
            #region Genarate Store-In ID
            int poReceivingCount = _db.WareHouse_POReceiving.Count();

            if (poReceivingCount == 0)
            {
                poReceivingCount = 1;
            }
            else
            {
                poReceivingCount++;
            }

            string poReceivingCID = "SupRet" +
                                DateTime.Now.ToString("dd") +
                                DateTime.Now.ToString("MM") +
                                DateTime.Now.ToString("yy") + poReceivingCount.ToString().PadLeft(5, '0');
            #endregion
            //string challanNo = _db.WareHouse_POReceiving.Where(x => x.ID == vmWareHousePOReceivingSlave.WareHouse_POReceivingFk).Select(x => x.Challan).FirstOrDefault();
            WareHouse_POReceiving wareHousePOReceiving = new WareHouse_POReceiving
            {
                ChallanCID = poReceivingCID,
                Challan = poReceivingCID,
                ChallanDate = vmWareHousePOReceivingSlave.ChallanDate,
                Remarks = vmWareHousePOReceivingSlave.CausesOfReturn,
                Common_SupplierFK = vmWareHousePOReceivingSlave.Common_SupplierFK,

                UserID = _vMLogin.ID,


                Procurement_PurchaseOrderFk = 0,
                User_DepartmentFk = 0,
                Common_ShopFK = 0

            };
            _db.WareHouse_POReceiving.Add(wareHousePOReceiving);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHousePOReceiving.ID;
            }
            return result;
        }
        public async Task<int> WareHousePOReturnEdit(VMWareHousePOReturnSlave vmWareHousePOReceivingSlave)
        {
            var result = -1;

            WareHouse_POReceiving poReceiving = _db.WareHouse_POReceiving.Find(vmWareHousePOReceivingSlave.WareHouse_POReceivingFk);

            poReceiving.Remarks = vmWareHousePOReceivingSlave.CausesOfReturn;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = poReceiving.ID;
            }
            return result;
        }



        public async Task<VMWareHousePOReceivingSlave> WareHousePurchaseOrderGet()
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();


            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Common_ShopFK == _vMLogin.Common_ShopFK)
                                                                              join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                                              join t3 in _db.Common_Shop on t1.Common_ShopFK equals t3.ID

                                                                              where t1.Active
                                                                              && t2.Active

                                                                              select new VMWareHousePOReceivingSlave
                                                                              {
                                                                                  Procurement_PurchaseOrderFk = t1.ID,
                                                                                  SupplierName = t2.Name,
                                                                                  DeliveryAddress = t1.DeliveryAddress,
                                                                                  POCID = t1.CID,
                                                                                  PODate = t1.OrderDate,
                                                                                  ShopName = t3.Name
                                                                              }).OrderByDescending(x => x.Procurement_PurchaseOrderFk).AsEnumerable());
            return vmWareHousePOReceivingSlave;
        }

        public async Task<VMUserDepartment> WareHouseStoreDepartmentGet()
        {
            VMUserDepartment vmUserDepartment = new VMUserDepartment();
            vmUserDepartment.EnumerableDataList = await Task.Run(() => (from t1 in _db.User_Department
                                                                        where t1.Active && t1.IsStore
                                                                        select new VMUserDepartment
                                                                        {
                                                                            Name = t1.Name,
                                                                            InventoryTypeEnumFk = t1.InventoryTypeEnumFk,
                                                                            ID = t1.ID,
                                                                            Time = t1.Time
                                                                        }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmUserDepartment;
        }

        public async Task<int> UserStoreDepartmentAdd(VMUserDepartment vmUserDepartment)
        {
            var result = -1;

            User_Department userDepartment = new User_Department
            {
                IsStore = true,
                InventoryTypeEnumFk = vmUserDepartment.InventoryTypeEnumFk,
                Name = vmUserDepartment.Name,
                UserID = _vMLogin.ID,
                Common_ShopFK = _vMLogin.Common_ShopFK

            };
            _db.User_Department.Add(userDepartment);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userDepartment.ID;
            }
            return result;
        }
        public async Task<int> UserStoreDepartmentEdit(VMUserDepartment vmUserDepartment)
        {
            var result = -1;

            User_Department userDepartment = _db.User_Department.Find(vmUserDepartment.ID);

            userDepartment.Name = vmUserDepartment.Name;
            userDepartment.InventoryTypeEnumFk = vmUserDepartment.InventoryTypeEnumFk;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userDepartment.ID;
            }
            return result;
        }
        public async Task<int> UserStoreDepartmentDelete(int id)
        {
            var result = -1;
            User_Department userDepartment = _db.User_Department.Find(id);
            if (userDepartment != null)
            {
                userDepartment.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userDepartment.ID;
                }
            }
            return result;
        }


        public VMWareHousePOReceivingSlavePartial GetPODetailsByID(int poId)
        {
            VMWareHousePOReceivingSlavePartial vmWareHousePOReceivingSlavePartial = new VMWareHousePOReceivingSlavePartial();

            vmWareHousePOReceivingSlavePartial = (from t1 in _db.Procurement_PurchaseOrder
                                                      //join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                  where t1.Active && t1.ID == poId

                                                  select new VMWareHousePOReceivingSlavePartial
                                                  {
                                                      Procurement_PurchaseOrderFk = t1.ID,
                                                      POCID = t1.CID,
                                                      PODate = t1.OrderDate
                                                  }).FirstOrDefault();
            return vmWareHousePOReceivingSlavePartial;
        }

        public async Task<VMWareHousePOReceivingSlave> WareHousePOSlaveReceivingDetailsByPOGet(int id)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();

            vmWareHousePOReceivingSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder
                                                                join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                                where t1.Active && t1.ID == id
                                                                && t1.Active

                                                                select new VMWareHousePOReceivingSlave
                                                                {
                                                                    Procurement_PurchaseOrderFk = t1.ID,
                                                                    SupplierName = t2.Name,
                                                                    DeliveryAddress = t1.DeliveryAddress,
                                                                    POCID = t1.CID,
                                                                    PODate = t1.OrderDate
                                                                }).OrderByDescending(x => x.ID).FirstOrDefault());

            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
                                                                              join t2 in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals t2.ID
                                                                              join t3 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t3.ID
                                                                              join t5 in _db.Common_Product on t3.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              where t1.Active && t2.Active && t3.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                              && t3.Procurement_PurchaseOrderFK == id && !t1.IsReturn

                                                                              // orderby t1.Time
                                                                              select new VMWareHousePOReceivingSlave
                                                                              {
                                                                                  ID = t1.ID,
                                                                                  Common_ProductFk = t1.Common_ProductFK,
                                                                                  ReceivedQuantity = t1.ReceivedQuantity,
                                                                                  PriviousReceivedQuantity = t1.ReceivedQuantity + (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn && x.ID < t1.ID).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  POQuantity = t3.PurchaseQuantity,
                                                                                  ReturnQuantity = (from x in _db.WareHouse_POReceivingSlave
                                                                                                    join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                                    where
                                                                                                     x.Procurement_PurchaseOrderSlaveFk == t3.ID &&
                                                                                                     //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                                     x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                                    select x.ReceivedQuantity).DefaultIfEmpty(0).Sum(),
                                                                                  RemainingQuantity = ((t3.PurchaseQuantity - ((_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn && x.Time < t1.Time).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()) + t1.ReceivedQuantity))
                                                                                  + (from x in _db.WareHouse_POReceivingSlave
                                                                                     join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                     where
                                                                                      x.Procurement_PurchaseOrderSlaveFk == t3.ID &&
                                                                                      //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                      x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                     select x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),

                                                                                  StockLossQuantity = t1.StockLossQuantity,
                                                                                  ProductName = t5.Name,
                                                                                  SystemBarcode = t5.SystemBarcode,
                                                                                  ProductSubCategory = t6.Name,
                                                                                  ProductCategory = t7.Name,
                                                                                  Brand = t9.Name,
                                                                                  ChallanCID = t2.ChallanCID,
                                                                                  Challan = t2.Challan,
                                                                                  ChallanDate = t2.ChallanDate,
                                                                                  WareHouse_POReceivingFk = t2.ID

                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());

            return vmWareHousePOReceivingSlave;
        }

        public async Task<VMWareHousePOReceivingSlave> WareHousePOSlaveReturnDetailsByPOGet(int id)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();

            vmWareHousePOReceivingSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder
                                                                join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                                where t1.Active && t1.ID == id
                                                                && t1.Active

                                                                select new VMWareHousePOReceivingSlave
                                                                {
                                                                    Procurement_PurchaseOrderFk = t1.ID,
                                                                    SupplierName = t2.Name,
                                                                    DeliveryAddress = t1.DeliveryAddress,
                                                                    POCID = t1.CID,
                                                                    PODate = t1.OrderDate
                                                                }).OrderByDescending(x => x.ID).FirstOrDefault());

            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
                                                                              join t2 in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals t2.ID
                                                                              join t3 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t3.ID
                                                                              join t5 in _db.Common_Product on t3.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              where t1.Active && t2.Active && t3.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                              && t3.Procurement_PurchaseOrderFK == id && t1.IsReturn

                                                                              orderby t1.Time
                                                                              select new VMWareHousePOReceivingSlave
                                                                              {
                                                                                  ID = t1.ID,
                                                                                  ReceivedQuantity = t1.ReceivedQuantity,
                                                                                  PriviousReceivedQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn && x.ID < t1.ID).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  POQuantity = t3.PurchaseQuantity,
                                                                                  ReturnQuantity = (from x in _db.WareHouse_POReceivingSlave
                                                                                                    join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                                    where
                                                                                                     x.Procurement_PurchaseOrderSlaveFk == t3.ID &&
                                                                                                     x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                                     x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                                    select x.ReceivedQuantity).DefaultIfEmpty(0).Sum(),
                                                                                  RemainingQuantity = ((t3.PurchaseQuantity - ((_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn && x.Time < t1.Time).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum())))
                                                                                  + (t1.ReceivedQuantity + (from x in _db.WareHouse_POReceivingSlave
                                                                                                            join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                                            where
                                                                                                             x.Procurement_PurchaseOrderSlaveFk == t3.ID &&
                                                                                                             x.ID < t1.ID &&
                                                                                                             //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                                             x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                                            select x.ReceivedQuantity).DefaultIfEmpty(0).Sum())),

                                                                                  StockLossQuantity = t1.StockLossQuantity,
                                                                                  ProductName = t5.Name,
                                                                                  SystemBarcode = t5.SystemBarcode,
                                                                                  ProductSubCategory = t6.Name,
                                                                                  ProductCategory = t7.Name,
                                                                                  Brand = t9.Name,
                                                                                  ChallanCID = t2.ChallanCID,
                                                                                  Challan = t2.Challan,
                                                                                  ChallanDate = t2.ChallanDate,
                                                                                  WareHouse_POReceivingFk = t2.ID,
                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());



            return vmWareHousePOReceivingSlave;
        }



        public async Task<int> WareHousePOReceivingSlaveAdd(VMWareHousePOReceivingSlave vmModel, VMWareHousePOReceivingSlavePartial vmModelList)
        {
            var result = -1;
            var dataListSlavePartial = vmModelList.DataListSlavePartial.Where(x => x.ReceivedQuantity > 0 || x.StockLossQuantity > 0).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    WareHouse_POReceivingSlave wareHousePOReceivingSlave = new WareHouse_POReceivingSlave
                    {
                        Procurement_PurchaseOrderSlaveFk = dataListSlavePartial[i].Procurement_PurchaseOrderSlaveFk,
                        ReceivedQuantity = dataListSlavePartial[i].ReceivedQuantity,
                        StockLossQuantity = dataListSlavePartial[i].StockLossQuantity,
                        PurchasingPrice = dataListSlavePartial[i].PurchasingPrice,
                        TotalOverhead = dataListSlavePartial[i].TotalOverhead,

                        Common_ProductFK = dataListSlavePartial[i].Common_ProductFK,
                        //Common_BrandFk = dataListSlavePartial[i].Common_BrandFk != null ? dataListSlavePartial[i].Common_BrandFk.Value :0,
                        //Common_ProductCategoryFk = dataListSlavePartial[i].Common_ProductCategoryFk,
                        //Common_ProductSubCategoryFk = dataListSlavePartial[i].Common_ProductSubCategoryFk,
                        //Common_SupplierFk = dataListSlavePartial[i].Common_SupplierFk != null? dataListSlavePartial[i].Common_SupplierFk.Value:0,


                        WareHouse_POReceivingFk = vmModel.WareHouse_POReceivingFk,
                        Damage = dataListSlavePartial[i].Damage,
                        WareHouse_BinSlaveFk = dataListSlavePartial[i].WareHouse_BinSlaveFk,
                        User = _vMLogin.ID.ToString(),
                        UserID = _vMLogin.ID,
                        Common_ShopFK = _vMLogin.Common_ShopFK

                    };
                    _db.WareHouse_POReceivingSlave.Add(wareHousePOReceivingSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        vmModel.ID = wareHousePOReceivingSlave.ID;
                    }
                    if (dataListSlavePartial[i].PurchasingPrice > 0)
                    {
                        await ProductGRNEdit(wareHousePOReceivingSlave);
                    }
                }
            }

            return result;
        }

        public async Task<int> ProductGRNEdit(WareHouse_POReceivingSlave vmPoReceivingSlave)
        {
            var result = -1;


            var priviousStockQty = _db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == vmPoReceivingSlave.Common_ProductFK && x.ID != vmPoReceivingSlave.ID).Select(x => x.ReceivedQuantity - x.Damage).DefaultIfEmpty(0).Sum()
                                    - _db.Marketing_SalesSlave.Where(x => x.Common_ProductFK == vmPoReceivingSlave.Common_ProductFK).Select(x => x.Quantity).DefaultIfEmpty(0).Sum(); // need diduct sales quantity

            Common_Product commonProduct = _db.Common_Product.Find(vmPoReceivingSlave.Common_ProductFK);

            var priviousStockPrice = (priviousStockQty * commonProduct.CostingPrice);
            var priviousPurchasingPrice = (priviousStockQty * commonProduct.PurchasePrice);


            commonProduct.CostingPrice = ((((vmPoReceivingSlave.ReceivedQuantity * vmPoReceivingSlave.PurchasingPrice) + (priviousStockPrice > 0 ? priviousStockPrice : 0)) +
                                            vmPoReceivingSlave.TotalOverhead) /
                                             ((vmPoReceivingSlave.ReceivedQuantity - vmPoReceivingSlave.Damage) + (priviousStockQty > 0 ? priviousStockQty : 0)));


            commonProduct.PurchasePrice = (((vmPoReceivingSlave.ReceivedQuantity * vmPoReceivingSlave.PurchasingPrice) + (priviousPurchasingPrice > 0 ? priviousPurchasingPrice : 0)) /
                                             ((vmPoReceivingSlave.ReceivedQuantity - vmPoReceivingSlave.Damage) + (priviousStockQty > 0 ? priviousStockQty : 0)));
            commonProduct.Time = DateTime.Now;
            commonProduct.User = _vMLogin.Name;
            commonProduct.UserID = _vMLogin.ID;


            Common_ProductCosting common_ProductCosting = new Common_ProductCosting
            {
                Common_ProductFK = vmPoReceivingSlave.Common_ProductFK,
                LaborCost = 0,
                ManufacturingOverhead = 0,
                TransportationOverhead = 0,
                OthersCost = vmPoReceivingSlave.TotalOverhead,
                OthersCostNote = "",
                WareHouse_POReceivingSlaveFk = vmPoReceivingSlave.ID,
                CostingPrice = commonProduct.CostingPrice,
                PurchasePrice = commonProduct.PurchasePrice,

            };
            _db.Common_ProductCosting.Add(common_ProductCosting);

            WareHouse_POReceivingSlave poReceivingSlave = _db.WareHouse_POReceivingSlave.Find(vmPoReceivingSlave.ID);
            poReceivingSlave.IsGRNCompleted = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }

            return result;
        }

        public async Task<int> WareHouseConsumptionSlaveAdd(VMWareHouseConsumptionSlave vmModel, VMWareHouseRequisitionSlavePartial vmModelList)
        {
            var result = -1;
            var dataListSlavePartial = vmModelList.DataListSlavePartial.Where(x => x.ConsumeQuantity > 0).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    WareHouse_ConsumptionSlave wareHouseConsumptionSlave = new WareHouse_ConsumptionSlave
                    {
                        WareHouse_ConsumptionFk = vmModel.WareHouseConsumptionFk,
                        Common_ProductFK = dataListSlavePartial[i].Common_ProductFK,

                        ConsumeQuantity = dataListSlavePartial[i].ConsumeQuantity,

                        Common_BrandFk = dataListSlavePartial[i].Common_BrandFk,
                        Common_ProductCategoryFk = dataListSlavePartial[i].Common_ProductCategoryFk,
                        Common_ProductSubCategoryFk = dataListSlavePartial[i].Common_ProductSubCategoryFk,
                        Common_SupplierFk = dataListSlavePartial[i].Common_SupplierFk,
                        CostingPrice = dataListSlavePartial[i].CostingPrice,
                        MRPPrice = dataListSlavePartial[i].MRPPrice,
                        Procurement_PurchaseRequisitionSlaveFK = dataListSlavePartial[i].Procurement_PurchaseRequisitionSlaveFK,


                        User = _vMLogin.ID.ToString(),
                        UserID = _vMLogin.ID,
                        Common_ShopFK = vmModel.Common_ShopFK // shop from requisition

                    };
                    _db.WareHouse_ConsumptionSlave.Add(wareHouseConsumptionSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = wareHouseConsumptionSlave.ID;
                    }
                }
            }
            return result;
        }

        public async Task<int> WareHouseGeneralConsumptionSlaveAdd(VMWareHouseConsumptionSlave vmModel)
        {
            var result = -1;
            WareHouse_ConsumptionSlave wareHouseConsumptionSlave = new WareHouse_ConsumptionSlave
            {
                ConsumeQuantity = vmModel.ConsumeQuantity,
                WareHouse_ConsumptionFk = vmModel.WareHouseConsumptionFk,
                Common_BrandFk = vmModel.Common_BrandFk,
                Common_ProductCategoryFk = vmModel.Common_ProductCategoryFk,
                Common_SupplierFk = vmModel.Common_SupplierFk,
                Common_ProductSubCategoryFk = vmModel.Common_ProductSubCategoryFk,
                CostingPrice = vmModel.CostingPrice,
                MRPPrice = vmModel.MRPPrice,
                Common_ProductFK = vmModel.Common_ProductFK,
                Procurement_PurchaseRequisitionSlaveFK = 0,


                User = _vMLogin.ID.ToString(),
                UserID = _vMLogin.ID,
                Common_ShopFK = vmModel.Common_ShopFK // shop from requisition

            };
            _db.WareHouse_ConsumptionSlave.Add(wareHouseConsumptionSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseConsumptionSlave.ID;
            }
            return result;
        }

        public async Task<int> WareHouseTransferShopToShopSlaveAdd(VMWareHouse_TransferShopToShopSlave vmModel)
        {
            var result = -1;
            WareHouse_TransferShopToShopSlave transferShopToShopSlave = new WareHouse_TransferShopToShopSlave
            {
                Common_BrandFk = vmModel.Common_BrandFk,
                Common_ProductCategoryFk = vmModel.Common_ProductCategoryFk,
                Common_ProductSubCategoryFk = vmModel.Common_ProductSubCategoryFk,
                Common_SupplierFk = vmModel.Common_SupplierFk,
                Common_ProductFK = vmModel.Common_ProductFK,
                WareHouse_TransferShopToShopFk = vmModel.WareHouse_TransferShopToShopFk,
                CostingPrice = vmModel.CostingPrice,
                MRPPrice = vmModel.MRPPrice,
                TransferQuantity = vmModel.TransferQuantity,
                User = _vMLogin.ID.ToString(),
                UserID = _vMLogin.ID,
                Common_ShopFK = vmModel.ToCommon_ShopFK
            };

            _db.WareHouse_TransferShopToShopSlave.AddRange(transferShopToShopSlave);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = transferShopToShopSlave.ID;
            }


            return result;
        }
        public async Task<int> WareHouseProductReturnSlaveAdd(VMWareHousePOReturnSlave vmModel, VMWareHousePOReturnSlavePartial vmModelList)
        {
            var result = -1;
            var dataListSlavePartial = vmModelList.DataListSlavePartial.Where(x => x.ReturnQuantity > 0).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    WareHouse_POReceivingSlave wareHousePOReceivingSlave = new WareHouse_POReceivingSlave
                    {
                        IsReturn = true,
                        Procurement_PurchaseOrderSlaveFk = 0,
                        ReceivedQuantity = dataListSlavePartial[i].ReturnQuantity,
                        WareHouse_POReceivingFk = vmModel.WareHouse_POReceivingFk,
                        Common_ProductFK = dataListSlavePartial[i].Common_ProductFK,

                        User = _vMLogin.ID.ToString(),
                        UserID = _vMLogin.ID,
                        Common_ShopFK = 0

                    };
                    _db.WareHouse_POReceivingSlave.Add(wareHousePOReceivingSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = wareHousePOReceivingSlave.ID;
                    }
                }
            }

            return result;
        }
        public async Task<int> WareHouseProductReturnToSupplierAdd(VMWareHousePOReturnSlave vmModel)
        {
            var result = -1;
            WareHouse_POReceivingSlave wareHousePOReceivingSlave = new WareHouse_POReceivingSlave
            {
                IsReturn = true,
                Procurement_PurchaseOrderSlaveFk = 0,
                ReceivedQuantity = vmModel.ReturnQuantity,
                WareHouse_POReceivingFk = vmModel.WareHouse_POReceivingFk,
                Common_ProductFK = vmModel.Common_ProductFk,

                User = _vMLogin.ID.ToString(),
                UserID = _vMLogin.ID,
                Common_ShopFK = 0

            };
            _db.WareHouse_POReceivingSlave.Add(wareHousePOReceivingSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHousePOReceivingSlave.ID;
            }

            return result;
        }

        public async Task<int> WareHousePOSalseReturnSlaveAdd(VMWareHousePOReturnSlave vmModel, VMWareHousePOReturnSlavePartial vmModelList)
        {
            var result = -1;
            var dataListSlavePartial = vmModelList.DataListSlavePartial.Where(x => x.ReturnQuantity > 0).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    WareHouse_POReceivingSlave wareHousePOReceivingSlave = new WareHouse_POReceivingSlave
                    {
                        IsReturn = true,
                        Procurement_PurchaseOrderSlaveFk = dataListSlavePartial[i].Procurement_PurchaseOrderSlaveFk,
                        ReceivedQuantity = dataListSlavePartial[i].ReturnQuantity,

                        WareHouse_POReceivingFk = vmModel.WareHouse_POReceivingFk,
                        User = _vMLogin.ID.ToString(),
                        UserID = _vMLogin.ID,
                        Common_ShopFK = _vMLogin.Common_ShopFK

                    };
                    _db.WareHouse_POReceivingSlave.Add(wareHousePOReceivingSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = wareHousePOReceivingSlave.ID;
                    }
                }
            }

            return result;
        }
        public List<object> GetExistingChallanListByPOData(int id)
        {
            var poReceivingList = new List<object>();
            foreach (var poReceiving in _db.WareHouse_POReceiving.Where(x => x.Active && x.Procurement_PurchaseOrderFk == id).ToList())
            {
                poReceivingList.Add(new
                {
                    Text = poReceiving.ChallanCID + " Supplier Challan: " + poReceiving.Challan + " Date: " + poReceiving.ChallanDate.ToLongDateString(),
                    Value = poReceiving.ID
                });
            }
            return poReceivingList;
        }
        public List<object> UserStoreDepartment()
        {
            var poReceivingList = new List<object>();
            foreach (var x in _db.User_Department.Where(x => x.Active && x.IsStore).ToList())
            {
                poReceivingList.Add(new
                {
                    Text = x.Name,
                    Value = x.ID
                });
            }
            return poReceivingList;
        }

        //public List<VMWareHousePOReceiving> GetExistingChallanListByPOData(int id)
        //{
        //    var v = (from t1 in _db.WareHouse_POReceiving
        //             where t1.Procurement_PurchaseOrderFk == id && t1.Active
        //             select new VMWareHousePOReceiving()
        //             {
        //                 ChallanCID = t1.ChallanCID + " Supplier Challan: " + t1.Challan + " Date: "+ t1.ChallanDate,
        //                 Challan = t1.Challan,
        //             }).OrderByDescending(a => a.ID).ToList();
        //    return v;
        //}

        public async Task<VMWareHousePOReceivingSlave> GetProcurementPurchaseOrder(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder
                                          join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                          where t1.ID == id
                                          select new VMWareHousePOReceivingSlave
                                          {
                                              SupplierName = t2.Name,
                                              Common_SupplierFK = t1.Common_SupplierFK,
                                              PODate = t1.OrderDate,
                                              DeliveryAddress = t1.DeliveryAddress,
                                              DeliveryDate = t1.DeliveryDate,
                                              Procurement_PurchaseOrderFk = t1.ID
                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMCommonUnit> GetCommonUnitByItem(int id)
        {
            var v = await Task.Run(() => (from t2 in _db.Common_Product.Where(x => x.ID == id)
                                          join t1 in _db.Common_Unit on t2.Common_UnitFk equals t1.ID
                                          select new VMCommonUnit
                                          {
                                              Name = t1.Name,
                                              ID = t1.ID
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VMPurchaseRequisition> ProcurementPurchaseRequisitionGet(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.ID == id)
                                          join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                          join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                          join t4 in _db.Common_Shop on t1.Common_ShopFK equals t4.ID

                                          select new VMPurchaseRequisition
                                          {
                                              FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                              ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                              FromUserDepartmentName = t2.Name + " (" + t4.Name + ")",
                                              ToUserDepartmentName = t3.Name + " (" + t4.Name + ")",
                                              Common_ShopFK = t1.Common_ShopFK
                                          }).FirstOrDefault());
            return v;
        }


        public List<VMWareHousePOReturnSlavePartial> GetPOSalesReturnData(int poReceivingId)
        {

            var list = (from t1 in _db.WareHouse_POReceivingSlave
                        join tr in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals tr.ID
                        join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t2.ID
                        join t3 in _db.Procurement_PurchaseOrder on t2.Procurement_PurchaseOrderFK equals t3.ID
                        join t5 in _db.Common_Product on t2.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                             t1.WareHouse_POReceivingFk == poReceivingId && !t1.IsReturn


                        select new VMWareHousePOReturnSlavePartial
                        {
                            ChallanCID = tr.ChallanCID + " " + tr.Challan,
                            POCID = t3.CID,
                            ProductName = t5.Name,
                            ProductDescription = t2.Description,
                            POQuantity = t2.PurchaseQuantity,
                            Procurement_PurchaseOrderSlaveFk = t1.Procurement_PurchaseOrderSlaveFk,
                            PriviousReceivedQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.Procurement_PurchaseOrderSlaveFk && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                            ReceivedChallanQuantity = t1.ReceivedQuantity,
                            PriviousReturnQuantity = (from x in _db.WareHouse_POReceivingSlave
                                                      join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                      where
                                                       x.Procurement_PurchaseOrderSlaveFk == t2.ID &&
                                                       //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                       x.Active && x.IsReturn && y.Challan == tr.Challan
                                                      select x.ReceivedQuantity).DefaultIfEmpty(0).Sum(),
                        }).ToList();



            return list;
        }


        public List<VMWareHousePOReturnSlavePartial> GetProductReturnBySupplierData(int supplierId)
        {

            var list = (from t1 in _db.WareHouse_ConsumptionSlave
                        join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID
                        join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                             t1.Common_SupplierFk == supplierId

                        group new { t1, t2, t5, t6, t7, t8 } by new { t1.Common_ProductFK, t5.Name } into Group
                        select new VMWareHousePOReturnSlavePartial
                        {
                            ProductName = Group.First().t7.Name + " " + Group.First().t6.Name + " " + Group.Key.Name,
                            Common_ProductFK = Group.Key.Common_ProductFK,
                            UnitName = Group.First().t8.Name,

                            PriviousReceivedQuantity = Group.Sum(x => x.t1.ConsumeQuantity),
                            SalesQuantity = (from st1 in _db.Marketing_SalesSlave
                                             join st2 in _db.Marketing_Sales on st1.Marketing_SalesFk equals st2.ID
                                             where st1.Common_ProductFK == Group.Key.Common_ProductFK
                                             && st2.Status > 0 //Reduse Sales and Damage
                                             select st1.Quantity).DefaultIfEmpty(0).Sum(),

                            PriviousReturnQuantity = (from x in _db.WareHouse_POReceivingSlave
                                                      join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                      where x.Common_ProductFK == Group.Key.Common_ProductFK &&
                                                       x.Active && x.IsReturn
                                                      select x.ReceivedQuantity).DefaultIfEmpty(0).Sum(),
                        }).ToList();



            return list;
        }

        public List<VMWareHousePOReceivingSlavePartial> GetProcurementPurchaseOrderSlaveData(int poId)
        {

            var list = (from t1 in _db.Procurement_PurchaseOrderSlave
                        join t2 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFK equals t2.ID
                        join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        where t1.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                                 t1.Procurement_PurchaseOrderFK == poId


                        select new VMWareHousePOReceivingSlavePartial
                        {
                            ProductName = t5.Name,
                            ProductDescription = t1.Description,
                            Common_ProductFK = t1.Common_ProductFK,
                            POQuantity = t1.PurchaseQuantity,
                            Procurement_PurchaseOrderSlaveFk = t1.ID,
                            PurchasingPrice = t1.PurchasingPrice,
                            Common_BrandFk = t5.Common_BrandFk,
                            Common_ProductCategoryFk = t6.Common_ProductCategoryFk,
                            Common_ProductSubCategoryFk = t5.Common_ProductSubCategoryFk,
                            Common_SupplierFk = t5.Common_SupplierFk,
                            ReturnQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                            RemainingQuantity = ((t1.PurchaseQuantity + (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()))
                                                                                  -
                                                                                  (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum())),




                            PriviousReceivedQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                            PriviousReturnQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                            UnitName = t8.Name,

                        }).ToList();



            return list;
        }

        public List<VMWareHouseRequisitionPOSlavePartial> GetProcurementPurchaseOrderSlaveDataByStyle(int styleId)
        {

            var list = (from t1 in _db.Procurement_PurchaseOrderSlave
                        join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        where t1.Active && t5.Active && t6.Active && t7.Active && t8.Active

                        select new VMWareHouseRequisitionPOSlavePartial
                        {
                            ProductName = t5.Name,
                            ProductDescription = t1.Description,
                            POQuantity = t1.PurchaseQuantity,
                            Procurement_PurchaseOrderSlaveFk = t1.ID,
                            CategoryID = t6.Common_ProductCategoryFk,
                            // Store out minus korte hobe.
                            StoreAvailableQuantity = ((_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()) -
                                                       (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t1.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum())) -
                                                       ((from x in _db.WareHouse_ConsumptionSlave
                                                         where x.Procurement_PurchaseRequisitionSlaveFK == t1.Procurement_PurchaseRequisitionSlaveFK && x.Active
                                                         select x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),
                            PreviousConsume = ((from x in _db.WareHouse_ConsumptionSlave
                                                where x.Procurement_PurchaseRequisitionSlaveFK == t1.Procurement_PurchaseRequisitionSlaveFK && x.Active
                                                select x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),
                            UnitName = t7.Name
                        }).ToList();




            return list;
        }

        public async Task<VMWareHousePOReceivingSlave> WareHousePOReceivingSlaveGet(int id)
        {
            VMWareHousePOReceivingSlave vmWareHousePOReceivingSlave = new VMWareHousePOReceivingSlave();
            vmWareHousePOReceivingSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceiving
                                                                join t2 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFk equals t2.ID
                                                                join t3 in _db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                                                                where t1.Active && t1.ID == id
                                                                select new VMWareHousePOReceivingSlave
                                                                {
                                                                    ChallanCID = t1.ChallanCID,
                                                                    Challan = t1.Challan,
                                                                    ChallanDate = t1.ChallanDate,
                                                                    POCID = t2.CID,
                                                                    PODate = t2.OrderDate,
                                                                    SupplierName = t3.Name,
                                                                    WareHouse_POReceivingFk = t1.ID,
                                                                    DeliveryAddress = t2.DeliveryAddress,
                                                                    Procurement_PurchaseOrderFk = t1.Procurement_PurchaseOrderFk,
                                                                    ProcurementPurchaseOrderList = new SelectList(PODropDownList(), "Value", "Text"),

                                                                }).FirstOrDefault());


            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
                                                                              join t2 in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals t2.ID
                                                                              join t3 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t3.ID
                                                                              join t5 in _db.Common_Product on t3.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              where t1.Active && t2.Active && t3.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                              && t1.WareHouse_POReceivingFk == id && !t1.IsReturn

                                                                              select new VMWareHousePOReceivingSlave
                                                                              {
                                                                                  ID = t1.ID,
                                                                                  ReceivedQuantity = t1.ReceivedQuantity,
                                                                                  PriviousReceivedQuantity = t1.ReceivedQuantity + (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn && x.ID < t1.ID).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  RemainingQuantity = ((t3.PurchaseQuantity -
                                                                                  (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()))
                                                                                  + (from x in _db.WareHouse_POReceivingSlave
                                                                                     join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                     where
                                                                                      x.Procurement_PurchaseOrderSlaveFk == t3.ID &&
                                                                                      //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                      x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                     select x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  POQuantity = t3.PurchaseQuantity,
                                                                                  ReturnQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t3.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  StockLossQuantity = t1.StockLossQuantity,
                                                                                  ProductName = t5.Name,
                                                                                  SystemBarcode = t5.SystemBarcode,
                                                                                  ProductSubCategory = t6.Name,
                                                                                  ProductCategory = t7.Name,
                                                                                  Brand = t9.Name,
                                                                                  Common_ProductFk = t5.ID,
                                                                                  IsGRNCompleted = t1.IsGRNCompleted,

                                                                                  PurchasingPrice = t1.PurchasingPrice,
                                                                                  CostingPrice = t5.CostingPrice,
                                                                                  MRPPrice = t5.MRPPrice,
                                                                                  TransportationOverhead = t1.TotalOverhead,
                                                                                  TotalCostingPrice = (t1.ReceivedQuantity * t5.CostingPrice),
                                                                                  TotalMRPPrice = (t1.ReceivedQuantity * t5.MRPPrice),
                                                                                  GPParcentage = t5.MRPPrice > 0 ? ((((t5.MRPPrice - t5.CostingPrice) / t5.MRPPrice) * 100)) : 0
                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());



            return vmWareHousePOReceivingSlave;
        }

        public async Task<VMWareHousePOReturnSlave> WareHousePOSalseReturnSlaveGet(int id)
        {
            VMWareHousePOReturnSlave vmWareHousePOReceivingSlave = new VMWareHousePOReturnSlave();
            vmWareHousePOReceivingSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceiving
                                                                join t2 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFk equals t2.ID
                                                                join t3 in _db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                                                                where t1.Active && t1.ID == id
                                                                select new VMWareHousePOReturnSlave
                                                                {
                                                                    ChallanCID = t1.ChallanCID,
                                                                    Challan = t1.Challan,
                                                                    ChallanDate = t1.ChallanDate,
                                                                    POCID = t2.CID,
                                                                    PODate = t2.OrderDate,
                                                                    SupplierName = t3.Name,
                                                                    WareHouse_POReceivingFk = t1.ID,
                                                                    DeliveryAddress = t2.DeliveryAddress,
                                                                    CausesOfReturn = t1.Remarks,
                                                                    Procurement_PurchaseOrderFk = t1.Procurement_PurchaseOrderFk,
                                                                    ProcurementPurchaseOrderList = new SelectList(PODropDownList(), "Value", "Text")
                                                                }).FirstOrDefault());



            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
                                                                              join t2 in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals t2.ID
                                                                              join t4 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFk equals t4.ID
                                                                              join t5 in _db.Common_Product on t4.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              join t10 in _db.Procurement_PurchaseOrder on t4.Procurement_PurchaseOrderFK equals t10.ID
                                                                              where t1.Active && t2.Active && t4.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                              && t1.WareHouse_POReceivingFk == id && t1.IsReturn
                                                                              select new VMWareHousePOReturnSlave
                                                                              {
                                                                                  POCID = t10.CID,
                                                                                  ID = t1.ID,
                                                                                  ReturnQuantity = t1.ReceivedQuantity,
                                                                                  PriviousReceivedQuantity = (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t4.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),
                                                                                  RemainingQuantity = ((t4.PurchaseQuantity -
                                                                                  (_db.WareHouse_POReceivingSlave.Where(x => x.Procurement_PurchaseOrderSlaveFk == t4.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()))
                                                                                  + (from x in _db.WareHouse_POReceivingSlave
                                                                                     join y in _db.WareHouse_POReceiving on x.WareHouse_POReceivingFk equals y.ID
                                                                                     where
                                                                                      x.Procurement_PurchaseOrderSlaveFk == t4.ID &&
                                                                                      //x.WareHouse_POReceivingFk == t1.WareHouse_POReceivingFk &&
                                                                                      x.Active && x.IsReturn && y.Challan == t2.Challan
                                                                                     select x.ReceivedQuantity).DefaultIfEmpty(0).Sum()),

                                                                                  POQuantity = t4.PurchaseQuantity,
                                                                                  StockLossQuantity = t1.StockLossQuantity,
                                                                                  ProductName = t5.Name,
                                                                                  ProductDescription = t4.Description,
                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());

            return vmWareHousePOReceivingSlave;
        }


        public async Task<VMWareHousePOReturnSlave> WareHouseProductReturnSlaveGet(int id)
        {
            VMWareHousePOReturnSlave vmWareHousePOReceivingSlave = new VMWareHousePOReturnSlave();
            vmWareHousePOReceivingSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceiving
                                                                join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                                where t1.Active && t1.ID == id
                                                                select new VMWareHousePOReturnSlave
                                                                {
                                                                    Challan = t1.Challan,
                                                                    ChallanCID = t1.ChallanCID,
                                                                    ChallanDate = t1.ChallanDate,
                                                                    SupplierName = t2.Name,
                                                                    WareHouse_POReceivingFk = t1.ID,
                                                                    CausesOfReturn = t1.Remarks,
                                                                    Common_SupplierFK = t1.Common_SupplierFK,
                                                                }).FirstOrDefault());



            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
                                                                              join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              where t1.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                              && t1.WareHouse_POReceivingFk == id && t1.IsReturn
                                                                              select new VMWareHousePOReturnSlave
                                                                              {
                                                                                  ID = t1.ID,
                                                                                  SystemBarcode = t5.SystemBarcode,
                                                                                  ReturnQuantity = t1.ReceivedQuantity,
                                                                                  ProductName = t5.Name,
                                                                                  SubCategoryName = t6.Name,
                                                                                  CategoryName = t7.Name,
                                                                                  UnitName = t8.Name,
                                                                                  BrandName = t9.Name,
                                                                                  CostingPrice = t5.CostingPrice,
                                                                                  MRPPrice = t5.MRPPrice,
                                                                                 TotalCostingPrice = t5.CostingPrice * t1.ReceivedQuantity,
                                                                                 TotalMRPPrice = t5.MRPPrice * t1.ReceivedQuantity


                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());

            return vmWareHousePOReceivingSlave;
        }

        public async Task<VMWareHousePOReturnSlave> WareHouseProductReturnGet()
        {
            VMWareHousePOReturnSlave vmWareHousePOReceivingSlave = new VMWareHousePOReturnSlave();
            vmWareHousePOReceivingSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_POReceiving
                                                                join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                                                where t1.Active
                                                                select new VMWareHousePOReturnSlave
                                                                {
                                                                    ID = t1.ID,
                                                                    Challan = t1.Challan,
                                                                    ChallanCID = t1.ChallanCID,
                                                                    ChallanDate = t1.ChallanDate,
                                                                    SupplierName = t2.Name,
                                                                    WareHouse_POReceivingFk = t1.ID,
                                                                    CausesOfReturn = t1.Remarks,
                                                                    Common_SupplierFK = t1.Common_SupplierFK,
                                                                }).OrderByDescending(x => x.ID).AsEnumerable());



            return vmWareHousePOReceivingSlave;
        }


        public object GetAutoCompletePR(string prefix)
        {
            var v = (from t1 in _db.Procurement_PurchaseRequisition
                     where t1.Active && ((t1.RequisitionCID.StartsWith(prefix)) || (t1.RequisitionDate.ToString().StartsWith(prefix)))

                     select new
                     {
                         label = t1.RequisitionCID != null ? t1.RequisitionCID + " Date" + t1.RequisitionDate.ToShortDateString() : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(20).ToList();

            return v;
        }

        public async Task<VMWareHouseConsumptionSlave> VMWareHouseConsumptionSlaveGet(int id)
        {
            VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave = new VMWareHouseConsumptionSlave();
            vmWareHouseConsumptionSlave = await Task.Run(() => (from t1 in _db.WareHouse_Consumption
                                                                join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                join t4 in _db.User_User on t1.UserID equals t4.ID
                                                                join t5 in _db.Common_Shop on t4.Common_ShopFK equals t5.ID
                                                                join t6 in _db.Common_Shop on t1.Common_ShopFK equals t6.ID



                                                                where t1.Active && t1.ID == id
                                                                select new VMWareHouseConsumptionSlave
                                                                {
                                                                    Common_ShopFK = t1.Common_ShopFK,

                                                                    UserName = t4.Name,
                                                                    AcknowledgedBy = t1.AcknowledgedBy,
                                                                    Date = t1.Date,
                                                                    ConsumptionCID = t1.ConsumptionCID,
                                                                    FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                    ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                                                    Acknowledgement = t1.Acknowledgement,
                                                                    FromUserDepartmentName = t2.Name + " (" + t5.Name + ")",
                                                                    ToUserDepartmentName = t3.Name + " (" + t6.Name + ")",
                                                                    WareHouseConsumptionFk = t1.ID,
                                                                    SendByStoreRequisition = t1.SendByStoreRequisition,
                                                                    RequisitionList = new SelectList(PurchaseRequisitionGetDropDown(), "Value", "Text")
                                                                }).FirstOrDefault());


            vmWareHouseConsumptionSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave
                                                                              join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID
                                                                              join t3 in _db.Procurement_PurchaseRequisitionSlave on t1.Procurement_PurchaseRequisitionSlaveFK equals t3.ID into t3_Join
                                                                              from t3 in t3_Join.DefaultIfEmpty()
                                                                              join wr in _db.Procurement_PurchaseRequisition on t3.Procurement_PurchaseRequisitionFK equals wr.ID into wr_Join
                                                                              from wr in wr_Join.DefaultIfEmpty()
                                                                              join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                              join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                              join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                              join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                              join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                              where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                               && t1.WareHouse_ConsumptionFk == id

                                                                              select new VMWareHouseConsumptionSlave
                                                                              {

                                                                                  RequisitionCID = wr != null ? wr.RequisitionCID : "",
                                                                                  ID = t1.ID,
                                                                                  WareHouseConsumptionFk = t1.WareHouse_ConsumptionFk,
                                                                                  ConsumeQuantity = t1.ConsumeQuantity,
                                                                                  SystemBarcode = t5.SystemBarcode,
                                                                                  BrandName = t9.Name,
                                                                                  SubCategoryName = t6.Name,
                                                                                  CategoryName = t7.Name,
                                                                                  ProductName = t5.Name,
                                                                                  UnitName = t8.Name,
                                                                                  RequiredQuantity = t3 != null ? t3.RequisitionQuantity : 0m,
                                                                                  PreviousSendQuantity = _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == t1.Common_ProductFK).Select(x => x.ConsumeQuantity).DefaultIfEmpty(0).Sum(),

                                                                              }).OrderByDescending(x => x.ID).AsEnumerable());


            return vmWareHouseConsumptionSlave;
        }

        public async Task<VMWareHouse_TransferShopToShopSlave> VMWareHouseTransferShopToShopSlaveGet(int id)
        {
            VMWareHouse_TransferShopToShopSlave vmWareHouseTransferShopToShopSlave = new VMWareHouse_TransferShopToShopSlave();
            vmWareHouseTransferShopToShopSlave = await Task.Run(() => (from t1 in _db.WareHouse_TransferShopToShop
                                                                       join t4 in _db.User_User on t1.UserID equals t4.ID
                                                                       join t5 in _db.Common_Shop on t4.Common_ShopFK equals t5.ID
                                                                       join t6 in _db.Common_Shop on t1.ToCommon_ShopFK equals t6.ID

                                                                       where t1.Active && t1.ID == id
                                                                       select new VMWareHouse_TransferShopToShopSlave
                                                                       {
                                                                           UserName = t4.Name,
                                                                           AcknowledgedBy = t1.AcknowledgedBy,
                                                                           Date = t1.Date,
                                                                           TransferCID = t1.TransferCID,
                                                                           Acknowledgement = t1.Acknowledgement,
                                                                           FromShopName = t5.Name,
                                                                           ToShopName = t6.Name,
                                                                           WareHouse_TransferShopToShopFk = t1.ID,

                                                                       }).FirstOrDefault());
            vmWareHouseTransferShopToShopSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_TransferShopToShopSlave
                                                                                     join t2 in _db.WareHouse_TransferShopToShop on t1.WareHouse_TransferShopToShopFk equals t2.ID
                                                                                     join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                                     join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                                                                                     join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                                                                                     join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                                     join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                                                                                     where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active
                                                                                      && t1.WareHouse_TransferShopToShopFk == id

                                                                                     select new VMWareHouse_TransferShopToShopSlave
                                                                                     {
                                                                                         ID = t1.ID,
                                                                                         WareHouse_TransferShopToShopFk = t1.WareHouse_TransferShopToShopFk,
                                                                                         TransferQuantity = t1.TransferQuantity,
                                                                                         SystemBarcode = t5.SystemBarcode,
                                                                                         BrandName = t9.Name,
                                                                                         SubCategoryName = t6.Name,
                                                                                         CategoryName = t7.Name,
                                                                                         ProductName = t5.Name,
                                                                                         UnitName = t8.Name,
                                                                                         MRPPrice = t5.MRPPrice,
                                                                                         CostingPrice = t5.CostingPrice
                                                                                     }).OrderByDescending(x => x.ID).AsEnumerable());

            return vmWareHouseTransferShopToShopSlave;
        }

        public async Task<int> WareHouseonsumptionAcknowledgement(int id)
        {
            var result = -1;
            if (id == 0) return result;
            var wareHouseConsumption = _db.WareHouse_Consumption.Find(id);
            wareHouseConsumption.Acknowledgement = true;
            wareHouseConsumption.AcknowledgementDate = DateTime.Now;
            wareHouseConsumption.AcknowledgedBy = _vMLogin.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseConsumption.ID;
            }
            return result;
        }

        public async Task<int> WareHouseTransferShopToShopAcknowledgement(int id)
        {
            var result = -1;
            if (id == 0) return result;
            var wareHouseConsumption = _db.WareHouse_TransferShopToShop.Find(id);
            wareHouseConsumption.Acknowledgement = true;
            wareHouseConsumption.AcknowledgementDate = DateTime.Now;
            wareHouseConsumption.AcknowledgedBy = _vMLogin.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseConsumption.ID;
            }
            return result;
        }


        public async Task<VMWareHouseConsumption> WareHouseConsumptionGet()
        {
            VMWareHouseConsumption vmWareHouseConsumption = new VMWareHouseConsumption();
            var v = await Task.Run(() => (from t1 in _db.WareHouse_Consumption
                                          join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                          join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                          join t4 in _db.User_User on t1.UserID equals t4.ID
                                          join t5 in _db.Common_Shop on t1.Common_ShopFK equals t5.ID

                                          where t1.Active && t2.Active && t3.Active && t4.Active // && t1.EnumStoreTypeFk == enumStoreType
                                          && t1.FromUser_DepartmentFk == _vMLogin.UserDepartmentId

                                          && t1.Common_ShopFK == _vMLogin.Common_ShopFK
                                          select new VMWareHouseConsumption
                                          {
                                              SendByStoreRequisition = t1.SendByStoreRequisition,
                                              UserName = t4.Name,
                                              ConsumptionCID = t1.ConsumptionCID,
                                              Date = t1.Date,
                                              Remarks = t1.Remarks,
                                              ID = t1.ID,
                                              FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                              ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                              FromUserDepartmentName = t2.Name, //+ " (" + t5.Name + ")",
                                              ToUserDepartmentName = t3.Name,// + " (" + t5.Name + ")",
                                              ShopName = t5.Name,
                                              UserID = t1.UserID,
                                              Acknowledgement = t1.Acknowledgement,
                                              AcknowledgementDate = t1.AcknowledgementDate,
                                              RequisitionCIDList = (from a in _db.Procurement_PurchaseRequisitionSlave
                                                                    join c in _db.Procurement_PurchaseRequisition on a.Procurement_PurchaseRequisitionFK equals c.ID
                                                                    where (_db.WareHouse_ConsumptionSlave.Where(x => x.WareHouse_ConsumptionFk == t1.ID).Select(x => x.Procurement_PurchaseRequisitionSlaveFK).Distinct()).Contains(a.ID)
                                                                    select c.RequisitionCID + " Date: " + c.RequisitionDate.ToLongDateString()).Distinct().ToList(),

                                          }).OrderByDescending(x => x.ID).AsEnumerable());
            vmWareHouseConsumption.ConsumptionDataList = _vMLogin.UserDepartmentId != (int)EnumDepartment.Admin ? v.Where(x => x.FromUser_DepartmentFk == _vMLogin.UserDepartmentId || x.ToUser_DepartmentFK == _vMLogin.UserDepartmentId) : v;
            return vmWareHouseConsumption;
        }
        public async Task<VMWareHouseConsumption> WareHouseConsumptionGetReceived()
        {
            VMWareHouseConsumption vmWareHouseConsumption = new VMWareHouseConsumption();
            var v = await Task.Run(() => (from t1 in _db.WareHouse_Consumption
                                          join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                          join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                          join t4 in _db.User_User on t1.UserID equals t4.ID
                                          where t1.Active && t2.Active && t3.Active && t4.Active
                                          && t1.ToUser_DepartmentFK == _vMLogin.UserDepartmentId

                                          && (_vMLogin.UserAccessLevelId == (int)UserAccessLevel.Basic ? t1.Common_ShopFK == _vMLogin.Common_ShopFK : t1.Common_ShopFK > 0)

                                          select new VMWareHouseConsumption
                                          {
                                              SendByStoreRequisition = t1.SendByStoreRequisition,
                                              UserName = t4.Name,
                                              ConsumptionCID = t1.ConsumptionCID,
                                              Date = t1.Date,
                                              Remarks = t1.Remarks,
                                              ID = t1.ID,
                                              FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                              ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                              FromUserDepartmentName = t2.Name,
                                              ToUserDepartmentName = t3.Name,
                                              UserID = t1.UserID,
                                              Acknowledgement = t1.Acknowledgement,
                                              AcknowledgementDate = t1.AcknowledgementDate
                                              //RequisitionCIDList = (from a in _db.Procurement_PurchaseRequisitionSlave
                                              //                      join c in _db.Procurement_PurchaseRequisition on a.Procurement_PurchaseRequisitionFK equals c.ID
                                              //                      where (_db.WareHouse_ConsumptionSlave.Where(x => x.WareHouse_ConsumptionFk == t1.ID).Select(x => x.Procurement_PurchaseRequisitionSlaveFK).Distinct()).Contains(a.ID)
                                              //                      select c.RequisitionCID + " Date: " + c.RequisitionDate.ToLongDateString()).Distinct().ToList(),

                                          }).OrderByDescending(x => x.ID).AsEnumerable());
            vmWareHouseConsumption.ConsumptionDataList = _vMLogin.UserDepartmentId != (int)EnumDepartment.Admin ? v.Where(x => x.FromUser_DepartmentFk == _vMLogin.UserDepartmentId || x.ToUser_DepartmentFK == _vMLogin.UserDepartmentId) : v;
            return vmWareHouseConsumption;
        }

        public async Task<VMWareHouse_TransferShopToShopSlave> WareHouseTransferShopToShopGet()
        {
            VMWareHouse_TransferShopToShopSlave vmWareHouseTransferShopToShopSlave = new VMWareHouse_TransferShopToShopSlave();
            vmWareHouseTransferShopToShopSlave.DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_TransferShopToShop
                                                                                     join t2 in _db.Common_Shop on t1.Common_ShopFK equals t2.ID
                                                                                     join t3 in _db.Common_Shop on t1.ToCommon_ShopFK equals t3.ID
                                                                                     join t4 in _db.User_User on t1.UserID equals t4.ID
                                                                                     where t1.Active && t2.Active && t3.Active && t4.Active


                                                                                     && (_vMLogin.UserAccessLevelId == (int)UserAccessLevel.Basic ? t1.ToCommon_ShopFK == _vMLogin.Common_ShopFK : t1.Common_ShopFK > 0)

                                                                                     select new VMWareHouse_TransferShopToShopSlave
                                                                                     {
                                                                                         TransferCID = t1.TransferCID,
                                                                                         FromShopName = t2.Name,
                                                                                         UserName = t4.Name,
                                                                                         ToShopName = t3.Name,
                                                                                         Date = t1.Date,
                                                                                         ID = t1.ID,
                                                                                         AcknowledgedBy = t1.AcknowledgedBy,
                                                                                         Acknowledgement = t1.Acknowledgement,
                                                                                         AcknowledgementDate = t1.AcknowledgementDate
                                                                                     }).OrderByDescending(x => x.ID).AsEnumerable());
            //vmWareHouseTransferShopToShopSlave.DataListSlave = _vMLogin.UserAccessLevelId != (int)UserAccessLevel.Basic ? v.Where(x => x.ToCommon_ShopFK == _vMLogin.Common_ShopFK) : v;
            return vmWareHouseTransferShopToShopSlave;
        }


        public List<object> PODropDownList()
        {
            var styleList = new List<object>();


            var styles = (from t1 in _db.Procurement_PurchaseOrder
                          where t1.Active

                          &&
                          ((from x in _db.Procurement_PurchaseOrderSlave
                            where x.Active == true && x.Procurement_PurchaseOrderFK == t1.ID
                            select x.PurchaseQuantity).DefaultIfEmpty(0).Sum()) >

                              ((from x in _db.WareHouse_POReceivingSlave
                                join y in _db.Procurement_PurchaseOrderSlave on x.Procurement_PurchaseOrderSlaveFk equals y.ID
                                where x.Active == true && y.Active == true
                                   && y.Procurement_PurchaseOrderFK == t1.ID && !x.IsReturn
                                select x.ReceivedQuantity).DefaultIfEmpty(0).Sum())
                          select new
                          {
                              PONumber = t1.CID + ". Date: " + t1.OrderDate.ToShortDateString(),
                              POID = t1.ID
                          }).OrderByDescending(x => x.POID).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.PONumber, Value = style.POID });
            }
            return styleList;
        }

        public List<object> POForSalesReturnDropDownList()
        {
            var styleList = new List<object>();
            var styles = (from t1 in _db.Procurement_PurchaseOrder
                          where t1.Active

                          &&
                          ((from x in _db.WareHouse_POReceivingSlave
                            join y in _db.Procurement_PurchaseOrderSlave on x.Procurement_PurchaseOrderSlaveFk equals y.ID
                            where x.Active == true && y.Active == true
                               && y.Procurement_PurchaseOrderFK == t1.ID && !x.IsReturn
                            select x.ReceivedQuantity).DefaultIfEmpty(0).Sum()) > 0

                          select new
                          {
                              PONumber = t1.CID + ". Date: " + t1.OrderDate.ToShortDateString(),
                              POID = t1.ID
                          }).OrderByDescending(x => x.POID).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.PONumber, Value = style.POID });
            }
            return styleList;
        }




        public List<object> UserStoreDepartmentDropDownList()
        {
            var userDepartmentList = new List<object>();
            foreach (var userDepartment in _db.User_Department.Where(x => x.Active && x.IsStore).OrderBy(x => x.Name).ToList())
            {
                userDepartmentList.Add(new { Text = userDepartment.Name, Value = userDepartment.ID });
            }
            return userDepartmentList;
        }
        public List<object> CommonRawItemDropDownList()
        {
            var commonRawItemList = new List<object>();
            var subCat = (from t1 in _db.Common_Product
                          join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                          join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                          where t1.Active && t2.Active && t3.Active
                          select new
                          {
                              ItemID = t1.ID,
                              ItemName = t1.Name,
                          }).ToList();

            foreach (var commonRawItem in subCat)
            {
                commonRawItemList.Add(new { Text = commonRawItem.ItemName, Value = commonRawItem.ItemID });
            }
            return commonRawItemList;
        }
        public List<object> CommonAllRawItemDropDownList()
        {
            var commonRawItemList = new List<object>();
            var subCat = (from t1 in _db.Common_Product
                          join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                          join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                          where t1.Active && t2.Active && t3.Active
                          select new
                          {
                              ItemID = t1.ID,
                              ItemName = t1.Name,
                          }).OrderBy(x => x.ItemName).ToList();

            foreach (var commonRawItem in subCat)
            {
                commonRawItemList.Add(new { Text = commonRawItem.ItemName, Value = commonRawItem.ItemID });
            }
            return commonRawItemList;
        }


        public async Task<List<VMCommonProductSubCategory>> CommonProductSubCategoryGet(int id)
        {

            List<VMCommonProductSubCategory> vMRSC = await Task.Run(() => (_db.Common_ProductSubCategory.Where(x => id <= 0 || x.Common_ProductCategoryFk == id)).Select(x => new VMCommonProductSubCategory() { ID = x.ID, Name = x.Name }).OrderBy(x => x.Name).ToListAsync());


            return vMRSC;
        }
        public async Task<List<VMCommonProduct>> CommonProductGet(int id)
        {
            List<VMCommonProduct> vMRI = await Task.Run(() => (_db.Common_Product.Where(x => id <= 0 || x.Common_ProductSubCategoryFk == id).Select(x => new VMCommonProduct() { ID = x.ID, Name = x.Name + " [" + x.SystemBarcode + "]" })).OrderBy(x => x.Name).ToListAsync());

            return vMRI;
        }

        public List<object> CommonRawSubCategoryDropDownList()
        {
            var commonRawSubCategoryList = new List<object>();

            var subCat = (from t1 in _db.Common_ProductSubCategory
                          join t2 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t2.ID
                          where t1.Active && t2.Active
                          select new
                          {
                              SubCatID = t1.ID,
                              SubCatName = t1.Name,
                          }).OrderBy(x => x.SubCatName).ToList();
            foreach (var commonRawSubCategory in subCat)
            {
                commonRawSubCategoryList.Add(new { Text = commonRawSubCategory.SubCatName, Value = commonRawSubCategory.SubCatID });
            }
            return commonRawSubCategoryList;
        }

        public List<object> CommonRawCategoryDropDownList()
        {
            var commonRawCategoryList = new List<object>();
            foreach (var commonRawCategory in _db.Common_ProductCategory.Where(x => x.Active).OrderBy(x => x.Name).ToList())
            {
                commonRawCategoryList.Add(new { Text = commonRawCategory.Name, Value = commonRawCategory.ID });
            }
            return commonRawCategoryList;
        }

        public List<object> CommonShopDropDownList()
        {
            var commonShopList = new List<object>();
            //commonRawCategoryList.Add(new { Text = "All", Value = 0 });

            foreach (var commonShop in _db.Common_Shop.Where(x => x.Active && x.ShopTypeEnumFk > 0).OrderBy(x => x.Name).ToList())
            {
                commonShopList.Add(new { Text = commonShop.Name, Value = commonShop.ID });
            }
            return commonShopList;
        }

        public List<object> CommonCommonBrandList()
        {
            var commonShopList = new List<object>();

            foreach (var commonShop in _db.Common_Brand.Where(x => x.Active).OrderBy(x => x.Name).ToList())
            {
                commonShopList.Add(new { Text = commonShop.Name, Value = commonShop.ID });
            }
            return commonShopList;
        }

        public List<object> CommonCommonSupplierList()
        {
            var commonShopList = new List<object>();

            foreach (var commonShop in _db.Common_Supplier.Where(x => x.Active).OrderBy(x => x.Name).ToList())
            {
                commonShopList.Add(new { Text = commonShop.Name, Value = commonShop.ID });
            }
            return commonShopList;
        }
        public List<object> CommonRawYarnCategoryDropDownList()
        {
            var commonRawCategoryList = new List<object>();
            foreach (var commonRawCategory in _db.Common_ProductCategory.Where(x => x.Active).OrderBy(x => x.Name).ToList())
            {
                commonRawCategoryList.Add(new { Text = commonRawCategory.Name, Value = commonRawCategory.ID });
            }
            return commonRawCategoryList;
        }

        public List<object> PurchaseRequisitionGetDropDown()
        {
            var requisitionList = new List<object>();
            foreach (var requisition in _db.Procurement_PurchaseRequisition.Where(x => x.Active).ToList())
            {
                //var enumProcess = "";// (EnumProcess)requisition.FromUser_DepartmentFk;
                requisitionList.Add(new { Text = requisition.RequisitionCID + " Date: " + requisition.RequisitionDate.ToLongDateString(), Value = requisition.ID });
            }
            return requisitionList;
        }
        public List<object> WareHouseConsumptionGetDropDown()
        {
            var requisitionList = new List<object>();
            foreach (var consumption in _db.WareHouse_Consumption.Where(x => x.Active && !x.IsRetuen).ToList())
            {
                //var enumProcess = "";// (EnumProcess)requisition.FromUser_DepartmentFk;
                requisitionList.Add(new { Text = consumption.ConsumptionCID + " Date: " + consumption.Date.ToLongDateString(), Value = consumption.ID });
            }
            return requisitionList;
        }
        public List<VMWareHouseRequisitionSlavePartial> WareHouseRequisitionSlaveData(int requisitionId)
        {
            var list = (from t1 in _db.Procurement_PurchaseRequisitionSlave
                        join wr in _db.Procurement_PurchaseRequisition on t1.Procurement_PurchaseRequisitionFK equals wr.ID
                        join t2 in _db.Procurement_PurchaseOrderSlave on t1.ID equals t2.Procurement_PurchaseRequisitionSlaveFK into t2_Join
                        from t2 in t2_Join.DefaultIfEmpty()
                        join po in _db.Procurement_PurchaseOrder on t2.Procurement_PurchaseOrderFK equals po.ID into po_Join
                        from po in po_Join.DefaultIfEmpty()
                        join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

                        where t1.Active && t5.Active && t6.Active && t7.Active && t8.Active && t1.Procurement_PurchaseRequisitionFK == requisitionId

                        select new VMWareHouseRequisitionSlavePartial
                        {
                            RequisitionCID = wr.RequisitionCID,
                            POCID = po != null ? po.CID : "",
                            CostingPrice = t5.CostingPrice,
                            Procurement_PurchaseOrderSlaveFK = t2 != null ? t2.ID : 0,
                            Common_ProductFK = t1.Common_ProductFK,
                            SystemBarcode = t5.SystemBarcode,
                            CommonProductName = t5.Name,
                            SubCategoryName = t6.Name,
                            CategoryName = t7.Name,
                            BrandName = t9.Name,
                            //ProductDescription = t2.Description,
                            Procurement_PurchaseRequisitionSlaveFK = t1.ID,
                            RequestQuantity = t1.RequisitionQuantity,
                            CommonUnitName = t8.Name,
                            Common_ShopFK = t1.Common_ShopFK,
                            StoreAvailableQuantity = ((_db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == t5.ID && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()) -
                                                       (_db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == t5.ID && x.Active && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum())) -
                                                       ((from x in _db.WareHouse_ConsumptionSlave
                                                         where x.Common_ProductFK == t5.ID && x.Active
                                                         select x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),
                            PreviousSendQuantity = ((from x in _db.WareHouse_ConsumptionSlave
                                                     where x.Procurement_PurchaseRequisitionSlaveFK == t1.ID && x.Active
                                                     select x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),


                        }).ToList();


            return list;
        }

        public async Task<int> WareHouseConsumptionSlaveAdd(VMWareHouseConsumptionSlave vmModel)
        {
            var result = -1;
            var dataListSlavePartial = vmModel.DataToListSlave.Where(x => x.ReturnQuantity > 0).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    WareHouse_ConsumptionSlave wareHouseConsumptionSlave = new WareHouse_ConsumptionSlave
                    {
                        ConsumeQuantity = dataListSlavePartial[i].ConsumeQuantity,
                        WareHouse_ConsumptionFk = vmModel.WareHouseConsumptionFk,
                        Procurement_PurchaseRequisitionSlaveFK = dataListSlavePartial[i].WareHouse_RequisitionSlaveFk,
                        User = _vMLogin.ID.ToString(),
                        UserID = _vMLogin.ID,
                        Common_ShopFK = _vMLogin.Common_ShopFK

                    };
                    _db.WareHouse_ConsumptionSlave.Add(wareHouseConsumptionSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = wareHouseConsumptionSlave.ID;
                    }
                }
            }
            return result;
        }

        public object GetAutoWareHouseConsumption(string prefix)
        {
            var v = (from t1 in _db.WareHouse_Consumption.Where(x => x.Active && !x.IsRetuen)

                     where t1.Active && !t1.IsRetuen && ((t1.ConsumptionCID.StartsWith(prefix)) || (t1.Date.ToString().StartsWith(prefix)))

                     select new
                     {
                         label = t1.ConsumptionCID != null ? t1.ConsumptionCID + " Date " + t1.Date.ToShortDateString() : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(20).ToList();

            return v;
        }
        public async Task<int> WareHouseConsumptionReturnAdd(VMWareHouseConsumptionSlave vmWareHouseConsumptionSlave)
        {

            var result = -1;
            #region Genarate Store-Out-Return ID
            int consumptionCount = _db.WareHouse_Consumption.Count();

            if (consumptionCount == 0)
            {
                consumptionCount = 1;
            }
            else
            {
                consumptionCount++;
            }


            string consumptionCID = "SOR" +
                DateTime.Now.ToString("dd") +
                DateTime.Now.ToString("MM") +
                DateTime.Now.ToString("yy") + consumptionCount.ToString().PadLeft(5, '0'); ;
            #endregion
            WareHouse_Consumption wareHouseConsumption = new WareHouse_Consumption
            {
                ConsumptionCID = consumptionCID,
                Date = DateTime.Now,
                FromUser_DepartmentFk = _vMLogin.UserDepartmentId,
                ToUser_DepartmentFK = vmWareHouseConsumptionSlave.ToUser_DepartmentFK,
                Acknowledgement = vmWareHouseConsumptionSlave.Acknowledgement,
                IsRetuen = true,
                User = _vMLogin.ID.ToString(),
                UserID = _vMLogin.ID,
                Common_ShopFK = _vMLogin.Common_ShopFK

            };
            _db.WareHouse_Consumption.Add(wareHouseConsumption);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = wareHouseConsumption.ID;
            }
            return result;
        }


        public List<VMWareHouseConsumptionSlave> WareHouseConsumptionSlaveData(int consumptionId)
        {
            var list = (from t1 in _db.WareHouse_ConsumptionSlave
                        join t0 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t0.ID
                        join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseRequisitionSlaveFK equals t2.Procurement_PurchaseRequisitionSlaveFK
                        join po in _db.Procurement_PurchaseOrder on t2.Procurement_PurchaseOrderFK equals po.ID
                        join t5 in _db.Common_Product on t2.Common_ProductFK equals t5.ID
                        join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
                        join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
                        join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                        where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active
                        && !t0.IsRetuen && t1.WareHouse_ConsumptionFk == consumptionId

                        select new VMWareHouseConsumptionSlave
                        {

                            ConsumeQuantity = t1.ConsumeQuantity,
                            ProductName = t5.Name,
                            WareHouse_RequisitionSlaveFk = t2.ID,
                            //ProductDescription = t5.Description,
                            UnitName = t8.Name,
                            ToUser_DepartmentFK = t0.FromUser_DepartmentFk,

                            PreviousSendQuantity = (from a in _db.WareHouse_ConsumptionSlave.Where(x => x.Active && x.Procurement_PurchaseRequisitionSlaveFK == t1.Procurement_PurchaseRequisitionSlaveFK)
                                                    join b in _db.WareHouse_Consumption.Where(x => x.Active && x.IsRetuen) on a.WareHouse_ConsumptionFk equals b.ID
                                                    select a.ConsumeQuantity).DefaultIfEmpty(0).Sum(),


                        }).ToList();


            return list;
        }
        public async Task<VMCommonProduct> WareHouseRawItemInventoryGet(VMCommonProduct vmCommonProduct)
        {
            vmCommonProduct = await Task.Run(() => (from t4 in _db.Common_Company

                                                    select new VMCommonProduct
                                                    {

                                                        Common_ProductCategoryFk = vmCommonProduct.Common_ProductCategoryFk,
                                                        Common_ProductSubCategoryFk = vmCommonProduct.Common_ProductSubCategoryFk,
                                                        Common_ShopFK = vmCommonProduct.Common_ShopFK,
                                                        ID = vmCommonProduct.ID,
                                                        FromDate = vmCommonProduct.FromDate,
                                                        ToDate = vmCommonProduct.ToDate,
                                                        IsCategoryWise = vmCommonProduct.IsCategoryWise,
                                                        CompanyName = t4.Name,
                                                        CompanyAddress = t4.Address,
                                                        CompanyPhone = t4.Phone,
                                                        CompanyEmail = t4.Email,
                                                        ShopName = _db.Common_Shop.Where(x => x.Active && x.ID == vmCommonProduct.Common_ShopFK).Select(x => x.Name + " (" + x.Code + ")").FirstOrDefault(),

                                                    }).FirstOrDefault());


            int ID = 0;


            //if (vmCommonProduct.Common_ShopFK == 2)
            //{

            //    vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.WareHouse_POReceivingSlave
            //                                                     join t2 in _db.WareHouse_POReceiving on t1.WareHouse_POReceivingFk equals t2.ID

            //                                                     join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
            //                                                     join t6 in _db.Common_ProductSubCategory on t5.Common_ProductSubCategoryFk equals t6.ID
            //                                                     join t7 in _db.Common_ProductCategory on t6.Common_ProductCategoryFk equals t7.ID
            //                                                     join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
            //                                                     join t9 in _db.Common_Brand on t5.Common_BrandFk equals t9.ID

            //                                                     where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
            //                                                    (t2.ChallanDate.Date >= vmCommonProduct.FromDate.Date && t2.ChallanDate.Date <= vmCommonProduct.ToDate.Date)
            //                                                     && !t1.IsReturn
            //                                                     && ((vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0) ? t7.ID == vmCommonProduct.Common_ProductCategoryFk :
            //                                                      (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0) ? t6.ID == vmCommonProduct.Common_ProductSubCategoryFk :
            //                                                      (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID > 0) ? t5.ID == vmCommonProduct.ID :

            //                                                       t1.ID > 0)


            //                                                     group new { t1, t2, t5, t6, t7, t8, t9 } by new { t6.Common_ProductCategoryFk, t5.Common_ProductSubCategoryFk, t1.Common_ProductFK } into Group
            //                                                     select new VMCommonProduct
            //                                                     {
            //                                                         Common_ProductCategoryFk = Group.Key.Common_ProductCategoryFk,
            //                                                         Common_ProductSubCategoryFk = Group.Key.Common_ProductSubCategoryFk,
            //                                                         ID = Group.Key.Common_ProductFK,

            //                                                         Name = Group.FirstOrDefault().t5.Name,
            //                                                         CategoryName = Group.FirstOrDefault().t7.Name,
            //                                                         SubCategoryName = Group.FirstOrDefault().t6.Name,
            //                                                         UnitName = Group.FirstOrDefault().t8.Name,
            //                                                         ReceivedQuantity = Group.Sum(x => x.t1.ReceivedQuantity),
            //                                                         ReturnQuantity = _db.WareHouse_POReceivingSlave
            //                                                                             .Where(x => x.Procurement_PurchaseOrderSlaveFk == Group.FirstOrDefault().t1.Procurement_PurchaseOrderSlaveFk && x.Active && x.IsReturn)
            //                                                                             .Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum(),

            //                                                         ConsumeQuantity = (from ts1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
            //                                                                            join ts2 in _db.WareHouse_Consumption.Where(x => !x.IsRetuen) on ts1.WareHouse_ConsumptionFk equals ts2.ID
            //                                                                            select ts1.ConsumeQuantity).DefaultIfEmpty(0).Sum(),

            //                                                         ConsumeReturnQuantity = (from ts1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
            //                                                                                  join ts2 in _db.WareHouse_Consumption.Where(x => x.IsRetuen) on ts1.WareHouse_ConsumptionFk equals ts2.ID
            //                                                                                  select ts1.ConsumeQuantity).DefaultIfEmpty(0).Sum(),
            //                                                         MRPPrice = Group.Sum(x => x.t5.MRPPrice),
            //                                                         GPParcentage = Group.Sum(x => x.t5.CostingPrice > 0 ? (((x.t5.MRPPrice - x.t5.CostingPrice) * 100) / x.t5.CostingPrice) : 0)

            //                                                     }).AsEnumerable());





            //}
            //else
            //{

            if (vmCommonProduct.IsCategoryWise)
            {
                vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave
                                                                 join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID

                                                                 join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                 //join t6 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t6.ID
                                                                 join t7 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t7.ID

                                                                 // join t9 in _db.Common_Brand on t1.Common_BrandFk equals t9.ID

                                                                 where t1.Active && t2.Active && t7.Active &&
                                                                  (t2.Date.Date >= vmCommonProduct.FromDate.Date && t2.Date.Date <= vmCommonProduct.ToDate.Date)
                                                                  && !t2.IsRetuen
                                                                 && t2.Acknowledgement && t2.Common_ShopFK == vmCommonProduct.Common_ShopFK

                                                                 && ((vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0) ? t1.Common_ProductCategoryFk == vmCommonProduct.Common_ProductCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0) ? t1.Common_ProductSubCategoryFk == vmCommonProduct.Common_ProductSubCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID > 0) ? t1.Common_ProductFK == vmCommonProduct.ID :

                                                                   t1.Common_ProductFK > 0)


                                                                 group new { t1, t2, t5, t7 } by new { t1.Common_ProductCategoryFk } into Group
                                                                 select new VMCommonProduct
                                                                 {

                                                                     CategoryName = Group.FirstOrDefault().t7.Name,
                                                                     ReceivedQuantity = Group.Sum(x => x.t1.ConsumeQuantity),
                                                                     MRPPrice = Group.Sum(x => x.t1.ConsumeQuantity * x.t5.MRPPrice),
                                                                     CostingPrice = Group.Sum(x => x.t1.ConsumeQuantity * x.t5.CostingPrice),
                                                                     TotalSaleMRPPrice = (from ts1 in _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date))
                                                                                          join ts2 in _db.Common_Product on ts1.Common_ProductFK equals ts2.ID
                                                                                          select ts1.Quantity * ts2.MRPPrice).DefaultIfEmpty(0).Sum(),
                                                                     TotalSaleCostingPrice = (from ts1 in _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date))
                                                                                              join ts2 in _db.Common_Product on ts1.Common_ProductFK equals ts2.ID

                                                                                              select ts1.Quantity * ts2.CostingPrice).DefaultIfEmpty(0).Sum(),


                                                                     ConsumeQuantity = _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductCategoryFk == 34 && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date)).Select(x => x.Quantity).DefaultIfEmpty(0).Sum(),

                                                                     //TransferOutQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                       join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                       select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                      join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                      select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     //TransferOutMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                       join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                       select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                      join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                      select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferOutCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                           select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                     //                          join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                          select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),





                                                                     // t5.MRPPrice > 0 ? ((((t5.MRPPrice - t5.CostingPrice) / t5.MRPPrice) * 100)) : 0

                                                                     //GPParcentage = Group.Sum(x => x.t1.MRPPrice > 0 ? ((((x.t1.MRPPrice - x.t1.CostingPrice) / x.t1.MRPPrice) * 100)) : 0),
                                                                     //ConsumeReturnQuantity = (from ts1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK && Group.First().t2.Common_ShopFK == vmCommonProduct.Common_ShopFK)
                                                                     //                         join ts2 in _db.WareHouse_Consumption.Where(x => x.IsRetuen) on ts1.WareHouse_ConsumptionFk equals ts2.ID
                                                                     //                         select ts1.ConsumeQuantity).DefaultIfEmpty(0).Sum(),

                                                                 }).AsEnumerable());
            }
            else
            {
                vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave
                                                                 join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID

                                                                 join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                 join t6 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t6.ID
                                                                 join t7 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t7.ID
                                                                 join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                 join t9 in _db.Common_Brand on t1.Common_BrandFk equals t9.ID

                                                                 where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                                                                  (t2.Date.Date >= vmCommonProduct.FromDate.Date && t2.Date.Date <= vmCommonProduct.ToDate.Date)
                                                                  && !t2.IsRetuen
                                                                 && t2.Acknowledgement && t2.Common_ShopFK == vmCommonProduct.Common_ShopFK

                                                                 && ((vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0) ? t7.ID == vmCommonProduct.Common_ProductCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0) ? t6.ID == vmCommonProduct.Common_ProductSubCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID > 0) ? t5.ID == vmCommonProduct.ID :

                                                                   t5.ID > 0)


                                                                 group new { t1, t2, t5, t6, t7, t8, t9 } by new { t6.Common_ProductCategoryFk, t5.Common_ProductSubCategoryFk, t1.Common_ProductFK } into Group
                                                                 select new VMCommonProduct
                                                                 {
                                                                     SystemBarcode = Group.FirstOrDefault().t5.SystemBarcode,
                                                                     Brand = Group.FirstOrDefault().t9.Name,

                                                                     Name = Group.FirstOrDefault().t5.Name,
                                                                     CategoryName = Group.FirstOrDefault().t7.Name,
                                                                     SubCategoryName = Group.FirstOrDefault().t6.Name,
                                                                     UnitName = Group.FirstOrDefault().t8.Name,
                                                                     ReceivedQuantity = Group.Sum(x => x.t1.ConsumeQuantity),

                                                                     //TransferOutQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                       join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                       select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                      join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                      select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     //TransferOutMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                       join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                       select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                      join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                      select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferOutCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                           select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),

                                                                     //TransferInCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                     //                          join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                     //                          select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),

                                                                     ConsumeQuantity = _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date)).Select(x => x.Quantity).DefaultIfEmpty(0).Sum(),
                                                                     //TotalSaleMRPPrice =  Group.Sum(y => y.t1.MRPPrice * _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date)).Select(x => x.Quantity).DefaultIfEmpty(0).Sum()),
                                                                     //TotalSaleCostingPrice = Group.Sum(y => y.t1.CostingPrice * _db.Marketing_SalesSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK && x.Common_ShopFK == vmCommonProduct.Common_ShopFK && (x.Time.Date >= vmCommonProduct.FromDate.Date && x.Time.Date <= vmCommonProduct.ToDate.Date)).Select(x => x.Quantity * x.CostingPrice).DefaultIfEmpty(0).Sum()),

                                                                     MRPPrice = Group.First().t5.MRPPrice,
                                                                     CostingPrice = Group.First().t5.CostingPrice,



                                                                 }).Distinct().AsEnumerable());
                int hggggggg = 0;

            }


            // }



            return vmCommonProduct;
        }


        public async Task<VMCommonProduct> WareHouseConsumptionReportGet(VMCommonProduct vmCommonProduct)
        {
            vmCommonProduct = await Task.Run(() => (from t4 in _db.Common_Company

                                                    select new VMCommonProduct
                                                    {

                                                        Common_ProductCategoryFk = vmCommonProduct.Common_ProductCategoryFk,
                                                        Common_ProductSubCategoryFk = vmCommonProduct.Common_ProductSubCategoryFk,
                                                        Common_ShopFK = vmCommonProduct.Common_ShopFK,
                                                        ID = vmCommonProduct.ID,
                                                        FromDate = vmCommonProduct.FromDate,
                                                        ToDate = vmCommonProduct.ToDate,
                                                        IsCategoryWise = vmCommonProduct.IsCategoryWise,
                                                        CompanyName = t4.Name,
                                                        CompanyAddress = t4.Address,
                                                        CompanyPhone = t4.Phone,
                                                        CompanyEmail = t4.Email,
                                                        ShopName = _db.Common_Shop.Where(x => x.Active && x.ID == vmCommonProduct.Common_ShopFK).Select(x => x.Name + " (" + x.Code + ")").FirstOrDefault(),

                                                    }).FirstOrDefault());


            if (vmCommonProduct.IsCategoryWise)
            {
                vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave
                                                                 join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID

                                                                 join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                 join t6 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t6.ID
                                                                 join t7 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t7.ID
                                                                 join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                 join t9 in _db.Common_Brand on t1.Common_BrandFk equals t9.ID

                                                                 where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                                                                  (t2.Date.Date >= vmCommonProduct.FromDate.Date && t2.Date.Date <= vmCommonProduct.ToDate.Date)
                                                                  && !t2.IsRetuen
                                                                 && t2.Acknowledgement && t2.Common_ShopFK == vmCommonProduct.Common_ShopFK

                                                                 && ((vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0) ? t7.ID == vmCommonProduct.Common_ProductCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0) ? t6.ID == vmCommonProduct.Common_ProductSubCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID > 0) ? t5.ID == vmCommonProduct.ID :

                                                                   t5.ID > 0)


                                                                 group new { t1, t2, t5, t6, t7, t8, t9 } by new { t1.Common_ProductCategoryFk } into Group
                                                                 select new VMCommonProduct
                                                                 {

                                                                     CategoryName = Group.FirstOrDefault().t7.Name,
                                                                     ReceivedQuantity = Group.Sum(x => x.t1.ConsumeQuantity),
                                                                     TransferOutQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                            join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                            select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     TransferInQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                           select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     TransferOutMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                            join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                            select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferInMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                           select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferOutCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                                join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                                select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferInCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductCategoryFk == Group.Key.Common_ProductCategoryFk)
                                                                                               join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                               select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),
                                                                     MRPPrice = Group.Sum(x => x.t1.MRPPrice * x.t1.ConsumeQuantity),
                                                                     CostingPrice = Group.Sum(x => x.t1.CostingPrice * x.t1.ConsumeQuantity),



                                                                 }).AsEnumerable());
            }
            else
            {
                vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave
                                                                 join t2 in _db.WareHouse_Consumption on t1.WareHouse_ConsumptionFk equals t2.ID

                                                                 join t5 in _db.Common_Product on t1.Common_ProductFK equals t5.ID
                                                                 join t6 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t6.ID
                                                                 join t7 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t7.ID
                                                                 join t8 in _db.Common_Unit on t5.Common_UnitFk equals t8.ID
                                                                 join t9 in _db.Common_Brand on t1.Common_BrandFk equals t9.ID

                                                                 where t1.Active && t2.Active && t5.Active && t6.Active && t7.Active && t8.Active &&
                                                                  (t2.Date.Date >= vmCommonProduct.FromDate.Date && t2.Date.Date <= vmCommonProduct.ToDate.Date)
                                                                  && !t2.IsRetuen
                                                                 && t2.Acknowledgement && t2.Common_ShopFK == vmCommonProduct.Common_ShopFK

                                                                 && ((vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0) ? t7.ID == vmCommonProduct.Common_ProductCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0) ? t6.ID == vmCommonProduct.Common_ProductSubCategoryFk :
                                                                  (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID > 0) ? t5.ID == vmCommonProduct.ID :

                                                                   t5.ID > 0)


                                                                 group new { t1, t2, t5, t6, t7, t8, t9 } by new { t6.Common_ProductCategoryFk, t5.Common_ProductSubCategoryFk, t1.Common_ProductFK } into Group
                                                                 select new VMCommonProduct
                                                                 {
                                                                     SystemBarcode = Group.FirstOrDefault().t5.SystemBarcode,
                                                                     Brand = Group.FirstOrDefault().t9.Name,

                                                                     Name = Group.FirstOrDefault().t5.Name,
                                                                     CategoryName = Group.FirstOrDefault().t7.Name,
                                                                     SubCategoryName = Group.FirstOrDefault().t6.Name,
                                                                     UnitName = Group.FirstOrDefault().t8.Name,
                                                                     ReceivedQuantity = Group.Sum(x => x.t1.ConsumeQuantity),

                                                                     TransferOutQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                            join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                            select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     TransferInQuantity = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                           select x.TransferQuantity).DefaultIfEmpty(0).Sum(),

                                                                     TransferOutMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                            join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                            select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferInMRPPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                           join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                           select x.TransferQuantity * x.MRPPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferOutCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                                join y in _db.WareHouse_TransferShopToShop.Where(x => x.Common_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                                select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),

                                                                     TransferInCostingPrice = (from x in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Active && x.Common_ProductFK == Group.Key.Common_ProductFK)
                                                                                               join y in _db.WareHouse_TransferShopToShop.Where(x => x.ToCommon_ShopFK == vmCommonProduct.Common_ShopFK && x.Active) on x.WareHouse_TransferShopToShopFk equals y.ID
                                                                                               select x.TransferQuantity * x.CostingPrice).DefaultIfEmpty(0).Sum(),
                                                                     MRPPrice = Group.Sum(x => x.t1.ConsumeQuantity * x.t1.MRPPrice),
                                                                     CostingPrice = Group.Sum(x => x.t1.ConsumeQuantity * x.t1.CostingPrice)
                                                                 }).AsEnumerable());
                int hggggggg = 0;

            }


            // }



            return vmCommonProduct;
        }
    }
}
