﻿using System;
using System.Threading.Tasks;
using KG.Core.Services.Home;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;


namespace KG.Core.Services.User
{
    public class SoftwareAdmin : Attribute, IAsyncAuthorizationFilter
    {

        public Task OnAuthorizationAsync(AuthorizationFilterContext filterContext)
        {
           // var environment = filterContext.HttpContext.Session.GetString("Environment");
            var environment = filterContext.HttpContext.Request.Cookies["Environment"];
            if (environment == "Production")
            {

                var user = filterContext.HttpContext.Session.GetString("User");

                if (user != null)
                {
                   
                    var controllerActionDescriptor = filterContext.ActionDescriptor as ControllerActionDescriptor;

                    #region ID SET
                    string controllerName = controllerActionDescriptor?.ControllerName;
                    string actionName = controllerActionDescriptor?.ActionName;
                    string iD = filterContext.RouteData.Values["id"]?.ToString();
                    string iD2 = filterContext.RouteData.Values["id2"]?.ToString();

                    //string param = "";
                    //if (iD != null)
                    //{
                    //    actionName += "/" + iD.ToString();

                    //}
                    #endregion


                    HomeService homeService = new HomeService(null, filterContext.HttpContext);

                    if (!homeService.CheckPermission(controllerName, actionName, iD, iD2))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Privacy" }, { "controller", "Auth" } });
                    }
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Auth" } });
                }


            }
            return Task.CompletedTask;
        }

    }
}
