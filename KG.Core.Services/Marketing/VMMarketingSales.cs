﻿using KG.Core.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Security.Cryptography;
using KG.Core.Services.Configuration;

namespace KG.Core.Services.Marketing
{
    public class VMMarketingSales : BaseVM
    {
      

        public string SaleInvoiceNo { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public int Common_CustomerFK { get; set; } = 0;
        public string CustomerName { get; set; }
        public string CustomerMobile { get; set; }

        public string CustomerMemberShipNo { get; set; }

        public string CustomerAddress { get; set; }
        public string CustomerCode { get; set; }


        public decimal TotalPrice { get; set; }
        //public decimal TotalDiscount { get; set; }
        public decimal TotalProductDiscount { get; set; }
        public decimal TotalInvoiceDiscount { get; set; }
        public decimal DiscountParcentage { get; set; }

        public decimal TotalAfterDiscount { get; set; }

        public decimal ProductDiscountValue { get; set; }
        public decimal ProductDiscountPercent { get; set; }

        public decimal TotalVat { get; set; }
        public decimal VatParcentage { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal CostingSubTotal { get; set; }

        public decimal Point { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal TotalIncludingVat { get; set; }

        public decimal PayableAmount { get; set; }
        public decimal? TotalPaid { get; set; }
        public decimal ReturnAmount { get; set; }
        public decimal RedeemPoint { get; set; }

        //public decimal TotalDue
        //{
        //    get { return Math.Round(((PayableAmount - TotalPaid) - TotalAdjustment), 2); }
        //}
        public int Status { get; set; }

        public int PaymentMethod { get; set; }

        public string Description { get; set; }

        public IEnumerable<VMMarketingSales> DataList { get; set; }
        public decimal ActualInvoiceValue { get; set; }
        public int CustomerPaymentMethodEnumFk { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ShopName { get; set; }
        public bool IsCategoryWise { get; set; }
        public bool IsSupplierWise { get; set; }
        public int IsCompleteInvoice { get; set; }
     


        public bool IsSalesSummery { get; set; }


        public SelectList ShopList { get; set; } = new SelectList(new List<object>());
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>());

    }

    public class VMMarketingSalesSlave : VMMarketingSales
    {

        public int Marketing_SalesFk { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }
        public string Brand { get; set; }
        public int Common_ProductFK { get; set; }
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFK { get; set; }
        public int Common_ProductSubCategoryFK { get; set; }

        public string ProductSpecification { get; set; }


        public decimal? Quantity { get; set; }
        public decimal StockAvailableQuantity { get; set; }

        public decimal MRPPrice { get; set; }

        public decimal SubTotal { get; set; }

       
       
      
        public IEnumerable<VMMarketingSalesSlave> DataListSlave { get; set; }
        public SelectList ProductSelectList { get; set; } = new SelectList(new List<object>());
        public PaymentMethodEnum CustomerPaymentMethodEnum { get { return (PaymentMethodEnum)this.CustomerPaymentMethodEnumFk; } }// = SupplierPaymentMethodEnum.Cash;
        public string CustomerPaymentMethodEnumName { get { return BaseFunctionalities.GetEnumDescription(CustomerPaymentMethodEnum); } }
        public SelectList CustomerPaymentMethodEnumList  { get { return new SelectList(BaseFunctionalities.GetEnumList<PaymentMethodEnum>(), "Value", "Text"); } }


        public SelectList ProductCategorySelectList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductSubCategorySelectList { get; set; } = new SelectList(new List<object>());
        public string SystemBarcode { get;  set; }
        public string ProductBarcode { get; set; }

        public int LastMarketingSalesFk { get; set; }
        public string ShopAddress { get; set; }
        public string ShopContact { get; set; }
        public string ShopEmail { get; set; }
        public decimal TotalPoint { get; set; }
        public decimal RedeemPoint { get; set; }
        public decimal TotalRedeemPoint { get; set; }
        public decimal BalancePoint { get; set; }

        public string CompanyAddress { get; set; }
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public bool IsDiscounted { get;  set; }
        public bool IsRedeemPoint { get; set; }
    }

    public class VMMarketingSalesReturn : BaseVM
    {
        public string CID { get; set; }
        public DateTime ReturnDate { get; set; }
        public decimal TotalReturnQuantity { get; set; }

        public decimal TotalReturnAmount { get; set; }
        public decimal TotalReturnAdjustedAmount { get; set; }
        public decimal TotalTax { get; set; }
        //public decimal PayableReturnAmount
        //{
        //    get
        //    {
        //        return (TotalReturnAmount - TotalReturnAdjustedAmount) + ((TotalReturnAmount - TotalReturnDiscount) * (TotalTax / 100));
        //    }
        //}
        public decimal TotalReturnPaid { get; set; }
        //public decimal TotalReturnDue
        //{
        //    get { return PayableReturnAmount - TotalReturnPaid; }
        //}
        public string Status { get; set; }
        public string ReturnPaymentType { get; set; }
        public string ReturnPaymentMethod { get; set; }
        public string ReturnPaymentReference { get; set; }

        public string Reason { get; set; }


        public int Marketing_SalesFK { get; set; }


    }

    public class VMMarketingSalesReturnSlave : VMMarketingSalesReturn
    {
        public decimal ReturnQuantity { get; set; }
        public decimal SalePrice { get; set; }
        public decimal SubTotal
        {
            get
            {
                return ReturnQuantity * SalePrice;
            }
        }
        public decimal AdjustedSalePrice { get; set; }
        public string Description { get; set; }



        public int Marketing_SalesReturnFK { get; set; }


        public int Marketing_SalesSlaveFK { get; set; }

        public int Common_ProductFK { get; set; }


    }

    public class VMMarketingSalesReport : VMMarketingSalesSlave
    {

        public decimal Cash { get; set; }
        public decimal DBBL { get; set; }
        public decimal City { get; set; }
        public decimal bKash { get; set; }
        public decimal Rocket { get; set; }
        public decimal StaffDue { get; set; }
       
        public decimal NetProfit { get; set; }
        public decimal GrossProfit { get; set; }
      

        //public int Common_ProductCategoryFk { get; set; }
        //public int Common_ProductSubCategoryFk { get; set; }

        public SelectList ProductCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductList { get; set; } = new SelectList(new List<object>());
       
        public SelectList ProductSubCategoryList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMMarketingSalesReport> DataListSalesReport { get; set; }

        public List<SalesPaymentMethod> SalesByPaymentMethod { get; set; }
        public string ProductCategoryName { get; internal set; }
        public string ActualInvoiceValueInWord { get; set; }
    }
    public class SalesPaymentMethod : BaseVM
    {
        public decimal ActualInvoiceValue { get; set; }
    }
}
