﻿
using Microsoft.AspNetCore.Http;
using KG.Core.Services.Home;
using System.Threading.Tasks;
using KG.Core.Entity.Marketing;
using System.Linq;
using System;
using System.Collections.Generic;
using KG.Core.Services.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using KG.Core.Entity.Configuration;

namespace KG.Core.Services.Marketing
{
    public class MarketingSaleService : BaseService
    {
        private readonly HttpContext _httpContext;
        private readonly VMLogin _vmLogin;
        public MarketingSaleService(IPosDbContext db, HttpContext httpContext)
        {
            _db = db;
            _httpContext = httpContext;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }

        public async Task<int> MarketingSalesAdd(VMMarketingSalesSlave vmMarketingSalesSlave)
        {

            var result = -1;
            var saleInvoice = _db.Marketing_Sales.Where(x => x.Time.ToShortDateString() == DateTime.Today.ToShortDateString()).Count() + 1;
            string saleInvoiceNo = @"SI-" +
                           DateTime.Now.ToString("yy") +
                           DateTime.Now.ToString("MM") +
                           DateTime.Now.ToString("dd") + "-" +

                            saleInvoice.ToString().PadLeft(6, '0');

            Marketing_Sales marketingSales = new Marketing_Sales
            {
                SaleInvoiceNo = saleInvoiceNo,
                Date = vmMarketingSalesSlave.Date,
                Common_CustomerFK = vmMarketingSalesSlave.Common_CustomerFK,
                RedeemPoint = vmMarketingSalesSlave.RedeemPoint,
                VatParcentage = vmMarketingSalesSlave.VatParcentage,
                TotalVat = vmMarketingSalesSlave.TotalVat,
                TotalIncludingVat = vmMarketingSalesSlave.TotalIncludingVat,

                DiscountParcentage = vmMarketingSalesSlave.DiscountParcentage,
                PayableAmount = vmMarketingSalesSlave.PayableAmount,

                CustomerPaymentMethodEnumFk = vmMarketingSalesSlave.CustomerPaymentMethodEnumFk,
                TotalPaid = vmMarketingSalesSlave.TotalPaid != null ? vmMarketingSalesSlave.TotalPaid.Value : 0,
                ReturnAmount = vmMarketingSalesSlave.ReturnAmount,
                CustomerName = vmMarketingSalesSlave.CustomerName,
                CustomerMobile = vmMarketingSalesSlave.CustomerMobile,
                CustomerAddress = vmMarketingSalesSlave.CustomerAddress,
                CustomerPoint = vmMarketingSalesSlave.Point,
                Status = (int)EnumSaleStatus.Pending,

                User = _vmLogin.Name,
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK

            };
            _db.Marketing_Sales.Add(marketingSales);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = marketingSales.ID;
            }

            return result;
        }

        public async Task<(int id, bool isDiscounted)> MarketingSalesEdit(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;

            var salesSlave = _db.Marketing_SalesSlave.Where(x => x.Marketing_SalesFk == vmMarketingSalesSlave.Marketing_SalesFk && x.Active).AsEnumerable();
            Marketing_Sales marketingSales = await _db.Marketing_Sales.FindAsync(vmMarketingSalesSlave.Marketing_SalesFk);
            if (marketingSales != null)
            {
                marketingSales.Description = vmMarketingSalesSlave.Description;
                marketingSales.Common_CustomerFK = vmMarketingSalesSlave.Common_CustomerFK;
                marketingSales.IsDiscounted = vmMarketingSalesSlave.TotalInvoiceDiscount > 0 ? true : false;
                marketingSales.IsRedeemPoint = vmMarketingSalesSlave.RedeemPoint > 0 ? true : false;

                marketingSales.VatParcentage = vmMarketingSalesSlave.VatParcentage;
                marketingSales.TotalVat = salesSlave.Select(x => x.VatValue).DefaultIfEmpty(0).Sum();
                marketingSales.TotalIncludingVat = vmMarketingSalesSlave.TotalIncludingVat;
                marketingSales.RedeemPoint = vmMarketingSalesSlave.RedeemPoint;


                marketingSales.DiscountParcentage = (salesSlave.Select(x => x.ProductDiscountValue + x.InvoiceDiscount).DefaultIfEmpty(0).Sum() * 100) / salesSlave.Select(x => x.Quantity * x.MRPPrice).DefaultIfEmpty(0).Sum();
                marketingSales.TotalProductDiscount = salesSlave.Select(x => x.ProductDiscountValue).DefaultIfEmpty(0).Sum();

                marketingSales.TotalInvoiceDiscount = salesSlave.Select(x => x.InvoiceDiscount).DefaultIfEmpty(0).Sum(); ;

                marketingSales.PayableAmount = vmMarketingSalesSlave.PayableAmount;
                marketingSales.CustomerMobile = vmMarketingSalesSlave.CustomerMobile;
                marketingSales.CustomerName = vmMarketingSalesSlave.CustomerName;
                marketingSales.CustomerAddress = vmMarketingSalesSlave.CustomerAddress;
                marketingSales.CustomerPoint = vmMarketingSalesSlave.Point;

                marketingSales.CustomerPaymentMethodEnumFk = vmMarketingSalesSlave.CustomerPaymentMethodEnumFk;
                marketingSales.TotalPaid = vmMarketingSalesSlave.TotalPaid != null ? vmMarketingSalesSlave.TotalPaid.Value : 0;
                marketingSales.ReturnAmount = vmMarketingSalesSlave.ReturnAmount;
                marketingSales.ActualInvoiceValue = vmMarketingSalesSlave.TotalPaid != null ? (vmMarketingSalesSlave.TotalPaid.Value - vmMarketingSalesSlave.ReturnAmount) : 0;
                marketingSales.Status = (vmMarketingSalesSlave.TotalPaid > 0) ? (int)EnumSaleStatus.Finalized : (int)EnumSaleStatus.Pending;
                marketingSales.User = _vmLogin.Name;
                marketingSales.UserID = _vmLogin.ID;
                marketingSales.Common_ShopFK = _vmLogin.Common_ShopFK;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSales.ID;
                }
            }

            
            if (vmMarketingSalesSlave.Common_CustomerFK > 0)
            {
                var IsPointed = _db.Common_CustomerPoint.Where(x => x.Marketing_SalesFk == marketingSales.ID && x.IsEarnPoint).FirstOrDefault();
                if (IsPointed == null && vmMarketingSalesSlave.Point > 0)
                {
                    Common_CustomerPoint commonCustomerPoint = new Common_CustomerPoint
                    {
                        Common_CustomerFk = vmMarketingSalesSlave.Common_CustomerFK,
                        Marketing_SalesFk = vmMarketingSalesSlave.Marketing_SalesFk,
                        Point = vmMarketingSalesSlave.Point,
                        IsEarnPoint = true,
                        User = _vmLogin.Name,
                        UserID = _vmLogin.ID
                    };
                    _db.Common_CustomerPoint.Add(commonCustomerPoint);
                    await _db.SaveChangesAsync();
                }

                if (vmMarketingSalesSlave.RedeemPoint > 0)
                {
                  
                    var IsEarnPointed = _db.Common_CustomerPoint.Where(x => x.Marketing_SalesFk == marketingSales.ID && !x.IsEarnPoint).FirstOrDefault();
                    if (IsEarnPointed == null)
                    {
                        Common_CustomerPoint commonCustomerPoint = new Common_CustomerPoint
                        {
                            Common_CustomerFk = vmMarketingSalesSlave.Common_CustomerFK,
                            Marketing_SalesFk = vmMarketingSalesSlave.Marketing_SalesFk,
                            Point = vmMarketingSalesSlave.RedeemPoint,
                            IsEarnPoint = false,
                            User = _vmLogin.Name,
                            UserID = _vmLogin.ID
                        };
                        _db.Common_CustomerPoint.Add(commonCustomerPoint);
                        await _db.SaveChangesAsync();
                    }
                    
                }
            }
            

            return (marketingSales.ID, marketingSales.IsDiscounted);



        }

        public async Task<int> MarketingSalesDelete(int id)
        {
            var result = -1;
            Marketing_Sales marketingSales = await _db.Marketing_Sales.FindAsync(id);
            if (marketingSales != null)
            {
                marketingSales.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSales.ID;
                }
            }
            return result;
        }


        public async Task<int> MarketingSalesSlaveAdd(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            //var vData = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == vM.Procurement_PurchaseRequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Merchandising_StyleID == vM.VMMerchandising_StyleID && x.Merchandising_StyleSlaveFK == vM.VMMerchandising_StyleSlaveID && x.Merchandising_BOFFK == vM.VMMerchandising_BOFID && x.Active)));
            //var pRSlave = _db.Common_Product.Find(vmMarketingSalesSlave.Common_ProductFK);
            Marketing_SalesSlave marketingSalesSlave = new Marketing_SalesSlave
            {
                Marketing_SalesFk = vmMarketingSalesSlave.Marketing_SalesFk,
                Common_ProductFK = vmMarketingSalesSlave.Common_ProductFK,
                Common_BrandFk = vmMarketingSalesSlave.Common_BrandFk,
                Common_ProductCategoryFk = vmMarketingSalesSlave.Common_ProductCategoryFK,
                Common_ProductSubCategoryFk = vmMarketingSalesSlave.Common_ProductSubCategoryFK,
                Common_SupplierFk = vmMarketingSalesSlave.Common_SupplierFk,
                ProductSpecification = vmMarketingSalesSlave.ProductSpecification,
                Quantity = vmMarketingSalesSlave.Quantity.Value,
                MRPPrice = vmMarketingSalesSlave.MRPPrice,
                CostingPrice = vmMarketingSalesSlave.CostingPrice,
                Point = vmMarketingSalesSlave.Point,
                ProductDiscountValue = vmMarketingSalesSlave.ProductDiscountValue,
                ProductDiscountPercent = ((vmMarketingSalesSlave.ProductDiscountValue * 100) / (vmMarketingSalesSlave.Quantity.Value * vmMarketingSalesSlave.MRPPrice)),
                SubTotal = vmMarketingSalesSlave.SubTotal,
                VAT = vmMarketingSalesSlave.VatParcentage,
                VatValue = ((vmMarketingSalesSlave.Quantity.Value * vmMarketingSalesSlave.MRPPrice) / 100) * vmMarketingSalesSlave.VatParcentage,
                PurchasePrice = vmMarketingSalesSlave.PurchasePrice,
                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK

            };
            _db.Marketing_SalesSlave.Add(marketingSalesSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = marketingSalesSlave.Marketing_SalesFk;
            }

            return result;
        }

        public async Task<int> MarketingSalesSlaveEdit(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            Marketing_SalesSlave marketingSalesSlave = await _db.Marketing_SalesSlave.FindAsync(vmMarketingSalesSlave.ID);

            var product = _db.Common_Product.Find(marketingSalesSlave.Common_ProductFK);
            if (marketingSalesSlave != null)
            {
                marketingSalesSlave.Quantity = vmMarketingSalesSlave.Quantity.Value;
                marketingSalesSlave.ProductDiscountPercent = ((product.DiscountValue + marketingSalesSlave.InvoiceDiscount) * 100 / (vmMarketingSalesSlave.Quantity.Value * product.MRPPrice));
                marketingSalesSlave.ProductDiscountValue = (product.DiscountValue * vmMarketingSalesSlave.Quantity.Value);
                marketingSalesSlave.SubTotal = (vmMarketingSalesSlave.Quantity.Value * product.MRPPrice) - (product.DiscountValue * vmMarketingSalesSlave.Quantity.Value);
                marketingSalesSlave.Point = product.Point;
                marketingSalesSlave.VAT = product.VATPercent;
                marketingSalesSlave.VatValue = ((vmMarketingSalesSlave.Quantity.Value * product.MRPPrice) / 100) * product.VATPercent;

                marketingSalesSlave.User = _vmLogin.Name.ToString();
                marketingSalesSlave.UserID = _vmLogin.ID;



                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSalesSlave.Marketing_SalesFk;
                }
            }
            return result;
        }

        public async Task<int> MarketingDamageProductSlaveEdit(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            Marketing_SalesSlave marketingSalesSlave = await _db.Marketing_SalesSlave.FindAsync(vmMarketingSalesSlave.ID);

            var product = _db.Common_Product.Find(marketingSalesSlave.Common_ProductFK);
            if (marketingSalesSlave != null)
            {
                marketingSalesSlave.Quantity = vmMarketingSalesSlave.Quantity.Value;
                marketingSalesSlave.ProductDiscountPercent = 0;
                marketingSalesSlave.ProductDiscountValue = 0;
                marketingSalesSlave.SubTotal = 0;
                marketingSalesSlave.Point = 0;
                marketingSalesSlave.VAT = 0;
                marketingSalesSlave.VatValue = 0;

                marketingSalesSlave.User = _vmLogin.Name.ToString();
                marketingSalesSlave.UserID = _vmLogin.ID;



                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSalesSlave.Marketing_SalesFk;
                }
            }
            return result;
        }

        public async Task<int> DistributeDiscountOnMarketingSalesSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            List<Marketing_SalesSlave> marketingSalesSlaveList = await _db.Marketing_SalesSlave.Where(x => x.Active && x.Marketing_SalesFk == vmMarketingSalesSlave.Marketing_SalesFk).ToListAsync();

            var totalSubTotal = marketingSalesSlaveList.Sum(x => x.SubTotal);
            var ProductDiscountValue = marketingSalesSlaveList.Sum(x => x.ProductDiscountValue);

            if (marketingSalesSlaveList != null)
            {
                //(Number((totalDiscount * 100) / totalIncludingVat)
                marketingSalesSlaveList.ForEach(x =>
                {
                    x.InvoiceDiscount = ((vmMarketingSalesSlave.TotalInvoiceDiscount / totalSubTotal) * x.SubTotal);
                    x.ProductDiscountPercent = ((((vmMarketingSalesSlave.TotalInvoiceDiscount / totalSubTotal) * x.SubTotal) + x.InvoiceDiscount) * 100) / x.SubTotal;
                    x.SubTotal = x.SubTotal - ((vmMarketingSalesSlave.TotalInvoiceDiscount / totalSubTotal) * x.SubTotal);
                });


                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }

        public async Task<int> DistributeRedeemPointOnMarketingSalesSlave(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            List<Marketing_SalesSlave> marketingSalesSlaveList = await _db.Marketing_SalesSlave.Where(x => x.Active && x.Marketing_SalesFk == vmMarketingSalesSlave.Marketing_SalesFk).ToListAsync();

            var totalSubTotal = marketingSalesSlaveList.Sum(x => x.SubTotal);


            if (marketingSalesSlaveList != null)
            {
                //(Number((totalDiscount * 100) / totalIncludingVat)
                marketingSalesSlaveList.ForEach(x =>
                {
                    x.RedeemProductPoint = ((vmMarketingSalesSlave.RedeemPoint / totalSubTotal) * x.SubTotal);
                    x.SubTotal = x.SubTotal - ((vmMarketingSalesSlave.RedeemPoint / totalSubTotal) * x.SubTotal);
                });


                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        public async Task<VMMarketingSalesSlave> MarketingSalesGetByID(int id)
        {
            var shop = await _db.Common_Shop.Where(x => x.ID == _vmLogin.Common_ShopFK).FirstOrDefaultAsync();

            var vmPurchaseRequisitionSlave = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.ID == id && x.Active)
                                                                   select new VMMarketingSalesSlave
                                                                   {
                                                                       Marketing_SalesFk = t1.ID,
                                                                       SaleInvoiceNo = t1.SaleInvoiceNo,
                                                                       Date = t1.Date,
                                                                       Description = t1.Description,
                                                                       Common_CustomerFK = t1.Common_CustomerFK,
                                                                       ActualInvoiceValue = t1.ActualInvoiceValue,
                                                                       VatParcentage = t1.VatParcentage,
                                                                       TotalVat = t1.TotalVat,
                                                                       TotalIncludingVat = t1.TotalIncludingVat,
                                                                       DiscountParcentage = t1.DiscountParcentage,
                                                                       TotalProductDiscount = t1.TotalProductDiscount,
                                                                       TotalInvoiceDiscount = t1.TotalInvoiceDiscount,
                                                                       PayableAmount = t1.PayableAmount,
                                                                       CustomerPaymentMethodEnumFk = t1.CustomerPaymentMethodEnumFk,
                                                                       TotalPaid = t1.TotalPaid,
                                                                       ReturnAmount = t1.ReturnAmount,
                                                                       Status = t1.Status,
                                                                       User = t1.User,
                                                                       UserID = t1.UserID,
                                                                       Common_ShopFK = t1.Common_ShopFK,
                                                                       CustomerName = t1.CustomerName,
                                                                       CustomerMobile = t1.CustomerMobile,
                                                                       IsDiscounted = t1.IsDiscounted,
                                                                       IsRedeemPoint = t1.IsRedeemPoint,
                                                                       ShopName = shop.Name,
                                                                       ShopAddress = shop.Address,
                                                                       ShopEmail = shop.Email,
                                                                       ShopContact = shop.Contact,
                                                                       CustomerAddress = t1.CustomerAddress,
                                                                       Point = t1.CustomerPoint,
                                                                       TotalPoint = t1.Common_CustomerFK > 0 ? _db.Common_CustomerPoint.Where(x => x.Active && x.Common_CustomerFk == t1.Common_CustomerFK && x.IsEarnPoint).Select(x => x.Point).DefaultIfEmpty(0).Sum() : 0,
                                                                       TotalRedeemPoint = t1.Common_CustomerFK > 0 ? _db.Common_CustomerPoint.Where(x => x.Active && x.Common_CustomerFk == t1.Common_CustomerFK && !x.IsEarnPoint).Select(x => x.Point).DefaultIfEmpty(0).Sum() : 0,
                                                                       RedeemPoint = t1.RedeemPoint




                                                                   }).FirstOrDefault());



            return vmPurchaseRequisitionSlave;
        }

        public async Task<VMMarketingSalesSlave> ReCallLastInvoice()
        {
            var vmPurchaseRequisitionSlave = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active && x.Common_ShopFK == _vmLogin.Common_ShopFK && ((x.Status == (int)EnumSaleStatus.Pending || x.Status == (int)EnumSaleStatus.Finalized)))
                                                                   select new VMMarketingSalesSlave
                                                                   {
                                                                       LastMarketingSalesFk = t1.ID,
                                                                   }).LastOrDefault());
            if (vmPurchaseRequisitionSlave == null)
            {
                vmPurchaseRequisitionSlave = new VMMarketingSalesSlave();
                vmPurchaseRequisitionSlave.LastMarketingSalesFk = 0;
            }


            return vmPurchaseRequisitionSlave;
        }
        public async Task<VMMarketingSalesSlave> ReCallLastDamageInvoice()
        {
            var vmPurchaseRequisitionSlave = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active && x.Common_ShopFK == _vmLogin.Common_ShopFK && x.Status == (int)EnumSaleStatus.DamageProduct)
                                                                   select new VMMarketingSalesSlave
                                                                   {
                                                                       LastMarketingSalesFk = t1.ID,
                                                                   }).LastOrDefault());
            if (vmPurchaseRequisitionSlave == null)
            {
                vmPurchaseRequisitionSlave = new VMMarketingSalesSlave();
                vmPurchaseRequisitionSlave.LastMarketingSalesFk = 0;
            }


            return vmPurchaseRequisitionSlave;
        }
        public async Task<IEnumerable<VMMarketingSales>> MarketingSalesGet(VMMarketingSalesReport marketingSales)
        {

            var DataList = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active
                                                 && x.Time.Date >= marketingSales.FromDate.Date && x.Time.Date <= marketingSales.ToDate.Date)

                                                 join t2 in _db.Common_Shop on t1.Common_ShopFK equals t2.ID

                                                 where _vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic ? t1.UserID == _vmLogin.ID : t1.Common_ShopFK == _vmLogin.Common_ShopFK
                                                 && (marketingSales.Common_ProductFK > 0 ?
                                                 (_db.Marketing_SalesSlave.Where(x => x.Marketing_SalesFk == t1.ID && x.Active).Select(x => x.Common_ProductFK).Contains(marketingSales.Common_ProductFK)) : t1.Active)

                                                 && (marketingSales.IsCompleteInvoice == 1 ? t1.Status == (int)EnumSaleStatus.Finalized : t1.Status == (int)EnumSaleStatus.Pending)
                                                 select new VMMarketingSales
                                                 {
                                                     ID = t1.ID,
                                                     SaleInvoiceNo = t1.SaleInvoiceNo,
                                                     Date = t1.Date,
                                                     Description = t1.Description,
                                                     Common_CustomerFK = t1.Common_CustomerFK,
                                                     ActualInvoiceValue = t1.ActualInvoiceValue,
                                                     VatParcentage = t1.VatParcentage,
                                                     TotalVat = t1.TotalVat,
                                                     TotalIncludingVat = t1.TotalIncludingVat,

                                                     DiscountParcentage = t1.DiscountParcentage,
                                                     //TotalDiscount = t1.TotalDiscount,
                                                     TotalInvoiceDiscount = t1.TotalInvoiceDiscount,
                                                     TotalProductDiscount = t1.TotalProductDiscount,
                                                     PayableAmount = t1.PayableAmount,
                                                     CustomerName = t1.CustomerName,
                                                     CustomerMobile = t1.CustomerMobile,
                                                     CustomerPaymentMethodEnumFk = t1.CustomerPaymentMethodEnumFk,
                                                     TotalPaid = t1.TotalPaid,
                                                     ReturnAmount = t1.ReturnAmount,
                                                     RedeemPoint = t1.RedeemPoint,
                                                     Point = t1.CustomerPoint,

                                                     Status = t1.Status,
                                                     User = t1.User,
                                                     UserID = t1.UserID,
                                                     Common_ShopFK = t1.Common_ShopFK,
                                                     ShopName = t2.Name

                                                 }).OrderByDescending(x => x.ID).AsEnumerable());

            return DataList;
        }

        public async Task<IEnumerable<VMMarketingSales>> MarketingDamageGet(VMMarketingSalesReport marketingSales)
        {

            var DataList = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active
                                                 && x.Time.Date >= marketingSales.FromDate.Date && x.Time.Date <= marketingSales.ToDate.Date)

                                                 join t2 in _db.Common_Shop on t1.Common_ShopFK equals t2.ID

                                                 where _vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic ? t1.UserID == _vmLogin.ID : t1.Common_ShopFK == _vmLogin.Common_ShopFK

                                                 && t1.Status == (int)EnumSaleStatus.DamageProduct
                                                 select new VMMarketingSales
                                                 {
                                                     ID = t1.ID,
                                                     SaleInvoiceNo = t1.SaleInvoiceNo,
                                                     Date = t1.Date,
                                                     Description = t1.Description,
                                                     Common_CustomerFK = t1.Common_CustomerFK,
                                                     ActualInvoiceValue = t1.ActualInvoiceValue,
                                                     VatParcentage = t1.VatParcentage,
                                                     TotalVat = t1.TotalVat,
                                                     TotalIncludingVat = t1.TotalIncludingVat,

                                                     DiscountParcentage = t1.DiscountParcentage,
                                                     //TotalDiscount = t1.TotalDiscount,
                                                     TotalInvoiceDiscount = t1.TotalInvoiceDiscount,
                                                     TotalProductDiscount = t1.TotalProductDiscount,
                                                     PayableAmount = t1.PayableAmount,

                                                     CustomerPaymentMethodEnumFk = t1.CustomerPaymentMethodEnumFk,
                                                     TotalPaid = t1.TotalPaid,
                                                     ReturnAmount = t1.ReturnAmount,
                                                     RedeemPoint = t1.RedeemPoint,
                                                     Point = t1.CustomerPoint,

                                                     Status = t1.Status,
                                                     User = t1.User,
                                                     UserID = t1.UserID,
                                                     Common_ShopFK = t1.Common_ShopFK,
                                                     ShopName = t2.Name

                                                 }).OrderByDescending(x => x.ID).AsEnumerable());

            return DataList;
        }
        public async Task<IEnumerable<VMMarketingSalesSlave>> MarketingSalesSlaveGet(int id)
        {

            var DataListSlave = await Task.Run(() => (from t1 in _db.Marketing_SalesSlave.Where(x => x.Marketing_SalesFk == id && x.Active)
                                                      join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                      join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                      join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                      join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                      select new VMMarketingSalesSlave
                                                      {
                                                          ID = t1.ID,
                                                          Marketing_SalesFk = t1.Marketing_SalesFk,
                                                          Common_ProductFK = t1.Common_ProductFK,
                                                          ProductSpecification = t2.Remarks,
                                                          MRPPrice = t1.MRPPrice,
                                                          UnitName = t3.Name,
                                                          ProductName = t4.Name + " " + t2.Name + " " + t2.SystemBarcode,
                                                          Quantity = t1.Quantity,

                                                          SubTotal = (t1.Quantity * t1.MRPPrice),
                                                          ProductDiscountPercent = t1.ProductDiscountPercent,
                                                          ProductDiscountValue = t1.ProductDiscountValue + t1.InvoiceDiscount,
                                                          VatParcentage = t2.VATPercent,
                                                          TotalVat = t1.VatValue,
                                                          CostingPrice = t1.CostingPrice,
                                                          CostingSubTotal = (t1.Quantity * t1.CostingPrice),
                                                          Description = t1.Remarks
                                                      }).OrderByDescending(x => x.ID).AsEnumerable());







            return DataListSlave;
        }

        public async Task<VMMarketingSalesSlave> CustomerDueReportGet(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            vmMarketingSalesSlave = await Task.Run(() => (from t4 in _db.Common_Company

                                                          select new VMMarketingSalesSlave
                                                          {
                                                              Common_ShopFK = vmMarketingSalesSlave.Common_ShopFK,
                                                              FromDate = vmMarketingSalesSlave.FromDate,
                                                              ToDate = vmMarketingSalesSlave.ToDate,
                                                              CompanyName = t4.Name,
                                                              CompanyAddress = t4.Address,
                                                              CompanyPhone = t4.Phone,
                                                              CompanyEmail = t4.Email,
                                                              ShopName = _db.Common_Shop.Where(x => x.Active && x.ID == vmMarketingSalesSlave.Common_ShopFK).Select(x => x.Name + " (" + x.Code + ")").FirstOrDefault(),

                                                          }).FirstOrDefault());


            vmMarketingSalesSlave.DataList = await Task.Run(() => (from t1 in _db.Marketing_Sales
                                                                   join t2 in _db.Common_Customer on t1.Common_CustomerFK equals t2.ID
                                                                   where t1.Active && t2.Active && t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.StaffDue &&
                                                                    (t1.Date.Date >= vmMarketingSalesSlave.FromDate.Date && t1.Date.Date <= vmMarketingSalesSlave.ToDate.Date) &&
                                                                    t1.Common_ShopFK == vmMarketingSalesSlave.Common_ShopFK


                                                                   group new { t1, t2 } by new { t1.Common_CustomerFK } into Group
                                                                   select new VMMarketingSalesSlave
                                                                   {

                                                                       CustomerName = Group.FirstOrDefault().t2.Name,
                                                                       CustomerMobile = Group.FirstOrDefault().t2.Phone,
                                                                       CustomerAddress = Group.FirstOrDefault().t2.Address,
                                                                       CustomerCode = Group.FirstOrDefault().t2.MemberShipNo,
                                                                       TotalPaid = Group.Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),

                                                                   }).AsEnumerable());



            return vmMarketingSalesSlave;
        }
        public async Task<VMMarketingSalesSlave> CustomerInvoiceReportGet(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            vmMarketingSalesSlave = await Task.Run(() => (from t4 in _db.Common_Company

                                                          select new VMMarketingSalesSlave
                                                          {
                                                              Common_ShopFK = vmMarketingSalesSlave.Common_ShopFK,
                                                              FromDate = vmMarketingSalesSlave.FromDate,
                                                              ToDate = vmMarketingSalesSlave.ToDate,
                                                              CompanyName = t4.Name,
                                                              CompanyAddress = t4.Address,
                                                              CompanyPhone = t4.Phone,
                                                              CompanyEmail = t4.Email,
                                                              ShopName = _db.Common_Shop.Where(x => x.Active && x.ID == vmMarketingSalesSlave.Common_ShopFK).Select(x => x.Name + " (" + x.Code + ")").FirstOrDefault(),

                                                          }).FirstOrDefault());


            vmMarketingSalesSlave.DataList = await Task.Run(() => (from t1 in _db.Marketing_Sales
                                                                   where t1.Active &&
                                                                    (t1.Date.Date >= vmMarketingSalesSlave.FromDate.Date && t1.Date.Date <= vmMarketingSalesSlave.ToDate.Date) &&
                                                                    t1.Common_ShopFK == vmMarketingSalesSlave.Common_ShopFK


                                                                   group new { t1 } by new { t1.CustomerMobile } into Group
                                                                   select new VMMarketingSalesSlave
                                                                   {

                                                                       CustomerName = Group.FirstOrDefault().t1.CustomerName,
                                                                       CustomerMobile = Group.FirstOrDefault().t1.CustomerMobile,
                                                                       ID = Group.Count(),
                                                                       Date = Group.FirstOrDefault().t1.Date,
                                                                       TotalPaid = Group.Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),

                                                                   }).AsEnumerable());



            return vmMarketingSalesSlave;
        }
        public object GetAutoCompleteProduct(string prefix)
        {
            var v = (from t1 in _db.Common_Product
                     join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID //into t2_Join
                     //from t2 in t2_Join.DefaultIfEmpty()
                     join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID //into t3_Join
                     //from t3 in t3_Join.DefaultIfEmpty()
                     join t4 in _db.Common_Brand on t1.Common_BrandFk equals t4.ID// into t4_Join
                     join t5 in _db.Common_Supplier on t1.Common_SupplierFk equals t5.ID into t5_Join

                     from t5 in t5_Join.DefaultIfEmpty()
                     where t1.Active && ((t1.Name.StartsWith(prefix)) || (t1.SystemBarcode.StartsWith(prefix)) || (t2.Name.StartsWith(prefix))
                                        || (t3.Name.StartsWith(prefix)) || (t4.Name.StartsWith(prefix)) || (t5.Name.StartsWith(prefix)) || (t5.Code.StartsWith(prefix)))

                     select new
                     {
                         label = "[" + t1.SystemBarcode + "] " + t3.Name + " || " + t2.Name + " || " + t1.Name + " || " + t4.Name + " || " + (t5 != null ? t5.Name + "[" + t5.Code + "]" + " || " : "") + t1.MRPPrice,
                         val = t1.ID,
                         //category = t3.Name
                     }).OrderByDescending(x => x.val).Take(200).ToList();

            return v;
        }
        public object GetAutoCompleteProductBySupplier(string prefix, int supplierId)
        {
            var v = (from t1 in _db.Common_Product
                     join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                     join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                     join t4 in _db.Common_Brand on t1.Common_BrandFk equals t4.ID
                     join t5 in _db.Common_Supplier on t1.Common_SupplierFk equals t5.ID into t5_Join
                     from t5 in t5_Join.DefaultIfEmpty()
                     where t1.Active && (t5 != null ? t1.Common_SupplierFk == supplierId : t1.ID > 0) && ((t1.Name.StartsWith(prefix)) || (t1.SystemBarcode.StartsWith(prefix)) || (t2.Name.StartsWith(prefix))
                                        || (t3.Name.StartsWith(prefix)) || (t4.Name.StartsWith(prefix)) || (t5.Name.StartsWith(prefix)) || (t5.Code.StartsWith(prefix)))


                     select new
                     {
                         label = "[" + t1.SystemBarcode + "] " + t3.Name + " || " + t2.Name + " || " + t1.Name + " || " + t4.Name + " || " + (t5 != null ? t5.Name + "[" + t5.Code + "]" + " || " : "") + t1.MRPPrice,
                         val = t1.ID,
                         //category = t3.Name
                     }).OrderByDescending(x => x.val).Take(200).ToList();

            return v;
        }

        public object GetAutoCompleteCustomer(string prefix)
        {
            return _db.Common_Customer.Where(x => x.Active && (x.Name.StartsWith(prefix) || x.Phone.StartsWith(prefix) || x.MemberShipNo.StartsWith(prefix))).Select(x => new
            {
                label = x.Name,
                val = x.ID
            }).OrderBy(x => x.label).Take(10).ToList();
        }
        public async Task<int> MarketingSalesSlaveDelete(int id)
        {
            var result = -1;

            Marketing_SalesSlave marketingSalesSlave = await _db.Marketing_SalesSlave.FindAsync(id);
            if (marketingSalesSlave != null)
            {
                marketingSalesSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSalesSlave.ID;
                }
            }
            return result;
        }
        //public IEnumerable GetPaymentMethodEnum()
        //{
        //    var PaymentTypeList = new List<object>();

        //    foreach (var eVal in Enum.GetValues(typeof(PaymentMethodEnum)))
        //    {
        //        PaymentTypeList.Add(new SelectListItem { Text = Enum.GetName(typeof(PaymentMethodEnum), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
        //    }
        //    return PaymentTypeList;
        //}

        public List<object> CommonShopDropDownList()
        {
            var List = new List<object>();
            var data = _db.Common_Shop.Where(a => a.Active && a.ShopTypeEnumFk > 0 && (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic ? a.ID == _vmLogin.Common_ShopFK : a.ID > 0)).ToList();

            foreach (var x in data)
            {
                List.Add(new { Text = x.Name, Value = x.ID });
            }
            return List;
        }

        public List<object> CommonSupplierDropDownList()
        {
            var List = new List<object>();
            _db.Common_Supplier
        .Where(x => x.Active).Select(x => x).OrderBy(x => x.Name).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> ProductCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_ProductCategory
        .Where(x => x.Active).Select(x => x).OrderBy(x => x.Name).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> ProductSubCategoryDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_ProductSubCategory
        .Where(x => x.Active).Where(x => x.Active && (x.Common_ProductCategoryFk == id || id <= 0)).Select(x => x).OrderBy(x => x.Name).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> ProductDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_Product
        .Where(x => x.Active).Where(x => x.Active && (x.Common_ProductSubCategoryFk == id || id <= 0)).Select(x => x).OrderBy(x => x.Name).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.SystemBarcode + " " + x.Name
        }));
            return List;

        }
        public async Task<List<VMCommonProductSubCategory>> CommonProductSubCategoryGet(int id)
        {

            List<VMCommonProductSubCategory> vMRSC = await Task.Run(() => (_db.Common_ProductSubCategory.Where(x => x.Active && (id <= 0 || x.Common_ProductCategoryFk == id))).Select(x => new VMCommonProductSubCategory() { ID = x.ID, Name = x.Name }).ToListAsync());

            return vMRSC;
        }
        public async Task<List<VMCommonProduct>> CommonProductGet(int id)
        {
            List<VMCommonProduct> vMRI = await Task.Run(() => (_db.Common_Product.Where(x => x.Active && (id <= 0 || x.Common_ProductSubCategoryFk == id)).Select(x => new VMCommonProduct() { ID = x.ID, Name = x.Name })).ToListAsync());

            return vMRI;
        }
        public async Task<VMCommonProduct> CommonProductSingle(int id)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct = await Task.Run(() => (from t1 in _db.Common_Product.Where(x => x.ID == id && x.Active)
                                                    join t2 in _db.Common_Unit.Where(x => x.Active) on t1.Common_UnitFk equals t2.ID
                                                    join t3 in _db.Common_ProductSubCategory.Where(x => x.Active) on t1.Common_ProductSubCategoryFk equals t3.ID

                                                    select new VMCommonProduct
                                                    {
                                                        ID = t1.ID,
                                                        Name = t1.Name,
                                                        UnitName = t2.Name,
                                                        Common_UnitFk = t1.Common_UnitFk,
                                                        MRPPrice = t1.MRPPrice,
                                                        VATPercent = t1.EnableOrDisableProductVat == true ? t1.VATPercent : 0,
                                                        DiscountPercent = t1.DiscountExpiryDate >= DateTime.Today ? t1.DiscountPercent : 0,
                                                        DiscountValue = t1.DiscountExpiryDate >= DateTime.Today ? t1.DiscountValue : 0,
                                                        EnableOrDisableProductVat = t1.EnableOrDisableProductVat,
                                                        Common_BrandFk = t1.Common_BrandFk,
                                                        Common_SupplierFk = t1.Common_SupplierFk,
                                                        Common_ProductSubCategoryFk = t1.Common_ProductSubCategoryFk,
                                                        Common_ProductCategoryFk = t3.Common_ProductCategoryFk,
                                                        Point = t1.Point,
                                                        CostingPrice = t1.CostingPrice,
                                                        PurchasePrice = t1.PurchasePrice,
                                                        // this value available only for bazar
                                                        PreviousStock = (((((from ts1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active) // Received From Store
                                                                             join ts2 in _db.WareHouse_Consumption.Where(x => x.Acknowledgement && x.Common_ShopFK == _vmLogin.Common_ShopFK) on ts1.WareHouse_ConsumptionFk equals ts2.ID
                                                                             select ts1.ConsumeQuantity).DefaultIfEmpty(0).Sum())
                                                                        -
                                                                        ((from ts1 in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Common_ProductFK == id && x.Active)  // Transfer from
                                                                          join ts2 in _db.WareHouse_TransferShopToShop.Where(x => x.Acknowledgement && x.Common_ShopFK == _vmLogin.Common_ShopFK) on ts1.WareHouse_TransferShopToShopFk equals ts2.ID
                                                                          select ts1.TransferQuantity).DefaultIfEmpty(0).Sum()))
                                                                         +
                                                                         ((from ts1 in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Common_ProductFK == id && x.Active) //Transfer to
                                                                           join ts2 in _db.WareHouse_TransferShopToShop.Where(x => x.Acknowledgement && x.ToCommon_ShopFK == _vmLogin.Common_ShopFK) on ts1.WareHouse_TransferShopToShopFk equals ts2.ID
                                                                           select ts1.TransferQuantity).DefaultIfEmpty(0).Sum())

                                                                         ))


                                                                      - ((from ts1 in _db.Marketing_SalesSlave // Sales
                                                                             .Where(x => x.Common_ProductFK == id && x.Active
                                                                             && (_vmLogin.Common_ShopFK > 0 ? x.Common_ShopFK == _vmLogin.Common_ShopFK : x.Common_ShopFK > 0))
                                                                          join ts2 in _db.Marketing_Sales.Where(x => x.Active && ((x.Status == (int)EnumSaleStatus.Finalized) || (x.Status == (int)EnumSaleStatus.DamageProduct)))
                                                                          on ts1.Marketing_SalesFk equals ts2.ID
                                                                          select ts1.Quantity).DefaultIfEmpty(0).Sum()
                                                                            +
                                                                         (_db.WareHouse_POReceivingSlave
                                                                             .Where(x => x.Common_ProductFK == id && x.Active && x.IsReturn) //Supplier return
                                                                             .Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum()))
                                                    }).FirstOrDefault());

            return vmCommonProduct;
        }
        public async Task<VMCommonCustomer> CommonCustomerById(int id)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();

            vmCommonCustomer = await Task.Run(() => (from t1 in _db.Common_Customer.Where(x => x.ID == id && x.Active)
                                                     select new VMCommonCustomer
                                                     {
                                                         ID = t1.ID,
                                                         Name = t1.Name,
                                                         Address = t1.Address,
                                                         Phone = t1.Phone,
                                                         MemberShipNo = t1.MemberShipNo,
                                                         TotalEarnPoint = _db.Common_CustomerPoint.Where(x => x.Active && x.Common_CustomerFk == t1.ID && x.IsEarnPoint).Select(x => x.Point).DefaultIfEmpty(0).Sum(),
                                                         TotalRedeemPoint = _db.Common_CustomerPoint.Where(x => x.Active && x.Common_CustomerFk == t1.ID && !x.IsEarnPoint).Select(x => x.Point).DefaultIfEmpty(0).Sum(),

                                                     }).FirstOrDefault());

            return vmCommonCustomer;
        }
        public async Task<VMCommonCustomer> CommonCustomerSingle(string memberShipNo)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();

            vmCommonCustomer = await Task.Run(() => (from t1 in _db.Common_Customer.Where(x => x.MemberShipNo == memberShipNo && x.Active)
                                                     select new VMCommonCustomer
                                                     {
                                                         ID = t1.ID,
                                                         Name = t1.Name,
                                                         Address = t1.Address,
                                                         Phone = t1.Phone,
                                                         MemberShipNo = t1.MemberShipNo

                                                     }).FirstOrDefault());

            return vmCommonCustomer;
        }
        public async Task<VMCommonCustomer> CommonCustomerSingleBuPhone(string phone)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();
            if (phone != null)
            {
                vmCommonCustomer = await Task.Run(() => (from t1 in _db.Common_Customer.Where(x => x.Phone == phone && x.Phone != null && x.Active)
                                                         select new VMCommonCustomer
                                                         {
                                                             ID = t1.ID,
                                                             Name = t1.Name,
                                                             Address = t1.Address,
                                                             Phone = t1.Phone,
                                                             MemberShipNo = t1.MemberShipNo
                                                         }).FirstOrDefault());
            }


            return vmCommonCustomer;
        }
        public async Task<VMCommonCustomer> CommonCustomerSingle(int id)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();

            vmCommonCustomer = await Task.Run(() => (from t1 in _db.Common_Customer.Where(x => x.ID == id && x.Active)

                                                     select new VMCommonCustomer
                                                     {
                                                         ID = t1.ID,
                                                         Name = t1.Name,
                                                         Phone = t1.Phone,
                                                         MemberShipNo = t1.MemberShipNo,
                                                         CustomerLoyalityPoint = t1.CustomerLoyalityPoint,
                                                         Code = t1.Code

                                                     }).FirstOrDefault());

            return vmCommonCustomer;
        }
        public async Task<VMMarketingSalesSlave> GetSingleMarketingSalesSlave(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Marketing_SalesSlave
                                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                          join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID


                                          where t1.ID == id
                                          select new VMMarketingSalesSlave
                                          {
                                              Common_ProductFK = t1.Common_ProductFK,
                                              Marketing_SalesFk = t1.Marketing_SalesFk,
                                              Quantity = t1.Quantity,
                                              ProductName = t2.Name,
                                              UnitName = t3.Name,
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VMMarketingSalesSlave> GetSingleMarketingSalesCustomer(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Common_Customer
                                          where t1.ID == id
                                          select new VMMarketingSalesSlave
                                          {
                                              Common_CustomerFK = t1.ID,
                                              CustomerName = t1.Name
                                          }).FirstOrDefault());
            return v;
        }


        public async Task<VMMarketingSalesReport> GetShopInfo(VMMarketingSalesReport model)
        {


            var Slave = await Task.Run(() => (from t1 in _db.Common_Shop.Where(x => x.Active && x.ID == model.Common_ShopFK)


                                              select new VMMarketingSalesReport
                                              {

                                                  ShopName = t1.Name,
                                                  ShopAddress = t1.Address,
                                                  ShopContact = t1.Contact,
                                                  ShopEmail = t1.Email,
                                                  FromDate = model.FromDate,
                                                  ToDate = model.ToDate,
                                                  Common_ShopFK = t1.ID,
                                                  Common_ProductCategoryFK = model.Common_ProductCategoryFK,
                                                  Common_ProductSubCategoryFK = model.Common_ProductSubCategoryFK,
                                                  Common_ProductFK = model.Common_ProductFK
                                              }).FirstOrDefault());

            return Slave;
        }

        public async Task<VMMarketingSalesReport> MarketingSalesReportGet(VMMarketingSalesReport model)
        {
            VMMarketingSalesReport vMMarketingSales = new VMMarketingSalesReport();
            if (model.Common_ShopFK > 0)
            {
                vMMarketingSales = await Task.Run(() => (from t1 in _db.Common_Shop.Where(x => x.Active && x.ID == model.Common_ShopFK)


                                                         select new VMMarketingSalesReport
                                                         {

                                                             ShopName = t1.Name,
                                                             ShopAddress = t1.Address,
                                                             ShopContact = t1.Contact,
                                                             ShopEmail = t1.Email,
                                                             FromDate = model.FromDate,
                                                             ToDate = model.ToDate,
                                                             IsCategoryWise = model.IsCategoryWise,
                                                             IsSalesSummery = model.IsSalesSummery,
                                                             Common_ShopFK = t1.ID,
                                                             Common_ProductCategoryFK = model.Common_ProductCategoryFK,
                                                             Common_ProductSubCategoryFK = model.Common_ProductSubCategoryFK,
                                                             Common_ProductFK = model.Common_ProductFK
                                                         }).FirstOrDefault());
            }


            if (model.IsSalesSummery)
            {
                vMMarketingSales.DataListSalesReport = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active)

                                                                             where t1.Time.Date >= model.FromDate.Date && t1.Time.Date <= model.ToDate.Date


                                                                                      && (model.Common_ShopFK > 0 ? t1.Common_ShopFK == model.Common_ShopFK : t1.Common_ShopFK > 0)


                                                                             group new { t1 } by new { t1.Common_ShopFK } into Group
                                                                             select new VMMarketingSalesReport
                                                                             {
                                                                                 ID = Group.Select(x => x.t1.ID).Count(),
                                                                                 TotalRedeemPoint = Group.Sum(x => x.t1.RedeemPoint),
                                                                                 TotalPoint = Group.Sum(x => x.t1.CustomerPoint),
                                                                                 TotalVat = Group.Sum(x => x.t1.TotalVat),
                                                                                 PayableAmount = Group.Sum(x => x.t1.PayableAmount),
                                                                                 //TotalDiscount = Group.Sum(x => x.t1.TotalDiscount),
                                                                                 TotalProductDiscount = Group.Sum(x => x.t1.TotalProductDiscount),
                                                                                 TotalInvoiceDiscount = Group.Sum(x => x.t1.TotalInvoiceDiscount),

                                                                                 ActualInvoiceValue = Group.Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 Cash = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.Cash).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 DBBL = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.DBBL).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 City = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.City).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 bKash = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.bKash).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 Rocket = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.Rocket).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),
                                                                                 StaffDue = Group.Where(x => x.t1.CustomerPaymentMethodEnumFk == (int)PaymentMethodEnum.StaffDue).Sum(x => x.t1.TotalPaid - x.t1.ReturnAmount),


                                                                             }).OrderByDescending(x => x.ID).AsEnumerable());

            }

            else if (model.IsCategoryWise)
            {
                vMMarketingSales.DataListSalesReport = await Task.Run(() => (from t1 in _db.Marketing_SalesSlave.Where(x => x.Active)
                                                                             join t0 in _db.Marketing_Sales.Where(x => x.Active) on t1.Marketing_SalesFk equals t0.ID

                                                                             join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                                             join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                                             join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                                             join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                                             join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID

                                                                             where t1.Time.Date >= model.FromDate.Date && t1.Time.Date <= model.ToDate.Date &&
                                                                                ((model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK == 0 && model.Common_ProductFK == 0) ? t5.ID == model.Common_ProductCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK == 0) ? t4.ID == model.Common_ProductSubCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK > 0) ? t2.ID == model.Common_ProductFK :

                                                                                      t1.ID > 0)

                                                                                      && (model.Common_ShopFK > 0 ? t0.Common_ShopFK == model.Common_ShopFK : t0.Common_ShopFK > 0)


                                                                             group new { t1, t0, t2, t3, t4, t5, t6 } by new { t4.Common_ProductCategoryFk } into Group
                                                                             select new VMMarketingSalesReport
                                                                             {

                                                                                 CostingPrice = Group.Sum(x => x.t1.Quantity * x.t1.CostingPrice),
                                                                                 MRPPrice = Group.Sum(x => x.t1.Quantity * x.t1.MRPPrice),
                                                                                 TotalVat = Group.Sum(x => x.t1.VatValue),
                                                                                 ProductCategoryName = Group.First().t5.Name,
                                                                                 UnitName = Group.First().t3.Name,
                                                                                 Quantity = Group.Sum(x => x.t1.Quantity),
                                                                                 ProductDiscountValue = Group.Sum(x => x.t1.ProductDiscountValue),
                                                                                 SubTotal = Group.Sum(x => ((x.t1.SubTotal) + x.t1.VatValue)),
                                                                                 GrossProfit = Group.Sum(x => ((x.t1.SubTotal) - (x.t1.CostingPrice * x.t1.Quantity))),

                                                                             }).OrderByDescending(x => x.ID).AsEnumerable());

            }

            else if (model.IsSupplierWise)
            {
                vMMarketingSales.DataListSalesReport = await Task.Run(() => (from t1 in _db.Marketing_SalesSlave.Where(x => x.Active)
                                                                             join t0 in _db.Marketing_Sales.Where(x => x.Active) on t1.Marketing_SalesFk equals t0.ID

                                                                             join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                                             join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                                             join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                                             join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                                             join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID

                                                                             where t1.Time.Date >= model.FromDate.Date && t1.Time.Date <= model.ToDate.Date &&
                                                                                ((model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK == 0 && model.Common_ProductFK == 0) ? t5.ID == model.Common_ProductCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK == 0) ? t4.ID == model.Common_ProductSubCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK > 0) ? t2.ID == model.Common_ProductFK :

                                                                                      t1.ID > 0)

                                                                                      && (model.Common_ShopFK > 0 ? t0.Common_ShopFK == model.Common_ShopFK : t0.Common_ShopFK > 0)


                                                                             group new { t1, t0, t2, t3, t4, t5, t6 } by new { t4.Common_ProductCategoryFk } into Group
                                                                             select new VMMarketingSalesReport
                                                                             {

                                                                                 CostingPrice = Group.Sum(x => x.t1.Quantity * x.t1.CostingPrice),
                                                                                 MRPPrice = Group.Sum(x => x.t1.Quantity * x.t1.MRPPrice),
                                                                                 TotalVat = Group.Sum(x => x.t1.VatValue),
                                                                                 ProductCategoryName = Group.First().t5.Name,
                                                                                 UnitName = Group.First().t3.Name,
                                                                                 Quantity = Group.Sum(x => x.t1.Quantity),
                                                                                 ProductDiscountValue = Group.Sum(x => x.t1.ProductDiscountValue),
                                                                                 SubTotal = Group.Sum(x => ((x.t1.SubTotal) + x.t1.VatValue)),
                                                                                 GrossProfit = Group.Sum(x => ((x.t1.SubTotal) - (x.t1.CostingPrice * x.t1.Quantity))),
                                                                             }).OrderByDescending(x => x.ID).AsEnumerable());

            }
            else
            {
                vMMarketingSales.DataListSalesReport = await Task.Run(() => (from t1 in _db.Marketing_SalesSlave.Where(x => x.Active)
                                                                             join t0 in _db.Marketing_Sales.Where(x => x.Active) on t1.Marketing_SalesFk equals t0.ID

                                                                             join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                                             join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                                             join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                                             join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                                             join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID

                                                                             where t1.Time.Date >= model.FromDate.Date && t1.Time.Date <= model.ToDate.Date &&
                                                                              (model.Common_BrandFk > 0 ? t2.Common_BrandFk == model.Common_BrandFk : t2.Common_BrandFk > 0) &&
                                                                                ((model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK == 0 && model.Common_ProductFK == 0) ? t5.ID == model.Common_ProductCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK == 0) ? t4.ID == model.Common_ProductSubCategoryFK :
                                                                                     (model.Common_ProductCategoryFK > 0 && model.Common_ProductSubCategoryFK > 0 && model.Common_ProductFK > 0) ? t2.ID == model.Common_ProductFK :

                                                                                      t1.ID > 0)

                                                                                      && (model.Common_ShopFK > 0 ? t0.Common_ShopFK == model.Common_ShopFK : t0.Common_ShopFK > 0)


                                                                             group new { t1, t0, t2, t3, t4, t5, t6 } by new { t1.Common_ProductFK } into Group
                                                                             select new VMMarketingSalesReport
                                                                             {

                                                                                 CostingPrice = Group.Sum(x => x.t1.Quantity * x.t1.CostingPrice),
                                                                                 MRPPrice = Group.Sum(x => x.t1.Quantity * x.t1.MRPPrice),
                                                                                 TotalVat = Group.Sum(x => x.t1.VatValue),
                                                                                 SystemBarcode = Group.First().t2.SystemBarcode,
                                                                                 ProductName = Group.First().t5.Name + " " + Group.First().t4.Name + " " + Group.First().t2.Name + "-Brand: " + Group.First().t6.Name,
                                                                                 UnitName = Group.First().t3.Name,
                                                                                 Quantity = Group.Sum(x => x.t1.Quantity),
                                                                                 ProductDiscountValue = Group.Sum(x => x.t1.ProductDiscountValue),
                                                                                 SubTotal = Group.Sum(x => ((x.t1.SubTotal) + x.t1.VatValue)),
                                                                                 GrossProfit = Group.Sum(x => ((x.t1.MRPPrice * x.t1.Quantity) - (x.t1.CostingPrice * x.t1.Quantity))),
                                                                                 //GrossProfit = Group.Sum(x => ((x.t1.MRPPrice * x.t1.Quantity) - ((x.t1.PurchasePrice * x.t1.Quantity))))
                                                                             }).OrderByDescending(x => x.ID).AsEnumerable());

            }


            return vMMarketingSales;
        }


        public async Task<int> MarketingDamageProductAdd(VMMarketingSalesSlave vmMarketingSalesSlave)
        {

            var result = -1;
            var saleInvoice = _db.Marketing_Sales.Where(x => x.Time.ToShortDateString() == DateTime.Today.ToShortDateString()).Count() + 1;
            string saleInvoiceNo = @"SI-" +
                           DateTime.Now.ToString("yy") +
                           DateTime.Now.ToString("MM") +
                           DateTime.Now.ToString("dd") + "-" +

                            saleInvoice.ToString().PadLeft(6, '0');

            Marketing_Sales marketingSales = new Marketing_Sales
            {
                SaleInvoiceNo = saleInvoiceNo,
                Date = vmMarketingSalesSlave.Date,
                Common_CustomerFK = vmMarketingSalesSlave.Common_CustomerFK,
                RedeemPoint = vmMarketingSalesSlave.RedeemPoint,
                VatParcentage = vmMarketingSalesSlave.VatParcentage,
                TotalVat = vmMarketingSalesSlave.TotalVat,
                TotalIncludingVat = vmMarketingSalesSlave.TotalIncludingVat,

                DiscountParcentage = vmMarketingSalesSlave.DiscountParcentage,
                PayableAmount = vmMarketingSalesSlave.PayableAmount,

                CustomerPaymentMethodEnumFk = vmMarketingSalesSlave.CustomerPaymentMethodEnumFk,
                TotalPaid = vmMarketingSalesSlave.TotalPaid != null ? vmMarketingSalesSlave.TotalPaid.Value : 0,
                ReturnAmount = vmMarketingSalesSlave.ReturnAmount,
                CustomerName = vmMarketingSalesSlave.CustomerName,
                CustomerMobile = vmMarketingSalesSlave.CustomerMobile,
                CustomerAddress = vmMarketingSalesSlave.CustomerAddress,
                CustomerPoint = vmMarketingSalesSlave.Point,
                Status = (int)EnumSaleStatus.DamageProduct,

                User = _vmLogin.Name,
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK

            };
            _db.Marketing_Sales.Add(marketingSales);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = marketingSales.ID;
            }

            return result;
        }

        public async Task<int> MarketingDamageProductSlaveAdd(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;
            //var vData = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == vM.Procurement_PurchaseRequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Merchandising_StyleID == vM.VMMerchandising_StyleID && x.Merchandising_StyleSlaveFK == vM.VMMerchandising_StyleSlaveID && x.Merchandising_BOFFK == vM.VMMerchandising_BOFID && x.Active)));
            //var pRSlave = _db.Common_Product.Find(vmMarketingSalesSlave.Common_ProductFK);
            Marketing_SalesSlave marketingSalesSlave = new Marketing_SalesSlave
            {
                Marketing_SalesFk = vmMarketingSalesSlave.Marketing_SalesFk,
                Common_ProductFK = vmMarketingSalesSlave.Common_ProductFK,
                Common_BrandFk = vmMarketingSalesSlave.Common_BrandFk,
                Common_ProductCategoryFk = vmMarketingSalesSlave.Common_ProductCategoryFK,
                Common_ProductSubCategoryFk = vmMarketingSalesSlave.Common_ProductSubCategoryFK,
                Common_SupplierFk = vmMarketingSalesSlave.Common_SupplierFk,
                ProductSpecification = vmMarketingSalesSlave.ProductSpecification,
                Quantity = vmMarketingSalesSlave.Quantity.Value,
                Remarks = vmMarketingSalesSlave.Description,
                MRPPrice = 0,
                CostingPrice = vmMarketingSalesSlave.CostingPrice,
                Point = 0,
                ProductDiscountValue = 0,
                ProductDiscountPercent = 0,
                SubTotal = 0,
                VAT = 0,
                VatValue = 0,
                PurchasePrice = 0,
                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK

            };
            _db.Marketing_SalesSlave.Add(marketingSalesSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = marketingSalesSlave.Marketing_SalesFk;
            }

            return result;
        }

        public async Task<(int id, bool isDiscounted)> MarketingDamageProductEdit(VMMarketingSalesSlave vmMarketingSalesSlave)
        {
            var result = -1;

            var salesSlave = _db.Marketing_SalesSlave.Where(x => x.Marketing_SalesFk == vmMarketingSalesSlave.Marketing_SalesFk && x.Active).AsEnumerable();
            Marketing_Sales marketingSales = await _db.Marketing_Sales.FindAsync(vmMarketingSalesSlave.Marketing_SalesFk);
            if (marketingSales != null)
            {
                marketingSales.Description = vmMarketingSalesSlave.Description;
                marketingSales.Common_CustomerFK = vmMarketingSalesSlave.Common_CustomerFK;
                marketingSales.IsDiscounted = vmMarketingSalesSlave.TotalInvoiceDiscount > 0 ? true : false;
                marketingSales.IsRedeemPoint = vmMarketingSalesSlave.RedeemPoint > 0 ? true : false;

                marketingSales.VatParcentage = 0;
                marketingSales.TotalVat = 0;
                marketingSales.TotalIncludingVat = vmMarketingSalesSlave.TotalIncludingVat;
                marketingSales.RedeemPoint = vmMarketingSalesSlave.RedeemPoint;


                marketingSales.DiscountParcentage = 0;
                marketingSales.TotalProductDiscount = 0;

                marketingSales.TotalInvoiceDiscount = 0;

                marketingSales.PayableAmount = vmMarketingSalesSlave.PayableAmount;
                marketingSales.CustomerMobile = vmMarketingSalesSlave.CustomerMobile;
                marketingSales.CustomerName = vmMarketingSalesSlave.CustomerName;
                marketingSales.CustomerAddress = vmMarketingSalesSlave.CustomerAddress;
                marketingSales.CustomerPoint = vmMarketingSalesSlave.Point;

                marketingSales.CustomerPaymentMethodEnumFk = vmMarketingSalesSlave.CustomerPaymentMethodEnumFk;
                marketingSales.TotalPaid = vmMarketingSalesSlave.TotalPaid != null ? vmMarketingSalesSlave.TotalPaid.Value : 0;
                marketingSales.ReturnAmount = vmMarketingSalesSlave.ReturnAmount;
                marketingSales.ActualInvoiceValue = vmMarketingSalesSlave.TotalPaid != null ? (vmMarketingSalesSlave.TotalPaid.Value - vmMarketingSalesSlave.ReturnAmount) : 0;
                marketingSales.Status = (int)EnumSaleStatus.DamageProduct;
                marketingSales.User = _vmLogin.Name;
                marketingSales.UserID = _vmLogin.ID;
                marketingSales.Common_ShopFK = _vmLogin.Common_ShopFK;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = marketingSales.ID;
                }
            }


            return (marketingSales.ID, marketingSales.IsDiscounted);



        }

    }
}
