﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KG.Core.Services.Home
{
    public class VMLogin : BaseVM
    {
        public string UserName { get; set; } 
        public string Password { get; set; }

        public string LblError { get; set; }
        public bool Locked { get;  set; }
        public int UserAccessLevelId { get; set; }
        public int UserDepartmentId { get; set; }
      

        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Common_ShopCounterFK { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }

        public string CompanyMobile { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyEmail { get; set; }

        public string Logo { get; set; }
        public string Manifesto { get; set; }
        public string ShopName { get; set; }
    }


}
