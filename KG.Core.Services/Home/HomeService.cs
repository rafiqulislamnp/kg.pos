﻿using KG.Core.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KG.Core.Services.Home
{
    public class HomeService : BaseService
    {
        private readonly HttpContext _httpContext;

        public HomeService(IPosDbContext db, HttpContext httpContext)
        {
            _db = db;
            _httpContext = httpContext;
        }
        public VMLogin CommonCompanyGet()
        {
            var vmHome = (from t1 in _db.Common_Company.Where(x => x.Active)
                          select new VMLogin
                          {
                              CompanyName = t1.Name,
                              Code = t1.ShortName,
                              CompanyAddress = t1.Address,
                              CompanyEmail = t1.Email,
                              Logo = t1.Logo,
                              CompanyMobile = t1.Phone,
                              Manifesto = t1.Manifesto
                          }).FirstOrDefault();

            return vmHome;
        }
        public VMLogin UserLogin(string userName, string password)
        {
            string hashedPassword = GetHashedPassword(password);
            int userId = 0;
            var v = (from t1 in _db.User_User
                     where t1.UserName == userName && t1.Password == hashedPassword && t1.Active
                     select new VMLogin
                     {
                         Name = t1.Name,
                         Photo = t1.Photo,
                         Common_ShopFK = t1.Common_ShopFK,
                         ShopName = t1.Common_ShopFK > 0? _db.Common_Shop.Where(x => x.ID == t1.Common_ShopFK).Select(x => x.Name).FirstOrDefault():"",
                         Email = t1.Email,
                         Address = t1.Address,
                         UserName = t1.UserName,
                         Mobile = t1.Mobile,

                         ID = t1.ID,
                         Locked = t1.Locked,
                         Password = t1.Password,
                         UserAccessLevelId = t1.User_AccessLevelFK,
                         UserDepartmentId = t1.User_DepartmentFK,
                         Common_ShopCounterFK = t1.Common_ShopCounterFK
                     }).FirstOrDefault();
            if (v != null)
            {
                userId = v.ID;
            }
            if (userId > 0)
            {
                if (v.Locked == true)
                {
                    v.LblError = "You are not authorised!";
                    return v;
                }
                else
                {
                    DefinePermission(userId);
                    return v;
                }
            }
            else
            {
                VMLogin vM = new VMLogin();
                vM.LblError = "User / Password does not match!";
                return vM;
            }

        }
        private void DefinePermission(int userId)
        {
            string tempMenu = GetMenu(userId);
            int tml = tempMenu.Length / 10;

            _httpContext.Session.SetString("Menu1", tempMenu.Substring(0, tml));
            _httpContext.Session.SetString("Menu2", tempMenu.Substring(tml, tml));
            _httpContext.Session.SetString("Menu3", tempMenu.Substring(tml * 2, tml));
            _httpContext.Session.SetString("Menu4", tempMenu.Substring(tml * 3, tml));
            _httpContext.Session.SetString("Menu5", tempMenu.Substring(tml * 4, tml));
            _httpContext.Session.SetString("Menu6", tempMenu.Substring(tml * 5, tml));
            _httpContext.Session.SetString("Menu7", tempMenu.Substring(tml * 6, tml));
            _httpContext.Session.SetString("Menu8", tempMenu.Substring(tml * 7, tml));
            _httpContext.Session.SetString("Menu9", tempMenu.Substring(tml * 8, tml));
            _httpContext.Session.SetString("Menu10", tempMenu.Substring(tml * 9, tempMenu.Length - (tml * 9)));

            string tempAMenu = GetAloneMethod(userId);
            tml = tempAMenu.Length / 10;

            _httpContext.Session.SetString("Alone1", tempAMenu.Substring(0, tml));
            _httpContext.Session.SetString("Alone2", tempAMenu.Substring(tml, tml));
            _httpContext.Session.SetString("Alone3", tempAMenu.Substring(tml * 2, tml));
            _httpContext.Session.SetString("Alone4", tempAMenu.Substring(tml * 3, tml));
            _httpContext.Session.SetString("Alone5", tempAMenu.Substring(tml * 4, tml));
            _httpContext.Session.SetString("Alone6", tempAMenu.Substring(tml * 5, tml));
            _httpContext.Session.SetString("Alone7", tempAMenu.Substring(tml * 6, tml));
            _httpContext.Session.SetString("Alone8", tempAMenu.Substring(tml * 7, tml));
            _httpContext.Session.SetString("Alone9", tempAMenu.Substring(tml * 8, tml));
            _httpContext.Session.SetString("Alone10", tempAMenu.Substring(tml * 9, tempAMenu.Length - (tml * 9)));
        }
        public string GetMenu(int userId)
        {
            string str = "";
            var v = (from t1 in _db.User_RoleMenuItem
                     join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     join t3 in _db.User_SubMenu on t2.User_SubMenuFk equals t3.ID
                     join t4 in _db.User_Menu on t3.User_MenuFk equals t4.ID
                     join t5 in _db.User_RoleAssign on t1.User_RoleFK equals t5.User_RoleFk
                     join t6 in _db.User_Role on t5.User_RoleFk equals t6.ID
                     where t2.IsAlone == false && t1.Active && t2.Active && t3.Active && t4.Active && t5.Active
                     && t1.IsAllowed == true && t5.User_UserFk == userId
                     group new { t1, t2, t3, t4, t5, t6 } by new { t3.User_MenuFk , t2.User_SubMenuFk, t1.User_MenuItemFk } into Group
                     select new
                     {
                         UserMenuItemID = Group.Key.User_MenuItemFk,
                         SUbMenuFK = Group.Key.User_SubMenuFk,
                         MenuFk = Group.Key.User_MenuFk,
                         Menu = Group.First().t4.Name,
                         SubMenu = Group.First().t3.Name,
                         ItemMenu = Group.First().t2.Name,
                         Method = Group.First().t2.Method,
                         MenuPriority = Group.First().t4.Priority,
                         SubMenuPriority = Group.First().t3.Priority,
                         MenuItemPriority = Group.First().t2.Priority
                     }).Distinct().OrderBy(x => x.MenuPriority).ThenBy(x => x.SubMenuPriority).ThenBy(x => x.MenuItemPriority).ToList(); 
            string Menu = "", SubMenu = "", Menuitem = "", Method = "";

            foreach (var a in v)
            {
                if (Menu != a.Menu)
                {
                    Menu = a.Menu;
                    str += "]" + a.Menu;
                }
                Menu = a.Menu;

                if (SubMenu != a.SubMenu)
                {
                    SubMenu = a.SubMenu;
                    str += "$" + a.SubMenu;
                }
                Menuitem = a.ItemMenu;
                Method = a.Method;
                str += "#" + Menuitem + "|" + Method + "^";
            }
            return str + "=";
        }
        //public string GetMenu(int userId)
        //{
        //    string str = "";
        //    var v = (from t1 in _db.User_RoleMenuItem
        //             join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
        //             join t3 in _db.User_SubMenu on t2.User_SubMenuFk equals t3.ID
        //             join t4 in _db.User_Menu on t3.User_MenuFk equals t4.ID
        //             join t5 in _db.User_RoleAssign on t1.User_RoleFK equals t5.User_RoleFk
        //             join t6 in _db.User_Role on t5.User_RoleFk equals t6.ID
        //             where t2.IsAlone == false && t1.Active && t2.Active && t3.Active && t4.Active && t5.Active
        //             && t1.IsAllowed == true && t5.User_UserFk == userId
        //             select new
        //             {
        //                 UserMenuItemID = t1.User_MenuItemFk,
        //                 SUbMenuFK = t2.User_SubMenuFk,
        //                 MenuFk = t3.User_MenuFk,
        //                 Menu = t4.Name,
        //                 SubMenu = t3.Name,
        //                 ItemMenu = t2.Name,
        //                 Method = t2.Method,
        //                 MenuPriority = t4.Priority,
        //                 SubMenuPriority = t3.Priority,
        //                 MenuItemPriority = t2.Priority
        //             }).Distinct().OrderBy(x => x.MenuPriority).ThenBy(x => x.SubMenuPriority).ThenBy(x => x.MenuItemPriority).AsEnumerable();

        //    var dataGrouped = (from data in v
        //                       group data by new { data.Menu, data.SubMenu, data.ItemMenu } into Group

        //                       select new
        //                       {
        //                           Menu = Group.Key.Menu,
        //                           SubMenu = Group.Key.SubMenu,
        //                           ItemMenu = Group.Key.ItemMenu,
        //                           Items = Group
        //                       }).ToList();


        //    string Menu = "", SubMenu = "", Menuitem = "", Method = "";

        //    foreach (var a in dataGrouped)
        //    {
        //        if (Menu != a.Menu)
        //        {
        //            Menu = a.Menu;
        //            str += "]" + a.Menu;
        //        }
        //        Menu = a.Menu;
        //        if (SubMenu != a.SubMenu)
        //        {
        //            SubMenu = a.SubMenu;
        //            str += "$" + a.SubMenu;
        //        }
        //        foreach (var item in a.Items)
        //        {
        //            Menuitem = item.ItemMenu;
        //            Method = item.Method;
        //            str += "#" + Menuitem + "|" + Method + "^";
        //        }
        //    }
        //    return str + "=";
        //}
        public static string FirstCharToUpper(string value)
        {
            char[] array = value.ToCharArray();
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
        public string GetAloneMethod(int userId)
        {

            string str = "";
            var v = (from t1 in _db.User_RoleMenuItem
                     join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     join t5 in _db.User_RoleAssign on t1.User_RoleFK equals t5.User_RoleFk
                     join t6 in _db.User_Role on t5.User_RoleFk equals t6.ID

                     //t1.User_RoleFK == (from userUser in _db.User_User where userUser.ID == userId select userUser.User_RoleFK).FirstOrDefault()
                     where t2.IsAlone == true
                              && t1.Active && t2.Active && t5.Active && t6.Active
                                && t1.IsAllowed == true && t5.User_UserFk == userId
                     select new
                     {
                         Method = t2.Method,
                         //Name = t3.Name

                     }).Distinct().ToList();
            foreach (var a in v)
            {
                string temp = a.Method.Replace("/", "@(");
                str += "@" + temp + ")|";
            }
            return str;
        }

        public bool CheckPermission(string Controller, string Method, string routeValue, string routeValue2)
        {

            bool flag = false;
            Controller = Controller.ToLower();
            Method = Method.ToLower();

            var MenuC1 = _httpContext.Session.GetString("Menu1");
            var MenuC2 = _httpContext.Session.GetString("Menu2");
            var MenuC3 = _httpContext.Session.GetString("Menu3");
            var MenuC4 = _httpContext.Session.GetString("Menu4");
            var MenuC5 = _httpContext.Session.GetString("Menu5");
            var MenuC6 = _httpContext.Session.GetString("Menu6");
            var MenuC7 = _httpContext.Session.GetString("Menu7");
            var MenuC8 = _httpContext.Session.GetString("Menu8");
            var MenuC9 = _httpContext.Session.GetString("Menu9");
            var MenuC10 = _httpContext.Session.GetString("Menu10");
            var AloneC1 = _httpContext.Session.GetString("Alone1");
            var AloneC2 = _httpContext.Session.GetString("Alone2");
            var AloneC3 = _httpContext.Session.GetString("Alone3");
            var AloneC4 = _httpContext.Session.GetString("Alone4");
            var AloneC5 = _httpContext.Session.GetString("Alone5");
            var AloneC6 = _httpContext.Session.GetString("Alone6");
            var AloneC7 = _httpContext.Session.GetString("Alone7");
            var AloneC8 = _httpContext.Session.GetString("Alone8");
            var AloneC9 = _httpContext.Session.GetString("Alone9");
            var AloneC10 = _httpContext.Session.GetString("Alone10");


            string menu = AloneC1.ToString().ToLower() + AloneC2.ToString().ToLower() + AloneC3.ToString().ToLower() + AloneC4.ToString().ToLower() + AloneC5.ToString().ToLower() + AloneC6.ToString().ToLower() + AloneC7.ToString().ToLower() + AloneC8.ToString().ToLower() + AloneC9.ToString().ToLower() + AloneC10.ToString().ToLower()
                       + MenuC1.ToString().ToLower() + MenuC2.ToString().ToLower() + MenuC3.ToString().ToLower() + MenuC4.ToString().ToLower() + MenuC5.ToString().ToLower() + MenuC6.ToString().ToLower() + MenuC7.ToString().ToLower() + MenuC8.ToString().ToLower() + MenuC9.ToString().ToLower() + MenuC10.ToString().ToLower();
            if (routeValue != null && routeValue2 != null)
            {
                string aloneUrlWithId = "@" + Controller + "@(" + Method + "/" + routeValue + "/" + routeValue2 + ")";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
                aloneUrlWithId = Controller + "/" + Method + "/" + routeValue + "/" + routeValue2 + "^";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
            }

            if (routeValue != null)
            {
                string aloneUrlWithId = "@" + Controller + "@(" + Method + "/" + routeValue + ")";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
                aloneUrlWithId = Controller + "/" + Method + "/" + routeValue + "^";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
            }

            if (menu.Contains("@" + Controller + "@(" + Method + ")"))
            {
                flag = true;
            }

            if (menu.Contains(Controller + "/" + Method + "^"))
            {
                flag = true;
            }
            return flag;
        }
        public string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

    }

    public class EnumModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }


}
