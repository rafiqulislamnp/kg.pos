﻿
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace KG.Core.Services
{
    public static class ReportType
    {
        public static string PDF = "PDF";
        public static string EXCEL = "EXCEL";
        public static string WORD = "WORD";
    }
    public static class SessionExtensionService
    {
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
    public static class Environment
    {
        public static bool IsDevelopment = false;
    }
    public enum UserAccessLevel
    {
        Basic = 1,
        Ultimate,
        Manager
    }

   
    public enum ActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Get,
        Attech,
        Approve,
        Close,
        UnApprove,
        ReOpen,
        Finalize,
        Acknowledgement
    }
    public enum StatusEnum
    {
        Draft = 1,
        Approved,
        Closed
    }
    public enum UserDepartment
    {
        Admin = 1,
        Procurement,
        WareHouse,
        Sales
    }
    public enum ShopTypeEnum
    {
        [Description("Super Shop")]
        SuperShop = 1,
        [Description("Express Shop")]
        ExpressShop
    }

    public static class BaseFunctionalities
    {
        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }

        public static List<object> GetEnumList<T>()
        {
            var list = new List<object>();

            foreach (var item in Enum.GetValues(typeof(T)))
            {
                list.Add(new { Value = (int)item, Text = GetEnumDescription((Enum)Enum.Parse(typeof(T), item.ToString())) });
            }

            return list;
        }

        internal static string GetEnumDescription(string originType)
        {
            throw new NotImplementedException();
        }
    }

    public abstract class BaseService
    {
        public IPosDbContext _db { get; set; }
    }
    public abstract class BaseVM
    {
        public string ReportName { get; set; }
        public string ReportType { get; set; }
        public int Common_ShopFK { get; set; }
        public int ID { get; set; }
        public string User { get; set; }
        public int UserID { get; set; }
        public DateTime Time { get; set; }
        public ActionEnum ActionEum { get { return (ActionEnum)this.ActionId; } }
        public int ActionId { get; set; } = 1;
        public bool IsActive { get; set; }
        public string Code { get; set; }
        public string error { get; set; } = "";
        public string DepartmentId { get; set; }
    }

    public enum EnumDepartment
    {
        Admin = 1,       
        Procurement = 2,
        WareHouse = 3,
        Sales = 4,


    }
    public enum PaymentMethodEnum
    {
        Cash = 1,      
        DBBL,
        City,
        bKash,
        Rocket,
        StaffDue
        
    }
   

    public enum EnumSaleStatus
    {
        Pending,
        Finalized,       
        DamageProduct
    }
    #region New Procurment Enum
    public enum EnumPOStatus
    {
        Draft,
        Submitted,
        Closed
    }
    public enum EnumPRStatus
    {
        Draft,
        Submitted,
        Closed
    }
   

    public enum POStatusInPR
    {
        [Description("PO not rise yet")]
        PONotRise,
        [Description("Partial PO Created")]
        PartialPOCreated,
        [Description("Full PO Created")]
        FullPOCreated


    }
    #endregion
}
