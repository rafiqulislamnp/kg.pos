﻿using KG.Core.Services;
using KG.Core.Services.Home;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;

using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Globalization;
using KG.Core.Entity.Configuration;
using KG.Core.Entity.WareHouse;
using System.Collections;
using KG.Core.Services.Marketing;

namespace KG.Core.Services.Configuration
{
    public class ConfigurationService : BaseService
    {
        private readonly HttpContext _httpContext;
        private readonly VMLogin _vmLogin;
        public ConfigurationService(IPosDbContext db, HttpContext httpContext)
        {
            _db = db;
            _httpContext = httpContext;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }
        public async Task<VMCommonProductCategory> GetSingleProductCategory(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Common_ProductCategory
                                          where t1.ID == id && t1.Active
                                          select new VMCommonProductCategory
                                          {
                                              ID = t1.ID,
                                              Name = t1.Name,
                                              IsLock = t1.IsLock,
                                              UserID = t1.UserID
                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMCommonProductSubCategory> GetSingleProductSubCategory(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Common_ProductSubCategory
                                          where t1.ID == id && t1.Active
                                          select new VMCommonProductSubCategory
                                          {
                                              ID = t1.ID,
                                              Name = t1.Name,
                                              IsLock = t1.IsLock,
                                              UserID = t1.UserID
                                          }).FirstOrDefault());
            return v;
        }

        #region Common Unit
        public async Task<VMCommonUnit> GetUnit()
        {
            VMCommonUnit vmCommonUnit = new VMCommonUnit();
            vmCommonUnit.DataList = await Task.Run(() => (from t1 in _db.Common_Unit
                                                          where t1.Active
                                                          select new VMCommonUnit
                                                          {
                                                              ID = t1.ID,
                                                              Name = t1.Name,
                                                              UserID = t1.UserID,
                                                              User = t1.User,
                                                              IsLock = t1.IsLock
                                                          }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonUnit;
        }
        public async Task<VMCommonUnit> GetSingleCommonUnit(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Common_Unit
                                          where t1.ID == id && t1.Active
                                          select new VMCommonUnit
                                          {
                                              ID = t1.ID,
                                              Name = t1.Name,
                                              IsLock = t1.IsLock,
                                              UserID = t1.UserID,


                                          }).FirstOrDefault());
            return v;
        }

        public async Task<int> UnitAdd(VMCommonUnit vmCommonUnit)
        {
            var result = -1;
            Common_Unit commonUnit = new Common_Unit
            {
                Name = vmCommonUnit.Name,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_Unit.Add(commonUnit);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonUnit.ID;
            }
            return result;
        }
        public async Task<int> UnitEdit(VMCommonUnit vmCommonUnit)
        {
            var result = -1;
            Common_Unit commonUnit = _db.Common_Unit.Find(vmCommonUnit.ID);
            commonUnit.Name = vmCommonUnit.Name;
            commonUnit.User = _vmLogin.Name;
            commonUnit.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonUnit.ID;
            }
            return result;
        }
        public async Task<int> UnitDelete(int id)
        {
            int result = -1;
            if (id != 0)
            {
                Common_Unit commonUnit = _db.Common_Unit.Find(id);
                commonUnit.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonUnit.ID;
                }
            }
            return result;
        }
        public async Task<int> UnitLockUnlock(int id)
        {
            var result = -1;
            Common_Unit commonUnit = await _db.Common_Unit.FindAsync(id);
            if (commonUnit != null)
            {
                if (commonUnit.IsLock)
                {
                    commonUnit.IsLock = false;
                }
                else
                {
                    commonUnit.IsLock = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonUnit.ID;
                }
            }
            return result;
        }

        public async Task<int> CommonProductCategoryLockUnlock(int id)
        {
            var result = -1;
            Common_ProductCategory commonProductCategory = await _db.Common_ProductCategory.FindAsync(id);
            if (commonProductCategory != null)
            {
                if (commonProductCategory.IsLock)
                {
                    commonProductCategory.IsLock = false;
                }
                else
                {
                    commonProductCategory.IsLock = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProductCategory.ID;
                }
            }
            return result;
        }

        public object GetAutoCompleteBrand(string prefix)
        {
            var v = (from t1 in _db.Common_Brand

                     where t1.Active && ((t1.Name.StartsWith(prefix)))

                     select new
                     {
                         label = t1.Name != null ? t1.Name : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(10).ToList();

            return v;
        }
        public object GetAutoCompleteSupplier(string prefix)
        {
            var v = (from t1 in _db.Common_Supplier

                     where t1.Active && ((t1.Name.StartsWith(prefix)))

                     select new
                     {
                         label = t1.Name != null ? t1.Name : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(10).ToList();

            return v;
        }
        public object GetAutoCompleteProductCategory(string prefix)
        {
            var v = (from t1 in _db.Common_ProductCategory

                     where t1.Active && ((t1.Name.StartsWith(prefix)))

                     select new
                     {
                         label = t1.Name != null ? t1.Name : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(10).ToList();

            return v;
        }
        public async Task<int> CommonProductSubCategoryLockUnlock(int id)
        {
            var result = -1;
            Common_ProductSubCategory commonProductSubCategory = await _db.Common_ProductSubCategory.FindAsync(id);
            if (commonProductSubCategory != null)
            {
                if (commonProductSubCategory.IsLock)
                {
                    commonProductSubCategory.IsLock = false;
                }
                else
                {
                    commonProductSubCategory.IsLock = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProductSubCategory.ID;
                }
            }
            return result;
        }
        public async Task<int> CommonProductLockUnlock(int id)
        {
            var result = -1;
            Common_Product commonProduct = await _db.Common_Product.FindAsync(id);
            if (commonProduct != null)
            {
                if (commonProduct.IsLock)
                {
                    commonProduct.IsLock = false;
                }
                else
                {
                    commonProduct.IsLock = true;
                }
                commonProduct.Time = DateTime.Now;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProduct.ID;
                }
            }
            return result;
        }
        public async Task<List<VMCommonProductSubCategory>> CommonProductSubCategoryGet(int id)
        {

            List<VMCommonProductSubCategory> vmCommonProductSubCategoryList = await Task.Run(() => (_db.Common_ProductSubCategory.Where(x => x.Active && x.Common_ProductCategoryFk == id)).Select(x => new VMCommonProductSubCategory() { ID = x.ID, Name = x.Name }).OrderBy(x => x.Name).ToListAsync());


            return vmCommonProductSubCategoryList;
        }
        public async Task<List<VMCommonDivisions>> CommonDivisionsGet(int id)
        {

            List<VMCommonDivisions> vmCommonDivisions = await Task.Run(() => (_db.Common_Divisions.Where(x => x.Active && x.Common_CountriesFk == id)).Select(x => new VMCommonDivisions() { ID = x.ID, Name = x.Name }).OrderBy(x => x.Name).ToListAsync());

            return vmCommonDivisions;
        }

        public async Task<List<VMCommonDistricts>> CommonDistrictsGet(int id)
        {

            List<VMCommonDistricts> vmCommonDistricts = await Task.Run(() => (_db.Common_Districts.Where(x => x.Active && x.Common_DivisionsFk == id)).Select(x => new VMCommonDistricts() { ID = x.ID, Name = x.Name }).OrderBy(x => x.Name).ToListAsync());

            return vmCommonDistricts;
        }


        #endregion

        #region Common Product Generic



        #endregion

        //public async Task<VMCommonCustomerContact> GetCustomerContact(int id)
        //{
        //    VMCommonCustomerContact vmCommonCustomerContact = new VMCommonCustomerContact();

        //    vmCommonCustomerContact = (from t1 in _db.Common_Customer

        //                               where t1.ID == id && t1.Active
        //                               select new VMCommonCustomerContact
        //                               {
        //                                   Common_CustomerFk = t1.ID,
        //                                   Name = t1.Name,
        //                                   Address = t1.Address,
        //                                   CustomerLoyalityPoint = t1.CustomerLoyalityPoint,
        //                                   Mobile = t1.Mobile,
        //                                   DOB = t1.DOB,
        //                                   Email = t1.Email,
        //                                   Sex = t1.Sex
        //                               }).FirstOrDefault();
        //    vmCommonCustomerContact.CustomerContactDataList = await Task.Run(() => (from t1 in _db.Common_CustomerContact

        //                                                                            where t1.Active && t1.Common_CustomerFk == id
        //                                                                            select new VMCommonCustomerContact
        //                                                                            {
        //                                                                                ID = t1.ID,
        //                                                                                ContactName = t1.Name,
        //                                                                                ContactAddress = t1.Address,
        //                                                                                ContactMobile = t1.Mobile,
        //                                                                                ContactEmail = t1.Email,

        //                                                                                User = t1.User,
        //                                                                            }).OrderByDescending(x => x.ID).AsEnumerable());
        //    return vmCommonCustomerContact;
        //}


        public async Task<VMCommonSupplier> GetSupplier()
        {
            VMCommonSupplier vmCommonSupplier = new VMCommonSupplier();
            vmCommonSupplier.DataList = await Task.Run(() => (from t1 in _db.Common_Supplier
                                                              join t2 in _db.Common_Thana on t1.Common_ThanaFk equals t2.ID
                                                              join t3 in _db.Common_Districts on t2.Common_DistrictsFk equals t3.ID
                                                              join t4 in _db.Common_Divisions on t3.Common_DivisionsFk equals t4.ID
                                                              join t5 in _db.Common_Countries on t4.Common_CountriesFk equals t5.ID

                                                              //join t6 in _db.Common_Shop on t1.Common_ShopFK equals t6.ID

                                                              where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active
                                                              select new VMCommonSupplier
                                                              {
                                                                  ID = t1.ID,
                                                                  Name = t1.Name,
                                                                  Email = t1.Email,
                                                                  Contact = t1.Contact,
                                                                  Address = t1.Address,
                                                                  Code = t1.Code,
                                                                  Common_DistrictsFk = t2.Common_DistrictsFk,
                                                                  Common_ThanaFk = t1.Common_ThanaFk,
                                                                  District = t3.Name,
                                                                  Thana = t2.Name,
                                                                  Common_CountriesFk = t4.ID,
                                                                  Country = t4.Name,
                                                                  UserID = t1.UserID,
                                                                  User = t1.User,
                                                                  ShopName = t3.Name,
                                                                  Remarks = t1.Remarks
                                                              }).OrderByDescending(x => x.ID).AsEnumerable());
            //if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            //{
            //    vmCommonSupplier.DataList = vmCommonSupplier.DataList.Where(x => x.Common_ShopFk == _vmLogin.Common_ShopFk).AsEnumerable();
            //}

            return vmCommonSupplier;
        }


        public async Task<int> SupplierAdd(VMCommonSupplier vmCommonSupplier)
        {
            var result = -1;
            #region Genarate Supplier code
            int totalSupplier = _db.Common_Supplier.Count();


            if (totalSupplier == 0)
            {
                totalSupplier = 1;
            }
            else
            {
                totalSupplier++;
            }

            var newString = totalSupplier.ToString().PadLeft(4, '0');
            #endregion
            Common_Supplier commonSupplier = new Common_Supplier
            {
                Name = vmCommonSupplier.Name,
                Common_ThanaFk = vmCommonSupplier.Common_ThanaFk,
                Address = vmCommonSupplier.Address,
                Contact = vmCommonSupplier.Contact,
                Email = vmCommonSupplier.Email,
                Remarks = vmCommonSupplier.Remarks,
                User = _vmLogin.Name,
                Code = newString,
                Common_ShopFK = _vmLogin.Common_ShopFK,
                UserID = _vmLogin.ID
            };
            _db.Common_Supplier.Add(commonSupplier);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonSupplier.ID;
            }
            return result;
        }


        public async Task<int> SupplierEdit(VMCommonSupplier vmCommonSupplier)
        {
            var result = -1;
            Common_Supplier commonSupplier = _db.Common_Supplier.Find(vmCommonSupplier.ID);
            commonSupplier.Name = vmCommonSupplier.Name;
            commonSupplier.Common_ThanaFk = vmCommonSupplier.Common_ThanaFk;
            commonSupplier.Address = vmCommonSupplier.Address;
            commonSupplier.Contact = vmCommonSupplier.Contact;
            commonSupplier.Email = vmCommonSupplier.Email;
            commonSupplier.Remarks = vmCommonSupplier.Remarks;
            commonSupplier.Common_ShopFK = _vmLogin.Common_ShopFK;

            commonSupplier.User = _vmLogin.Name;
            commonSupplier.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonSupplier.ID;
            }
            return result;
        }
        public async Task<int> SupplierDelete(int id)
        {
            int result = -1;
            if (id != 0)
            {
                Common_Supplier commonSupplier = _db.Common_Supplier.Find(id);
                commonSupplier.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonSupplier.ID;
                }
            }
            return result;
        }


        #region Common Product Category
        public async Task<VMCommonProductCategory> GetProductCategory()
        {
            VMCommonProductCategory vmCommonProductCategory = new VMCommonProductCategory();
            vmCommonProductCategory.DataList = await Task.Run(() => (from t1 in _db.Common_ProductCategory
                                                                     where t1.Active
                                                                     select new VMCommonProductCategory
                                                                     {
                                                                         ID = t1.ID,
                                                                         Name = t1.Name,
                                                                         IsLock = t1.IsLock,
                                                                         UserID = t1.UserID,
                                                                         User = t1.User,
                                                                         Code = t1.ShortName
                                                                     }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonProductCategory;
        }

        public async Task<int> ProductCategoryAdd(VMCommonProductCategory vmCommonProductCategory)
        {
            var result = -1;
            Common_ProductCategory commonProductCategory = new Common_ProductCategory
            {
                Name = vmCommonProductCategory.Name,
                ShortName = vmCommonProductCategory.Name.Substring(0, 2),
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_ProductCategory.Add(commonProductCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProductCategory.ID;
            }
            return result;
        }


        public async Task<int> ProductCategoryEdit(VMCommonProductCategory vmCommonProductCategory)
        {
            var result = -1;
            Common_ProductCategory commonProductCategory = _db.Common_ProductCategory.Find(vmCommonProductCategory.ID);
            commonProductCategory.Name = vmCommonProductCategory.Name;
            commonProductCategory.User = _vmLogin.Name;
            commonProductCategory.ShortName = vmCommonProductCategory.Name.Substring(0, 2);

            commonProductCategory.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProductCategory.ID;
            }
            return result;
        }

        public async Task<int> ProductCategoryDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_ProductCategory commonProductCategory = _db.Common_ProductCategory.Find(id);
                commonProductCategory.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProductCategory.ID;
                }
            }
            return result;
        }


        #endregion

        #region Common Brand
        public async Task<VMCommonBrand> GetCommonBrand()
        {
            VMCommonBrand vmCommonBrand = new VMCommonBrand();
            vmCommonBrand.DataList = await Task.Run(() => (from t1 in _db.Common_Brand
                                                           where t1.Active
                                                           select new VMCommonBrand
                                                           {
                                                               ID = t1.ID,
                                                               Name = t1.Name,
                                                               IsLock = t1.IsLock,

                                                               UserID = t1.UserID,
                                                               User = t1.User
                                                           }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonBrand;
        }

        public async Task<int> CommonBrandAdd(VMCommonBrand vmCommonBrand)
        {
            var result = -1;
            Common_Brand commonBrand = new Common_Brand
            {
                Name = vmCommonBrand.Name,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_Brand.Add(commonBrand);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonBrand.ID;
            }
            return result;
        }


        public async Task<int> CommonBrandEdit(VMCommonBrand vmCommonBrand)
        {
            var result = -1;
            Common_Brand commonBrand = _db.Common_Brand.Find(vmCommonBrand.ID);
            commonBrand.Name = vmCommonBrand.Name;
            commonBrand.User = _vmLogin.Name;
            commonBrand.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonBrand.ID;
            }
            return result;
        }

        public async Task<int> CommonBrandDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_Brand commonBrand = _db.Common_Brand.Find(id);
                commonBrand.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonBrand.ID;
                }
            }
            return result;
        }

        public async Task<int> CommonBrandLockUnlock(int id)
        {
            var result = -1;
            Common_Brand commonBrand = await _db.Common_Brand.FindAsync(id);
            if (commonBrand != null)
            {
                if (commonBrand.IsLock)
                {
                    commonBrand.IsLock = false;
                }
                else
                {
                    commonBrand.IsLock = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonBrand.ID;
                }
            }
            return result;
        }
        #endregion



        #region Common Product Subcategory
        public async Task<VMCommonProductSubCategory> GetProductSubCategory(int id)
        {
            VMCommonProductSubCategory vmCommonProductSubCategory = new VMCommonProductSubCategory();

            if (id > 0)
            {
                vmCommonProductSubCategory = await Task.Run(() => (from t1 in _db.Common_ProductCategory

                                                                   where t1.Active
                                                                   && t1.ID == id
                                                                   select new VMCommonProductSubCategory
                                                                   {
                                                                       Common_ProductCategoryFk = t1.ID,
                                                                       CategoryName = t1.Name,
                                                                       Code = t1.ShortName

                                                                   }).FirstOrDefaultAsync());
            }




            vmCommonProductSubCategory.DataList = await Task.Run(() => (from t1 in _db.Common_ProductSubCategory
                                                                        join t2 in _db.Common_ProductCategory on t1.Common_ProductCategoryFk equals t2.ID
                                                                        where t1.Active && t2.Active
                                                                        && id > 0 ? t1.Common_ProductCategoryFk == id : t1.Active
                                                                        select new VMCommonProductSubCategory
                                                                        {
                                                                            Common_ProductCategoryFk = t2.ID,
                                                                            CategoryName = t2.Name,
                                                                            Code = t1.ShortName,
                                                                            ID = t1.ID,
                                                                            Name = t1.Name,
                                                                            UserID = t1.UserID,
                                                                            IsLock = t1.IsLock,
                                                                            User = t1.User
                                                                        }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonProductSubCategory;
        }


        public async Task<int> ProductSubCategoryAdd(VMCommonProductSubCategory vmCommonProductSubCategory)
        {
            var result = -1;
            var categoryShortName = _db.Common_ProductCategory.Where(x => x.ID == vmCommonProductSubCategory.Common_ProductCategoryFk).Select(x => x.ShortName).FirstOrDefault();
            Common_ProductSubCategory commonProductSubCategory = new Common_ProductSubCategory
            {
                Name = vmCommonProductSubCategory.Name,
                ShortName = categoryShortName + vmCommonProductSubCategory.Name.Substring(0, 2),
                Common_ProductCategoryFk = vmCommonProductSubCategory.Common_ProductCategoryFk,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_ProductSubCategory.Add(commonProductSubCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProductSubCategory.ID;
            }
            return result;
        }
        public async Task<int> ProductSubCategoryEdit(VMCommonProductSubCategory vmCommonProductSubCategory)
        {
            var categoryShortName = _db.Common_ProductCategory.Where(x => x.ID == vmCommonProductSubCategory.Common_ProductCategoryFk).Select(x => x.ShortName).FirstOrDefault();

            var result = -1;
            Common_ProductSubCategory commonProductSubCategory = _db.Common_ProductSubCategory.Find(vmCommonProductSubCategory.ID);
            commonProductSubCategory.ShortName = categoryShortName + vmCommonProductSubCategory.Name.Substring(0, 2);
            commonProductSubCategory.Name = vmCommonProductSubCategory.Name;
            commonProductSubCategory.Common_ProductCategoryFk = vmCommonProductSubCategory.Common_ProductCategoryFk;
            commonProductSubCategory.User = _vmLogin.Name;
            commonProductSubCategory.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProductSubCategory.ID;
            }
            return result;
        }

        public string[] AddedProductInBin(int id)
        {
            string[] arrCount = null;
            var exRole = (from t1 in _db.Common_ProductInBinSlave
                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                          where t1.Common_BinSlaveFk == id && t1.Active && t2.Active
                          select new
                          {
                              ProductID = t1.Common_ProductFK,
                              Name = t2.Name
                          }).Distinct().ToList();

            if (exRole.Any())
            {
                arrCount = new string[exRole.Count];
                for (int i = 0; i < exRole.Count; i++)
                {
                    arrCount[i] = exRole[i].ProductID.ToString();
                }

            }
            return arrCount;
        }
        public async Task<int> ProductSubCategoryDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_ProductSubCategory commonProductSubCategory = _db.Common_ProductSubCategory.Find(id);
                commonProductSubCategory.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProductSubCategory.ID;
                }
            }
            return result;
        }

        public async Task<int> CommonProductInBinSlaveAdd(VMCommonBinSlave vmCommonBinSlave)
        {

            var result = -1;
            Common_ProductInBinSlave ommonProductInBinSlave = new Common_ProductInBinSlave
            {
                Common_ShopFK = vmCommonBinSlave.Common_ShopFK,
                Common_BinSlaveFk = vmCommonBinSlave.ID,
                Common_ProductFK = vmCommonBinSlave.Common_ProductFK

            };

            _db.Common_ProductInBinSlave.Add(ommonProductInBinSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;

        }
        public async Task<int> CommonProductInBinSlaveDelete(VMCommonBinSlave vmCommonBinSlave)
        {
            var result = -1;

            Common_ProductInBinSlave extProductInBinSlave = _db.Common_ProductInBinSlave.Find(vmCommonBinSlave.Common_ProductInBinSlaveFk);
            extProductInBinSlave.Active = false;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public List<object> ProductCategoryDropDownList()
        {
            var productCategoryList = new List<object>();
            var productCategoryes = _db.Common_ProductCategory.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in productCategoryes)
            {
                productCategoryList.Add(new { Text = x.Name, Value = x.ID });
            }
            return productCategoryList;
        }
        #endregion

        public class EnumModel
        {
            public int Value { get; set; }
            public string Text { get; set; }
        }
        public List<object> CommonCountriesDropDownList()
        {
            var list = new List<object>();
            var v = _db.Common_Countries.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }
        public List<object> CommonDistrictsDropDownList()
        {
            var list = new List<object>();
            var v = _db.Common_Districts.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }
        public List<object> CommonDivisionsDropDownList()
        {
            var list = new List<object>();
            var v = _db.Common_Divisions.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }
        public List<object> CommonThanaDropDownList()
        {
            var list = new List<object>();
            var v = _db.Common_Thana.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in v)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }

        public string UploadFile(IFormFile file, string folderName, string webRootPath)
        {
            string fName = "";
            if (file != null)
            {
                //string folderName = "Product";
                //string webRootPath = _webHostEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string exten = Path.GetFileName(file.FileName);
                    fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                    string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                    string fullPath = Path.Combine(newPath, fName);
                    FileStream stream;
                    using (stream = new FileStream(fullPath, FileMode.OpenOrCreate))
                    {
                        file.CopyTo(stream);
                        stream.Position = 0;
                    }


                }
            }
            return fName;
        }

        #region Common Product
        public async Task<VMCommonProduct> GetProduct(int productSubCategoryOrBrandFk, int productCategoryOrIsBrandFk)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            if (productCategoryOrIsBrandFk > 0 || (productSubCategoryOrBrandFk > 0 && productCategoryOrIsBrandFk == 0))
            {
                vmCommonProduct = await Task.Run(() => (from t2 in _db.Common_ProductCategory
                                                        join t3 in _db.Common_ProductSubCategory on t2.ID equals t3.Common_ProductCategoryFk
                                                        where t2.Active
                                                        && (productSubCategoryOrBrandFk > 0 && productCategoryOrIsBrandFk == 0) ? t3.ID == productSubCategoryOrBrandFk : t2.ID == productCategoryOrIsBrandFk
                                                        select new VMCommonProduct
                                                        {
                                                            CategoryName = t2.Name,
                                                            Common_ProductCategoryFk = t2.ID

                                                        }).FirstOrDefaultAsync());

            }

            if (productCategoryOrIsBrandFk == -1)
            {
                vmCommonProduct = await Task.Run(() => (from t2 in _db.Common_Brand
                                                        where t2.Active
                                                        && t2.ID == productSubCategoryOrBrandFk
                                                        select new VMCommonProduct
                                                        {
                                                            Brand = t2.Name,
                                                            Common_BrandFk = t2.ID

                                                        }).FirstOrDefaultAsync());

            }

            if (productCategoryOrIsBrandFk == -2)
            {
                vmCommonProduct = await Task.Run(() => (from t2 in _db.Common_Supplier
                                                        where t2.Active
                                                        && t2.ID == productSubCategoryOrBrandFk
                                                        select new VMCommonProduct
                                                        {
                                                            SupplierName = t2.Name,
                                                            Common_SupplierFk = t2.ID

                                                        }).FirstOrDefaultAsync());

            }
            vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.Common_Product
                                                             join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                                                             join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                                                             join t4 in _db.Common_Unit on t1.Common_UnitFk equals t4.ID
                                                             join t5 in _db.Common_Brand on t1.Common_BrandFk equals t5.ID into t5_Join
                                                             from t5 in t5_Join.DefaultIfEmpty()
                                                             join t7 in _db.Common_Supplier on t1.Common_SupplierFk equals t7.ID into t7_Join
                                                             from t7 in t7_Join.DefaultIfEmpty()
                                                             where t1.Active && t2.Active && t3.Active && t4.Active
                                                             && ((productCategoryOrIsBrandFk == -1) ? t1.Common_BrandFk == productSubCategoryOrBrandFk : (productSubCategoryOrBrandFk > 0 && productCategoryOrIsBrandFk == 0) ? t1.Common_ProductSubCategoryFk == productSubCategoryOrBrandFk : (productCategoryOrIsBrandFk > 0 && productSubCategoryOrBrandFk == 0) ? t2.Common_ProductCategoryFk == productCategoryOrIsBrandFk : (productCategoryOrIsBrandFk == -2) ? t1.Common_SupplierFk == productSubCategoryOrBrandFk : t1.Active)
                                                             select new VMCommonProduct
                                                             {
                                                                 ID = t1.ID,
                                                                 Name = t1.Name,
                                                                 MRPPrice = t1.MRPPrice,
                                                                 SubCategoryName = t2.Name,
                                                                 CategoryName = t3.Name,
                                                                 UnitName = t4.Name,
                                                                 Image = t1.Image,
                                                                 UserID = t1.UserID,
                                                                 User = t1.User,
                                                                 IsLock = t1.IsLock,
                                                                 Common_BrandFk = t1.Common_BrandFk,
                                                                 MaximumSalesLimit = t1.MaximumSalesLimit,
                                                                 DiscountValue = t1.DiscountValue,
                                                                 DiscountPercent = t1.DiscountPercent,
                                                                 DiscountExpiryDate = t1.DiscountExpiryDate,
                                                                 ProductBarcode = t1.ProductBarcode,
                                                                 Point = t1.Point,
                                                                 SystemBarcode = t1.SystemBarcode,
                                                                 VATPercent = t1.VATPercent,
                                                                 CostingPrice = t1.CostingPrice,
                                                                 ExpiryDate = t1.ExpiryDate,
                                                                 Brand = t5 != null ? t5.Name : "",
                                                                 SupplierName = t7 != null ? t7.Name : "",
                                                                 GPParcentage = t1.CostingPrice > 0 ? (((t1.MRPPrice - t1.CostingPrice) * 100) / t1.CostingPrice) : 0

                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonProduct;
        }

        public async Task<VMCommonProduct> GetDiscountedtProductList()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.Common_Product
                                                             join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                                                             join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                                                             join t4 in _db.Common_Unit on t1.Common_UnitFk equals t4.ID
                                                             join t5 in _db.Common_Brand on t1.Common_BrandFk equals t5.ID into t5_Join
                                                             from t5 in t5_Join.DefaultIfEmpty()
                                                             join t7 in _db.Common_Supplier on t1.Common_SupplierFk equals t7.ID into t7_Join
                                                             from t7 in t7_Join.DefaultIfEmpty()
                                                             where t1.Active && t2.Active && t3.Active && t4.Active
                                                             && t1.DiscountExpiryDate >= DateTime.Today
                                                             select new VMCommonProduct
                                                             {
                                                                 ID = t1.ID,
                                                                 Name = t1.Name,
                                                                 MRPPrice = t1.MRPPrice,
                                                                 SubCategoryName = t2.Name,
                                                                 CategoryName = t3.Name,
                                                                 UnitName = t4.Name,
                                                                 Image = t1.Image,
                                                                 UserID = t1.UserID,
                                                                 User = t1.User,
                                                                 IsLock = t1.IsLock,
                                                                 Common_BrandFk = t1.Common_BrandFk,
                                                                 MaximumSalesLimit = t1.MaximumSalesLimit,
                                                                 DiscountValue = t1.DiscountValue,
                                                                 DiscountPercent = t1.DiscountPercent,
                                                                 DiscountExpiryDate = t1.DiscountExpiryDate,
                                                                 ProductBarcode = t1.ProductBarcode,
                                                                 SystemBarcode = t1.SystemBarcode,
                                                                 CostingPrice = t1.CostingPrice,
                                                                 ExpiryDate = t1.ExpiryDate,
                                                                 Brand = t5 != null ? t5.Name : "",
                                                                 SupplierName = t7 != null ? t7.Name : ""
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonProduct;
        }

        public async Task<VMCommonProduct> GetAllProduct()
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();


            vmCommonProduct.DataList = await Task.Run(() => (from t1 in _db.Common_Product
                                                             join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                                                             join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                                                             join t4 in _db.Common_Unit on t1.Common_UnitFk equals t4.ID
                                                             join t5 in _db.Common_Brand on t1.Common_BrandFk equals t5.ID into t5_Join
                                                             from t5 in t5_Join.DefaultIfEmpty()
                                                             join t7 in _db.Common_Supplier on t1.Common_SupplierFk equals t7.ID into t7_Join
                                                             from t7 in t7_Join.DefaultIfEmpty()
                                                             where t1.Active && t2.Active && t3.Active && t4.Active
                                                             select new VMCommonProduct
                                                             {
                                                                 ID = t1.ID,
                                                                 Name = t1.Name,
                                                                 MRPPrice = t1.MRPPrice,
                                                                 SubCategoryName = t2.Name,
                                                                 CategoryName = t3.Name,
                                                                 UnitName = t4.Name,
                                                                 Image = t1.Image,
                                                                 UserID = t1.UserID,
                                                                 User = t1.User,
                                                                 IsLock = t1.IsLock,
                                                                 Common_BrandFk = t1.Common_BrandFk,
                                                                 MaximumSalesLimit = t1.MaximumSalesLimit,
                                                                 DiscountValue = t1.DiscountValue,
                                                                 DiscountPercent = t1.DiscountPercent,
                                                                 DiscountExpiryDate = t1.DiscountExpiryDate,
                                                                 ProductBarcode = t1.ProductBarcode,
                                                                 Point = t1.Point,
                                                                 SystemBarcode = t1.SystemBarcode,
                                                                 VATPercent = t1.VATPercent,
                                                                 CostingPrice = t1.CostingPrice,
                                                                 ExpiryDate = t1.ExpiryDate,
                                                                 Brand = t5 != null ? t5.Name : "",
                                                                 SupplierName = t7 != null ? t7.Name : "",
                                                                 GPParcentage = t1.CostingPrice > 0 ? (((t1.MRPPrice - t1.CostingPrice) * 100) / t1.CostingPrice) : 0

                                                             }).OrderBy(x => x.CategoryName).OrderBy(x => x.SubCategoryName).AsEnumerable());
            return vmCommonProduct;
        }
        public async Task<VMCommonProduct> GetProductByBinSlaveId(int binSlaveID)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();
            vmCommonProduct.DataList = await Task.Run(() => (from t0 in _db.Common_ProductInBinSlave.Where(x => x.Common_BinSlaveFk == binSlaveID && x.Active)
                                                             join t1 in _db.Common_Product on t0.Common_ProductFK equals t1.ID
                                                             join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                                                             join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                                                             join t4 in _db.Common_Unit on t1.Common_UnitFk equals t4.ID
                                                             join t5 in _db.Common_Brand on t1.Common_BrandFk equals t5.ID into t5_Join
                                                             from t5 in t5_Join.DefaultIfEmpty()
                                                             join t7 in _db.Common_Supplier on t1.Common_SupplierFk equals t7.ID into t7_Join
                                                             from t7 in t7_Join.DefaultIfEmpty()
                                                             where t1.Active && t2.Active && t3.Active && t4.Active

                                                             select new VMCommonProduct
                                                             {
                                                                 ID = t0.ID,
                                                                 Common_ProductInBinSlaveFk = t0.ID,
                                                                 Name = t1.Name,
                                                                 MRPPrice = t1.MRPPrice,
                                                                 SubCategoryName = t2.Name,
                                                                 CategoryName = t3.Name,
                                                                 UnitName = t4.Name,
                                                                 Image = t1.Image,
                                                                 UserID = t1.UserID,
                                                                 User = t1.User,
                                                                 IsLock = t1.IsLock,
                                                                 Common_BrandFk = t1.Common_BrandFk,
                                                                 MaximumSalesLimit = t1.MaximumSalesLimit,
                                                                 DiscountValue = t1.DiscountValue,
                                                                 DiscountPercent = t1.DiscountPercent,
                                                                 DiscountExpiryDate = t1.DiscountExpiryDate,
                                                                 ProductBarcode = t1.ProductBarcode,
                                                                 Point = t1.Point,
                                                                 SystemBarcode = t1.SystemBarcode,
                                                                 VATPercent = t1.VATPercent,
                                                                 CostingPrice = t1.CostingPrice,
                                                                 ExpiryDate = t1.ExpiryDate,
                                                                 Brand = t5 != null ? t5.Name : "",
                                                                 SupplierName = t7 != null ? t7.Name : "",
                                                                 GPParcentage = t1.CostingPrice > 0 ? (((t1.MRPPrice - t1.CostingPrice) * 100) / t1.CostingPrice) : 0

                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonProduct;
        }

        public async Task<int> ProductAdd(VMCommonProduct vmCommonProduct)
        {
            var result = -1;
            #region Genarate Product No

            string subCatShortName = _db.Common_ProductCategory.Where(x => x.ID == vmCommonProduct.Common_ProductSubCategoryFk).Select(x => x.ShortName.ToUpper()).FirstOrDefault();


            int lsatProduct = _db.Common_Product.Select(x => x.ID).OrderByDescending(ID => ID).FirstOrDefault();
            if (lsatProduct == 0)
            {
                lsatProduct = 1;
            }
            else
            {
                lsatProduct++;
            }

            var productID = lsatProduct.ToString().PadLeft(6, '0');


            #endregion

            Common_Product commonProduct = new Common_Product
            {
                ProductID = productID,
                Name = vmCommonProduct.Name,

                CBTID = vmCommonProduct.CBTID,
                CMPIDX = vmCommonProduct.CMPIDX,
                CSSID = vmCommonProduct.CSSID,
                SSID = vmCommonProduct.SSID,



                DisContinued = vmCommonProduct.DisContinued,
                Point = vmCommonProduct.Point,
                ProductBarcode = vmCommonProduct.ProductBarcode,

                MaximumSalesLimit = vmCommonProduct.MaximumSalesLimit,


                SystemBarcode = vmCommonProduct.SystemBarcode,
                VATPercent = vmCommonProduct.VATPercent,
                DiscountExpiryDate = vmCommonProduct.DiscountExpiryDate,
                DiscountPercent = vmCommonProduct.DiscountPercent,
                DiscountValue = vmCommonProduct.DiscountValue,
                CostingPrice = vmCommonProduct.CostingPrice,
                ExpiryDate = vmCommonProduct.ExpiryDate,
                IsLock = vmCommonProduct.IsLock,
                Common_BrandFk = vmCommonProduct.Common_BrandFk,

                Common_SupplierFk = vmCommonProduct.Common_SupplierFk,
                Common_ProductSubCategoryFk = vmCommonProduct.Common_ProductSubCategoryFk,
                Common_ShopFK = _vmLogin.Common_ShopFK,
                Common_UnitFk = vmCommonProduct.Common_UnitFk,
                Image = vmCommonProduct.Image,

                MRPPrice = vmCommonProduct.MRPPrice,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_Product.Add(commonProduct);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }
            return result;
        }
        public async Task<int> ProductEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;

            #region Genarate Product No

            string subCatShortName = _db.Common_ProductCategory.Where(x => x.ID == vmCommonProduct.Common_ProductSubCategoryFk).Select(x => x.ShortName.ToUpper()).FirstOrDefault();


            int lsatProduct = _db.Common_Product.Select(x => x.ID).OrderByDescending(ID => ID).FirstOrDefault();
            if (lsatProduct == 0)
            {
                lsatProduct = 1;
            }
            else
            {
                lsatProduct++;
            }

            var productID = lsatProduct.ToString().PadLeft(6, '0');


            #endregion
            Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);

            commonProduct.ProductID = productID;
            commonProduct.Name = vmCommonProduct.Name;
            commonProduct.CBTID = vmCommonProduct.CBTID;
            commonProduct.CMPIDX = vmCommonProduct.CMPIDX;
            commonProduct.CSSID = vmCommonProduct.CSSID;
            commonProduct.SSID = vmCommonProduct.SSID;
            commonProduct.DisContinued = vmCommonProduct.DisContinued;


            commonProduct.Point = vmCommonProduct.Point;
            commonProduct.ProductBarcode = vmCommonProduct.ProductBarcode;
            commonProduct.MaximumSalesLimit = vmCommonProduct.MaximumSalesLimit;
            commonProduct.SystemBarcode = vmCommonProduct.SystemBarcode;
            commonProduct.VATPercent = vmCommonProduct.VATPercent;
            commonProduct.DiscountExpiryDate = vmCommonProduct.DiscountExpiryDate;
            commonProduct.DiscountPercent = vmCommonProduct.DiscountPercent;
            commonProduct.DiscountValue = vmCommonProduct.DiscountValue;
            commonProduct.CostingPrice = vmCommonProduct.CostingPrice;
            commonProduct.ExpiryDate = vmCommonProduct.ExpiryDate;
            commonProduct.IsLock = vmCommonProduct.IsLock;
            commonProduct.Common_BrandFk = vmCommonProduct.Common_BrandFk;
            commonProduct.Common_SupplierFk = vmCommonProduct.Common_SupplierFk;
            commonProduct.Common_ProductSubCategoryFk = vmCommonProduct.Common_ProductSubCategoryFk;
            commonProduct.Common_ShopFK = _vmLogin.Common_ShopFK;
            commonProduct.Common_UnitFk = vmCommonProduct.Common_UnitFk;
            commonProduct.Image = vmCommonProduct.Image;
            commonProduct.MRPPrice = vmCommonProduct.MRPPrice;
            commonProduct.User = _vmLogin.Name;
            commonProduct.UserID = _vmLogin.ID;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }
            return result;
        }

        public async Task<int> ProductVatEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;
            if (vmCommonProduct.ID > 0)
            {
                Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);
                commonProduct.VATPercent = vmCommonProduct.VATPercent;
                commonProduct.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat;

            }
            else if (vmCommonProduct.Common_ProductSubCategoryFk > 0)
            {
                List<Common_Product> commonProductList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == vmCommonProduct.Common_ProductSubCategoryFk).ToList();
                commonProductList.ForEach(x => x.VATPercent = vmCommonProduct.VATPercent);
                //commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);


            }
            else if (vmCommonProduct.Common_ProductCategoryFk > 0)
            {
                List<Common_Product> commonProductList = new List<Common_Product>();
                var subCategores = _db.Common_ProductSubCategory.Where(x => x.Common_ProductCategoryFk == vmCommonProduct.Common_ProductCategoryFk).Select(x => x.ID).ToList();
                foreach (var subCategoryId in subCategores)
                {
                    List<Common_Product> productList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == subCategoryId).ToList();
                    commonProductList.AddRange(productList);
                }
                commonProductList.ForEach(x => x.VATPercent = vmCommonProduct.VATPercent);
                // commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);



            }
            else if (vmCommonProduct.Common_BrandFk > 0)
            {
                List<Common_Product> commonProductList = new List<Common_Product>();
                commonProductList = _db.Common_Product.Where(x => x.Common_BrandFk == vmCommonProduct.Common_BrandFk).ToList();

                commonProductList.ForEach(x => x.VATPercent = vmCommonProduct.VATPercent);
                // commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);



            }


            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }

        public async Task<int> EnableOrDisableProductVatEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;
            if (vmCommonProduct.ID > 0)
            {
                Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);
                commonProduct.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat;
            }
            else if (vmCommonProduct.Common_ProductSubCategoryFk > 0)
            {
                List<Common_Product> commonProductList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == vmCommonProduct.Common_ProductSubCategoryFk).ToList();
                commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);
            }
            else if (vmCommonProduct.Common_ProductCategoryFk > 0)
            {
                List<Common_Product> commonProductList = new List<Common_Product>();
                var subCategores = _db.Common_ProductSubCategory.Where(x => x.Common_ProductCategoryFk == vmCommonProduct.Common_ProductCategoryFk).Select(x => x.ID).ToList();
                foreach (var subCategoryId in subCategores)
                {
                    List<Common_Product> productList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == subCategoryId).ToList();
                    commonProductList.AddRange(productList);
                }
                commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);
            }
            else if (vmCommonProduct.Common_BrandFk > 0)
            {
                List<Common_Product> commonProductList = new List<Common_Product>();
                commonProductList = _db.Common_Product.Where(x => x.Common_BrandFk == vmCommonProduct.Common_BrandFk).ToList();

                commonProductList.ForEach(x => x.EnableOrDisableProductVat = vmCommonProduct.EnableOrDisableProductVat);



            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }

        public async Task<int> ProductDiscountEdit(VMCommonProduct vmCommonProduct)
        {


            var result = -1;
            List<Common_Product> commonProductList = new List<Common_Product>();
            if (vmCommonProduct.Common_SupplierFk > 0 && vmCommonProduct.Common_BrandFk == 0)
            {
                commonProductList = _db.Common_Product.Where(x => x.Common_SupplierFk == vmCommonProduct.Common_SupplierFk).ToList();
            }
            else if (vmCommonProduct.Common_SupplierFk == 0 && vmCommonProduct.Common_BrandFk > 0)
            {
                commonProductList = _db.Common_Product.Where(x => x.Common_BrandFk == vmCommonProduct.Common_BrandFk).ToList();
            }
            else if (vmCommonProduct.Common_SupplierFk > 0 && vmCommonProduct.Common_BrandFk > 0)
            {
                commonProductList = _db.Common_Product.Where(x => x.Common_BrandFk == vmCommonProduct.Common_BrandFk && x.Common_SupplierFk == vmCommonProduct.Common_SupplierFk).ToList();
            }
            if (commonProductList.Any())
            {
                if (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0)
                {
                    List<Common_Product> tempProductList = new List<Common_Product>();

                    var subCategores = _db.Common_ProductSubCategory.Where(x => x.Common_ProductCategoryFk == vmCommonProduct.Common_ProductCategoryFk).Select(x => x.ID).ToList();
                    foreach (var subCategoryId in subCategores)
                    {
                        var sortedData = commonProductList.Where(x => x.Common_ProductSubCategoryFk == subCategoryId).ToList();
                        tempProductList.AddRange(sortedData);
                    }
                    commonProductList = tempProductList;

                }
                else if (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0)
                {
                    commonProductList = commonProductList.Where(x => x.Common_ProductSubCategoryFk == vmCommonProduct.Common_ProductSubCategoryFk).ToList();
                }
                else if (vmCommonProduct.ID > 0)
                {
                    commonProductList = commonProductList.Where(x => x.ID == vmCommonProduct.ID).ToList();
                }
            }
            else
            {
                if (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk == 0 && vmCommonProduct.ID == 0)
                {
                    var subCategores = _db.Common_ProductSubCategory.Where(x => x.Common_ProductCategoryFk == vmCommonProduct.Common_ProductCategoryFk).Select(x => x.ID).ToList();
                    foreach (var subCategoryId in subCategores)
                    {
                        List<Common_Product> productList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == subCategoryId).ToList();
                        commonProductList.AddRange(productList);
                    }
                }
                else if (vmCommonProduct.Common_ProductCategoryFk > 0 && vmCommonProduct.Common_ProductSubCategoryFk > 0 && vmCommonProduct.ID == 0)
                {
                    commonProductList = _db.Common_Product.Where(x => x.Common_ProductSubCategoryFk == vmCommonProduct.Common_ProductSubCategoryFk).ToList();
                }
                else if (vmCommonProduct.ID > 0)
                {
                    commonProductList = _db.Common_Product.Where(x => x.ID == vmCommonProduct.ID).ToList();
                }
            }

            commonProductList.ForEach(x =>
            {
                x.DiscountPercent = vmCommonProduct.DiscountPercent;
                x.DiscountExpiryDate = vmCommonProduct.DiscountExpiryDate;
                x.DiscountValue = decimal.Multiply((decimal.Divide(x.MRPPrice, 100)), vmCommonProduct.DiscountPercent);
            });
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<int> ProductExpiryDateEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;
            Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);
            commonProduct.ExpiryDate = vmCommonProduct.ExpiryDate;

            commonProduct.Time = DateTime.Now;
            commonProduct.User = _vmLogin.Name;
            commonProduct.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }
            return result;
        }

        //public async Task<int> ProductDiscountEdit(VMCommonProduct vmCommonProduct)
        //{
        //    var result = -1;
        //    Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);
        //    commonProduct.DiscountValue = vmCommonProduct.DiscountValue;
        //    commonProduct.DiscountExpiryDate = vmCommonProduct.DiscountExpiryDate;

        //    commonProduct.Time = DateTime.Now;
        //    commonProduct.User = _vmLogin.Name;
        //    commonProduct.UserID = _vmLogin.ID;

        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = commonProduct.ID;
        //    }
        //    return result;
        //}

        public async Task<int> ProductGRNEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;


            var priviousStockQty = _db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == vmCommonProduct.ID && x.ID != vmCommonProduct.WareHouse_POReceivingSlaveFk).Select(x => x.ReceivedQuantity - x.Damage).DefaultIfEmpty(0).Sum()
                                    - _db.Marketing_SalesSlave.Where(x => x.Common_ProductFK == vmCommonProduct.ID).Select(x => x.Quantity).DefaultIfEmpty(0).Sum(); // need diduct sales quantity

            Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);

            var priviousStockPrice = (priviousStockQty * commonProduct.CostingPrice);
            var priviousPurchasingPrice = (priviousStockQty * commonProduct.PurchasePrice);


            commonProduct.CostingPrice = ((((vmCommonProduct.ReceivedQuantity * vmCommonProduct.PurchasePrice) + priviousStockPrice) +
                                            (vmCommonProduct.LaborCost + vmCommonProduct.ManufacturingOverhead + vmCommonProduct.TransportationOverhead + vmCommonProduct.OthersCost)) /
                                             ((vmCommonProduct.ReceivedQuantity - vmCommonProduct.DamageQuantity) + priviousStockQty));


            commonProduct.PurchasePrice = (((vmCommonProduct.ReceivedQuantity * vmCommonProduct.PurchasePrice) + priviousPurchasingPrice) /
                                             ((vmCommonProduct.ReceivedQuantity - vmCommonProduct.DamageQuantity) + priviousStockQty));
            commonProduct.Time = DateTime.Now;
            commonProduct.User = _vmLogin.Name;
            commonProduct.UserID = _vmLogin.ID;


            Common_ProductCosting common_ProductCosting = new Common_ProductCosting
            {
                Common_ProductFK = vmCommonProduct.ID,
                LaborCost = vmCommonProduct.LaborCost,
                ManufacturingOverhead = vmCommonProduct.ManufacturingOverhead,
                TransportationOverhead = vmCommonProduct.TransportationOverhead,
                OthersCost = vmCommonProduct.OthersCost,
                OthersCostNote = vmCommonProduct.OthersCostNote,
                WareHouse_POReceivingSlaveFk = vmCommonProduct.WareHouse_POReceivingSlaveFk,
                CostingPrice = commonProduct.CostingPrice,
                PurchasePrice = commonProduct.PurchasePrice,

            };
            _db.Common_ProductCosting.Add(common_ProductCosting);

            WareHouse_POReceivingSlave poReceivingSlave = _db.WareHouse_POReceivingSlave.Find(vmCommonProduct.WareHouse_POReceivingSlaveFk);
            poReceivingSlave.IsGRNCompleted = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }

            return result;
        }
        public async Task<int> ProductDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_Product commonProduct = _db.Common_Product.Find(id);
                commonProduct.Active = false;
                commonProduct.Time = DateTime.Now;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonProduct.ID;
                }
            }
            return result;
        }

        public List<object> ProductSubCategoryDropDownList()
        {
            var productSubCategoryList = new List<object>();
            var productSubCategory = _db.Common_ProductSubCategory.Where(a => a.Active == true).ToList();
            foreach (var x in productSubCategory)
            {
                productSubCategoryList.Add(new { Text = x.Name, Value = x.ID });
            }
            return productSubCategoryList;
        }


        public List<object> UnitDropDownList()
        {
            var UnitList = new List<object>();
            var Units = _db.Common_Unit.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in Units)
            {
                UnitList.Add(new { Text = x.Name, Value = x.ID });
            }
            return UnitList;
        }


        public List<object> SupplierDropDownList()
        {
            var UnitList = new List<object>();
            var Units = _db.Common_Supplier.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in Units)
            {
                UnitList.Add(new { Text = x.Name + " [" + x.Code + "]", Value = x.ID });
            }
            return UnitList;
        }
        public List<object> BrandDropDownList()
        {
            var UnitList = new List<object>();
            var Units = _db.Common_Brand.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in Units)
            {
                UnitList.Add(new { Text = x.Name, Value = x.ID });
            }
            return UnitList;
        }
        public VMCommonProduct GetCommonProductInfoToBarcodeGenarate(VMCommonProduct vmCommonProduct)
        {
            VMCommonProduct productModel = new VMCommonProduct();
            productModel.DataToListProduct = new List<VMCommonProduct>();

            foreach (var item in vmCommonProduct.DataToList.Where(x => x.QtyOfBarcodeGen > 0).ToList())
            {
                var product = GetCommonProductBarcodeByID(item.ID);
                for (int i = 0; i < item.QtyOfBarcodeGen; i++)
                {
                    productModel.DataToListProduct.Add(product);
                }
            }

            return productModel;
        }

        public VMCommonProduct GetCommonProductBarcodeByID(int id)
        {
            var v = (from t1 in _db.Common_Product
                     join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                     join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID

                     where t1.ID == id
                     select new VMCommonProduct
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         MRPPrice = t1.MRPPrice,
                         SystemBarcode = t1.SystemBarcode,
                         SubCategoryName = t2.Name,
                         CategoryName = t3.Name
                     }).FirstOrDefault();
            return v;
        }
        public VMCommonProduct GetCommonProductByID(int id)
        {
            var v = (from t1 in _db.Common_Product
                     join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                     join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID
                     join t4 in _db.Common_Unit on t1.Common_UnitFk equals t4.ID
                     join t5 in _db.Common_Brand on t1.Common_BrandFk equals t5.ID into t5_Join
                     from t5 in t5_Join.DefaultIfEmpty()
                     join t8 in _db.Common_Supplier on t1.Common_SupplierFk equals t8.ID into t8_Join
                     from t8 in t8_Join.DefaultIfEmpty()

                     where t1.ID == id
                     select new VMCommonProduct
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         ProductID = t1.ProductID,
                         Brand = t5.Name,
                         CBTID = t1.CBTID,
                         CMPIDX = t1.CMPIDX,

                         CSSID = t1.CSSID,
                         DisContinued = t1.DisContinued,
                         MaximumSalesLimit = t1.MaximumSalesLimit,
                         Point = t1.Point,
                         SSID = t1.SSID,
                         ProductBarcode = t1.ProductBarcode,
                         SupplierName = t8.Name,
                         VATPercent = t1.VATPercent,
                         DiscountValue = t1.DiscountValue,
                         CostingPrice = t1.CostingPrice,
                         Common_ProductCategoryFk = t3.ID,
                         Common_ProductSubCategoryFk = t1.Common_ProductSubCategoryFk,
                         DiscountExpiryDate = t1.DiscountExpiryDate,
                         Common_SupplierFk = t1!= null? t1.Common_SupplierFk:0,

                         Common_BrandFk = t1.Common_BrandFk,
                         DiscountPercent = t1.DiscountPercent,
                         Common_UnitFk = t1.Common_UnitFk,
                         MRPPrice = t1.MRPPrice,
                         //Description = t1.Description,
                         Image = t1.Image,
                         SystemBarcode = t1.SystemBarcode,
                         ExpiryDate = t1.ExpiryDate,
                         UnitName = t4.Name,
                         CategoryName = t3.Name,
                         SubCategoryName = t2.Name
                     }).FirstOrDefault();
            return v;
        }

        public VMCommonProduct GetCommonProductByChallanID(int id)
        {

            VMCommonProduct productModel = new VMCommonProduct();
            productModel.DataToListProduct = new List<VMCommonProduct>();
            var productList = GetCommonProductBarcodeByByChallanID(id);

            foreach (var item in productList.Where(x => x.QtyOfBarcodeGen > 0).ToList())
            {
                var product = GetCommonProductBarcodeByID(item.ID);
                for (int i = 0; i < item.QtyOfBarcodeGen; i++)
                {
                    productModel.DataToListProduct.Add(product);
                }
            }
            return productModel;
        }
        public IEnumerable<VMCommonProduct> GetCommonProductBarcodeByByChallanID(int id)
        {
            var v = (from t0 in _db.WareHouse_POReceivingSlave
                     join t1 in _db.Common_Product on t0.Common_ProductFK equals t1.ID
                     join t2 in _db.Common_ProductSubCategory on t1.Common_ProductSubCategoryFk equals t2.ID
                     join t3 in _db.Common_ProductCategory on t2.Common_ProductCategoryFk equals t3.ID

                     where t0.WareHouse_POReceivingFk == id
                     select new VMCommonProduct
                     {
                         ID = t1.ID,
                         Name = t1.Name,

                         MRPPrice = t1.MRPPrice,
                         SystemBarcode = t1.SystemBarcode,
                         CategoryName = t3.Name,
                         SubCategoryName = t2.Name,
                         QtyOfBarcodeGen = ((int)t0.ReceivedQuantity)
                     }).AsEnumerable();
            return v;
        }
        public string MakeSystemBarcode(int productCategoryFk)
        {
            string subCatShortName = _db.Common_ProductCategory.Where(x => x.ID == productCategoryFk).Select(x => x.ShortName.ToUpper()).FirstOrDefault();
            int lsatProduct = _db.Common_Product.Select(x => x.ID).LastOrDefault();
            if (lsatProduct == 0)
            {
                lsatProduct = 1;
            }
            else
            {
                lsatProduct++;
            }

            var systemBarcode = subCatShortName + lsatProduct.ToString().PadLeft(6, '0');

            return systemBarcode;
        }

        public VMCommonProduct GetPOReceivingSlaveByID(int receivingId, int productId)
        {
            var v = (from t1 in _db.WareHouse_POReceivingSlave
                     where t1.WareHouse_POReceivingFk == receivingId && t1.Common_ProductFK == productId
                     select new VMCommonProduct
                     {
                         WareHouse_POReceivingSlaveFk = t1.ID,
                         DamageQuantity = t1.Damage,
                         PurchasePrice = t1.PurchasingPrice,
                         ReceivedQuantity = t1.ReceivedQuantity,
                         TotalPrice = ((t1.ReceivedQuantity - t1.Damage) * t1.PurchasingPrice),
                         //PreviousStock need diduct sales quantity
                         PreviousStock = _db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == t1.Common_ProductFK && x.ID != t1.ID).Select(x => x.ReceivedQuantity - x.Damage).DefaultIfEmpty(0).Sum()

                     }).FirstOrDefault();
            return v;
        }
        public VMCommonSupplier GetCommonSupplierByID(int id)
        {
            var v = (from t1 in _db.Common_Supplier.Where(x => x.ID == id)
                     join t2 in _db.Common_Thana on t1.Common_ThanaFk equals t2.ID
                     join t3 in _db.Common_Districts on t2.Common_DistrictsFk equals t3.ID
                     join t4 in _db.Common_Divisions on t3.Common_DivisionsFk equals t4.ID
                     join t5 in _db.Common_Countries on t4.Common_CountriesFk equals t5.ID
                     where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active
                     select new VMCommonSupplier
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         Email = t1.Email,
                         Contact = t1.Contact,
                         Address = t1.Address,
                         Code = t1.Code,
                         Common_DistrictsFk = t2.Common_DistrictsFk,
                         Common_ThanaFk = t1.Common_ThanaFk,
                         Common_DivisionFk = t3.Common_DivisionsFk,

                         District = t3.Name,
                         Thana = t2.Name,
                         Common_CountriesFk = t4.ID,
                         Country = t4.Name,
                         UserID = t1.UserID,
                         User = t1.User,
                         ShopName = t3.Name,
                         Remarks = t1.Remarks,
                         CreditPeriod = t1.CreditPeriod,
                         BiltoBilCreditPeriod = t1.BilltoBillCreditPeriod,
                         Fax = t1.Fax
                     }).FirstOrDefault();
            return v;
        }
        public VMCommonShop GetCommonShopByID(int id)
        {
            var v = (from t1 in _db.Common_Shop
                     join t2 in _db.Common_Thana on t1.Common_ThanaFk equals t2.ID
                     join t3 in _db.Common_Districts on t2.Common_DistrictsFk equals t3.ID
                     join t4 in _db.Common_Divisions on t3.Common_DivisionsFk equals t4.ID
                     join t5 in _db.Common_Countries on t4.Common_CountriesFk equals t5.ID

                     where t1.ID == id && t1.Active && t2.Active && t3.Active
                     select new VMCommonShop
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         Address = t1.Address,
                         Contact = t1.Contact,
                         Description = t1.Description,
                         Common_ThanaFk = t1.Common_ThanaFk,
                         Common_DivisionFk = t3.Common_DivisionsFk,
                         Common_DistrictsFk = t2.Common_DistrictsFk,
                         Common_CountriesFk = t4.Common_CountriesFk,
                         ShopTypeEnumFk = t1.ShopTypeEnumFk,
                         Code = t1.Code,
                         Email = t1.Email,
                         OwnDeliveryService = t1.OwnDeliveryService,
                         ServiceEndTime = t1.ServiceEndTime,
                         ServiceStartTime = t1.ServiceStartTime,
                         TradeLicenceExpireDate = t1.TradeLicenceExpireDate,
                         TradeLicenceNumber = t1.TradeLicenceNumber,
                         TradeLicenceUrl = t1.TradeLicenceUrl
                     }).FirstOrDefault();
            return v;
        }



        public VMCommonCustomer GetCommonCustomerByID(int id)
        {
            var v = (from t1 in _db.Common_Customer
                     where t1.ID == id && t1.Active
                     select new VMCommonCustomer
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         Address = t1.Address,
                         CustomerTypeEnumFk = t1.CustomerTypeEnumFk,
                         CustomerLoyalityPoint = t1.CustomerLoyalityPoint,
                         Phone = t1.Phone,

                         DOB = t1.DOB,
                         Email = t1.Email,
                         Sex = t1.Sex

                     }).FirstOrDefault();
            return v;
        }

        public async Task<int> ProductMRPPriceEdit(VMCommonProduct vmCommonProduct)
        {
            var result = -1;
            Common_Product commonProduct = _db.Common_Product.Find(vmCommonProduct.ID);
            commonProduct.MRPPrice = vmCommonProduct.MRPPrice;
            commonProduct.CostingPrice = vmCommonProduct.CostingPrice;

            commonProduct.Time = DateTime.Now;
            commonProduct.User = _vmLogin.Name;
            commonProduct.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonProduct.ID;
            }
            return result;
        }


        #endregion

        #region Common Customer
        public async Task<VMCommonCustomer> GetCustomer(VMCommonCustomer model)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();
            vmCommonCustomer.DataList = await Task.Run(() => (from t1 in _db.Common_Customer

                                                              where t1.Active &&
                                                             (model.MemberShipNo != null ? t1.MemberShipNo == model.MemberShipNo :
                                                              model.Phone != null ? t1.Phone == model.Phone : t1.ID > 0)
                                                              select new VMCommonCustomer
                                                              {
                                                                  ID = t1.ID,
                                                                  Name = t1.Name,
                                                                  Address = t1.Address,
                                                                  MemberShipNo = t1.MemberShipNo,
                                                                  Phone = t1.Phone,
                                                                  DOB = t1.DOB,
                                                                  Email = t1.Email,
                                                                  Sex = t1.Sex,
                                                                  UserID = t1.UserID,
                                                                  User = t1.User,

                                                              }).OrderByDescending(x => x.ID).AsEnumerable());

            //if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            //{
            //    vmCommonCustomer.DataList = vmCommonCustomer.DataList.Where(x => x.Common_ShopFk == _vmLogin.Common_ShopFk).AsEnumerable();
            //}

            return vmCommonCustomer;
        }

        public async Task<int> CustomerAdd(VMCommonCustomer vmCommonCustomer)
        {
            var result = -1;
            #region Genarate Customer No



            #endregion
            Common_Customer commonCustomer = new Common_Customer
            {
                Name = vmCommonCustomer.Name,
                MemberShipNo = vmCommonCustomer.MemberShipNo,
                Phone = vmCommonCustomer.Phone,
                Email = vmCommonCustomer.Email,
                Address = vmCommonCustomer.Address,
                DOB = vmCommonCustomer.DOB,
                Sex = vmCommonCustomer.Sex,

                Common_ShopFK = _vmLogin.Common_ShopFK,

                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_Customer.Add(commonCustomer);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonCustomer.ID;
            }
            return result;
        }
        public async Task<int> CustomerEdit(VMCommonCustomer vmCommonCustomer)
        {
            var result = -1;
            Common_Customer commonCustomer = _db.Common_Customer.Find(vmCommonCustomer.ID);
            commonCustomer.Name = vmCommonCustomer.Name;
            commonCustomer.Address = vmCommonCustomer.Address;

            commonCustomer.Phone = vmCommonCustomer.Phone;

            commonCustomer.CustomerLoyalityPoint = vmCommonCustomer.CustomerLoyalityPoint;
            commonCustomer.DOB = vmCommonCustomer.DOB;
            commonCustomer.Email = vmCommonCustomer.Email;
            commonCustomer.Sex = vmCommonCustomer.Sex;

            commonCustomer.User = _vmLogin.Name;
            commonCustomer.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonCustomer.ID;
            }
            return result;
        }
        public async Task<int> CustomerDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_Customer commonCustomer = _db.Common_Customer.Find(id);
                commonCustomer.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonCustomer.ID;
                }
            }
            return result;
        }
        #endregion



        #region Common Shop
        public async Task<VMCommonShop> GetShop()
        {
            VMCommonShop vmCommonShop = new VMCommonShop();
            vmCommonShop.DataList = await Task.Run(() => (from t1 in _db.Common_Shop
                                                          join t2 in _db.Common_Thana on t1.Common_ThanaFk equals t2.ID
                                                          join t3 in _db.Common_Districts on t2.Common_DistrictsFk equals t3.ID
                                                          join t4 in _db.Common_Divisions on t3.Common_DivisionsFk equals t4.ID
                                                          join t5 in _db.Common_Countries on t4.Common_CountriesFk equals t5.ID

                                                          where t1.Active && t2.Active && t2.Active && t3.Active && t4.Active
                                                          select new VMCommonShop
                                                          {
                                                              ID = t1.ID,
                                                              Code = t1.Code,
                                                              Name = t1.Name,
                                                              Address = t1.Address,
                                                              Contact = t1.Contact,
                                                              Description = t1.Description,
                                                              Email = t1.Email,
                                                              OwnDeliveryService = t1.OwnDeliveryService,
                                                              ServiceEndTime = t1.ServiceEndTime,
                                                              ServiceStartTime = t1.ServiceStartTime,
                                                              Common_DistrictsFk = t2.Common_DistrictsFk,
                                                              Common_CountriesFk = t4.Common_CountriesFk,
                                                              ShopTypeEnumFk = t1.ShopTypeEnumFk,

                                                              Country = t4.Name,
                                                              UserID = t1.UserID,
                                                              User = t1.User,
                                                              TradeLicenceUrl = t1.TradeLicenceUrl,
                                                              TradeLicenceNumber = t1.TradeLicenceNumber,
                                                              TradeLicenceExpireDate = t1.TradeLicenceExpireDate
                                                          }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonShop;
        }

        public async Task<int> ShopAdd(VMCommonShop vmCommonShop)
        {
            var result = -1;
            #region Genarate Shop ID
            int totalShop = _db.Common_Shop.Count();
            //string postOffice = _db.Common_PostOffices.Where(x => x.ID == vmCommonShop.Common_PostOfficesFk).Select(x => x.Name).FirstOrDefault();

            if (totalShop == 0)
            {
                totalShop = 1;
            }
            else
            {
                totalShop++;
            }

            var newString = totalShop.ToString().PadLeft(2, '0');


            #endregion

            Common_Shop commonShop = new Common_Shop
            {
                Code = newString,
                Name = vmCommonShop.Name,
                Contact = vmCommonShop.Contact,
                Address = vmCommonShop.Address,
                ShopTypeEnumFk = vmCommonShop.ShopTypeEnumFk,

                Description = vmCommonShop.Description,
                Common_ThanaFk = vmCommonShop.Common_ThanaFk,
                Email = vmCommonShop.Email,

                OwnDeliveryService = vmCommonShop.OwnDeliveryService,
                ServiceEndTime = vmCommonShop.ServiceEndTime,
                ServiceStartTime = vmCommonShop.ServiceStartTime,
                User = _vmLogin.Name,
                TradeLicenceUrl = vmCommonShop.TradeLicenceUrl,
                TradeLicenceNumber = vmCommonShop.TradeLicenceNumber,
                TradeLicenceExpireDate = vmCommonShop.TradeLicenceExpireDate,
                UserID = _vmLogin.ID

            };
            _db.Common_Shop.Add(commonShop);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonShop.ID;
            }
            return result;
        }
        public async Task<int> ShopEdit(VMCommonShop vmCommonShop)
        {
            var result = -1;
            Common_Shop commonShop = _db.Common_Shop.Find(vmCommonShop.ID);
            commonShop.Name = vmCommonShop.Name;
            commonShop.Contact = vmCommonShop.Contact;
            commonShop.Address = vmCommonShop.Address;
            commonShop.Description = vmCommonShop.Description;
            commonShop.Common_ThanaFk = vmCommonShop.Common_ThanaFk;
            commonShop.ShopTypeEnumFk = vmCommonShop.ShopTypeEnumFk;
            commonShop.Email = vmCommonShop.Email;
            commonShop.OwnDeliveryService = vmCommonShop.OwnDeliveryService;
            commonShop.ServiceEndTime = vmCommonShop.ServiceEndTime;
            commonShop.ServiceStartTime = vmCommonShop.ServiceStartTime;
            commonShop.User = _vmLogin.Name;
            commonShop.UserID = _vmLogin.ID;
            commonShop.TradeLicenceUrl = vmCommonShop.TradeLicenceUrl != null ? vmCommonShop.TradeLicenceUrl : commonShop.TradeLicenceUrl;
            commonShop.TradeLicenceNumber = vmCommonShop.TradeLicenceNumber;
            commonShop.TradeLicenceExpireDate = vmCommonShop.TradeLicenceExpireDate;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonShop.ID;
            }
            return result;
        }
        public async Task<int> ShopDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_Shop commonShop = _db.Common_Shop.Find(id);
                commonShop.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonShop.ID;
                }
            }
            return result;
        }
        #endregion

        #region Common ShopFloor
        public async Task<VMCommonShopCounter> GetShopFloor(int id)
        {
            VMCommonShopCounter vmCommonShopCounter = new VMCommonShopCounter();
            vmCommonShopCounter.VMCommonShop = await Task.Run(() => GetCommonShopByID(id));
            vmCommonShopCounter.DataList = await Task.Run(() => (from t1 in _db.Common_ShopCounter
                                                                 join t2 in _db.Common_Shop on t1.Common_ShopFK equals t2.ID
                                                                 where t1.Active && t2.Active
                                                                 && id > 0 ? t1.Common_ShopFK == id : t1.Active
                                                                 select new VMCommonShopCounter
                                                                 {
                                                                     ID = t1.ID,
                                                                     Name = t1.Name,
                                                                     Common_ShopFK = t1.Common_ShopFK,
                                                                     ShopName = t2.Name,
                                                                     UserID = t1.UserID,
                                                                     User = t1.User
                                                                 }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmCommonShopCounter;
        }

        public async Task<int> ShopFloorAdd(VMCommonShopCounter vmCommonShopCounter)
        {
            var result = -1;
            Common_ShopCounter commonShopCounter = new Common_ShopCounter
            {
                Name = vmCommonShopCounter.Name,
                Common_ShopFK = vmCommonShopCounter.Common_ShopFK,
                Description = vmCommonShopCounter.Description,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_ShopCounter.Add(commonShopCounter);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonShopCounter.ID;
            }
            return result;
        }
        public async Task<int> ShopFloorEdit(VMCommonShopCounter vmCommonShopCounter)
        {
            var result = -1;
            Common_ShopCounter commonShopCounter = _db.Common_ShopCounter.Find(vmCommonShopCounter.ID);
            commonShopCounter.Name = vmCommonShopCounter.Name;
            commonShopCounter.Common_ShopFK = vmCommonShopCounter.Common_ShopFK;
            commonShopCounter.Description = vmCommonShopCounter.Description;
            commonShopCounter.User = _vmLogin.Name;
            commonShopCounter.UserID = _vmLogin.ID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonShopCounter.ID;
            }
            return result;
        }
        public async Task<int> ShopFloorDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Common_ShopCounter commonShopFloor = _db.Common_ShopCounter.Find(id);
                commonShopFloor.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = commonShopFloor.ID;
                }
            }
            return result;
        }
        #endregion

        #region Rack & Bin
        public List<VMCommonBinSlave> GetVirtualBinData(VMCommonBinSlave vmCommonBinSlave)
        {

            List<VMCommonBinSlave> vmCommonBinSlaveList = new List<VMCommonBinSlave>();
            var binId = 1;
            if (vmCommonBinSlave.Rows > 0)
            {
                vmCommonBinSlave.ShopName = _db.Common_Shop.Where(x => x.ID == _vmLogin.Common_ShopFK).Select(x => x.Name).FirstOrDefault();
                for (var row = 0; row < vmCommonBinSlave.Rows; row++)
                {
                    if (vmCommonBinSlave.Columns > 0)
                    {
                        for (var column = 1; column <= vmCommonBinSlave.Columns; column++)
                        {
                            var binCID = MakeBinCID(vmCommonBinSlave, row, column);
                            var binSlave = new VMCommonBinSlave
                            {
                                ID = binId++,
                                CID = binCID,

                                RackNo = vmCommonBinSlave.RackNo,
                                Rows = vmCommonBinSlave.Rows,
                                Columns = vmCommonBinSlave.Columns,
                                Dimension = vmCommonBinSlave.Dimension,
                                Common_ShopFK = _vmLogin.Common_ShopFK,

                                UnitName = vmCommonBinSlave.UnitName
                            };
                            vmCommonBinSlaveList.Add(binSlave);
                        }
                    }
                }
            }

            return vmCommonBinSlaveList;
        }
        private static string MakeBinCID(VMCommonBinSlave vmCommonBinSlave, int row, int column)
        {
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            if (row <= 25)
            {
                return "F" + vmCommonBinSlave.ShopName + "/R" + vmCommonBinSlave.RackNo + "/" + letters[row] + column;
            }
            return "F" + vmCommonBinSlave.ShopName + "/R" + vmCommonBinSlave.RackNo + "/" + letters[row - 26] + letters[row - 26] + "-" + column;// + "/" + vmCommonBinSlave.ShopFloor.Trim();// + "/" + vmCommonBinSlave.ShopName.Trim(); //+ "/Line" + storeBin.Row
        }

        public int GetExRackNo(int rack)
        {
            var exRack = _db.Common_Bin.Where(x => x.RackNo == rack && x.Active && x.Common_ShopFK == _vmLogin.Common_ShopFK).Select(x => x.RackNo).FirstOrDefault();
            return exRack;
        }
        public async Task<VMCommonBinSlave> CommonBinSlaveGet(int id)
        {
            VMCommonBinSlave vmCommonBinSlave = new VMCommonBinSlave();
            vmCommonBinSlave = await Task.Run(() => (from t1 in _db.Common_Bin
                                                     join t3 in _db.Common_Shop on t1.Common_ShopFK equals t3.ID
                                                     where t1.Active && t3.Active && t1.ID == id
                                                     select new VMCommonBinSlave
                                                     {

                                                         Columns = t1.Column,
                                                         Common_BinFk = t1.ID,
                                                         ShopName = t3.Name,
                                                         RackNo = t1.RackNo,
                                                         Rows = t1.Row,
                                                         Time = t1.Time,
                                                         UserID = t1.UserID

                                                     }).FirstOrDefault());


            vmCommonBinSlave.DataListSlave = await Task.Run(() => (from t1 in _db.Common_BinSlave
                                                                   join t2 in _db.Common_Bin on t1.Common_BinFk equals t2.ID

                                                                   where t1.Active && t2.Active
                                                                    && t1.Common_BinFk == id
                                                                   select new VMCommonBinSlave
                                                                   {
                                                                       //ProductNameList = (from t1s in _db.Common_ProductInBinSlave
                                                                       //                   join t2s in _db.Common_Product on t1s.Common_ProductFK equals t2s.ID
                                                                       //                   where t1s.Active && t2s.Active && t1s.Common_BinSlaveFk == t1.ID
                                                                       //                   select t2s.Name).ToList(),

                                                                       CID = t1.CID,
                                                                       Dimension = t1.Dimension,
                                                                       ID = t1.ID,
                                                                       Time = t1.Time,
                                                                       UserID = t1.UserID,
                                                                       Common_BinFk = t1.Common_BinFk
                                                                   }).OrderByDescending(x => x.ID).AsEnumerable());


            return vmCommonBinSlave;
        }
        //public List<object> CommonProductDropDownList()
        //{
        //    var productList = new List<object>();

        //    var products = _db.Common_Product.Where(a => a.Active == true).ToList();
        //    foreach (var x in products)
        //    {
        //        productList.Add(new { Text = x.Name, Value = x.ID });
        //    }
        //    return productList;
        //}


        public async Task<VMCommonBinSlave> CommonBinSlaveByIDGet(int id)
        {
            VMCommonBinSlave vmCommonBinSlave = new VMCommonBinSlave();
            vmCommonBinSlave = await Task.Run(() => (from t1 in _db.Common_BinSlave
                                                     join t2 in _db.Common_Bin on t1.Common_BinFk equals t2.ID

                                                     where t1.Active && t2.Active
                                                      && t1.ID == id
                                                     select new VMCommonBinSlave
                                                     {
                                                         //ProductNameList = (from t1s in _db.Common_ProductInBinSlave
                                                         //                   join t2s in _db.Common_Product on t1s.Common_ProductFK equals t2s.ID
                                                         //                   where t1s.Active && t2s.Active && t1s.Common_BinSlaveFk == t1.ID
                                                         //                   select t2s.Name).ToList(),
                                                         CID = t1.CID,
                                                         Dimension = t1.Dimension,
                                                         ID = t1.ID,
                                                         Time = t1.Time,
                                                         UserID = t1.UserID,
                                                         Common_BinFk = t1.Common_BinFk,
                                                         Common_ShopFK = t2.Common_ShopFK
                                                     }).FirstOrDefault());




            return vmCommonBinSlave;
        }

        public async Task<VMCommonBin> CommonBinGet()
        {
            VMCommonBin vmCommonBin = new VMCommonBin();
            vmCommonBin.DataList = await Task.Run(() => (from t1 in _db.Common_Bin
                                                         join t3 in _db.Common_Shop on t1.Common_ShopFK equals t3.ID
                                                         where t1.Active && t3.Active
                                                         select new VMCommonBin
                                                         {
                                                             ID = t1.ID,
                                                             Columns = t1.Column,

                                                             ShopName = t3.Name,
                                                             RackNo = t1.RackNo,
                                                             Rows = t1.Row,
                                                             Time = t1.Time,
                                                             UserID = t1.UserID,
                                                             Common_ShopFK = t1.Common_ShopFK
                                                         }).OrderByDescending(x => x.ID).AsEnumerable());

            //if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            //{
            //    vmCommonBin.DataList = vmCommonBin.DataList.Where(x => x.Common_ShopFk == _vmLogin.Common_ShopFk).AsEnumerable();
            //}

            return vmCommonBin;
        }

        public List<object> CommonProductDropDown()
        {
            var CommonProductList = new List<object>();

            var CommonProducts = _db.Common_Product.Where(a => a.Active == true).OrderBy(x => x.Name).ToList();
            foreach (var x in CommonProducts)
            {
                CommonProductList.Add(new { Text = x.Name + " " + x.SystemBarcode, Value = x.ID });
            }
            return CommonProductList;
        }

        public List<object> WareHousePOReceivingGRNNotCompletedDropDown(int productId)
        {
            var POReceivingList = new List<object>();

            var POReceiving = (from t1 in _db.WareHouse_POReceiving.Where(a => a.Active)
                               join t2 in _db.WareHouse_POReceivingSlave.Where(x => x.Active && !x.IsGRNCompleted && x.Common_ProductFK == productId) on t1.ID equals t2.WareHouse_POReceivingFk
                               select new
                               {
                                   Text = t1.Challan + " Date: " + t1.ChallanDate.ToLongDateString(),
                                   Value = t1.ID
                               }).Distinct().ToList();
            foreach (var x in POReceiving)
            {
                POReceivingList.Add(new { Text = x.Text, Value = x.Value });
            }
            return POReceivingList;
        }

        public List<object> CommonShopDropDown(int id = 0)
        {
            var requisitionList = new List<object>();
            var data = (from t1 in _db.Common_Shop
                        where t1.Active && id > 0 ? t1.ID == id : t1.Active
                        select new
                        {
                            Name = t1.Name,
                            ID = t1.ID
                        }).OrderBy(x => x.Name).ToList();
            foreach (var section in data)
            {
                requisitionList.Add(new { Text = section.Name, Value = section.ID });
            }
            return requisitionList;
        }
        public List<object> ShopDropDown()
        {
            var requisitionList = new List<object>();
            var data = (from t1 in _db.Common_Shop
                        where t1.Active
                        select new
                        {
                            Name = t1.Name,
                            ID = t1.ID
                        }).OrderBy(x => x.Name).ToList();

            //if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            //{
            //    data = data.Where(x => x.ID == _vmLogin.Common_ShopFK).ToList();
            //}
            foreach (var section in data)
            {
                requisitionList.Add(new { Text = section.Name, Value = section.ID });
            }
            return requisitionList;
        }
        public async Task<VMCommonBin> CommonBinSingleGet(int id)
        {
            VMCommonBin vmCommonBin = new VMCommonBin();
            vmCommonBin = await Task.Run(() => (from t1 in _db.Common_Bin.Where(x => x.ID == id)
                                                join t2 in _db.Common_Shop on t1.Common_ShopFK equals t2.ID
                                                where t1.Active
                                                select new VMCommonBin
                                                {
                                                    ID = t1.ID,
                                                    Columns = t1.Column,
                                                    ShopName = t2.Name,
                                                    RackNo = t1.RackNo,
                                                    Rows = t1.Row,
                                                    Time = t1.Time,
                                                    UserID = t1.UserID
                                                }).FirstOrDefault());
            return vmCommonBin;
        }

        public async Task<int> CommonBinAdd(VMCommonBinSlave vmCommonBinSlave)
        {

            var result = -1;
            Common_Bin commonBin = new Common_Bin
            {
                Column = vmCommonBinSlave.Columns,
                RackNo = vmCommonBinSlave.RackNo,
                Row = vmCommonBinSlave.Rows,
                Common_ShopFK = _vmLogin.Common_ShopFK,
                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID
            };
            _db.Common_Bin.Add(commonBin);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonBin.ID;
            }
            return result;
        }

        public async Task<int> CommonBinEdit(VMCommonBin vmCommonBin)
        {
            var result = -1;

            Common_Bin commonBin = _db.Common_Bin.Find(vmCommonBin.ID);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vmCommonBin.ID;
            }

            return result;
        }
        public async Task<int> CommonBinDelete(int id)
        {
            var result = -1;
            List<Common_BinSlave> commonBinSlaveList = new List<Common_BinSlave>();

            Common_Bin commonBin = _db.Common_Bin.Find(id);

            commonBin.Active = false;
            commonBinSlaveList = _db.Common_BinSlave.Where(x => x.Common_BinFk == commonBin.ID && x.Active).ToList();
            commonBinSlaveList.ForEach(x => x.Active = false);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commonBin.ID;
            }

            return result;
        }
        public async Task<int> CommonBinSlaveAdd(VMCommonBinSlave vmCommonBinSlave)
        {
            var result = -1;
            var dataListSlavePartial = vmCommonBinSlave.DataSlaveToList.Where(x => x.CID != null).ToList();
            if (dataListSlavePartial.Any())
            {
                for (int i = 0; i < dataListSlavePartial.Count(); i++)
                {
                    Common_BinSlave commonBinSlave = new Common_BinSlave
                    {
                        CID = dataListSlavePartial[i].CID,
                        Common_BinFk = vmCommonBinSlave.Common_BinFk,
                        Dimension = dataListSlavePartial[i].Dimension,
                        User = _vmLogin.ID.ToString(),
                        UserID = _vmLogin.ID
                    };
                    _db.Common_BinSlave.Add(commonBinSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = commonBinSlave.ID;
                    }
                }
            }
            return result;
        }


        #endregion

        public List<object> DDLDistrictListByDivisionID(int DivisionId)
        {
            var list = new List<object>();
            var vData = _db.Common_Districts.Where(a => a.Active == true && a.Common_DivisionsFk == DivisionId).ToList();
            foreach (var x in vData)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }
        public List<object> DDLThanaListByDistrictID(int DistrictId)
        {
            var list = new List<object>();
            var vData = _db.Common_Thana.Where(a => a.Active == true && a.Common_DistrictsFk == DistrictId).ToList();
            foreach (var x in vData)
            {
                list.Add(new { Text = x.Name, Value = x.ID });
            }
            return list;
        }

        public List<object> GetShopTypeEnum()
        {
            var PaymentTypeList = new List<object>();

            foreach (var eVal in Enum.GetValues(typeof(ShopTypeEnum)))
            {
                PaymentTypeList.Add(new SelectListItem { Text = Enum.GetName(typeof(ShopTypeEnum), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
            }
            return PaymentTypeList;
        }
        public async Task<VMSalesOrder> ProcurementOrderMastersGetByID(int customerId)
        {
            VMSalesOrder vmOrderMaster = new VMSalesOrder();
            vmOrderMaster = await Task.Run(() => (from t1 in _db.Common_Customer.Where(x => x.ID == customerId && x.Active)
                                                  select new VMSalesOrder
                                                  {

                                                      CommonCustomerName = t1.Name,
                                                      Common_CustomerFK = t1.ID,


                                                      PayableAmount = (from ts1 in _db.Marketing_SalesSlave
                                                                       join ts2 in _db.Marketing_Sales on ts1.Marketing_SalesFk equals ts2.ID

                                                                       where ts2.Active && ts1.Active && ts2.Status == (int)EnumSaleStatus.Finalized
                                                                       && ts2.Common_CustomerFK == customerId
                                                                       group new { ts1, ts2 } by new { ts2.ID } into Group
                                                                       select Group.Sum(x => x.ts1.SubTotal)).DefaultIfEmpty(0).Sum(),


                                                      InAmount = _db.Common_Payment.Where(x => x.Common_CustomerFK == customerId)
                                                                       .Select(x => x.InAmount).DefaultIfEmpty(0).Sum()
                                                  }).FirstOrDefault());

            vmOrderMaster.DataList = await Task.Run(() => (from t1 in _db.Common_Payment.Where(x => x.Common_CustomerFK == customerId && x.Active)

                                                           select new VMSalesOrder
                                                           {
                                                               Common_CustomerFK = t1.Common_CustomerFK,
                                                               PaymentDate = t1.TransactionDate,
                                                               InAmount = t1.InAmount,
                                                               ID = t1.ID,
                                                               ReferenceNo = t1.ReferenceNo,
                                                               User = t1.User

                                                           }).OrderByDescending(x => x.ID).AsEnumerable());



            return vmOrderMaster;
        }

        public async Task<long> PaymentAdd(VMSalesOrder vmSalesOrder)
        {
            long result = -1;

            Common_Payment payment = new Common_Payment
            {

                InAmount = Convert.ToDecimal(vmSalesOrder.PaidAmount),
                TransactionDate = vmSalesOrder.PaymentDate,
                Common_CustomerFK = vmSalesOrder.Common_CustomerFK,
                ReferenceNo = vmSalesOrder.ReferenceNo,
                Common_ShopFK = _vmLogin.Common_ShopFK,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID

            };
            _db.Common_Payment.Add(payment);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = payment.ID;
            }
            return result;
        }
        public async Task<VMMarketingSalesSlave> ProcurementOrderMastersListGetByCustomer(int customerId)
        {
            VMMarketingSalesSlave vmMarketingSalesSlave = new VMMarketingSalesSlave();
            vmMarketingSalesSlave = (from t1 in _db.Common_Customer.Where(x => x.ID == customerId)
                                     select new VMMarketingSalesSlave
                                     {
                                         CustomerName = t1.Name,
                                         TotalPaid = _db.Common_Payment.Where(x => x.Active && x.Common_CustomerFK == t1.ID).Select(x => x.InAmount).DefaultIfEmpty(0).Sum()

                                     }).FirstOrDefault();

            vmMarketingSalesSlave.DataList = await Task.Run(() => (from t1 in _db.Marketing_Sales.Where(x => x.Active && x.Status == (int)EnumSaleStatus.Finalized && x.Common_CustomerFK == customerId)

                                                                   select new VMMarketingSalesSlave
                                                                   {
                                                                       Marketing_SalesFk = t1.ID,
                                                                       Common_CustomerFK = t1.Common_CustomerFK,
                                                                       CustomerPaymentMethodEnumFk = t1.CustomerPaymentMethodEnumFk,
                                                                       SaleInvoiceNo = t1.SaleInvoiceNo,
                                                                       Date = t1.Date,
                                                                       Status = t1.Status,
                                                                       PayableAmount = (_db.Marketing_SalesSlave.Where(x => x.Active && x.Marketing_SalesFk == t1.ID).Select(x => x.Quantity * x.MRPPrice).DefaultIfEmpty(0).Sum()),

                                                                   }).OrderByDescending(x => x.Marketing_SalesFk).AsEnumerable());



            return vmMarketingSalesSlave;
        }
        public VMCommonCustomer GetCustomerById(int id)
        {
            VMCommonCustomer vmCommonCustomer = new VMCommonCustomer();
            var customer = _db.Common_Customer.Find(id);
            vmCommonCustomer.Name = customer.Name;
            return vmCommonCustomer;
        }

        public async Task<VmTransaction> GetLedgerInfoByCustomer(VmTransaction vmTransaction)
        {
            List<VmTransaction> tempList = new List<VmTransaction>();
            VmTransaction vmTransition = new VmTransaction();


            var DataList1 = (from t1 in _db.Marketing_Sales
                             join t2 in _db.Common_Customer on t1.Common_CustomerFK equals t2.ID

                             where t1.Common_CustomerFK == vmTransaction.Common_CustomerFK && t1.Active == true && t1.Status == (int)EnumSaleStatus.Finalized
                             select new VmTransaction
                             {
                                 Marketing_SalesFK = t1.ID,
                                 Date = t1.Date,
                                 Description = "Invoice No : " + t1.SaleInvoiceNo,
                                 Credit = 0,
                                 Debit = 0,
                                 Balance = 0,
                                 FirstCreateDate = t1.Time
                             }).Distinct().ToList();
            foreach (var data in DataList1)
            {
                data.Credit = GetOrderValue(data.Marketing_SalesFK);
            }


            var DataList2 = (from t1 in _db.Common_Payment
                             where t1.Common_CustomerFK == vmTransaction.Common_CustomerFK && t1.Active == true
                             select new VmTransaction
                             {
                                 Date = t1.TransactionDate,
                                 Description = "Payment Reference : " + t1.ReferenceNo,
                                 //Debit = 0,
                                 //Credit = t1.Amount,
                                 Credit = 0,
                                 Debit = t1.InAmount,
                                 Balance = 0,
                                 FirstCreateDate = t1.Time
                             }).Distinct().ToList();



            var DataList = DataList1.Union(DataList2).OrderBy(x => x.Date).ToList();


            var previuosBalanceTable = (from t in DataList
                                        where t.Date < vmTransaction.FromDate
                                        select
                                        (
                                         //t.Debit - t.Credit
                                         t.Credit - t.Debit
                                        )).ToList();

            var countForId = previuosBalanceTable.Count();

            var previuosBalance = (previuosBalanceTable).DefaultIfEmpty(0).Sum();

            var sortedV = (from t in DataList
                           where t.Date >= vmTransaction.FromDate && t.Date <= vmTransaction.ToDate
                           select new VmTransaction
                           {
                               ID = ++countForId,
                               Date = t.Date,
                               Description = t.Description,
                               Credit = t.Credit,
                               Debit = t.Debit,
                               Balance = 0
                           }).Distinct().ToList();


            var supplier = _db.Common_Customer.Find(vmTransaction.Common_CustomerFK);
            var shopInfo = _db.Common_Shop.Find(_vmLogin.Common_ShopFK);



            vmTransition.Date = vmTransaction.FromDate;
            vmTransition.Name = supplier.Name;
            vmTransition.Description = "Opening Balance";
            vmTransition.Debit = 0;
            vmTransition.Credit = 0;
            vmTransition.Balance = previuosBalance;
            vmTransition.FromDate = vmTransaction.FromDate;
            vmTransition.ToDate = vmTransaction.ToDate;
            vmTransition.CompanyName = shopInfo.Name;
            vmTransition.CompanyPhone = shopInfo.Contact;
            vmTransition.CompanyAddress = shopInfo.Address;
            vmTransition.CompanyEmail = shopInfo.Email;



            tempList.Add(vmTransition);

            foreach (var x in sortedV)
            {
                x.Balance = previuosBalance += x.Credit - x.Debit;// x.Debit - x.Credit;
                x.Name = supplier.Name;
                tempList.Add(x);
            }


            vmTransition.DataList = tempList;

            return vmTransition;
        }

        private decimal GetOrderValue(long marketingSalesId)
        {
            decimal orderValue = (from ts1 in _db.Marketing_SalesSlave
                                  where ts1.Marketing_SalesFk == marketingSalesId && ts1.Active
                                  select ts1.SubTotal).DefaultIfEmpty(0).Sum();
            return orderValue;

        }
    }
}
