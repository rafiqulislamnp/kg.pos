﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using KG.Core.Services;

namespace KG.Core.Services.Configuration
{

    public class VMCommonBrand : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }
        public IEnumerable<VMCommonBrand> DataList { get; set; }


    }


    public class VMTermsAndCondition : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }

        public IEnumerable<VMTermsAndCondition> DataList { get; set; }
    }
    public class VMCommonUnit : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }

        public IEnumerable<VMCommonUnit> DataList { get; set; }
    }




    public class VMCommonSupplier : BaseVM
    {


        public string Name { get; set; }
        public string Remarks { get; set; }
        public int Common_ThanaFk { get; set; }
        public int Common_DistrictsFk { get; set; }
        public int Common_DivisionFk { get; set; }
        public int Common_CountriesFk { get; set; }

        public string Thana { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Country { get; set; }
        public string Fax { get; set; }
        public bool BiltoBilCreditPeriod { get; set; }
        public int CreditPeriod { get; set; }
        public SelectList ThanaList { get; set; } = new SelectList(new List<object>());


        public SelectList CountryList { get; set; } = new SelectList(new List<object>());
        public SelectList DistrictList { get; set; } = new SelectList(new List<object>());
        public SelectList DivisionList { get; set; } = new SelectList(new List<object>());


        public string Contact { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public IEnumerable<VMCommonSupplier> DataList { get; set; }
        public string ShopName { get; internal set; }
    }

    public class VMCommonProductCategory : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }

        public IEnumerable<VMCommonProductCategory> DataList { get; set; }
    }


    public class VMCommonProductSubCategory : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public string CategoryName { get; set; }
        public bool IsLock { get; set; }

        public SelectList ProductCategoryList { get; set; } = new SelectList(new List<object>());

        public IEnumerable<VMCommonProductSubCategory> DataList { get; set; }


    }
    public class VMCommonProduct : BaseVM
    {
        public bool IsCategoryWise { get; set; }
        public int Common_ProductInBinSlaveFk { get; set; }
        public string Name { get; set; }
        public int Common_ProductCategoryFk { get; set; }

        public int Common_ProductSubCategoryFk { get; set; }
        public int? Common_BrandFk { get; set; } = 0;
        public int? Common_SupplierFk { get; set; } = 0;

        public string Brand { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }

        public string SupplierName { get; set; }
        public decimal MaximumSalesLimit { get; set; }
        public decimal ConsumeQuantity { get; set; }
        public decimal TransferInQuantity { get; set; }
        public decimal TransferInMRPPrice { get; set; }
        public decimal TransferOutMRPPrice { get; set; }

        public decimal TransferInCostingPrice { get; set; }
        public decimal TransferOutCostingPrice { get; set; }

        public decimal TransferOutQuantity { get; set; }


        public decimal ConsumeReturnQuantity { get; set; }

        public int WareHouse_POReceivingFk { get; set; }
        public int WareHouse_POReceivingSlaveFk { get; set; }
        public int Common_UnitFk { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime DiscountExpiryDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ShopName { get; set; }
        public decimal CostingPrice { get; set; }

        public string UnitName { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalSaleMRPPrice { get; set; }
        public decimal TotalSaleCostingPrice { get; set; }


        public decimal PreviousStock { get; set; }
        public decimal CurrentStock { get; set; }

        public string Image { get; set; }

        public IFormFile ImageFile { get; set; }

        public string SubCategoryName { get; set; }
        public string CategoryName { get; set; }

        public bool IsLock { get; set; }
        public decimal ReceivedQuantity { get; set; }
        public decimal ReturnQuantity { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal DamageQuantity { get; set; }



        public decimal LaborCost { get; set; }
        public decimal ManufacturingOverhead { get; set; }
        public decimal TransportationOverhead { get; set; }
        public decimal OthersCost { get; set; }
        public string OthersCostNote { get; set; }

        public string DisContinued { get; set; }



        public string CMPIDX { get; set; }
        public string SystemBarcode { get; set; }
        public string ProductBarcode { get; set; }
        public string CSSID { get; set; }
        public string SSID { get; set; }
        public string CBTID { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal VATPercent { get; set; }
        public bool EnableOrDisableProductVat { get; set; }



        public decimal Point { get; set; }

        public string ProductID { get; set; }

        public int QtyOfBarcodeGen { get; set; }

        public IEnumerable<VMCommonProduct> DataList { get; set; }


        public SelectList ProductCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductList { get; set; } = new SelectList(new List<object>());
        public SelectList WareHousePOReceivingList { get; set; } = new SelectList(new List<object>());


        public SelectList ProductSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList UnitList { get; set; } = new SelectList(new List<object>());
        public SelectList BrandList { get; set; } = new SelectList(new List<object>());
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>());
        public SelectList ShopList { get; set; } = new SelectList(new List<object>());

        public decimal GPParcentage { get; set; }
        public decimal CurrentStockCS { get; set; }
        public List<VMCommonProduct> DataToListProduct { get; set; }

        public List<VMCommonProductBarcode> DataToList { get; set; }
    }
    public class VMCommonProductBarcode
    {
        public int ID { get; set; }

        public int QtyOfBarcodeGen { get; set; }
        public string Name { get; set; }
       
    }


    public class VMCommonCustomer : BaseVM
    {



        public string Name { get; set; }
        public string MemberShipNo { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public DateTime? DOB { get; set; }
        public string Sex { get; set; }

        public string CustomerLoyalityPoint { get; set; }
        public decimal TotalEarnPoint { get; set; }
        public decimal RedeemPoint { get; set; }
        public decimal TotalRedeemPoint { get; set; }

        public string CustomerTypeName { get; set; }
        public int CustomerTypeEnumFk { get; set; }

        //public int Common_ThanaFk { get; set; }
        //public int Common_CountriesFk { get; set; }
        //public int Common_DistrictsFk { get; set; }
        //public int Common_DivisionFk { get; set; }

        //public string Division { get; set; }
        //public string Thana { get; set; }
        //public string District { get; set; }
        //public string Country { get; set; }

        //public SelectList CountryList { get; set; } = new SelectList(new List<object>());
        //public SelectList DivisionList { get; set; } = new SelectList(new List<object>());
        //public SelectList DistrictList { get; set; } = new SelectList(new List<object>());
        //public SelectList ThanaList { get; set; } = new SelectList(new List<object>());

        public IEnumerable<VMCommonCustomer> DataList { get; set; }


    }


    public class VMCommonShop : BaseVM
    {
        public string Name { get; set; }



        public int Common_ThanaFk { get; set; }

        public int Common_CountriesFk { get; set; }
        public int Common_DistrictsFk { get; set; }
        public int Common_DivisionFk { get; set; }
        public int ShopTypeEnumFk { get; set; }



        public string Thana { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public string Country { get; set; }


        public string Address { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public bool OwnDeliveryService { get; set; }
        public string ServiceStartTime { get; set; }
        public string ServiceEndTime { get; set; }
        public string Description { get; set; }

        public SelectList ThanaList { get; set; } = new SelectList(new List<object>());
        public SelectList DistrictList { get; set; } = new SelectList(new List<object>());
        public SelectList DivisionList { get; set; } = new SelectList(new List<object>());
        public SelectList CountryList { get; set; } = new SelectList(new List<object>());

        public ShopTypeEnum STypeEnum { get { return (ShopTypeEnum)this.ShopTypeEnumFk; } }
        public string ShopTypeEnumName { get { return BaseFunctionalities.GetEnumDescription(STypeEnum); } }
        public SelectList ShopTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ShopTypeEnum>(), "Value", "Text"); } }


        public string TradeLicenceNumber { get; set; }
        public DateTime TradeLicenceExpireDate { get; set; }
        public IFormFile TradeLicenceFile { get; set; }
        public string TradeLicenceUrl { get; set; }

        public IEnumerable<VMCommonShop> DataList { get; set; }
    }

    public class VMCommonShopCounter : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public SelectList ShopList { get; set; } = new SelectList(new List<object>());
        public string ShopName { get; set; }
        public IEnumerable<VMCommonShopCounter> DataList { get; set; }
        public VMCommonShop VMCommonShop { get; set; }

    }
    public class VMCommonBin : BaseVM
    {
        public string UnitName { get; set; }
        public string ShopName { get; set; }
        public int RackNo { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        public IEnumerable<VMCommonBin> DataList { get; set; }
        public SelectList ShopList { get; set; } = new SelectList(new List<object>());

    }
    public class VMCommonBinSlave : VMCommonBin
    {

        public int Common_BinFk { get; set; }
        public string CID { get; set; }
        public string Dimension { get; set; }
        public List<string> ProductNameList { get; set; }
        public string ProductName { get; set; }
        public VMCommonProduct VMCommonProduct { get; set; }
        public int Common_ProductInBinSlaveFk { get; set; }
        public int Common_ProductFK { get; set; }
        public IEnumerable<VMCommonBinSlave> DataListSlave { get; set; }
        public List<VMCommonBinSlave> DataSlaveToList { get; set; }
        public MultiSelectList ProductList { get; set; } = new MultiSelectList(new List<object>());

    }
    public class VMCommonCountries : BaseVM
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }

    }
    public class VMCommonDivisions : BaseVM
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }

        public int Common_CountriesFk { get; set; }
    }
    public class VMCommonDistricts : BaseVM
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }
        public int Common_DivisionsFk { get; set; }

    }


    public class VMCommonThana : BaseVM
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }
        public int Common_DistrictsFk { get; set; }

    }

    public class VmTransaction
    {

        public int ID { get; set; }
        public DateTime? Date { get; set; }
        public DateTime FirstCreateDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


        public long PurchaseOrdersId { get; set; }
        public int Marketing_SalesFK { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public double CustomerCredit { get; set; }

        public decimal Balance { get; set; }
        public decimal TotalDebit { get; set; }
        public decimal TotalCredit { get; set; }
        public VMCommonCustomer VMCommonCustomer { get; set; }
        public bool DrIncrease { get; set; }
        public decimal? CostOfGoodsSold { get; set; }
        public decimal? GrossProfit { get; set; }
        public decimal? NetProfit { get; set; }
        public decimal? Sales { get; set; }
        public decimal? Purchased { get; set; }
        public decimal? OperatingExp { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyLogo { get; set; }

        public decimal CurrencyRate { get; set; }
        public bool IsPoDeleted { get; set; }
        public string IpoNumber { get; set; }
      
        public int Common_CustomerFK { get; set; }


        public List<VmTransaction> DataList { get; set; }
        public double CourierCharge { get; set; }
    }

    public class VmInventoryDetails
    {

        public int ID { get; set; }
        public DateTime? Date { get; set; }
        public DateTime FirstCreateDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


        public long PurchaseOrdersId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public double Debit { get; set; }
        public decimal DebitDecimal { get; set; }

        public decimal Credit { get; set; }
        public double CustomerCredit { get; set; }

        public decimal Balance { get; set; }
        public decimal TotalDebit { get; set; }
        public decimal TotalCredit { get; set; }
        public VMCommonProduct VMCommonProduct { get; set; }
        public bool DrIncrease { get; set; }
        public decimal? CostOfGoodsSold { get; set; }
        public decimal? GrossProfit { get; set; }
        public decimal? NetProfit { get; set; }
        public decimal? Sales { get; set; }
        public decimal? Purchased { get; set; }
        public decimal? OperatingExp { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public decimal CurrencyRate { get; set; }
        public bool IsPoDeleted { get; set; }
        public string IpoNumber { get; set; }
        public int? CompanyFK { get; set; }
        public int? ProductFK { get; set; }


        public List<VmInventoryDetails> DataList { get; set; }
        public double CourierCharge { get; set; }
    }

    //public class VMCommonSupplier : BaseVM
    //{


    //    public string Name { get; set; }
    //    public int ZoneId { get; set; }
    //    public int SubZoneId { get; set; }

    //    public int Common_UpazilasFk { get; set; }
    //    public int Common_DistrictsFk { get; set; }
    //    public int Common_DivisionFk { get; set; }
    //    public int Common_CountriesFk { get; set; }

    //    public string Upazila { get; set; }
    //    public string District { get; set; }
    //    public string Division { get; set; }
    //    public string Country { get; set; }
    //    public string Fax { get; set; }
    //    public int CustomerTypeFk { get; set; }
    //    public bool BiltoBilCreditPeriod { get; set; }
    //    public int CreditPeriod { get; set; }
    //    public SelectList UpazilasList { get; set; } = new SelectList(new List<object>());
    //    public SelectList ZoneListList { get; set; } = new SelectList(new List<object>());
    //    public SelectList CountryList { get; set; } = new SelectList(new List<object>());
    //    public SelectList DistrictList { get; set; } = new SelectList(new List<object>());
    //    public SelectList DivisionList { get; set; } = new SelectList(new List<object>());
    //    public SelectList TerritoryList { get; set; } = new SelectList(new List<object>());

    //    //public CustomerType CustomerType { get { return (CustomerType)this.CustomerTypeFk; } }// = SupplierPaymentMethodEnum.Cash;
    //    //public string CustomerTypeName { get { return BaseFunctionalities.GetEnumDescription(CustomerType); } }
    //    //public SelectList CustomerTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<CustomerType>(), "Value", "Text"); } }

    //    public string ContactPerson { get; set; }
    //    public string Phone { get; set; }

    //    public string Email { get; set; }
    //    public string Address { get; set; }
    //    public bool IsForeign { get; set; }
    //    public string ZoneName { get; set; }
    //    public string ZoneIncharge { get; set; }
    //    public string NID { get; set; }
    //    public decimal? CreditLimit { get; set; }


    //    public IEnumerable<VMCommonSupplier> DataList { get; set; }
    //    public long? HeadGLId { get; internal set; }
    //}

    public class VMSalesOrder : BaseVM
    {
        public DateTime PaymentDate { get; set; }
        public int CustomerPaymentMethodEnumFK { get; set; }
        public string OrderNo { get; set; }
        public double TotalAmount { get; set; }
        public decimal PayableAmount { get; set; }
        public double PaidAmount { get; set; }

        public decimal GrandTotal { get; set; }
        public decimal DiscountRate { get; set; }
        public decimal DiscountAmount { get; set; }
        public int Status { get; set; }
        public string CommonCustomerName { get; set; }

        public string CustomerPhone { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerEmail { get; set; }

        public string ContactPerson { get; set; }

        public IEnumerable<VMSalesOrder> DataList { get; set; }
        public SelectList CustomerList { get; set; } = new SelectList(new List<object>());
        public SelectList TermNCondition { get; set; } = new SelectList(new List<object>());



        //public VendorsPaymentMethodEnum POPaymentMethod { get { return (VendorsPaymentMethodEnum)this.CustomerPaymentMethodEnumFK; } }// = SupplierPaymentMethodEnum.Cash;
        //public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }
        //public SelectList POPaymentMethodList { get { return new SelectList(BaseFunctionalities.GetEnumList<VendorsPaymentMethodEnum>(), "Value", "Text"); } }


        public EnumPOStatus EnumStatus { get { return (EnumPOStatus)this.Status; } }
        public string EnumStatusName { get { return BaseFunctionalities.GetEnumDescription(this.EnumStatus); } }
        public SelectList EnumStatusList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumPOStatus>(), "Value", "Text"); } }

        public int Common_CustomerFK { get; set; }
        public int Common_PaymentMathodFK { get; set; }
        public decimal InAmount { get; set; } = 0;
        public decimal OutAmount { get; set; } = 0;
        public string ReferenceNo { get; set; }
        public DateTime TransactionDate { get; set; }
    }


    
}