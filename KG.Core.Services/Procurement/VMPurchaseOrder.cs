﻿using KG.Core.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace KG.Core.Services.Procurement
{
    public class VMPurchaseOrder : BaseVM
    {

        public string CID { get; set; }
        public string RequisitionCID { get; set; }

        public int Common_SupplierFK { get; set; }
        public int SupplierPaymentMethodEnumFK { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public decimal TotalPOValue { get; set; }     
        public string TermsAndCondition { get; set; }
        public string Description { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool IsHold { get; set; }
        public bool IsCancel { get; set; }
        public int TermsAndConditionFk { get; set; }
        public int Status { get; set; }
       
        public int Procurement_PurchaseRequisitionFK { get; set; }

        public string SupplierName { get; set; }
      



        public IEnumerable<VMPurchaseOrder> DataList { get; set; }
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>());
        public SelectList TermNCondition { get; set; } = new SelectList(new List<object>());
        public SelectList PRList { get; set; } = new SelectList(new List<object>());


        public PaymentMethodEnum POPaymentMethod { get { return (PaymentMethodEnum)this.SupplierPaymentMethodEnumFK; } }// = SupplierPaymentMethodEnum.Cash;
        public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }
        public SelectList POPaymentMethodList { get { return new SelectList(BaseFunctionalities.GetEnumList<PaymentMethodEnum>(), "Value", "Text"); } }

       
        public EnumPOStatus EnumStatus { get { return (EnumPOStatus)this.Status; } }
        public string EnumStatusName { get { return BaseFunctionalities.GetEnumDescription(this.EnumStatus); } }
        public SelectList EnumStatusList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumPOStatus>(), "Value", "Text"); } }



        public decimal RequiredQuantity { get; set; }
       
    }

    public class VMPurchaseOrderSlave : VMPurchaseOrder
    {
        public int Procurement_PurchaseOrderFK { get; set; }
        public int? Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int? Common_ProductFK { get; set; }
        public int Common_ProductCategoryFK { get; set; }
        public int Common_ProductSubCategoryFK { get; set; }

        public decimal PurchaseQuantity { get; set; }
        public decimal StockAvailableQuantity { get; set; }
        public decimal ProcuredQuantity { get; set; }
        public string ProductName { get; set; }
        public decimal? RequisitionQuantity { get; set; }
        public decimal PurchasingPrice { get; set; }
        public decimal TotalPrice { get { return PurchaseQuantity * PurchasingPrice; } }
        
        public string TotalPriceInWord { get; set; }

        public string UnitName { get; set; }
        public SelectList ProductCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductList { get; set; } = new SelectList(new List<object>());
      

        public IEnumerable<VMPurchaseOrderSlave> DataListSlave { get; set; }
        public string SystemBarcode { get; set; }
        public string SubCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
    }

    public class VMPurchaseOrderSlavePartial : BaseVM
    {
        public int Procurement_PurchaseOrderFK { get; set; }
        public int? Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int? Common_ProductFK { get; set; }
        public int Common_ProductCategoryFK { get; set; }
        public int Common_ProductSubCategoryFK { get; set; }
        public decimal PurchaseQuantity { get; set; }
        public decimal PurchasingPrice { get; set; }
        public string UnitName { get; set; }
        public decimal ProcuredQuantity { get; set; }
        public string ProductName { get; set; }
        public string DescriptionSlave { get; set; }
        public decimal? RequisitionQuantity { get; set; }
        public decimal PurchaseQty { get; set; }
        public decimal TotalPrice { get; set; }
        public string SupplierNames { get; set; }
        public bool Flag { get; set; }
        public List<VMPurchaseOrderSlavePartial> DataListPartial { get; set; }
        public decimal CurrentStock { get; set; }
    }
    
}