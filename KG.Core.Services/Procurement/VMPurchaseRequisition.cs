﻿
using KG.Core.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace KG.Core.Services.Procurement
{
    //public class VMCommonApproval : BaseVM
    //{
    //    public string Name { get; set; }
    //    public bool IsNeed { get; set; }
    //    public IEnumerable<VMCommonApproval> DataList { get; set; }
    //}

    public class VMPurchaseRequisition : BaseVM
    {
        public string RequisitionCID { get; set; }
        public DateTime RequisitionDate { get; set; } = DateTime.Now;
        public int FromUser_DepartmentFk { get; set; }
        public int ToUser_DepartmentFK { get; set; }
        public int POStatusInPRFk { get; set; }
        public bool IsHold { get; set; }
        public bool IsCancel { get; set; }
    
        public int Status { get; set; }
       //public int Approval { get; set; }
        public string RequisitionRemarks { get; set; }
        public string FromUserDepartmentName { get; set; }
        public string ToUserDepartmentName { get; set; }

        public string ShopName { get; set; }

        public IEnumerable<VMPurchaseRequisition> DataList { get; set; }



        public POStatusInPR POStatusInPR { get { return (POStatusInPR)this.POStatusInPRFk; } }
        public string POStatusInPRName { get { return BaseFunctionalities.GetEnumDescription(this.POStatusInPR); } }
        public SelectList POStatusInPRList { get { return new SelectList(BaseFunctionalities.GetEnumList<POStatusInPR>(), "Value", "Text"); } }

        public EnumPRStatus EnumStatus { get { return (EnumPRStatus)this.Status; } } // Value comes from POTypeEnum
        public string EnumStatusName { get { return BaseFunctionalities.GetEnumDescription(this.EnumStatus); } }
        public SelectList EnumStatusList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumPRStatus>(), "Value", "Text"); } }



    }

    public class VMPurchaseRequisitionSlave : VMPurchaseRequisition
    {


        public int Procurement_PurchaseRequisitionFK { get; set; }
        public int Common_ProductFK { get; set; }
        public int Common_ProductSubCategoryFK { get; set; }
        public int Common_ProductCategoryFK { get; set; }
  
        public string UnitName { get; set; }
        public decimal RequisitionQuantity { get; set; }
        public string SupplierNames { get; set; }
        public IEnumerable<VMPurchaseRequisitionSlave> DataListSlave { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }

        public decimal CurrentStock { get; set; }
        public SelectList ProductSelectList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductCategorySelectList { get; set; } = new SelectList(new List<object>());
        public SelectList ProductSubCategorySelectList { get; set; } = new SelectList(new List<object>());
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string BrandName { get; set; }
        public string SystemBarcode { get; set; }
        public string ProductBarcode { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal CostingPrice { get; set; }
    }

    public class VMPRSlavePartial : BaseVM
    {
        public int Common_ProductFK { get; set; }
        public string UnitName { get; set; }
        public decimal RequisitionQuantity { get; set; } = 0;
        public string SupplierNames { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
       
        public decimal? PreviousRequisitionQuantity { get; set; }
        public bool Flag { get; set; }

        public List<VMPRSlavePartial> DataListPartial { get; set; }

    }
}