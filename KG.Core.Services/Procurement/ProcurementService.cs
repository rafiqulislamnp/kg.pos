﻿
using KG.Core.Entity.Procurement;
using KG.Core.Services;
using KG.Core.Services.Configuration;
using KG.Core.Services.Home;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace KG.Core.Services.Procurement
{
    public class ProcurementService : BaseService
    {
        private readonly HttpContext _httpContext;
        private readonly VMLogin _vmLogin;

        public ProcurementService(IPosDbContext db, HttpContext httpContext)
        {
            _db = db;
            _httpContext = httpContext;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }
        #region Common
        public List<object> CommonTremsAndConditionDropDownList()
        {
            var List = new List<object>();
            foreach (var item in _db.Common_TremsAndCondition.Where(a => a.Active == true).ToList())
            {
                List.Add(new { Text = item.Name, Value = item.ID });
            }
            return List;

        }

        public VMTermsAndCondition CommonTermsAndConditionsDropDownList(int id)
        {
            var item = (from t1 in _db.Common_TremsAndCondition.Where(a => a.Active == true && a.ID == id)
                        select new VMTermsAndCondition
                        {
                            Description = t1.Description,
                            ID = t1.ID
                        }).FirstOrDefault();
            return item;
        }
        public List<object> SupplierDropDownList()
        {
            var SupplierList = new List<object>();
            _db.Common_Supplier.Where(x => x.Active).Select(x => x).ToList().ForEach(x => SupplierList.Add(new
            {
                Value = x.ID.ToString(),
                Text = x.Name
            }));
            return SupplierList;
        }

        public List<object> ProcurementPurchaseOrderDropDownBySupplier(int supplierId)
        {
            var procurementPurchaseOrderList = new List<object>();
            _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Common_SupplierFK == supplierId).Select(x => x).ToList().ForEach(x => procurementPurchaseOrderList.Add(new
            {
                Value = x.ID.ToString(),
                Text = x.CID + " Date: " + x.OrderDate.ToLongDateString()
            }));
            return procurementPurchaseOrderList;
        }

        public List<object> ProductCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_ProductCategory
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> ProductSubCategoryDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_ProductSubCategory
        .Where(x => x.Active).Where(x => x.Common_ProductCategoryFk == id || id <= 0).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> ProductDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_Product
        .Where(x => x.Active).Where(x => x.Common_ProductSubCategoryFk == id || id <= 0).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }


        public List<object> UserDropDownList()
        {
            var List = new List<object>();
            _db.User_User
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;
        }
        public List<object> UnitDropDownList()
        {
            var List = new List<object>();
            _db.Common_Unit
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }




        #endregion


        #region New Purchase Requisition
        public async Task<VMPurchaseRequisitionSlave> ProcurementPurchaseRequisitionGetByID(int id)
        {
            var vmPurchaseRequisitionSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.ID == id && x.Active)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID
                                                                   join t5 in _db.Common_Shop on t1.Common_ShopFK equals t5.ID


                                                                   select new VMPurchaseRequisitionSlave
                                                                   {
                                                                       Procurement_PurchaseRequisitionFK = t1.ID,
                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       Status = t1.Status,
                                                                       ShopName = t5.Name + "[" + t5.Code + "]",
                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       UserID = t1.UserID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       User = t4.Name,
                                                                   }).FirstOrDefault());

            return vmPurchaseRequisitionSlave;
        }
        public async Task<IEnumerable<VMPurchaseRequisitionSlave>> ProcurementPurchaseRequisitionSlaveGet(int id)
        {

            var DataListSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == id && x.Active)
                                                      join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                      join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                      join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                      join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                      join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID

                                                      select new VMPurchaseRequisitionSlave
                                                      {
                                                          ID = t1.ID,
                                                          Procurement_PurchaseRequisitionFK = t1.Procurement_PurchaseRequisitionFK,
                                                          Common_ProductFK = t1.Common_ProductFK,
                                                          RequisitionQuantity = t1.RequisitionQuantity,
                                                          CategoryName = t5.Name,
                                                          SubCategoryName = t4.Name,
                                                          BrandName = t6.Name,
                                                          SystemBarcode = t2.SystemBarcode,
                                                          ProductBarcode = t2.ProductBarcode,
                                                          MRPPrice = t1.MRPPrice,
                                                          CostingPrice = t1.CostingPrice,
                                                          UnitName = t3.Name,
                                                          ProductName = t2.Name,
                                                          SupplierNames = t1.SupplierNames,
                                                          ProductDescription = t1.Remarks,
                                                          CurrentStock = t1.CurrentStock

                                                      }).OrderByDescending(x => x.ID).AsEnumerable()); ;



            return DataListSlave;
        }


        public async Task<IEnumerable<VMPurchaseRequisitionSlave>> PurchaseRequisitionSummeryGet(int id)
        {

            var DataListSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == id && x.Active)
                                                      join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                      join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                      join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                      join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                      join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID
                                                      group new { t1,t2,t3,t4,t5,t6} by new { t5.ID, t5.Name} into Group
                                                      select new VMPurchaseRequisitionSlave
                                                      {
                                                          
                                                          RequisitionQuantity = Group.Sum(x=>x.t1.RequisitionQuantity),
                                                          CategoryName =Group.Key.Name,
                                                          MRPPrice = Group.Sum(x => x.t1.RequisitionQuantity * x.t1.MRPPrice),
                                                          CostingPrice = Group.Sum(x => x.t1.RequisitionQuantity * x.t1.CostingPrice),
                                                          
                                                      }).OrderByDescending(x => x.ID).AsEnumerable()); ;



            return DataListSlave;
        }

        public async Task<VMPurchaseRequisitionSlave> GetPurchaseRequisitionSlaveSingle(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave
                                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                          join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                          join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                          join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID


                                          where t1.ID == id
                                          select new VMPurchaseRequisitionSlave
                                          {
                                              Common_ProductFK = t1.Common_ProductFK,
                                              ProductDescription = t1.Remarks,
                                              Common_ProductCategoryFK = t4.ID,
                                              Common_ProductSubCategoryFK = t3.ID,
                                              ID = t1.ID,
                                              CurrentStock = t1.CurrentStock,
                                              //Merchandising_BOFFK = t1.Merchandising_BOFFK,
                                              //Merchandising_StyleID = t1.Merchandising_StyleID,
                                              //Merchandising_StyleSlaveFK = t1.Merchandising_StyleSlaveFK,
                                              Procurement_PurchaseRequisitionFK = t1.Procurement_PurchaseRequisitionFK,
                                              RequisitionQuantity = t1.RequisitionQuantity,
                                              SupplierNames = t1.SupplierNames
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<int> ProcurementPurchaseRequisitionSlaveAdd(VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave)
        {
            var result = -1;
            //var vData = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == vM.Procurement_PurchaseRequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Merchandising_StyleID == vM.VMMerchandising_StyleID && x.Merchandising_StyleSlaveFK == vM.VMMerchandising_StyleSlaveID && x.Merchandising_BOFFK == vM.VMMerchandising_BOFID && x.Active)));
            //Procurement_PurchaseRequisitionSlave pRSlave = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == vM.Procurement_PurchaseRequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Merchandising_StyleID == vM.VMMerchandising_StyleID && x.Merchandising_StyleSlaveFK == vM.VMMerchandising_StyleSlaveID && x.Merchandising_BOFFK == vM.VMMerchandising_BOFID && x.Active).FirstOrDefaultAsync()));
            Procurement_PurchaseRequisitionSlave pRSlave = new Procurement_PurchaseRequisitionSlave
            {
                Procurement_PurchaseRequisitionFK = vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK,
                RequisitionQuantity = vmPurchaseRequisitionSlave.RequisitionQuantity,

                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK,
                CurrentStock = vmPurchaseRequisitionSlave.CurrentStock,
                SupplierNames = vmPurchaseRequisitionSlave.SupplierNames,
                Common_ProductFK = vmPurchaseRequisitionSlave.Common_ProductFK,
                Remarks = vmPurchaseRequisitionSlave.ProductDescription,

            };
            _db.Procurement_PurchaseRequisitionSlave.Add(pRSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = pRSlave.ID;
            }

            return result;
        }


        public async Task<int> ProcurementPurchaseRequisitionSlaveEdit(VMPurchaseRequisitionSlave vmPurchaseRequisitionSlave)
        {
            var result = -1;
            Procurement_PurchaseRequisitionSlave procurementPurchaseRequisitionSlave = await _db.Procurement_PurchaseRequisitionSlave.FindAsync(vmPurchaseRequisitionSlave.ID);
            if (procurementPurchaseRequisitionSlave != null)
            {
                //procurementPurchaseRequisitionSlave.Common_RawItemFK = vmPurchaseRequisitionSlave.Common_RawItemFK;
                //procurementPurchaseRequisitionSlave.Asset_RegisterFK = vmPurchaseRequisitionSlave.Asset_RegisterFK;
                //procurementPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK = vmPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK;

                procurementPurchaseRequisitionSlave.Remarks = vmPurchaseRequisitionSlave.ProductDescription;
                procurementPurchaseRequisitionSlave.RequisitionQuantity = vmPurchaseRequisitionSlave.RequisitionQuantity;
                procurementPurchaseRequisitionSlave.SupplierNames = vmPurchaseRequisitionSlave.SupplierNames;
                procurementPurchaseRequisitionSlave.UserID = _vmLogin.ID;
                procurementPurchaseRequisitionSlave.Common_ShopFK = _vmLogin.Common_ShopFK;


                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisitionSlave.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionSlaveDelete(int id)
        {
            var result = -1;

            Procurement_PurchaseRequisitionSlave procurementPurchaseRequisitionSlave = await _db.Procurement_PurchaseRequisitionSlave.FindAsync(id);
            if (procurementPurchaseRequisitionSlave != null)
            {
                procurementPurchaseRequisitionSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK;
                }
            }
            return result;
        }

        public async Task<VMPurchaseRequisition> ProcurementPurchaseRequisitionListGet()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.Active && x.POStatusInPRFk <= (int)POStatusInPR.PartialPOCreated && x.Status < (int)EnumPRStatus.Closed && !x.IsCancel && !x.IsHold)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID
                                                                   join t5 in _db.Common_Shop on t1.Common_ShopFK equals t5.ID
                                                                   where t1.Common_ShopFK == _vmLogin.Common_ShopFK
                                                                   select new VMPurchaseRequisition
                                                                   {
                                                                       ID = t1.ID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,
                                                                       POStatusInPRFk = t1.POStatusInPRFk,
                                                                       ShopName = t5.Name + "[" + t5.Code + "]",
                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       IsHold = t1.IsHold,
                                                                       IsCancel = t1.IsCancel,
                                                                       Status = t1.Status,
                                                                       User = t4.Name,
                                                                       UserID = t1.UserID,

                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,

                                                                   }).OrderByDescending(x => x.ID).AsEnumerable()); ;
            return vmPurchaseRequisition;
        }
        public async Task<VMPurchaseRequisition> ProcurementClosedPurchaseRequisitionListGet()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.Active && x.Status == (int)EnumPRStatus.Closed)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID

                                                                   select new VMPurchaseRequisition
                                                                   {
                                                                       ID = t1.ID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,

                                                                       POStatusInPRFk = t1.POStatusInPRFk,


                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       IsHold = t1.IsHold,
                                                                       IsCancel = t1.IsCancel,
                                                                       Status = t1.Status,
                                                                       User = t4.Name,
                                                                       UserID = t1.UserID,

                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,

                                                                   }).OrderByDescending(x => x.ID).AsEnumerable()); ;
            return vmPurchaseRequisition;
        }

        public async Task<VMPurchaseRequisition> ProcurementHoldPurchaseRequisitionListGet()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.Active && x.IsHold)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID

                                                                   select new VMPurchaseRequisition
                                                                   {
                                                                       ID = t1.ID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,

                                                                       POStatusInPRFk = t1.POStatusInPRFk,


                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       IsHold = t1.IsHold,
                                                                       IsCancel = t1.IsCancel,
                                                                       Status = t1.Status,
                                                                       User = t4.Name,
                                                                       UserID = t1.UserID,
                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,

                                                                   }).OrderByDescending(x => x.ID).AsEnumerable()); ;
            return vmPurchaseRequisition;
        }

        public async Task<VMPurchaseRequisition> ProcurementPOStatusInPRPurchaseRequisitionListGet()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.Active && x.POStatusInPRFk == (int)POStatusInPR.FullPOCreated)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID

                                                                   select new VMPurchaseRequisition
                                                                   {
                                                                       ID = t1.ID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,

                                                                       POStatusInPRFk = t1.POStatusInPRFk,


                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       IsHold = t1.IsHold,
                                                                       IsCancel = t1.IsCancel,
                                                                       Status = t1.Status,
                                                                       User = t4.Name,
                                                                       UserID = t1.UserID,
                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,

                                                                   }).OrderByDescending(x => x.ID).AsEnumerable()); ;
            return vmPurchaseRequisition;
        }


        public async Task<VMPurchaseRequisition> ProcurementCancelPurchaseRequisitionListGet()
        {
            VMPurchaseRequisition vmPurchaseRequisition = new VMPurchaseRequisition();
            vmPurchaseRequisition.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition.Where(x => x.Active && x.IsCancel)
                                                                   join t2 in _db.User_Department on t1.FromUser_DepartmentFk equals t2.ID
                                                                   join t3 in _db.User_Department on t1.ToUser_DepartmentFK equals t3.ID
                                                                   join t4 in _db.User_User on t1.UserID equals t4.ID

                                                                   select new VMPurchaseRequisition
                                                                   {
                                                                       ID = t1.ID,
                                                                       FromUserDepartmentName = t2.Name,
                                                                       ToUserDepartmentName = t3.Name,

                                                                       POStatusInPRFk = t1.POStatusInPRFk,


                                                                       RequisitionCID = t1.RequisitionCID,
                                                                       RequisitionDate = t1.RequisitionDate,
                                                                       RequisitionRemarks = t1.Remarks,
                                                                       IsHold = t1.IsHold,
                                                                       IsCancel = t1.IsCancel,
                                                                       Status = t1.Status,
                                                                       User = t4.Name,
                                                                       UserID = t1.UserID,
                                                                       FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                                                       ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                                                       Time = t1.Time
                                                                   }).OrderByDescending(x => x.ID).AsEnumerable()); ;
            return vmPurchaseRequisition;
        }




        public async Task<int> ProcurementPurchaseRequisitionAdd(VMPurchaseRequisition vmPurchaseRequisition)
        {

            var result = -1;
            var totalRequisition = _db.Procurement_PurchaseRequisition.Where(x => x.Time.ToShortDateString() == DateTime.Today.ToShortDateString()).Count() + 1;
            string requisitionCID = @"REQ" +
                                    DateTime.Now.ToString("dd") +
                                    DateTime.Now.ToString("MM") +
                                    DateTime.Now.ToString("yy") + "-" + totalRequisition.ToString().PadLeft(2, '0');

            Procurement_PurchaseRequisition procurementPurchaseRequisition = new Procurement_PurchaseRequisition
            {
                RequisitionCID = requisitionCID,
                Status = vmPurchaseRequisition.Status,
                RequisitionDate = vmPurchaseRequisition.RequisitionDate,
                User = _vmLogin.Name,
                UserID = _vmLogin.ID,
                FromUser_DepartmentFk = _vmLogin.UserDepartmentId,
                ToUser_DepartmentFK = (int)EnumDepartment.Procurement,
                Common_ShopFK = _vmLogin.Common_ShopFK,


                Remarks = vmPurchaseRequisition.RequisitionRemarks
            };
            _db.Procurement_PurchaseRequisition.Add(procurementPurchaseRequisition);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = procurementPurchaseRequisition.ID;
            }

            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionEdit(VMPurchaseRequisition vmPurchaseRequisition)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(vmPurchaseRequisition.ID);
            if (procurementPurchaseRequisition != null)
            {
                procurementPurchaseRequisition.RequisitionDate = vmPurchaseRequisition.RequisitionDate;
                procurementPurchaseRequisition.Remarks = vmPurchaseRequisition.RequisitionRemarks;

                procurementPurchaseRequisition.UserID = _vmLogin.ID;
                procurementPurchaseRequisition.Common_ShopFK = _vmLogin.Common_ShopFK;


                if (await _db.SaveChangesAsync() > 0)
                {
                    result = vmPurchaseRequisition.ID;
                }
            }
            return result;
        }

        public async Task<VMPurchaseRequisitionSlave> GetSingleProcurementPurchaseRequisitionSlave(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave
                                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                          join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                          join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                          join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                          join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID


                                          where t1.ID == id
                                          select new VMPurchaseRequisitionSlave
                                          {
                                              Procurement_PurchaseRequisitionFK = t1.Procurement_PurchaseRequisitionFK,
                                              CurrentStock = t1.CurrentStock,

                                              Common_ProductCategoryFK = t5.ID,
                                              Common_ProductSubCategoryFK = t4.ID,
                                              Common_ProductFK = t2.ID,
                                              ProductName = t2.Name,
                                              SubCategoryName = t4.Name,
                                              BrandName = t6.Name,

                                              CategoryName = t5.Name,
                                              SystemBarcode = t2.SystemBarcode,
                                              MRPPrice = t2.MRPPrice,
                                              RequisitionQuantity = t1.RequisitionQuantity,
                                              SupplierNames = t1.SupplierNames,
                                              UnitName = t3.Name,
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VMPurchaseRequisition> GetSingleProcurementPurchaseRequisition(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisition


                                          where t1.ID == id
                                          select new VMPurchaseRequisition
                                          {
                                              ID = t1.ID,
                                              IsHold = t1.IsHold,
                                              FromUser_DepartmentFk = t1.FromUser_DepartmentFk,
                                              IsCancel = t1.IsCancel,
                                              IsActive = t1.Active,
                                              POStatusInPRFk = t1.POStatusInPRFk,

                                              RequisitionCID = t1.RequisitionCID,
                                              RequisitionDate = t1.RequisitionDate,
                                              Status = t1.Status,
                                              ToUser_DepartmentFK = t1.ToUser_DepartmentFK,
                                              UserID = t1.UserID,
                                              RequisitionRemarks = t1.Remarks,
                                              Common_ShopFK = t1.Common_ShopFK
                                          }).FirstOrDefault());
            return v;
        }




        #endregion

        #region PR Submit Hold-UnHold Cancel-Renew Closed-Reopen Delete

        public async Task<int> ProcurementPurchaseRequisitionDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (procurementPurchaseRequisition != null)
            {
                procurementPurchaseRequisition.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisition.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionSubmit(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (procurementPurchaseRequisition != null)
            {
                if (procurementPurchaseRequisition.Status == (int)EnumPRStatus.Draft)
                {
                    procurementPurchaseRequisition.Status = (int)EnumPRStatus.Submitted;
                }
                else
                {
                    procurementPurchaseRequisition.Status = (int)EnumPRStatus.Draft;

                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisition.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionHoldUnHold(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (procurementPurchaseRequisition != null)
            {
                if (procurementPurchaseRequisition.IsHold)
                {
                    procurementPurchaseRequisition.IsHold = false;
                }
                else
                {
                    procurementPurchaseRequisition.IsHold = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisition.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionCancelRenew(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (procurementPurchaseRequisition != null)
            {
                if (procurementPurchaseRequisition.IsCancel)
                {
                    procurementPurchaseRequisition.IsCancel = false;
                }
                else
                {
                    procurementPurchaseRequisition.IsCancel = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisition.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseRequisitionClosedReopen(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition procurementPurchaseRequisition = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (procurementPurchaseRequisition != null)
            {
                if (procurementPurchaseRequisition.Status == (int)EnumPRStatus.Closed)
                {
                    procurementPurchaseRequisition.Status = (int)EnumPRStatus.Draft;
                }
                else
                {
                    procurementPurchaseRequisition.Status = (int)EnumPRStatus.Closed;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseRequisition.ID;
                }
            }
            return result;
        }

        #endregion

        #region PO Submit Hold-UnHold Cancel-Renew Closed-Reopen Delete

        public async Task<int> ProcurementPurchaseOrderSubmit(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (procurementPurchaseOrder != null)
            {
                if (procurementPurchaseOrder.Status == (int)EnumPRStatus.Draft)
                {
                    procurementPurchaseOrder.Status = (int)EnumPRStatus.Submitted;
                }
                else
                {
                    procurementPurchaseOrder.Status = (int)EnumPRStatus.Draft;

                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrder.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderHoldUnHold(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (procurementPurchaseOrder != null)
            {
                if (procurementPurchaseOrder.IsHold)
                {
                    procurementPurchaseOrder.IsHold = false;
                }
                else
                {
                    procurementPurchaseOrder.IsHold = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrder.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderCancelRenew(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (procurementPurchaseOrder != null)
            {
                if (procurementPurchaseOrder.IsCancel)
                {
                    procurementPurchaseOrder.IsCancel = false;
                }
                else
                {
                    procurementPurchaseOrder.IsCancel = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrder.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderClosedReopen(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (procurementPurchaseOrder != null)
            {
                if (procurementPurchaseOrder.Status == (int)EnumPOStatus.Closed)
                {
                    procurementPurchaseOrder.Status = (int)EnumPOStatus.Draft;
                }
                else
                {
                    procurementPurchaseOrder.Status = (int)EnumPOStatus.Closed;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrder.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (procurementPurchaseOrder != null)
            {
                procurementPurchaseOrder.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrder.ID;
                }
            }
            return result;
        }


        #endregion



        #region Purchase Order
        public async Task<VMPurchaseOrderSlavePartial> ProcurementPurchaseRequisitionSlavePartialGet(int purchaseRequisitionId)
        {
            VMPurchaseOrderSlavePartial vmPurchaseOrderSlavePartial = new VMPurchaseOrderSlavePartial();


            vmPurchaseOrderSlavePartial.DataListPartial = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == purchaseRequisitionId && x.Active)
                                                                                join t2 in _db.Procurement_PurchaseRequisition on t1.Procurement_PurchaseRequisitionFK equals t2.ID
                                                                                join t3 in _db.Common_Product on t1.Common_ProductFK equals t3.ID
                                                                                join t4 in _db.Common_ProductSubCategory on t3.Common_ProductSubCategoryFk equals t4.ID
                                                                                join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                                                join t6 in _db.Common_Unit on t3.Common_UnitFk equals t6.ID


                                                                                select new VMPurchaseOrderSlavePartial
                                                                                {
                                                                                    UnitName = t6.Name,
                                                                                    UserID = t1.UserID,
                                                                                    DescriptionSlave = t1.Remarks,
                                                                                    SupplierNames = t1.SupplierNames,
                                                                                    Common_ProductFK = t1.Common_ProductFK,
                                                                                    RequisitionQuantity = t1.RequisitionQuantity,
                                                                                    CurrentStock = t1.CurrentStock,
                                                                                    Procurement_PurchaseRequisitionSlaveFK = t1.ID,
                                                                                    ProcuredQuantity = ((from POS in _db.Procurement_PurchaseOrderSlave
                                                                                                         join PO in _db.Procurement_PurchaseOrder on POS.Procurement_PurchaseOrderFK equals PO.ID
                                                                                                         where PO.Active && POS.Active && POS.Procurement_PurchaseRequisitionSlaveFK == t1.ID
                                                                                                         select POS.PurchaseQuantity).DefaultIfEmpty(0).Sum()),
                                                                                    ProductName = t3.Name
                                                                                }).ToListAsync());
            return vmPurchaseOrderSlavePartial;
        }

        public async Task<int> ProcurementPurchaseOrderAdd(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {
            var result = -1;
            var poMax = _db.Procurement_PurchaseOrder.Where(x => x.Time.ToShortDateString() == DateTime.Today.ToShortDateString()).Count() + 1;
            string poCid = @"PO" +
                            DateTime.Now.ToString("dd") +
                            DateTime.Now.ToString("MM") +
                            DateTime.Now.ToString("yy") + "-" +

                             poMax.ToString().PadLeft(2, '0');
            Procurement_PurchaseOrder Procurement_PurchaseOrder = new Procurement_PurchaseOrder
            {
                CID = poCid,
                OrderDate = vmPurchaseOrderSlave.OrderDate,
                Common_SupplierFK = vmPurchaseOrderSlave.Common_SupplierFK,
                TermsAndCondition = vmPurchaseOrderSlave.TermsAndCondition,

                Description = vmPurchaseOrderSlave.Description,
                SupplierPaymentMethodEnumFK = vmPurchaseOrderSlave.SupplierPaymentMethodEnumFK,
                DeliveryAddress = vmPurchaseOrderSlave.DeliveryAddress,
                DeliveryDate = vmPurchaseOrderSlave.DeliveryDate,
                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK,

                Status = (int)EnumPOStatus.Draft,
            };
            _db.Procurement_PurchaseOrder.Add(Procurement_PurchaseOrder);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseOrder.ID;
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderEdit(VMPurchaseOrder vmPurchaseOrder)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(vmPurchaseOrder.ID);
            if (procurementPurchaseOrder != null)
            {
                procurementPurchaseOrder.OrderDate = vmPurchaseOrder.OrderDate;
                procurementPurchaseOrder.Description = vmPurchaseOrder.Description;
                procurementPurchaseOrder.DeliveryDate = vmPurchaseOrder.DeliveryDate;
                procurementPurchaseOrder.DeliveryAddress = vmPurchaseOrder.DeliveryAddress;
                procurementPurchaseOrder.Common_SupplierFK = vmPurchaseOrder.Common_SupplierFK;
                procurementPurchaseOrder.SupplierPaymentMethodEnumFK = vmPurchaseOrder.SupplierPaymentMethodEnumFK;
                procurementPurchaseOrder.TermsAndCondition = vmPurchaseOrder.TermsAndCondition;
                procurementPurchaseOrder.Common_ShopFK = _vmLogin.Common_ShopFK;


                procurementPurchaseOrder.UserID = _vmLogin.ID;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = vmPurchaseOrder.ID;
                }
            }
            return result;
        }

        public async Task<int> ProcurementPurchaseOrderSlaveEdit(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {
            var result = -1;
            Procurement_PurchaseOrderSlave model = await _db.Procurement_PurchaseOrderSlave.FindAsync(vmPurchaseOrderSlave.ID);
            if (model != null)
            {
                model.PurchaseQuantity = vmPurchaseOrderSlave.PurchaseQuantity;
                model.PurchasingPrice = vmPurchaseOrderSlave.PurchasingPrice;



                model.UserID = _vmLogin.ID;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = vmPurchaseOrderSlave.ID;
                }
            }
            return result;
        }
        private async Task<int> PRStatusFromPOUpdate(int procurementPurchaseRequisitionFK)
        {
            int result = -1;
            var procurementPurchaseRequisition = _db.Procurement_PurchaseRequisition.Where(x => x.ID == procurementPurchaseRequisitionFK).FirstOrDefault();

            var totalRequisitionQuantity = _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == procurementPurchaseRequisitionFK && x.Active).Select(x => x.RequisitionQuantity).DefaultIfEmpty(0).Sum();

            var totalPurchaseQuantity = (from t1 in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active)
                                         join t2 in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && x.Procurement_PurchaseRequisitionFK == procurementPurchaseRequisitionFK) on t1.Procurement_PurchaseRequisitionSlaveFK equals t2.ID
                                         select t1.PurchaseQuantity).DefaultIfEmpty(0).Sum();

            if (totalRequisitionQuantity <= Convert.ToDecimal(totalPurchaseQuantity))
            {
                procurementPurchaseRequisition.POStatusInPRFk = (int)POStatusInPR.FullPOCreated;
            }
            else if (totalRequisitionQuantity > Convert.ToDecimal(totalPurchaseQuantity))
            {
                procurementPurchaseRequisition.POStatusInPRFk = (int)POStatusInPR.PartialPOCreated;
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = procurementPurchaseRequisition.ID;
            }
            return result;
        }
        public async Task<int> ProcurementPurchaseOrderPOValueUpdate(VMPurchaseOrder vmPurchaseOrder)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(vmPurchaseOrder.ID);
            if (procurementPurchaseOrder != null)
            {
                procurementPurchaseOrder.TotalPOValue = vmPurchaseOrder.TotalPOValue;
                procurementPurchaseOrder.UserID = _vmLogin.ID;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = vmPurchaseOrder.ID;
                }
            }
            return result;
        }

        private async Task<int> z(List<VMPurchaseRequisition> model)
        {
            List<Procurement_PurchaseRequisition> enPRs = new List<Procurement_PurchaseRequisition>();
            int result = -1;


            decimal? PRqty = 0;
            decimal poQty = 0;

            if (model == null || model.Count <= 0) { return result; }

            enPRs = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Active && model.Any(y => x.ID == y.ID)).ToListAsync()));

            foreach (Procurement_PurchaseRequisition oItem in enPRs)
            {
                poQty = 0;
                PRqty = 0;
                PRqty = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).Select(x => x.RequisitionQuantity).DefaultIfEmpty(0).Sum()));
                poQty = await Task.Run(() => (_db.Procurement_PurchaseOrderSlave.Where(z => z.Active && _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).Any(w => w.ID == z.Procurement_PurchaseRequisitionSlaveFK)).Select(x => x.PurchaseQuantity).DefaultIfEmpty(0).Sum()));

                if (PRqty <= Convert.ToDecimal(poQty))
                {
                    oItem.Status = (int)POStatusInPR.FullPOCreated;
                }
                else if (PRqty > Convert.ToDecimal(poQty))
                {
                    oItem.Status = (int)POStatusInPR.PartialPOCreated;
                }
            }

            result = await _db.SaveChangesAsync();

            return result;
        }

        public async Task<int> ProcurementPurchaseOrderSlaveAdd(VMPurchaseOrderSlave vmPurchaseOrderSlave, VMPurchaseOrderSlavePartial vmPurchaseOrderSlavePartial)
        {
            var result = -1;
            var dataListPartial = vmPurchaseOrderSlavePartial.DataListPartial.Where(x => x.PurchaseQuantity > 0 && x.Flag);
            List<Procurement_PurchaseOrderSlave> procurementPurchaseOrderSlaveList = new List<Procurement_PurchaseOrderSlave>();
            decimal TotalPurchasingPrice = 0;
            foreach (var item in dataListPartial)
            {

                Procurement_PurchaseOrderSlave procurementPurchaseOrderSlave = new Procurement_PurchaseOrderSlave
                {
                    Procurement_PurchaseOrderFK = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK,
                    Procurement_PurchaseRequisitionSlaveFK = item.Procurement_PurchaseRequisitionSlaveFK,
                    Common_ProductFK = item.Common_ProductFK.Value,
                    PurchaseQuantity = item.PurchaseQuantity,
                    PurchasingPrice = item.PurchasingPrice,
                    Description = item.DescriptionSlave,
                    User = _vmLogin.ID.ToString(),
                    UserID = _vmLogin.ID,
                    Common_ShopFK = _vmLogin.Common_ShopFK


                };
                procurementPurchaseOrderSlaveList.Add(procurementPurchaseOrderSlave);
            }
            //TotalPurchasingPrice = procurementPurchaseOrderSlaveList.Sum(x => x.PurchasingPrice);
            _db.Procurement_PurchaseOrderSlave.AddRange(procurementPurchaseOrderSlaveList);
            await _db.SaveChangesAsync();
            result = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK;
            vmPurchaseOrderSlave.ID = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK;

            vmPurchaseOrderSlave.TotalPOValue = _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == vmPurchaseOrderSlave.Procurement_PurchaseOrderFK).Select(x => x.PurchaseQuantity * x.PurchasingPrice).DefaultIfEmpty(0m).Sum();
            await ProcurementPurchaseOrderPOValueUpdate(vmPurchaseOrderSlave);
            await PRStatusFromPOUpdate(vmPurchaseOrderSlave.Procurement_PurchaseRequisitionFK);


            return result;
        }


        public async Task<int> ProcurementGeneralPurchaseOrderSlaveAdd(VMPurchaseOrderSlave vmPurchaseOrderSlave)
        {
            var result = -1;

            Procurement_PurchaseOrderSlave procurementPurchaseOrderSlave = new Procurement_PurchaseOrderSlave
            {
                Procurement_PurchaseOrderFK = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK,
                Common_ProductFK = vmPurchaseOrderSlave.Common_ProductFK.Value,
                PurchaseQuantity = vmPurchaseOrderSlave.PurchaseQuantity,
                PurchasingPrice = vmPurchaseOrderSlave.PurchasingPrice,
                User = _vmLogin.ID.ToString(),
                UserID = _vmLogin.ID,
                Common_ShopFK = _vmLogin.Common_ShopFK


            };
            _db.Procurement_PurchaseOrderSlave.Add(procurementPurchaseOrderSlave);
            await _db.SaveChangesAsync();
            result = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK;
            vmPurchaseOrderSlave.ID = vmPurchaseOrderSlave.Procurement_PurchaseOrderFK;

            vmPurchaseOrderSlave.TotalPOValue = _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == vmPurchaseOrderSlave.Procurement_PurchaseOrderFK).Select(x => x.PurchaseQuantity * x.PurchasingPrice).DefaultIfEmpty(0m).Sum();
            await ProcurementPurchaseOrderPOValueUpdate(vmPurchaseOrderSlave);
            // await PRStatusFromPOUpdate(vmPurchaseOrderSlave.Procurement_PurchaseRequisitionFK);


            return result;
        }

        public async Task<VMPurchaseOrder> ProcurementPurchaseOrderListGet()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Status < (int)EnumPOStatus.Closed && !x.IsCancel && !x.IsHold && x.Common_ShopFK == _vmLogin.Common_ShopFK)
                                                             join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID

                                                             join t4 in _db.User_User on t1.UserID equals t4.ID


                                                             select new VMPurchaseOrder
                                                             {
                                                                 ID = t1.ID,
                                                                 SupplierName = t2.Name,

                                                                 CID = t1.CID,
                                                                 OrderDate = t1.OrderDate,
                                                                 Description = t1.Remarks,
                                                                 IsHold = t1.IsHold,
                                                                 IsCancel = t1.IsCancel,
                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 TotalPOValue = t1.TotalPOValue,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmPurchaseOrder;
        }







        public async Task<VMPurchaseOrder> ProcurementApprovalPurchaseOrderListGet()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Common_ShopFK == _vmLogin.Common_ShopFK)
                                                             join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID

                                                             join t4 in _db.User_User on t1.UserID equals t4.ID
                                                             select new VMPurchaseOrder
                                                             {
                                                                 ID = t1.ID,
                                                                 SupplierName = t2.Name,

                                                                 CID = t1.CID,
                                                                 OrderDate = t1.OrderDate,
                                                                 Description = t1.Remarks,
                                                                 IsHold = t1.IsHold,
                                                                 IsCancel = t1.IsCancel,
                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 TotalPOValue = t1.TotalPOValue,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmPurchaseOrder;
        }


        public async Task<VMPurchaseOrder> ProcurementCancelPurchaseOrderListGet()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.IsCancel && x.Common_ShopFK == _vmLogin.Common_ShopFK)
                                                             join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID

                                                             join t4 in _db.User_User on t1.UserID equals t4.ID
                                                             select new VMPurchaseOrder
                                                             {
                                                                 ID = t1.ID,
                                                                 SupplierName = t2.Name,

                                                                 CID = t1.CID,
                                                                 OrderDate = t1.OrderDate,
                                                                 Description = t1.Remarks,
                                                                 IsHold = t1.IsHold,
                                                                 IsCancel = t1.IsCancel,
                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 TotalPOValue = t1.TotalPOValue,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmPurchaseOrder;
        }


        public async Task<VMPurchaseOrder> ProcurementHoldPurchaseOrderListGet()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.IsHold && x.Common_ShopFK == _vmLogin.Common_ShopFK)
                                                             join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID

                                                             join t4 in _db.User_User on t1.UserID equals t4.ID
                                                             select new VMPurchaseOrder
                                                             {
                                                                 ID = t1.ID,
                                                                 SupplierName = t2.Name,

                                                                 CID = t1.CID,
                                                                 OrderDate = t1.OrderDate,
                                                                 Description = t1.Remarks,
                                                                 IsHold = t1.IsHold,
                                                                 IsCancel = t1.IsCancel,
                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 TotalPOValue = t1.TotalPOValue,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmPurchaseOrder;
        }


        public async Task<VMPurchaseOrder> ProcurementClosedPurchaseOrderListGet()
        {
            VMPurchaseOrder vmPurchaseOrder = new VMPurchaseOrder();
            vmPurchaseOrder.DataList = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Status == (int)EnumPOStatus.Closed && x.Common_ShopFK == _vmLogin.Common_ShopFK)
                                                             join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID

                                                             join t4 in _db.User_User on t1.UserID equals t4.ID

                                                             select new VMPurchaseOrder
                                                             {
                                                                 ID = t1.ID,
                                                                 SupplierName = t2.Name,

                                                                 CID = t1.CID,
                                                                 OrderDate = t1.OrderDate,
                                                                 Description = t1.Remarks,
                                                                 IsHold = t1.IsHold,
                                                                 IsCancel = t1.IsCancel,
                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 TotalPOValue = t1.TotalPOValue,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            return vmPurchaseOrder;
        }



        public async Task<int> ProcurementPurchaseOrderSlaveDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseOrderSlave procurementPurchaseOrderSlave = await _db.Procurement_PurchaseOrderSlave.FindAsync(id);
            if (procurementPurchaseOrderSlave != null)
            {
                procurementPurchaseOrderSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = procurementPurchaseOrderSlave.Procurement_PurchaseOrderFK;
                }
            }
            return result;
        }


        public async Task<VMPurchaseOrder> GetSingleProcurementPurchaseOrder(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder

                                          where t1.ID == id
                                          select new VMPurchaseOrder
                                          {
                                              ID = t1.ID,

                                              IsHold = t1.IsHold,
                                              IsCancel = t1.IsCancel,
                                              IsActive = t1.Active,


                                              CID = t1.CID,
                                              OrderDate = t1.OrderDate,
                                              Status = t1.Status,
                                              SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,
                                              UserID = t1.UserID,
                                              Common_SupplierFK = t1.Common_SupplierFK,


                                              DeliveryDate = t1.DeliveryDate,
                                              DeliveryAddress = t1.DeliveryAddress,

                                              Description = t1.Description,
                                              TermsAndCondition = t1.TermsAndCondition

                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VMPurchaseOrderSlave> GetSingleProcurementPurchaseOrderSlave(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrderSlave
                                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID

                                          where t1.ID == id
                                          select new VMPurchaseOrderSlave
                                          {
                                              ID = t1.ID,
                                              ProductName = t2.Name,
                                              PurchasingPrice = t1.PurchasingPrice,
                                              PurchaseQuantity = t1.PurchaseQuantity,
                                              Procurement_PurchaseOrderFK = t1.Procurement_PurchaseOrderFK

                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMPurchaseRequisitionSlave> GetSinglePurchaseRequisitionSlave(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Procurement_PurchaseRequisitionSlave
                                          join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID


                                          where t1.ID == id
                                          select new VMPurchaseRequisitionSlave
                                          {
                                              ID = t1.ID,
                                              ProductName = t2.Name,
                                              RequisitionQuantity = t1.RequisitionQuantity,
                                              ProductDescription = t1.Remarks,
                                              SupplierNames = t1.SupplierNames,
                                              Procurement_PurchaseRequisitionFK = t1.Procurement_PurchaseRequisitionFK

                                          }).FirstOrDefault());
            return v;
        }



        public async Task<IEnumerable<VMPurchaseOrderSlave>> ProcurementPurchaseOrderSlaveGet(int id)
        {

            var DataListSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == id)
                                                      join t0 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFK equals t0.ID
                                                      join t2 in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active) on t1.Procurement_PurchaseRequisitionSlaveFK equals t2.ID into t2_Join
                                                      from t2 in t2_Join.DefaultIfEmpty()
                                                      join t3 in _db.Common_Product.Where(x => x.Active) on t1.Common_ProductFK equals t3.ID
                                                      join t4 in _db.Common_ProductSubCategory.Where(x => x.Active) on t3.Common_ProductSubCategoryFk equals t4.ID
                                                      join t5 in _db.Common_ProductCategory.Where(x => x.Active) on t4.Common_ProductCategoryFk equals t5.ID
                                                      join t6 in _db.Common_Unit.Where(x => x.Active) on t3.Common_UnitFk equals t6.ID
                                                      join t7 in _db.Common_Brand.Where(x => x.Active) on t3.Common_BrandFk equals t7.ID

                                                      select new VMPurchaseOrderSlave
                                                      {
                                                          Procurement_PurchaseOrderFK = t1 != null ? t1.Procurement_PurchaseOrderFK : 0,
                                                          Procurement_PurchaseRequisitionSlaveFK = t1 != null ? t1.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                                          ID = t1 != null ? t1.ID : 0,
                                                          PurchaseQuantity = t1 != null ? t1.PurchaseQuantity : 0,
                                                          PurchasingPrice = t1 != null ? t1.PurchasingPrice : 0,
                                                          RequisitionQuantity = t2.RequisitionQuantity,
                                                          ProcuredQuantity = (t1 != null ? t1.PurchaseQuantity : 0) - (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseRequisitionSlaveFK == t2.ID) select new { PurchasingQuantity = POS.PurchaseQuantity }).Sum(x => x.PurchasingQuantity),
                                                          Common_SupplierFK = t0.Common_SupplierFK,
                                                          Common_ProductCategoryFK = t5.ID,
                                                          Common_ProductSubCategoryFK = t4.ID,
                                                          Common_ProductFK = t3.ID,
                                                          ProductName = t3.Name,
                                                          SystemBarcode = t3.SystemBarcode,
                                                          SubCategoryName = t4.Name,
                                                          CategoryName = t5.Name,
                                                          BrandName = t7.Name,
                                                          UnitName = t6.Name
                                                      }).OrderByDescending(x => x.ID).AsEnumerable());





            return DataListSlave;
        }

        public async Task<VMPurchaseOrderSlave> ProcurementPurchaseOrderGetByID(int id)
        {

            var vmPurchaseOrderSlave = await Task.Run(() => (from t1 in _db.Procurement_PurchaseOrder.Where(x => x.ID == id && x.Active)

                                                             join t3 in _db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                                                             join t4 in _db.User_User on t1.UserID equals t4.ID
                                                             select new VMPurchaseOrderSlave
                                                             {
                                                                 Procurement_PurchaseOrderFK = t1.ID,
                                                                 CID = t1.CID,

                                                                 OrderDate = t1.OrderDate,
                                                                 SupplierPaymentMethodEnumFK = t1.SupplierPaymentMethodEnumFK,

                                                                 Status = t1.Status,
                                                                 User = t4.Name,
                                                                 UserID = t1.UserID,

                                                                 Description = t1.Description,
                                                                 Common_SupplierFK = t1.Common_SupplierFK,
                                                                 SupplierName = t3 != null ? t3.Name : "",

                                                                 TermsAndCondition = t1.TermsAndCondition,

                                                                 DeliveryAddress = t1.DeliveryAddress,
                                                                 DeliveryDate = t1.DeliveryDate,
                                                                 Time = t1.Time,

                                                             }).FirstOrDefault());



            return vmPurchaseOrderSlave;
        }
        public async Task<VMCommonSupplier> GetPOWiseSuppliers()
        {
            VMCommonSupplier vMSupplier = new VMCommonSupplier();
            vMSupplier.DataList = await Task.Run(() => (from t0 in _db.Procurement_PurchaseOrder
                                                        join t1 in _db.Common_Supplier on t0.Common_SupplierFK equals t1.ID into t1_Join
                                                        from t1 in t1_Join.DefaultIfEmpty()
                                                        where t0.Active
                                                        group new { t1, t0 }
                                                        by new { t1.Name, t1.ID, t1.Email, t1.Contact, t1.Address }
                                                        into g
                                                        select new VMCommonSupplier
                                                        {
                                                            ID = g.Key.ID,
                                                            Name = g.Key.Name,
                                                            Email = g.Key.Email,
                                                            Contact = g.Key.Contact,
                                                            Address = g.Key.Address
                                                        }).ToList());
            return vMSupplier;
        }

        #endregion

        public List<object> ApprovedPRList()
        {
            var List = new List<object>();
            var data = (_db.Procurement_PurchaseRequisition.Where(a => a.Active && a.Status == (int)EnumPRStatus.Submitted).AsEnumerable());
            foreach (var item in data)
            {
                List.Add(new { Text = item.RequisitionCID + " Date: " + item.RequisitionDate.ToLongDateString(), Value = item.ID });
            }
            return List;

        }



        public List<object> ApprovedPOList()
        {
            var List = new List<object>();
            _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Status == (int)EnumPOStatus.Submitted).Select(x => x).ToList() // x.ProcurementOriginTypeEnumFK == procurementOriginTypeEnumFK &&
           .ForEach(x => List.Add(new
           {
               Value = x.ID,
               Text = x.CID + " Date: " + x.OrderDate.ToLongDateString()
           }));
            return List;

        }

        public async Task<List<VMPurchaseOrder>> ProcurementPurchaseOrderGet(int commonSupplierFK)
        {
            var x = await Task.Run(() => (from t1 in _db.WareHouse_POReceiving
                                          join t2 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFk equals t2.ID
                                          where t1.Active && t2.Common_SupplierFK == commonSupplierFK
                                          group new { t1, t2 } by new { t2.CID, t2.ID } into Group
                                          select new VMPurchaseOrder
                                          {
                                              CID = Group.Key.CID + " Order Date: " + Group.First().t2.OrderDate.ToLongDateString(),
                                              ID = Group.Key.ID
                                          }).ToListAsync());
            return x;

        }
        public object GetAutoCompletePR(string prefix)
        {
            var v = (from t1 in _db.Procurement_PurchaseRequisition
                     where t1.Active && t1.Status == (int)EnumPRStatus.Submitted && ((t1.RequisitionCID.StartsWith(prefix)) || (t1.Remarks.StartsWith(prefix)) || (t1.RequisitionDate.ToString().StartsWith(prefix)))
                     && _vmLogin.Common_ShopFK == t1.Common_ShopFK
                     select new
                     {
                         label = t1.RequisitionCID != null ? t1.RequisitionCID + " Date " + t1.RequisitionDate.ToShortDateString() : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(20).ToList();

            return v;
        }
        public object GetAutoCompletePO(string prefix)
        {
            var v = (from t1 in _db.Procurement_PurchaseOrder
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     where t1.Active && t1.Status == (int)EnumPOStatus.Submitted && ((t1.CID.StartsWith(prefix)) || (t2.Name.StartsWith(prefix)) || (t1.OrderDate.ToString().StartsWith(prefix)))

                     select new
                     {
                         label = t1.CID != null ? t1.CID + " Date " + t1.OrderDate.ToShortDateString() : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(20).ToList();

            return v;
        }
        public object GetAutoCompleteSupplier(string prefix)
        {
            var v = (from t1 in _db.Common_Supplier
                     where t1.Active && ((t1.Name.StartsWith(prefix)) || (t1.Code.StartsWith(prefix)))

                     select new
                     {
                         label = t1.Name != null ? t1.Name + " Supplier ID " + t1.Code : "",
                         val = t1.ID
                     }).OrderBy(x => x.label).Take(40).ToList();

            return v;
        }
        public async Task<VMCommonProduct> CommonProductSingle(int id = 0, int shopId = 0)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct = await Task.Run(() => (from t1 in _db.Common_Product.Where(x => x.ID == id && x.Active)
                                                    join t3 in _db.Common_ProductSubCategory.Where(x => x.Active) on t1.Common_ProductSubCategoryFk equals t3.ID

                                                    join t2 in _db.Common_Unit.Where(x => x.Active) on t1.Common_UnitFk equals t2.ID
                                                    select new VMCommonProduct
                                                    {
                                                        ID = t1.ID,
                                                        Name = t1.Name,
                                                        SubCategoryName = t3.Name,
                                                        UnitName = t2.Name,
                                                        Common_UnitFk = t1.Common_UnitFk,
                                                        MRPPrice = t1.MRPPrice,
                                                        VATPercent = t1.VATPercent,
                                                        SystemBarcode = t1.SystemBarcode,
                                                        CostingPrice = t1.CostingPrice,
                                                        Common_SupplierFk = t1.Common_SupplierFk,
                                                        Common_ProductSubCategoryFk = t1.Common_ProductSubCategoryFk,
                                                        Common_ProductCategoryFk = t3.Common_ProductCategoryFk,
                                                        Common_BrandFk = t1.Common_BrandFk,


                                                        CurrentStockCS = (_db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == id && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity - x.Damage).DefaultIfEmpty(0).Sum()
                                                                      - _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active).Select(x => x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),

                                                        CurrentStock = //(((((
                                                                  ((from ts1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active) // Received From Store
                                                                            join ts2 in _db.WareHouse_Consumption.Where(x => x.Acknowledgement && x.Common_ShopFK == shopId) on ts1.WareHouse_ConsumptionFk equals ts2.ID
                                                                            select ts1.ConsumeQuantity).DefaultIfEmpty(0).Sum())
                                                                       // -
                                                                        //((from ts1 in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Common_ProductFK == id && x.Active)  // Transfer from
                                                                        //  join ts2 in _db.WareHouse_TransferShopToShop.Where(x => x.Acknowledgement && x.Common_ShopFK == shopId) on ts1.WareHouse_TransferShopToShopFk equals ts2.ID
                                                                        //  select ts1.TransferQuantity).DefaultIfEmpty(0).Sum()))
                                                                        // +
                                                                        // ((from ts1 in _db.WareHouse_TransferShopToShopSlave.Where(x => x.Common_ProductFK == id && x.Active) //Transfer to
                                                                        //   join ts2 in _db.WareHouse_TransferShopToShop.Where(x => x.Acknowledgement && x.ToCommon_ShopFK == shopId) on ts1.WareHouse_TransferShopToShopFk equals ts2.ID
                                                                        //   select ts1.TransferQuantity).DefaultIfEmpty(0).Sum())
                                                                        // ))
                                                                        //- _db.Marketing_SalesSlave.Where(x => x.Common_ProductFK == id && x.Active
                                                                        //&& (shopId > 0 ? x.Common_ShopFK == shopId : x.Common_ShopFK > 0)).Select(x => x.Quantity).DefaultIfEmpty(0).Sum())
                                                                        //- _db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == id && x.Active 
                                                                        //  && x.IsReturn).Select(x => x.ReceivedQuantity).DefaultIfEmpty(0).Sum())
                                                    }).FirstOrDefault());

            return vmCommonProduct;
        }
        public async Task<VMCommonProduct> CommonProductByShopSingle(int id, int shopId)
        {
            VMCommonProduct vmCommonProduct = new VMCommonProduct();

            vmCommonProduct = await Task.Run(() => (from t1 in _db.Common_Product.Where(x => x.ID == id && x.Active)
                                                    join t3 in _db.Common_ProductSubCategory.Where(x => x.Active) on t1.Common_ProductSubCategoryFk equals t3.ID

                                                    join t2 in _db.Common_Unit.Where(x => x.Active) on t1.Common_UnitFk equals t2.ID
                                                    select new VMCommonProduct
                                                    {
                                                        ID = t1.ID,
                                                        Name = t1.Name,
                                                        SubCategoryName = t3.Name,
                                                        UnitName = t2.Name,
                                                        Common_UnitFk = t1.Common_UnitFk,
                                                        MRPPrice = t1.MRPPrice,
                                                        VATPercent = t1.VATPercent,
                                                        SystemBarcode = t1.SystemBarcode,

                                                        CurrentStockCS = (_db.WareHouse_POReceivingSlave.Where(x => x.Common_ProductFK == id && x.Active && !x.IsReturn).Select(x => x.ReceivedQuantity - x.Damage).DefaultIfEmpty(0).Sum()
                                                                      - _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active).Select(x => x.ConsumeQuantity).DefaultIfEmpty(0).Sum()),

                                                        CurrentStock = (_db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active && x.Common_ShopFK == shopId).Select(x => x.ConsumeQuantity).DefaultIfEmpty(0).Sum()
                                                                      - _db.Marketing_SalesSlave.Where(x => x.Common_ProductFK == id && x.Active && x.Common_ShopFK == shopId).Select(x => x.Quantity).DefaultIfEmpty(0).Sum())
                                                    }).FirstOrDefault());

            return vmCommonProduct;
        }

        public async Task<IEnumerable<VMCommonProduct>> ProcurementStockDetailsByProductGet(int id)
        {

            var DataListSlave = await Task.Run(() => (from t1 in _db.WareHouse_ConsumptionSlave.Where(x => x.Common_ProductFK == id && x.Active)
                                                      join t2 in _db.Common_Product on t1.Common_ProductFK equals t2.ID
                                                      join t3 in _db.Common_Unit on t2.Common_UnitFk equals t3.ID
                                                      join t4 in _db.Common_ProductSubCategory on t2.Common_ProductSubCategoryFk equals t4.ID
                                                      join t5 in _db.Common_ProductCategory on t4.Common_ProductCategoryFk equals t5.ID
                                                      join t6 in _db.Common_Brand on t2.Common_BrandFk equals t6.ID
                                                      join t7 in _db.Common_Shop on t1.Common_ShopFK equals t7.ID

                                                      group new { t1, t2, t3, t4, t5, t6, t7 } by new { t1.Common_ShopFK } into Group
                                                      select new VMCommonProduct
                                                      {

                                                          ShopName = Group.First().t7.Name,

                                                          CurrentStock = Group.Sum(x => x.t1.ConsumeQuantity) -

                                                          _db.Marketing_SalesSlave.Where(x => x.Common_ProductFK == id && x.Active && x.Common_ShopFK == Group.Key.Common_ShopFK).Select(x => x.Quantity).DefaultIfEmpty(0).Sum()

                                                      }).OrderByDescending(x => x.ID).AsEnumerable()); ;



            return DataListSlave;
        }
        public async Task<List<VMCommonProduct>> ProductCategoryGet()
        {
            List<VMCommonProduct> vMRSC = await Task.Run(() => (_db.Common_ProductCategory.Where(x => x.Active)).Select(x => new VMCommonProduct() { ID = x.ID, Name = x.Name }).ToListAsync());

            return vMRSC;
        }


        public async Task<List<VMCommonProductSubCategory>> CommonProductSubCategoryGet(int id)
        {

            List<VMCommonProductSubCategory> vMRSC = await Task.Run(() => (_db.Common_ProductSubCategory.Where(x => x.Active && (id <= 0 || x.Common_ProductCategoryFk == id))).Select(x => new VMCommonProductSubCategory() { ID = x.ID, Name = x.Name }).ToListAsync());


            return vMRSC;
        }
        public async Task<List<VMCommonProduct>> CommonProductGet(int id)
        {
            List<VMCommonProduct> vMRI = await Task.Run(() => (_db.Common_Product.Where(x => x.Active && (id <= 0 || x.Common_ProductSubCategoryFk == id)).Select(x => new VMCommonProduct() { ID = x.ID, Name = x.Name })).ToListAsync());

            return vMRI;
        }




        #region Enum Model
        public class EnumOriginModel
        {
            public int Value { get; set; }
            public string Text { get; set; }
        }
        public class EnumPOTypeModel
        {
            public int Value { get; set; }
            public string Text { get; set; }
        }

        #endregion
    }
}