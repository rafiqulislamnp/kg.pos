﻿using KG.Core.Entity.Configuration;
using KG.Core.Entity.Marketing;
using KG.Core.Entity.Procurement;
using KG.Core.Entity.User;
using KG.Core.Entity.WareHouse;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading;
using System.Threading.Tasks;


namespace KG.Core.Services
{
    public interface IPosDbContext
    {

        //User Credentials & Menu
        DbSet<User_Department> User_Department { get; set; }
        DbSet<User_User> User_User { get; set; }
        DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        DbSet<User_Menu> User_Menu { get; set; }
        DbSet<User_MenuItem> User_MenuItem { get; set; }
        DbSet<User_Role> User_Role { get; set; }
        DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }
        DbSet<User_SubMenu> User_SubMenu { get; set; }
        DbSet<User_RoleAssign> User_RoleAssign { get; set; }
        DbSet<User_ManagerAccessableUser> User_ManagerAccessableUser { get; set; }

         DbSet<Procurement_PurchaseOrder> Procurement_PurchaseOrder { get; set; }
         DbSet<Procurement_PurchaseOrderSlave> Procurement_PurchaseOrderSlave { get; set; }
         DbSet<Procurement_PurchaseRequisition> Procurement_PurchaseRequisition { get; set; }
         DbSet<Procurement_PurchaseRequisitionSlave> Procurement_PurchaseRequisitionSlave { get; set; }
         DbSet<WareHouse_POReceiving> WareHouse_POReceiving { get; set; }
         DbSet<WareHouse_POReceivingSlave> WareHouse_POReceivingSlave { get; set; }
       
         DbSet<WareHouse_Consumption> WareHouse_Consumption { get; set; }
         DbSet<WareHouse_ConsumptionSlave> WareHouse_ConsumptionSlave { get; set; }

        DbSet<Marketing_Sales> Marketing_Sales { get; set; }
        DbSet<Marketing_SalesSlave> Marketing_SalesSlave { get; set; }

        DbSet<Common_Company> Common_Company { get; set; }
        DbSet<Common_Payment> Common_Payment { get; set; }

        DbSet<Common_Unit> Common_Unit { get; set; }
       DbSet<Common_Countries> Common_Countries { get; set; }
       DbSet<Common_Divisions> Common_Divisions { get; set; }
       DbSet<Common_Districts> Common_Districts { get; set; }
       DbSet<Common_Thana> Common_Thana { get; set; }
       DbSet<Common_Supplier> Common_Supplier { get; set; }
      
       DbSet<Common_ProductCategory> Common_ProductCategory { get; set; }
       DbSet<Common_ProductSubCategory> Common_ProductSubCategory { get; set; }
        
        DbSet<Common_Product> Common_Product { get; set; }
        DbSet<Common_ProductCosting> Common_ProductCosting { get; set; }

        
        DbSet<Common_Brand> Common_Brand { get; set; }
     

        DbSet<WareHouse_TransferShopToShop> WareHouse_TransferShopToShop { get; set; }
        DbSet<WareHouse_TransferShopToShopSlave> WareHouse_TransferShopToShopSlave { get; set; }


        DbSet<Common_TremsAndCondition> Common_TremsAndCondition { get; set; }

        DbSet<Common_Customer> Common_Customer { get; set; }
       DbSet<Common_CustomerPoint> Common_CustomerPoint { get; set; }
       DbSet<Common_Shop> Common_Shop { get; set; }
   
       DbSet<Common_ShopCounter> Common_ShopCounter { get; set; }
       DbSet<Common_Bin> Common_Bin { get; set; }
       DbSet<Common_BinSlave> Common_BinSlave { get; set; }
       DbSet<Common_ProductInBinSlave> Common_ProductInBinSlave { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        //DatabaseFacade Database { get; }
        //ValueTask<EntityEntry> AddAsync(object entity, CancellationToken cancellationToken = default(CancellationToken));
        int ExecuteSqlCommand(string command);
        //Task<int> ExecuteSqlCommandAsync(RawSqlString command, params object[] parameters);

    }

}
