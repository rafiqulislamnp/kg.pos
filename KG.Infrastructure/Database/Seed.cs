﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using KG.Infrastructure;
using KG.Core.Entity.User;
using KG.Core.Entity.Configuration;

namespace KG.Infrastructure.Database
{
    public static class Seed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<InfrastructureDbContext>();
            context.Database.EnsureCreated();

            //Do not Delete this seed process. Never ever

            if (!context.User_Role.Any())
            {
                context.User_Role.Add(new User_Role() { Active = true, User = "User", Time = DateTime.Now, Name = "Admin", UserID = 1, Common_ShopFK = 1 });
                context.User_Role.Add(new User_Role() { Active = true, User = "User", Time = DateTime.Now, Name = "User", UserID = 1, Common_ShopFK = 1 });
                context.User_Role.Add(new User_Role() { Active = true, User = "User", Time = DateTime.Now, Name = "Manager", UserID = 1, Common_ShopFK = 1 });


                context.SaveChanges();
            }
            if (!context.User_AccessLevel.Any())
            {
                context.User_AccessLevel.Add(new User_AccessLevel() { Active = true, User = "User", Time = DateTime.Now, Name = "Basic", UserID = 1, Common_ShopFK = 1 });
                context.User_AccessLevel.Add(new User_AccessLevel() { Active = true, User = "User", Time = DateTime.Now, Name = "Ultimate", UserID = 1, Common_ShopFK = 1 });
                context.User_AccessLevel.Add(new User_AccessLevel() { Active = true, User = "User", Time = DateTime.Now, Name = "Manager", UserID = 1, Common_ShopFK = 1 });


                context.SaveChanges();
            }
            if (!context.Common_Shop.Any())
            {
                context.Common_Shop.Add(new Common_Shop() { Active = true, User = "User", Time = DateTime.Now, Name = "Krishibid Bazar (01)", UserID = 1, Common_ShopFK = 1, Common_ThanaFk = 1, Address = "Kazipara, Mirpur, Dhaka"});
               

                context.SaveChanges();
            }
            if (!context.Common_ShopCounter.Any())
            {
                context.Common_ShopCounter.Add(new Common_ShopCounter() { Active = true, User = "User", Time = DateTime.Now, Name = "KG01001", UserID = 1, Common_ShopFK = 1 });


                context.SaveChanges();
            }
            if (!context.User_Department.Any())
            {
                context.User_Department.Add(new User_Department() { Name = "Admin", Common_ShopFK = 1, UserID = 1, Active = true, Time = DateTime.Now, Code = "01" });
                context.User_Department.Add(new User_Department() { Name = "Procurement", Common_ShopFK = 1, UserID = 1, Active = true, Time = DateTime.Now, Code = "02" });
                context.User_Department.Add(new User_Department() { Name = "WareHouse", Common_ShopFK = 1, UserID = 1, Active = true, Time = DateTime.Now, Code = "03" });
                context.User_Department.Add(new User_Department() { Name = "Sales", Common_ShopFK = 1, UserID = 1, Active = true, Time = DateTime.Now, Code = "04" });


                context.SaveChanges();
            }
            if (!context.User_User.Any())
            {
                context.User_User.Add(new User_User()
                {
                    Active = true,
                    User = "Admin",
                    Time = DateTime.Now,
                    Name = "Admin",
                    UserName = "Admin",
                    Password = "96e79218965eb72c92a549dd5a330112",
                    User_AccessLevelFK = 1,
                    Mobile = "09876543212",
                    Email = "mail@mail.com",
                    Locked = false,
                    Address = "Dhaka",
                    User_DepartmentFK = 1,
                    UserID = 1,
                    Common_ShopFK = 1,
                    Common_ShopCounterFK = 1
                     

                });

                context.SaveChanges();
            }




        }
    }
}
