﻿
using KG.Core.Services;
using KG.Core.Entity.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;
using KG.Core.Entity.Procurement;
using KG.Core.Entity.WareHouse;
using KG.Core.Entity.Configuration;
using KG.Core.Entity.Marketing;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace KG.Infrastructure
{
    public class InfrastructureDbContext : DbContext, IPosDbContext
    {
        public InfrastructureDbContext()
        {
        }

        public InfrastructureDbContext(DbContextOptions<InfrastructureDbContext> options) : base(options)
        {

        }

        //User Credentials
        public virtual DbSet<User_Department> User_Department { get; set; }
        public virtual DbSet<User_User> User_User { get; set; }
        public virtual DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        public virtual DbSet<User_Menu> User_Menu { get; set; }
        public virtual DbSet<User_MenuItem> User_MenuItem { get; set; }
        public virtual DbSet<User_Role> User_Role { get; set; }
        public virtual DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }
        public virtual DbSet<User_SubMenu> User_SubMenu { get; set; }
        public virtual DbSet<User_RoleAssign> User_RoleAssign { get; set; }
        public virtual DbSet<User_ManagerAccessableUser> User_ManagerAccessableUser { get; set; }

        public virtual DbSet<Marketing_Sales> Marketing_Sales { get; set; }
        public virtual DbSet<Marketing_SalesSlave> Marketing_SalesSlave { get; set; }
        public virtual DbSet<WareHouse_TransferShopToShop> WareHouse_TransferShopToShop { get; set; }
        public virtual DbSet<WareHouse_TransferShopToShopSlave> WareHouse_TransferShopToShopSlave { get; set; }


        public virtual DbSet<Procurement_PurchaseOrder> Procurement_PurchaseOrder { get; set; }
        public virtual DbSet<Procurement_PurchaseOrderSlave> Procurement_PurchaseOrderSlave { get; set; }
        public virtual DbSet<Procurement_PurchaseRequisition> Procurement_PurchaseRequisition { get; set; }
        public virtual DbSet<Procurement_PurchaseRequisitionSlave> Procurement_PurchaseRequisitionSlave { get; set; }
        public virtual DbSet<WareHouse_POReceiving> WareHouse_POReceiving { get; set; }
        public virtual DbSet<WareHouse_POReceivingSlave> WareHouse_POReceivingSlave { get; set; }      
        public virtual DbSet<WareHouse_Consumption> WareHouse_Consumption { get; set; }
        public virtual DbSet<WareHouse_ConsumptionSlave> WareHouse_ConsumptionSlave { get; set; }

        
        public virtual DbSet<Common_Company> Common_Company { get; set; }
        public virtual DbSet<Common_Payment> Common_Payment { get; set; }
        public virtual DbSet<Common_Unit> Common_Unit { get; set; }
        public virtual DbSet<Common_Countries> Common_Countries { get; set; }
        public virtual DbSet<Common_Divisions> Common_Divisions { get; set; }
        public virtual DbSet<Common_Districts> Common_Districts { get; set; }
        public virtual DbSet<Common_Thana> Common_Thana { get; set; }
        public virtual DbSet<Common_Supplier> Common_Supplier { get; set; }
      
        public virtual DbSet<Common_Product> Common_Product { get; set; }
        public virtual DbSet<Common_ProductCosting> Common_ProductCosting { get; set; }
        public virtual DbSet<Common_Brand> Common_Brand { get; set; }
       
        public virtual DbSet<Common_ProductCategory> Common_ProductCategory { get; set; }
        public virtual DbSet<Common_ProductSubCategory> Common_ProductSubCategory { get; set; }
        public virtual DbSet<Common_Customer> Common_Customer { get; set; }
        public virtual DbSet<Common_CustomerPoint> Common_CustomerPoint { get; set; }
       
        public virtual DbSet<Common_Bin> Common_Bin { get; set; }
        public virtual DbSet<Common_BinSlave> Common_BinSlave { get; set; }
        public virtual DbSet<Common_ProductInBinSlave> Common_ProductInBinSlave { get; set; }
        public virtual DbSet<Common_TremsAndCondition> Common_TremsAndCondition { get; set; }


        public virtual DbSet<Common_Shop> Common_Shop { get; set; }
        public virtual DbSet<Common_ShopCounter> Common_ShopCounter { get; set; }

        //public override ValueTask<EntityEntry> AddAsync(object entity, CancellationToken cancellationToken)
        //{
        //    return base.AddAsync(entity);
        //}
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return base.SaveChangesAsync();
        }
        //public override DatabaseFacade Database { get; }
        public int ExecuteSqlCommand(string command)
        {
            return base.Database.ExecuteSqlCommand(command);
        }

        ////public async Task<int> ExecuteSqlCommandAsync(RawSqlString command, params object[] parameters)
        ////{
        ////    return await base.Database.ExecuteSqlCommandAsync(command, parameters);
        ////}
    }
}
