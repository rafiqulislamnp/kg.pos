﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RedeemPoint",
                table: "Marketing_Sales",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsEarnPoint",
                table: "Common_CustomerPoint",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RedeemPoint",
                table: "Marketing_Sales");

            migrationBuilder.DropColumn(
                name: "IsEarnPoint",
                table: "Common_CustomerPoint");
        }
    }
}
