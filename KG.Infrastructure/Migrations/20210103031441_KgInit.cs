﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class KgInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Common_Bin",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    RackNo = table.Column<int>(nullable: false),
                    Row = table.Column<int>(nullable: false),
                    Column = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Bin", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_BinSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_BinFk = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Dimension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_BinSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Brand",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Brand", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Color",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Color", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Countries",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BnName = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Countries", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Customer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    Profession = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    MemberShipNo = table.Column<string>(nullable: true),
                    CustomerLoyalityPoint = table.Column<string>(nullable: true),
                    CustomerTypeEnumFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Customer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_CustomerContact",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Common_CustomerFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CustomerContact", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Districts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BnName = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    lat = table.Column<string>(nullable: true),
                    lon = table.Column<string>(nullable: true),
                    Common_DivisionsFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Districts", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Divisions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BnName = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Common_CountriesFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Divisions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Product",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    DisContinued = table.Column<string>(nullable: true),
                    Common_ProductSubCategoryFk = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_BrandFk = table.Column<int>(nullable: true),
                    Common_ColorFk = table.Column<int>(nullable: true),
                    Common_SizeFk = table.Column<int>(nullable: true),
                    CMPIDX = table.Column<string>(nullable: true),
                    SystemBarcode = table.Column<string>(nullable: true),
                    ProductBarcode = table.Column<string>(nullable: true),
                    CSSID = table.Column<string>(nullable: true),
                    SSID = table.Column<string>(nullable: true),
                    CBTID = table.Column<string>(nullable: true),
                    DiscountPercent = table.Column<decimal>(nullable: false),
                    DiscountValue = table.Column<decimal>(nullable: false),
                    DiscountExpiryDate = table.Column<DateTime>(nullable: false),
                    VATPercent = table.Column<decimal>(nullable: false),
                    MaximumSalesLimit = table.Column<decimal>(nullable: false),
                    CostingPrice = table.Column<decimal>(nullable: false),
                    MRPPrice = table.Column<decimal>(nullable: false),
                    Point = table.Column<decimal>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false),
                    Common_UnitFk = table.Column<int>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false),
                    ProductID = table.Column<string>(nullable: true),
                    PurchasePrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Product", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    VATPercent = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false),
                    VATPrcnt = table.Column<int>(nullable: false),
                    Point = table.Column<int>(nullable: false),
                    sAmt = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductCategory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductCosting",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    WareHouse_POReceivingSlaveFk = table.Column<int>(nullable: false),
                    LaborCost = table.Column<decimal>(nullable: false),
                    ManufacturingOverhead = table.Column<decimal>(nullable: false),
                    TransportationOverhead = table.Column<decimal>(nullable: false),
                    OthersCost = table.Column<decimal>(nullable: false),
                    OthersCostNote = table.Column<string>(nullable: true),
                    CostingPrice = table.Column<decimal>(nullable: false),
                    PurchasePrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductCosting", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductInBinSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_BinSlaveFk = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductInBinSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductSubCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    VATPercent = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Common_ProductCategoryFk = table.Column<int>(nullable: false),
                    IsLock = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductSubCategory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Shop",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Common_ThanaFk = table.Column<int>(nullable: false),
                    OwnDeliveryService = table.Column<bool>(nullable: false),
                    ServiceStartTime = table.Column<string>(nullable: true),
                    ServiceEndTime = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TradelicenseTypeEnumfk = table.Column<int>(nullable: false),
                    TradeLicenceNumber = table.Column<string>(nullable: true),
                    TradeLicenceExpireDate = table.Column<DateTime>(nullable: false),
                    TradeLicenceUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Shop", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ShopCounter",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ShopCounter", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ShopDeliveryService",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ShopDeliveryService", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Size",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Size", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Supplier",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_ThanaFk = table.Column<int>(nullable: false),
                    Contact = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    PaymentMethod = table.Column<string>(nullable: true),
                    DOE = table.Column<DateTime>(nullable: false),
                    BilltoBillCreditPeriod = table.Column<bool>(nullable: false),
                    CreditPeriod = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Supplier", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_SupplierContact",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Common_SupplierFk = table.Column<int>(nullable: false),
                    Designations = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_SupplierContact", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_SupplierProduct",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_SupplierFk = table.Column<int>(nullable: false),
                    Common_ProductFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_SupplierProduct", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Thana",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    BnName = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Common_DistrictsFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Thana", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_TremsAndCondition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_TremsAndCondition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Unit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Unit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Marketing_Sales",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    SaleInvoiceNo = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Common_CustomerFK = table.Column<int>(nullable: false),
                    TotalDiscount = table.Column<decimal>(nullable: false),
                    TotalVat = table.Column<decimal>(nullable: false),
                    PayableAmount = table.Column<decimal>(nullable: false),
                    TotalPaid = table.Column<decimal>(nullable: false),
                    ReturnAmount = table.Column<decimal>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CustomerPaymentMethodEnumFk = table.Column<int>(nullable: false),
                    ActualInvoiceValue = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    VatParcentage = table.Column<decimal>(nullable: false),
                    TotalIncludingVat = table.Column<decimal>(nullable: false),
                    DiscountParcentage = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marketing_Sales", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Marketing_SalesSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Marketing_SalesFk = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    ProductSpecification = table.Column<string>(nullable: true),
                    PurchasePrice = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    MRPPrice = table.Column<decimal>(nullable: false),
                    SubTotal = table.Column<decimal>(nullable: false),
                    ProductDiscountValue = table.Column<decimal>(nullable: false),
                    ProductDiscountPercent = table.Column<decimal>(nullable: false),
                    VAT = table.Column<decimal>(nullable: false),
                    VatValue = table.Column<decimal>(nullable: false),
                    CostingPrice = table.Column<decimal>(nullable: false),
                    Point = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Marketing_SalesSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    SupplierPaymentMethodEnumFK = table.Column<int>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    TotalPOValue = table.Column<decimal>(nullable: false),
                    TermsAndCondition = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DeliveryAddress = table.Column<string>(nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    IsHold = table.Column<bool>(nullable: false),
                    IsCancel = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ApprovedDate = table.Column<DateTime>(nullable: true),
                    ApprovedHistory = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseOrder", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseOrderSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseRequisitionSlaveFK = table.Column<int>(nullable: true),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<decimal>(nullable: false),
                    PurchasingPrice = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseOrderSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseRequisition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    RequisitionCID = table.Column<string>(nullable: true),
                    RequisitionDate = table.Column<DateTime>(nullable: false),
                    FromUser_DepartmentFk = table.Column<int>(nullable: false),
                    ToUser_DepartmentFK = table.Column<int>(nullable: false),
                    IsHold = table.Column<bool>(nullable: false),
                    IsCancel = table.Column<bool>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    POStatusInPRFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseRequisition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseRequisitionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseRequisitionFK = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    RequisitionQuantity = table.Column<decimal>(nullable: false),
                    SupplierNames = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseRequisitionSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_AccessLevel",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_AccessLevel", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Department",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    IsStore = table.Column<bool>(nullable: false),
                    InventoryTypeEnumFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Department", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_ManagerAccessableUser",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_ManagerUserFk = table.Column<int>(nullable: false),
                    User_ManagerAccessableUserFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_ManagerAccessableUser", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Menu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Menu", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Role",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Role", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_RoleAssign",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_RoleFk = table.Column<int>(nullable: false),
                    User_UserFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_RoleAssign", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WareHouse_Consumption",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    ConsumptionCID = table.Column<string>(maxLength: 25, nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    FromUser_DepartmentFk = table.Column<int>(nullable: false),
                    ToUser_DepartmentFK = table.Column<int>(nullable: false),
                    Acknowledgement = table.Column<bool>(nullable: false),
                    AcknowledgementDate = table.Column<DateTime>(nullable: true),
                    AcknowledgedBy = table.Column<string>(nullable: true),
                    IsRetuen = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_Consumption", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WareHouse_ConsumptionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    WareHouse_ConsumptionFk = table.Column<int>(nullable: false),
                    Procurement_PurchaseRequisitionSlaveFK = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    ConsumeQuantity = table.Column<decimal>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_ConsumptionSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WareHouse_POReceiving",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ChallanCID = table.Column<string>(nullable: true),
                    Challan = table.Column<string>(maxLength: 150, nullable: false),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    Procurement_PurchaseOrderFk = table.Column<int>(nullable: false),
                    User_DepartmentFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_POReceiving", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WareHouse_POReceivingSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ProductInBinSlaveFK = table.Column<int>(nullable: true),
                    WareHouse_POReceivingFk = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderSlaveFk = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    ReceivedQuantity = table.Column<decimal>(nullable: false),
                    StockLossQuantity = table.Column<decimal>(nullable: false),
                    PurchasingPrice = table.Column<decimal>(nullable: false),
                    Damage = table.Column<decimal>(nullable: false),
                    WareHouse_BinSlaveFk = table.Column<int>(nullable: false),
                    IsReturn = table.Column<bool>(nullable: false),
                    IsDefineBin = table.Column<bool>(nullable: false),
                    IsGRNCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_POReceivingSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_User",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Locked = table.Column<bool>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: false),
                    User_AccessLevelFK = table.Column<int>(nullable: false),
                    Common_ShopCounterFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_User", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_User_User_AccessLevel_User_AccessLevelFK",
                        column: x => x.User_AccessLevelFK,
                        principalTable: "User_AccessLevel",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_User_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_SubMenu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    User_MenuFk = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_SubMenu", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_SubMenu_User_Menu_User_MenuFk",
                        column: x => x.User_MenuFk,
                        principalTable: "User_Menu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_MenuItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    User_SubMenuFk = table.Column<int>(nullable: false),
                    Method = table.Column<string>(nullable: true),
                    IsAlone = table.Column<bool>(nullable: false),
                    IsMultiLayerOption = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_MenuItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_MenuItem_User_SubMenu_User_SubMenuFk",
                        column: x => x.User_SubMenuFk,
                        principalTable: "User_SubMenu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_RoleMenuItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_RoleFK = table.Column<int>(nullable: false),
                    User_MenuItemFk = table.Column<int>(nullable: false),
                    IsAllowed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_RoleMenuItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_RoleMenuItem_User_MenuItem_User_MenuItemFk",
                        column: x => x.User_MenuItemFk,
                        principalTable: "User_MenuItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_RoleMenuItem_User_Role_User_RoleFK",
                        column: x => x.User_RoleFK,
                        principalTable: "User_Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_MenuItem_User_SubMenuFk",
                table: "User_MenuItem",
                column: "User_SubMenuFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleMenuItem_User_MenuItemFk",
                table: "User_RoleMenuItem",
                column: "User_MenuItemFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleMenuItem_User_RoleFK",
                table: "User_RoleMenuItem",
                column: "User_RoleFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_SubMenu_User_MenuFk",
                table: "User_SubMenu",
                column: "User_MenuFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_User_User_AccessLevelFK",
                table: "User_User",
                column: "User_AccessLevelFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_User_User_DepartmentFK",
                table: "User_User",
                column: "User_DepartmentFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Common_Bin");

            migrationBuilder.DropTable(
                name: "Common_BinSlave");

            migrationBuilder.DropTable(
                name: "Common_Brand");

            migrationBuilder.DropTable(
                name: "Common_Color");

            migrationBuilder.DropTable(
                name: "Common_Countries");

            migrationBuilder.DropTable(
                name: "Common_Customer");

            migrationBuilder.DropTable(
                name: "Common_CustomerContact");

            migrationBuilder.DropTable(
                name: "Common_Districts");

            migrationBuilder.DropTable(
                name: "Common_Divisions");

            migrationBuilder.DropTable(
                name: "Common_Product");

            migrationBuilder.DropTable(
                name: "Common_ProductCategory");

            migrationBuilder.DropTable(
                name: "Common_ProductCosting");

            migrationBuilder.DropTable(
                name: "Common_ProductInBinSlave");

            migrationBuilder.DropTable(
                name: "Common_ProductSubCategory");

            migrationBuilder.DropTable(
                name: "Common_Shop");

            migrationBuilder.DropTable(
                name: "Common_ShopCounter");

            migrationBuilder.DropTable(
                name: "Common_ShopDeliveryService");

            migrationBuilder.DropTable(
                name: "Common_Size");

            migrationBuilder.DropTable(
                name: "Common_Supplier");

            migrationBuilder.DropTable(
                name: "Common_SupplierContact");

            migrationBuilder.DropTable(
                name: "Common_SupplierProduct");

            migrationBuilder.DropTable(
                name: "Common_Thana");

            migrationBuilder.DropTable(
                name: "Common_TremsAndCondition");

            migrationBuilder.DropTable(
                name: "Common_Unit");

            migrationBuilder.DropTable(
                name: "Marketing_Sales");

            migrationBuilder.DropTable(
                name: "Marketing_SalesSlave");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseOrder");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseOrderSlave");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseRequisition");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropTable(
                name: "User_ManagerAccessableUser");

            migrationBuilder.DropTable(
                name: "User_RoleAssign");

            migrationBuilder.DropTable(
                name: "User_RoleMenuItem");

            migrationBuilder.DropTable(
                name: "User_User");

            migrationBuilder.DropTable(
                name: "WareHouse_Consumption");

            migrationBuilder.DropTable(
                name: "WareHouse_ConsumptionSlave");

            migrationBuilder.DropTable(
                name: "WareHouse_POReceiving");

            migrationBuilder.DropTable(
                name: "WareHouse_POReceivingSlave");

            migrationBuilder.DropTable(
                name: "User_MenuItem");

            migrationBuilder.DropTable(
                name: "User_Role");

            migrationBuilder.DropTable(
                name: "User_AccessLevel");

            migrationBuilder.DropTable(
                name: "User_Department");

            migrationBuilder.DropTable(
                name: "User_SubMenu");

            migrationBuilder.DropTable(
                name: "User_Menu");
        }
    }
}
