﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Common_ShopFK",
                table: "Procurement_PurchaseOrderSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ShopFK",
                table: "Procurement_PurchaseOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "EnableOrDisableProductVat",
                table: "Common_Product",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_ShopFK",
                table: "Procurement_PurchaseOrderSlave");

            migrationBuilder.DropColumn(
                name: "Common_ShopFK",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "EnableOrDisableProductVat",
                table: "Common_Product");
        }
    }
}
