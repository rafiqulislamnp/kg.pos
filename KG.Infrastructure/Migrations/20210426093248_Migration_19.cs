﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_ColorFk",
                table: "Common_Product");

            migrationBuilder.RenameColumn(
                name: "UnitPrice",
                table: "WareHouse_ConsumptionSlave",
                newName: "MRPPrice");

            migrationBuilder.AddColumn<int>(
                name: "Common_BrandFk",
                table: "WareHouse_ConsumptionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductCategoryFk",
                table: "WareHouse_ConsumptionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductSubCategoryFk",
                table: "WareHouse_ConsumptionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFk",
                table: "WareHouse_ConsumptionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "CostingPrice",
                table: "WareHouse_ConsumptionSlave",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_BrandFk",
                table: "WareHouse_ConsumptionSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductCategoryFk",
                table: "WareHouse_ConsumptionSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductSubCategoryFk",
                table: "WareHouse_ConsumptionSlave");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFk",
                table: "WareHouse_ConsumptionSlave");

            migrationBuilder.DropColumn(
                name: "CostingPrice",
                table: "WareHouse_ConsumptionSlave");

            migrationBuilder.RenameColumn(
                name: "MRPPrice",
                table: "WareHouse_ConsumptionSlave",
                newName: "UnitPrice");

            migrationBuilder.AddColumn<int>(
                name: "Common_ColorFk",
                table: "Common_Product",
                nullable: true);
        }
    }
}
