﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_24 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Common_Payment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    Common_CustomerFK = table.Column<int>(nullable: false),
                    Common_PaymentMathodFK = table.Column<int>(nullable: false),
                    InAmount = table.Column<decimal>(nullable: false),
                    OutAmount = table.Column<decimal>(nullable: false),
                    ReferenceNo = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Payment", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Common_Payment");
        }
    }
}
