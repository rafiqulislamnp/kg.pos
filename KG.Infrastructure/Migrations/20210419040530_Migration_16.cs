﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Common_SizeFk",
                table: "Common_Product",
                newName: "Common_SupplierFk");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Common_SupplierFk",
                table: "Common_Product",
                newName: "Common_SizeFk");
        }
    }
}
