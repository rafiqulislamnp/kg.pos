﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration25 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFK",
                table: "WareHouse_POReceiving",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_SupplierFK",
                table: "WareHouse_POReceiving");
        }
    }
}
