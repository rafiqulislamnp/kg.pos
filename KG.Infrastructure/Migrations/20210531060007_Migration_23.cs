﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RedeemProductPoint",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsRedeemPoint",
                table: "Marketing_Sales",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RedeemProductPoint",
                table: "Marketing_SalesSlave");

            migrationBuilder.DropColumn(
                name: "IsRedeemPoint",
                table: "Marketing_Sales");
        }
    }
}
