﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration27 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CostingPrice",
                table: "Procurement_PurchaseRequisitionSlave",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MRPPrice",
                table: "Procurement_PurchaseRequisitionSlave",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CostingPrice",
                table: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropColumn(
                name: "MRPPrice",
                table: "Procurement_PurchaseRequisitionSlave");
        }
    }
}
