﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Common_BrandFk",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductCategoryFk",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductSubCategoryFk",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFk",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_BrandFk",
                table: "Marketing_SalesSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductCategoryFk",
                table: "Marketing_SalesSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductSubCategoryFk",
                table: "Marketing_SalesSlave");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFk",
                table: "Marketing_SalesSlave");
        }
    }
}
