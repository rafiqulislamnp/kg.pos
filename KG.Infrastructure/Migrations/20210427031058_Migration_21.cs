﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Common_BrandFk",
                table: "WareHouse_TransferShopToShopSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductCategoryFk",
                table: "WareHouse_TransferShopToShopSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductSubCategoryFk",
                table: "WareHouse_TransferShopToShopSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFk",
                table: "WareHouse_TransferShopToShopSlave",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_BrandFk",
                table: "WareHouse_TransferShopToShopSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductCategoryFk",
                table: "WareHouse_TransferShopToShopSlave");

            migrationBuilder.DropColumn(
                name: "Common_ProductSubCategoryFk",
                table: "WareHouse_TransferShopToShopSlave");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFk",
                table: "WareHouse_TransferShopToShopSlave");
        }
    }
}
