﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DOB",
                table: "Common_Customer",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Age",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Catgry",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CusName",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DOE",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DOETime",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DiscAllowed",
                table: "Common_Customer",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DiscPer",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExpDt",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FName",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LName",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MName",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MaritalStatus",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MarriageDT",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OfficePhone",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OpeningPoint",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PID",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SlNo",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpouseName",
                table: "Common_Customer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Common_Customer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Age",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "Catgry",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "CusName",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "DOE",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "DOETime",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "DiscAllowed",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "DiscPer",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "ExpDt",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "FName",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "Fax",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "LName",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "MName",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "MaritalStatus",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "MarriageDT",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "OfficePhone",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "OpeningPoint",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "PID",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "SlNo",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "SpouseName",
                table: "Common_Customer");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Common_Customer");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DOB",
                table: "Common_Customer",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
