﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "InvoiceDiscount",
                table: "Marketing_SalesSlave",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsDiscounted",
                table: "Marketing_Sales",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalInvoiceDiscount",
                table: "Marketing_Sales",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalProductDiscount",
                table: "Marketing_Sales",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceDiscount",
                table: "Marketing_SalesSlave");

            migrationBuilder.DropColumn(
                name: "IsDiscounted",
                table: "Marketing_Sales");

            migrationBuilder.DropColumn(
                name: "TotalInvoiceDiscount",
                table: "Marketing_Sales");

            migrationBuilder.DropColumn(
                name: "TotalProductDiscount",
                table: "Marketing_Sales");
        }
    }
}
