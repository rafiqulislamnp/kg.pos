﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KG.Infrastructure.Migrations
{
    public partial class Migration_17 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WareHouse_TransferShopToShop",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    TransferCID = table.Column<string>(maxLength: 25, nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    ToCommon_ShopFK = table.Column<int>(nullable: false),
                    Acknowledgement = table.Column<bool>(nullable: false),
                    AcknowledgementDate = table.Column<DateTime>(nullable: true),
                    AcknowledgedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_TransferShopToShop", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "WareHouse_TransferShopToShopSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_ShopFK = table.Column<int>(nullable: false),
                    WareHouse_TransferShopToShopFk = table.Column<int>(nullable: false),
                    Common_ProductFK = table.Column<int>(nullable: false),
                    TransferQuantity = table.Column<decimal>(nullable: false),
                    CostingPrice = table.Column<decimal>(nullable: false),
                    MRPPrice = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WareHouse_TransferShopToShopSlave", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WareHouse_TransferShopToShop");

            migrationBuilder.DropTable(
                name: "WareHouse_TransferShopToShopSlave");
        }
    }
}
