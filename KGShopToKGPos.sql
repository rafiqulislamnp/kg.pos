USE [KG.POS]
GO
/****** Object:  StoredProcedure [dbo].[sp_Common_ProductInsert]    Script Date: 1/3/2021 1:30:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Common_ProductInsert]
AS
BEGIN
	delete  from [dbo].[Common_Product]
INSERT INTO [dbo].[Common_Product]
           ([Active]
           ,[User]
           ,[Time]
           ,[Remarks]
           ,[UserID]
           ,[Name]
           ,[Common_ProductSubCategoryFk]
           ,[Common_UnitFk]
           ,[MRPPrice]
           ,[SystemBarcode]
           ,[ProductID]
           ,[Image]
           ,[IsLock]
           ,[VATPercent]
           ,[SSID]
           ,[DiscountExpiryDate]
           ,[ExpiryDate]
           ,[CostingPrice]
           ,[Common_BrandFk]
           ,[Common_ColorFk]
           ,[Common_SizeFk]
           ,[CBTID]
           ,[CMPIDX]
           ,[CSSID]
           ,[DiscountPercent]
           ,[Point]
           ,[ProductBarcode]
           ,[DisContinued]
           ,[MaximumSalesLimit]
           ,[DiscountValue]
           ,[PurchasePrice])
     Select
           1 as Active
           ,'System' as UserU
           ,Getdate() as TimeU
           ,'' as Remarks
           ,1 as UserID
           ,p.PrdName + ' ' + s.[SSName] as Name
           , s.[PrdID] as Common_ProductSubCategoryFk
           ,1 as Common_UnitFk
           ,s.[RPU] as MRPPrice
           ,s.[Barcode] as SystemBarcode
           ,'' as ProductID
           ,''
           ,0 as IsLock
           ,s.[VATPrcnt] as VATPercent
           ,s.[SSID] as SSID
           ,getdate() as discountExpiry
           ,getdate() as ExpiryDate
           ,s.[CPU] as CostingPrice
           ,s.[BTID] as Common_BrandFk
           ,0 as Common_ColorFk
           ,0 as Common_SizeFk
           ,s.[CBTID] as CBTID
           ,s.[CBTID] as CMPIDX
           ,s.[CSSID] as CSSID
           ,s.[DiscPrcnt] as DiscountPercent
           ,s.[Point] as Point
           ,s.[sBarcode] as ProductBarcode
           ,s.[DisContinued] as DisContinued
           ,4 as MaximumSalesLimit
           ,0 as DiscountValue
           ,0 as PurchasePrice
		    
  FROM [ShopKBG].[dbo].[StyleSize] as s
  join [ShopKBG].[dbo].Product as p on p.PrdID = s.PrdID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Common_ProductSubCategoryInsert]    Script Date: 1/3/2021 1:30:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[sp_Common_ProductSubCategoryInsert]
	
AS
BEGIN
	delete  from [dbo].[Common_ProductSubCategory]
INSERT INTO [dbo].[Common_ProductSubCategory]
           ([Active]
           ,[User]
           ,[Time]
           ,[Remarks]
           ,[UserID]
           ,[Name]
           ,[Description]
           ,[Common_ProductCategoryFk]
           ,[IsLock]
           ,[ShortName]
           ,[VATPercent])
     SELECT
           1 as Active
           ,'System' as UserN
           ,Getdate() as Time
           ,'' as Remarks
           ,1 as UserID
           ,p.[PrdName] as Name
           ,'' as Description
           ,p.[GroupID] As Common_ProductCategoryFk
           ,0 as IsLock
           ,(LEFT(p.GroupName, 2) + LEFT(pg.GroupName , 2)) as ShortName
           ,p.[VATPrcnt] as VATPercent
  FROM [ShopKBG].[dbo].[Product] as p
  Join [ShopKBG].[dbo].PGroup as pg on p.GroupID = pg.GroupID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CommonSupplierInsert]    Script Date: 1/3/2021 1:30:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CommonSupplierInsert]
AS
BEGIN

delete from [dbo].[Common_Supplier]
	INSERT INTO [dbo].[Common_Supplier]
           ([Active]
           ,[User]
           ,[Time]
           ,[Remarks]
           ,[UserID]
           ,[Name]
           ,[Common_ThanaFk]
           ,[Contact]
           ,[Email]
           ,[Address]
           ,[Code]
           ,[BilltoBillCreditPeriod]
           ,[CreditPeriod]
           ,[Fax]
           ,[DOE]
           ,[PaymentMethod])
     SELECT
           1 as Active
           ,'System' as UserL
           ,getdate() as Time
           ,'' as Remarks
           ,1 as UserID
           ,[SupName] as Name
           ,1 as Common_ThanaFk
           ,[Contact] as Contact
           ,[Email] as Email
           ,[Address] as Address
           ,[SupID] as Code
           ,0 as BilltoBillCreditPeriod
           ,[CreditDays] as CreditPeriod
           ,[Fax] as Fax
           ,[DOE] as DOE
           ,[SupType] as PaymentMethod
     
     
  FROM [ShopKBG].[dbo].[Supplier]
END

GO
/****** Object:  StoredProcedure [dbo].[sp_ProductCategoryInsert]    Script Date: 1/3/2021 1:30:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_ProductCategoryInsert]
	
AS
BEGIN
Delete from [dbo].[Common_ProductCategory];

	INSERT INTO [dbo].[Common_ProductCategory]
           ([Active]
           ,[User]
           ,[Time]
           ,[Remarks]
           ,[UserID]
           ,[Name]
           ,[Description]
           ,[IsLock]
           ,[ShortName]
           ,[VATPercent]
           ,[DiscPrcnt]
           ,[Point]           
           ,[sAmt])
     SELECT
           1                      
           ,'Syatem'
           ,GETDATE()
           ,''
           ,1
           ,[GroupName]
           ,''
           ,0
           ,[Prefix]
           ,[VATPrcnt]
           ,[DiscPrcnt]
           ,[Point]           
           ,[sAmt]
		    FROM ShopKBG.[dbo].[PGroup]

		  
END

GO
/****** Object:  StoredProcedure [dbo].[spCommonBrandInsert]    Script Date: 1/3/2021 1:30:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCommonBrandInsert]
AS
BEGIN
delete from [Common_Brand]
INSERT INTO [dbo].[Common_Brand]
           ([Active]
           ,[User]
           ,[Time]
           ,[Remarks]
           ,[UserID]
           ,[Name]
           ,[Description]
           ,[IsLock])
     SELECT
	 1 as Active
           ,'System' as UserL
           ,getdate() as Time
           ,'' as Remarks
           ,1 as UserID
           ,[BTname] as Name
           ,'' as Description
           ,0 as IsLock
FROM [ShopKBG].[dbo].[BrandList]
  
END

GO
