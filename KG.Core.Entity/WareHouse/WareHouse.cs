﻿using KG.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KG.Core.Entity.WareHouse
{
   
   
   
    public class WareHouse_POReceiving : CoreModel
    {
        public string ChallanCID { get; set; }

        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string Challan { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ChallanDate { get; set; }

        public int Procurement_PurchaseOrderFk { get; set; }
        public int Common_SupplierFK { get; set; } = 0;
        public int User_DepartmentFk { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }

    public class WareHouse_POReceivingSlave : CoreModel
    {
        public int? Common_ProductInBinSlaveFK { get; set; }
        public int WareHouse_POReceivingFk { get; set; }
        public int Procurement_PurchaseOrderSlaveFk { get; set; }
        public int Common_ProductFK { get; set; }
        //public int Common_BrandFk { get; set; }
        //public int Common_SupplierFk { get; set; }
        //public int Common_ProductCategoryFk { get; set; }
        //public int Common_ProductSubCategoryFk { get; set; }

        public decimal ReceivedQuantity { get; set; } = 0;
        public decimal StockLossQuantity { get; set; } = 0;
        public decimal PurchasingPrice { get; set; } = 0;
        public decimal TotalOverhead { get; set; } = 0;
        public decimal Damage { get; set; } = 0;
        public int WareHouse_BinSlaveFk { get; set; }
        public bool IsReturn { get; set; }
       
        public bool IsDefineBin { get; set; }

        public bool IsGRNCompleted { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }

    }

    public class WareHouse_Consumption : CoreModel
    {
        [StringLength(25)]
        public string ConsumptionCID { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }  
        public int FromUser_DepartmentFk { get; set; }
        public int ToUser_DepartmentFK { get; set; }       
        public bool Acknowledgement { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public string AcknowledgedBy { get; set; }
        public bool IsRetuen { get; set; } = false;  //From emun named EnumStoreType
        public bool SendByStoreRequisition { get; set; }
    }

    public class WareHouse_ConsumptionSlave : CoreModel
    {
        public int WareHouse_ConsumptionFk { get; set; }
        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int Common_ProductFK { get; set; }
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal ConsumeQuantity { get; set; } = 0;
    }



    public class WareHouse_TransferShopToShop : CoreModel
    {
        [StringLength(25)]
        public string TransferCID { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }      
        public int ToCommon_ShopFK { get; set; }
        public bool Acknowledgement { get; set; }
        public DateTime? AcknowledgementDate { get; set; }
        public string AcknowledgedBy { get; set; }
    }

    public class WareHouse_TransferShopToShopSlave : CoreModel
    {
        public int WareHouse_TransferShopToShopFk { get; set; }
        public int Common_ProductFK { get; set; }
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        
        public decimal TransferQuantity { get; set; } = 0;
        public decimal CostingPrice { get; set; }
        public decimal MRPPrice { get; set; }

    }
}
