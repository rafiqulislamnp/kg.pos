﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace KG.Core.Entity.Marketing
{
    public class Marketing_Sales : CoreModel
    {
        public string SaleInvoiceNo { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public int Common_CustomerFK { get; set; } = 0;

        public decimal TotalDiscount { get; set; }
        public decimal TotalProductDiscount { get; set; }
        public decimal TotalInvoiceDiscount { get; set; }

        public bool IsDiscounted { get; set; }
        public bool IsRedeemPoint { get; set; }
        public decimal TotalVat { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal ReturnAmount { get; set; }
        
        public int Status { get; set; }

        public int CustomerPaymentMethodEnumFk { get; set; }

        public decimal ActualInvoiceValue { get; set; }
       
        public string Description { get; set; }
        public decimal VatParcentage { get; set; }
        public decimal TotalIncludingVat { get; set; }
        public decimal DiscountParcentage { get; set; }
        public string CustomerMobile { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public decimal CustomerPoint { get; set; }
        public decimal RedeemPoint { get; set; }
    }

    public class Marketing_SalesSlave : CoreModel
    {

        public int Marketing_SalesFk { get; set; }

        public int Common_ProductFK { get; set; }
        public string ProductSpecification { get; set; }
        public decimal PurchasePrice { get; set; }

        [Column(TypeName = "decimal(18,3)")]
        public decimal Quantity { get; set; }
        public decimal MRPPrice { get; set; }
        
        public decimal SubTotal { get; set; }
        public decimal ProductDiscountValue { get; set; }
        public decimal InvoiceDiscount { get; set; }

        public decimal ProductDiscountPercent { get; set; }

        public decimal VAT { get; set; }
        public decimal VatValue { get; set; }

        public decimal CostingPrice { get; set; }
        public decimal Point { get; set; }
        public int Common_BrandFk { get; set; }
        public int Common_SupplierFk { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public decimal RedeemProductPoint { get; set; }

    }


    public class Marketing_SalesReturn : CoreModel
    {
        public string CID { get; set; }
        public DateTime ReturnDate { get; set; }
        public decimal TotalReturnQuantity { get; set; }

        public decimal TotalReturnAmount { get; set; }
        public decimal TotalReturnAdjustedAmount { get; set; }
        public decimal TotalTax { get; set; }
        //public decimal PayableReturnAmount
        //{
        //    get
        //    {
        //        return (TotalReturnAmount - TotalReturnAdjustedAmount) + ((TotalReturnAmount - TotalReturnDiscount) * (TotalTax / 100));
        //    }
        //}
        public decimal TotalReturnPaid { get; set; }
        //public decimal TotalReturnDue
        //{
        //    get { return PayableReturnAmount - TotalReturnPaid; }
        //}
        public string Status { get; set; }
        public string ReturnPaymentType { get; set; }
        public string ReturnPaymentMethod { get; set; }
        public string ReturnPaymentReference { get; set; }

        public string Reason { get; set; }


        public int Marketing_SalesFK { get; set; }


    }

    public class Marketing_SalesReturnSlave : CoreModel
    {
        public decimal ReturnQuantity { get; set; }
        public decimal SalePrice { get; set; }
        public decimal SubTotal
        {
            get
            {
                return ReturnQuantity * SalePrice;
            }
        }
        public decimal AdjustedSalePrice { get; set; }
        public string Description { get; set; }



        public int Marketing_SalesReturnFK { get; set; }
      

        public int Marketing_SalesSlaveFK { get; set; }
       
        public int Common_ProductFK { get; set; }
       

    }


   
}
