﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KG.Core.Entity.Configuration
{
    public class Common_TremsAndCondition : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    public class Common_Unit : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }

    }

    public class Common_Company : CoreModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public string Manifesto { get; set; }

        [NotMapped]
        public override int Common_ShopFK { get; set; }

    }
    public class Common_Brand : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }

    
    

    public class Common_Countries : CoreModel
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    public class Common_Divisions : CoreModel
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }

        public int Common_CountriesFk { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    public class Common_Districts : CoreModel
    {
        public string Name { get; set; }
        public string BnName { get; set; }      
        public string Url { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public int Common_DivisionsFk { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }

    public class Common_Payment : CoreModel
    {   
        public int Common_CustomerFK { get; set; }
        public int Common_PaymentMathodFK { get; set; }
        public decimal InAmount { get; set; } = 0;
        public decimal OutAmount { get; set; } = 0;
        public string ReferenceNo { get; set; }
        public DateTime TransactionDate { get; set; }      
    }

    public class Common_Thana : CoreModel
    {
        public string Name { get; set; }
        public string BnName { get; set; }
        public string Url { get; set; }
        public int Common_DistrictsFk { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
   
    public class Common_Supplier : CoreModel
    {
        public string Name { get; set; }
        public int Common_ThanaFk { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Fax { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime DOE { get; set; }
        public bool BilltoBillCreditPeriod { get; set; }
        public int CreditPeriod { get; set; } //Days
        public string Code { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    
   
    public class Common_ProductCategory : CoreModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal VATPercent { get; set; }
        public string Description { get; set; }
        public bool IsLock { get; set; }

       // public int VATPrcnt { get; set; }
       
        public int Point { get; set; }
        public int sAmt { get; set; }

        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }

    public class Common_ProductSubCategory : CoreModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal VATPercent { get; set; }

        public string Description { get; set; }
        public int Common_ProductCategoryFk { get; set; }
        public bool IsLock { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    public class Common_Product : CoreModel
    {
       // public int Common_SupplierFk { get; set; } = 0;
        public string DisContinued { get; set; }

        public bool EnableOrDisableProductVat { get; set; }
        public int Common_ProductSubCategoryFk { get; set; }
        public string Name { get; set; }
        public int? Common_BrandFk { get; set; }      
        public int? Common_SupplierFk { get; set; }
        public string CMPIDX { get; set; }
        public string SystemBarcode { get; set; }
        public string ProductBarcode { get; set; }
        public string CSSID { get; set; }
        public string SSID { get; set; }
        public string CBTID { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal DiscountValue { get; set; }

        public DateTime DiscountExpiryDate { get; set; }
        public decimal VATPercent { get; set; }
        public decimal MaximumSalesLimit { get; set; }

        public decimal CostingPrice { get; set; }
        public decimal MRPPrice { get; set; }
       
        public decimal Point { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int Common_UnitFk { get; set; }

        public string Image { get; set; }
        public bool IsLock { get; set; }

        [NotMapped]
        public override int Common_ShopFK { get; set; }
        public string ProductID { get; set; }
        public decimal PurchasePrice { get; set; }
    }

    public class Common_ProductCosting : CoreModel
    {
        public int Common_ProductFK { get; set; }
        public int WareHouse_POReceivingSlaveFk { get; set; }
        public decimal LaborCost { get; set; }
        public decimal ManufacturingOverhead { get; set; }
        public decimal TransportationOverhead { get; set; }
        public decimal OthersCost { get; set; }
        public string OthersCostNote { get; set; }
        public decimal CostingPrice { get; set; }
        public decimal PurchasePrice { get; set; }
    }
   
    public class Common_Customer : CoreModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public DateTime? DOB { get; set; }
        public string Profession { get; set; }
        public string Sex { get; set; }
        public string Code { get; set; }
        public string MemberShipNo { get; set; }

        public string CustomerLoyalityPoint { get; set; }
        public int CustomerTypeEnumFk { get; set; }


        public string SlNo { get; set; }
        public string Catgry { get; set; }
        public string Title { get; set; }
        public string FName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string CusName { get; set; }
        public string City { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }
        public string OfficePhone { get; set; }
        public string Fax { get; set; }
        public string ExpDt { get; set; }
        public bool DiscAllowed { get; set; }
        public string DOE { get; set; }
        public string DOETime { get; set; }
        public string Age { get; set; }
        public string MaritalStatus { get; set; }
        public string MarriageDT { get; set; }
        public string DiscPer { get; set; }
        public string PID { get; set; }
        public string SpouseName { get; set; }
        public string OpeningPoint { get; set; }
     

    }
    public class Common_CustomerPoint : CoreModel
    {
        public int Common_CustomerFk { get; set; }
        public decimal Point { get; set; } = 0;
        public int Marketing_SalesFk { get; set; }
        public bool IsEarnPoint { get; set; }


        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    

    public class Common_Shop : CoreModel
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public int Common_ThanaFk { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
        public int ShopTypeEnumFk { get; set; }
        public bool OwnDeliveryService { get; set; }
        public string ServiceStartTime { get; set; }
        public string ServiceEndTime { get; set; }
        public string Description { get; set; }       
        public int TradelicenseTypeEnumfk { get; set; }
        public string TradeLicenceNumber { get; set; }
        public DateTime TradeLicenceExpireDate { get; set; }
        public string TradeLicenceUrl { get; set; }

    }

   

    
    public class Common_ShopCounter : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Common_Bin : CoreModel
    {
        public int RackNo { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
       
    }
    public class Common_BinSlave : CoreModel
    {
        public int Common_BinFk { get; set; }
        public string CID { get; set; }
        public string Dimension { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }
    public class Common_ProductInBinSlave : CoreModel
    {
        public int Common_BinSlaveFk { get; set; }
        public int Common_ProductFK { get; set; }
        [NotMapped]
        public override int Common_ShopFK { get; set; }
    }

}
