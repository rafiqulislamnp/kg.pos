﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace KG.Core.Entity.Procurement
{
    public class Procurement_PurchaseOrder : CoreModel
    {
        public string CID { get; set; }
        public int Common_SupplierFK { get; set; }
        public int SupplierPaymentMethodEnumFK { get; set; }
        public DateTime OrderDate { get; set; } = DateTime.Now;
        public decimal TotalPOValue { get; set; }
        //This Column for L/C
        public string TermsAndCondition { get; set; }
        public string Description { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime? DeliveryDate { get; set; }

        public bool IsHold { get; set; }
        public bool IsCancel { get; set; }

        public int Status { get; set; }
        //public int Approval { get; set; }

        public DateTime? ApprovedDate { get; set; }
        public string ApprovedHistory { get; set; }

      

    }

    public class Procurement_PurchaseOrderSlave : CoreModel
    {
        public int Procurement_PurchaseOrderFK { get; set; }
        public int? Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int Common_ProductFK { get; set; }     
        public decimal PurchaseQuantity { get; set; }
        public decimal PurchasingPrice { get; set; }      
        public string Description { get; set; }
      
    }



    //public class Procurement_PurchaseOrderExtTransfer : CoreModel
    //{
    //    public int? Procurement_PurchaseOrderFk { get; set; }
    //    public int? Common_ProductFk { get; set; }
    //    public int WareHouse_ConsumptionSlaveFk { get; set; }
    //    public decimal SendQuantity { get; set; }

    //}
}