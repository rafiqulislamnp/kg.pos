﻿
using KG.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace KG.Core.Entity.Procurement
{
    //public class Common_Approval : CoreModel
    //{
    //    public string Name { get; set; }
    //    public bool IsNeed { get; set; }
    //}

    public class Procurement_PurchaseRequisition : CoreModel
    {
        public string RequisitionCID { get; set; }
        public DateTime RequisitionDate { get; set; } = DateTime.Now;
        public int FromUser_DepartmentFk { get; set; }
        public int ToUser_DepartmentFK { get; set; }

        public bool IsHold { get; set; }
        public bool IsCancel { get; set; }

        public int Status { get; set; }
       // public int Approval { get; set; }

        public int POStatusInPRFk { get; set; }

    }

    public class Procurement_PurchaseRequisitionSlave : CoreModel
    {
        public int Procurement_PurchaseRequisitionFK { get; set; }
        public int Common_ProductFK { get; set; }
        public decimal RequisitionQuantity { get; set; }
        public string SupplierNames { get; set; }
        public decimal CurrentStock { get; set; }
        public decimal MRPPrice { get; set; }
        public decimal CostingPrice { get; set; }

    }

}